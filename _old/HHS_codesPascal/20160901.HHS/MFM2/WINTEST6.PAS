USES Graph,CRT,STDLIB,MouseLib,VESALib,WinLib;

CONST
  objListNr  = $8000;
  ButtonList = '1.Button a' + ZeroChr +
               '2.Button ab' + ZeroChr +
               '3.Button abc' + ZeroChr +
               '4.Button abcd' + ZeroChr;

(* oder alternativ mit Steuerzeichen
  ButtonList = C_Blue  + '~+~' + '1.Button a' +
               C_Green + '~+~' + '2.Button ab' +
               C_Red   + '~-~' + '3.Button abc' +
               C_Yellow+ '~+~' + '4.Button abcd';
*)

VAR
  buttonNr : WORD;
  outText  : STRING;

BEGIN
  WinInit(VGA);
  MouseInstall(MouseAllEvent,MousePoll);
  SetTextMode(TRUE,TRUE,TRUE);

  WinSetHeader(WinDefDes,Blue,C_RED+'Liste');
  SetTextStyle(TriplexFont,HorizDir,1);

  WinDefDes.TFont := GothicFont;

  ButtonNr := WinListBox (100,150,objListNr,
                          ObjButtonType OR Local OR VertDir OR PosCenter,
                          {Font8x16}TriplexFont,ButtonList,NilEvProc,outText);

  WRITELN('Button ',ButtonNr,' ausgew�hlt mit Namen ',outText);
  WRITELN('Teste dr�cken um das Programm zu beenden');
  REPEAT
  UNTIL(KeyPressed);
END.

