USES CRT,STDLIB,
TASKLIB;

VAR
  Timer : TimerType;
  Time  : LONGINT;
  Key   : CHAR;

BEGIN
  TimerStart (Timer);
  REPEAT
    Time := TimerRead(Timer);
    WRITE('Time=',Time : 9,CR);
  UNTIL(KeyPressed);
  Key := ReadKey;
END.
