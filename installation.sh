#!/bin/bash

# Set environment variables 
export EPICS_HOST_ARCH=linux-x86_64
export EPICS_BASE=/opt/epics/base
export SUPPORT=/opt/epics/support
export LC_ALL=C
export IOCS=/opt/epics/ioc
export TZ=Europe/Berlin
export PATH=${EPICS_BASE}/bin/${EPICS_HOST_ARCH}:${PATH}

ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone  

echo "export EPICS_HOST_ARCH=linux-x86_64" >> ~/.bashrc
echo "SUPPORT=/opt/epics/support" >> ~/.bashrc
echo "EPICS_BASE=/opt/epics/base" >> ~/.bashrc
echo "export LC_ALL=C" >> ~/.bashrc
echo "export PATH=${EPICS_BASE}/bin/${EPICS_HOST_ARCH}:${PATH}" >> ~/.bashrc

# Install build tools and utilities 
apt-get update -y
apt-get install -y sudo
sudo apt-get install -y --no-install-recommends \
    ca-certificates \
    libntirpc-dev libtirpc3 \
    tzdata \
    build-essential \
    busybox \
    git \
    wget \
    unzip \
    python3-minimal \
    python3-pip \
    python3-venv \
    re2c \
    rsync \
    ssh-client \
    dkms \
    lsb-release \
    lsb-core \
    libgconf-2-4 \
    libgtk-3-0 \
    libasound2 \
    libcanberra-gtk-module \
    libcanberra-gtk3-module \
    x11-apps \
    software-properties-common \
    gawk \
    iptables \
    libxss1 \
    libappindicator1 \
    libunwind8 \
    libx11-xcb1 \
    pandoc \
    libssh2-1 \
    libssh2-1-dev \
    openjdk-19-jre \
    python3-sphinx

git config --global advice.detachedHead false

pip install h5py matplotlib pyepics numpy pandas ophyd bluesky sphinx scipy sphinx-rtd-theme

# Create important folders 
mkdir /opt/epics
mkdir /opt/epics/base
mkdir /opt/epics/support

# Clone the EPICS repository 
git clone --depth 1 --recursive --branch R7.0.7 https://github.com/epics-base/epics-base.git ${EPICS_BASE}

# Build EPICS 
make -C ${EPICS_BASE} -j $(nproc)

# create RELEASE.local file
touch ${SUPPORT}/RELEASE.local
echo "EPICS_BASE=${EPICS_BASE}">> ${SUPPORT}/RELEASE.local
echo "SUPPORT=${SUPPORT}">>${SUPPORT}/RELEASE.local

# install autosave (OK)
git clone --depth 1 --recursive --branch R5-10-2 https://github.com/epics-modules/autosave.git ${SUPPORT}/autosave
make -C ${SUPPORT}/autosave -j $(nproc)
echo "AUTOSAVE=${SUPPORT}/autosave">> ${SUPPORT}/RELEASE.local

# install seq (OK)
git clone --depth 1 --recursive --branch vendor_2_2_8 https://github.com/ISISComputingGroup/EPICS-seq.git ${SUPPORT}/seq
make -C ${SUPPORT}/seq -j $(nproc)
echo "SEQ=${SUPPORT}/seq">> ${SUPPORT}/RELEASE.local
echo "SNCSEQ=${SUPPORT}/seq">>${SUPPORT}/RELEASE.local

# install sscan (OK)
git clone --depth 1 --recursive --branch R2-11-5 https://github.com/epics-modules/sscan.git ${SUPPORT}/sscan
sed -i 's/^SNCSEQ=/#SNCSEQ=/' ${SUPPORT}/sscan/configure/RELEASE # important
make -C ${SUPPORT}/sscan -j $(nproc)
echo "SSCAN=${SUPPORT}/sscan">> ${SUPPORT}/RELEASE.local

# install calc (OK)
git clone --depth 1 --recursive --branch R3-7-4 https://github.com/epics-modules/calc.git ${SUPPORT}/calc
make -C ${SUPPORT}/calc -j $(nproc)
echo "CALC=${SUPPORT}/calc">> ${SUPPORT}/RELEASE.local

# install asyn (OK)
git clone --depth 1 --recursive --branch R4-44-2 https://github.com/epics-modules/asyn.git ${SUPPORT}/asyn
sed -i '/TIRPC/s/^#//g' ${SUPPORT}/asyn/configure/CONFIG_SITE
make -C ${SUPPORT}/asyn -j $(nproc)
echo "ASYN=${SUPPORT}/asyn">> ${SUPPORT}/RELEASE.local

# install busy (OK)
git clone --depth 1 --recursive --branch R1-7-4 https://github.com/epics-modules/busy.git ${SUPPORT}/busy
make -C ${SUPPORT}/busy -j $(nproc)
echo "BUSY=${SUPPORT}/busy">> ${SUPPORT}/RELEASE.local

# install pcre (OK)
git clone --depth 1 --recursive --branch R8-44 https://github.com/chrschroeder/pcre.git ${SUPPORT}/pcre
make -C ${SUPPORT}/pcre -j $(nproc)
echo "PCRE=${SUPPORT}/pcre">> ${SUPPORT}/RELEASE.local

# install stream (OK)
git clone --depth 1 --recursive --branch 2.8.24 https://github.com/paulscherrerinstitute/StreamDevice.git ${SUPPORT}/stream
sed -i 's/^CALC=/#CALC=/' ${SUPPORT}/stream/configure/RELEASE # important
make -C ${SUPPORT}/stream -j $(nproc)
echo "STREAMDEVICE=${SUPPORT}/stream">> ${SUPPORT}/RELEASE.local

# install ipac (OK)
git clone --depth 1 --recursive --branch 2.16 https://github.com/epics-modules/ipac.git ${SUPPORT}/ipac
make -C ${SUPPORT}/ipac -j $(nproc)
echo "IPAC=${SUPPORT}/ipac">> ${SUPPORT}/RELEASE.local

# install the motor support module (OK)
git clone --depth 1 --branch R7-3-1 https://github.com/epics-modules/motor ${SUPPORT}/motor
sed -i '/^SNCSEQ=$(SUPPORT)\/seq-2-2-6/s/^/#/' ${SUPPORT}/motor/configure/RELEASE
make -C ${SUPPORT}/motor -j $(nproc)
echo "MOTOR=${SUPPORT}/motor">>${SUPPORT}/RELEASE.local

# install pmac (OK)
git clone --depth 1 --recursive https://github.com/dls-controls/pmac.git ${SUPPORT}/pmac && \
echo "BUILD_IOCS = NO" >> ${SUPPORT}/pmac/configure/CONFIG_SITE.linux-x86_64.Common && \ 
echo "SSH = /usr" >> ${SUPPORT}/pmac/configure/CONFIG_SITE.linux-x86_64.Common && \
echo "SSH_LIB = \$(SSH)/lib/x86_64-linux-gnu" >> ${SUPPORT}/pmac/configure/CONFIG_SITE.linux-x86_64.Common && \
echo "SSH_INCLUDE = -I\$(SSH)/include" >> ${SUPPORT}/pmac/configure/CONFIG_SITE.linux-x86_64.Common && \
echo "WITH_BOOST = NO" >> ${SUPPORT}/pmac/configure/CONFIG_SITE.linux-x86_64.Common && \
cd ${SUPPORT}/pmac/configure && \
rm RELEASE.local RELEASE.local.linux-x86_64 RELEASE.linux-x86_64.Common && \
cp ${SUPPORT}/RELEASE.local ${SUPPORT}/pmac/configure/RELEASE && \
make -C ${SUPPORT}/pmac install 

# create ioc
mkdir /opt/epics/ioc

# Clone and build PMAC IOC (OK)
git clone --depth 1 --recursive --branch main https://codebase.helmholtz.cloud/messlabor/helmholtzcoil/ioc-pmac-trigger.git /opt/epics/ioc/ioc-pmac-trigger
make -C ${IOCS}/ioc-pmac-trigger clean install -j $(nproc)

# Clone IOC GPIB DVM repository (OK)
git clone --depth 1 --recursive --branch main https://codebase.helmholtz.cloud/messlabor/helmholtzcoil/hp3458a-gpib.git ${IOCS}/hp3458a-gpib

# install Keysight libraries --> it needs sudo privileges
sudo chmod +x ${IOCS}/hp3458a-gpib/iolibrariessuite-installer_20.1.29718.0.run
sudo ${IOCS}/hp3458a-gpib/iolibrariessuite-installer_20.1.29718.0.run --mode unattended

# install keysight library kernels --> it needs sudo privileges
sudo chmod +x ${IOCS}/hp3458a-gpib/iokerneldrivers-installer_20.1.29718.0.run 
sudo ${IOCS}/hp3458a-gpib/iokerneldrivers-installer_20.1.29718.0.run --mode unattended

# install visa epics
git clone --depth 1 --recursive https://github.com/ISISComputingGroup/EPICS-VISA.git ${SUPPORT}/visa
sed -i 's#\$(APPNAME)_SYS_LIBS_Linux += visa#\$(APPNAME)_SYS_LIBS_Linux += iovisa#' ${SUPPORT}/visa/visa_lib.mak
sed -i 's#USR_INCLUDES += -I/usr/include/ni-visa#USR_INCLUDES += -I/opt/keysight/iolibs/include#' ${SUPPORT}/visa/VISAdrvApp/src/Makefile
sed -i '/\$(APPNAME)_DBD += VISAdrv.dbd/a \$(APPNAME)_DBD += calc.dbd' ${SUPPORT}/visa/VISAdrvTestApp/src/build.mak
sed -i '/\$(APPNAME)_LIBS += stream VISAdrv asyn/s/$/ calc/' ${SUPPORT}/visa/VISAdrvTestApp/src/build.mak
make -C ${SUPPORT}/visa clean install

# build ioc hp3458a-gpib
# cp ${IOCS}/hp3458a-gpib/configure/RELEASE.local ${IOCS}/RELEASE.local 
make -C ${IOCS}/hp3458a-gpib clean install -j $(nproc)

# download Phoebus  
cd /opt/epics && wget --no-check-certificate --continue https://controlssoftware.sns.ornl.gov/css_phoebus/nightly/phoebus-linux.zip
cd /opt/epics && unzip phoebus-linux.zip && rm phoebus-linux.zip

# Create measurement environment variables
echo "export HHS_PROJECT_ID=TEST"       >> ~/.bashrc
echo "export HHS_MAGNET_ID=TEST"        >> ~/.bashrc
echo "export HHS_ORIENTATION_ID=TEST"   >> ~/.bashrc
echo "export HHS_OUTPUT_PATH=TEST"      >> ~/.bashrc

# create executable at the desktop
ln -s /opt/epics/hhs-trigger-ui/HHS.sh ~/Desktop/
chmod +x ~/Desktop/HHS.sh

# create output folders on desktop
mkdir ~/Desktop/HHS_Results
mkdir ~/Desktop/HHS_Results/backup


# use the command below as an example of symbolic links
# ln -sf /opt/epics/hhs-trigger-ui/data/ ~/Desktop/HHS_Results 
