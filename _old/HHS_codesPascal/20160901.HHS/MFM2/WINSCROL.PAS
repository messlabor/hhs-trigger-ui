(* DEMO WinScroll *)

USES CRT,GRAPH,VesaLib,WinLib;

VAR
  MainId : WinCtrlPtrType;
  key    : CHAR;

BEGIN
  PathToDriver := 'C:\BP\BGI';
  WinInit(VGA);                                  {Fenstersystem installieren}
  MainId  := OpenWin(100,100,200,200,WinDefDes);
  Writeln('Zeile 1');
  Writeln('Zeile 2');
  key := ReadKey;
  WinScroll(TextHeight('M'));
  key := Readkey;
END.
