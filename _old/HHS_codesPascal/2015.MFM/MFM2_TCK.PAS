{$N+}
Unit mfm2_tck;
{03.02.2015 HHS+SW2}
{18.05.2001 globale Types, Const, Var }
{29.06.2007 Varianz,MessVar eingebaut}
INTERFACE

USES DOS, CRT, Graph, SCALELIB, MBBASE, BESLIB, SDLIB,
          STDLIB, MOUSELIB, VESALIB, WINLIB,
          TPDECL, AUXIO1;


TYPE
     (*************** stack variables ****)

     PointPtrType_Las  = ^Point_Las;
     PointPtrType_STWI = ^Point_STWI;
     PointPtrType_DVM  = ^Point_DVM;

     Point_Las  = record              	{laser data}
               Next	: PointPtrType_Las;   {pointer to next data point}
	       Temp	: real;
               Laser	: RECORD
                        Ch1   : real;
                        Ch2   : real;
                        Ch3   : real;
                        Ch4   : real;
                       END;
              END;

     Point_STWI = record              	{stretched wire data}
               Next	: PointPtrType_STWI; {pointer to next data point}
	       zory	: real;
               yint1	: real;
               yint2    : real;
              END;

     Point_DVM  = record              	    {DVM data}
               Last	: PointPtrType_DVM; {pointer to previous data point}
               DVM	: RECORD
                        Ch1   : real;
                        Ch2   : real;
                        Ch3   : real;
                       END;
               Next	: PointPtrType_DVM; {pointer to next data point}
              END;

    (**************** other variables *******)
    SMSRampe = Record
                 fd,pl:array[1..100] of word;
                 fd0,RampSteps,DeltaSteps:word;
                 RampHight:real;
               end;

    Definedatatype = RECORD
                     speed_x      : real;
                     speed_y      : real;
                     speed_z      : real;
                     speedctrl    : Integer;
                     autoreverse  : Integer;
                     zero         : Integer;
                     ninety       : Integer;
                     delay_1      : integer;
                     delay_2      : integer;
                     delay_3      : integer;
                     delay_4      : integer;
                     SaveStack    : integer;
                     SaveLogFile  : integer;
                     Red_factor   : integer;
                     END;

    Griddatatype   = RECORD
                     xmin         : real;
                     xmax         : real;
                     ianzx        : integer;
                     ymin         : real;
                     ymax         : real;
                     ianzy        : integer;
                     zmin         : real;
                     zmax         : real;
                     ianzz        : integer;
                     xoff1        : real;
                     xoff2        : real;
                     END;

    Analysisdatatype = RECORD
                     SpeedCorr    : integer;
                     PtsPerRev    : real;
                     NrOfRev      : integer;
                     OrdOfFA      : integer;
                     Hall_Pr_Resp : real;
                     END;

    DACdatatype = RECORD
                     StartVoltage : real;
                     EndVoltage   : real;
                     Time         : real;
                     Steps        : integer;
                     MaxDiff      : real;
                     MinDT        : real;
                     Speed_const  : real;
                     END;

    MBInc1datatype = RECORD
                     delay_1      : integer;
                     delay_2      : integer;
                     delay_3      : integer;
                     delay_4      : integer;
                     delay_5      : integer;
                     Time_Base    : integer;
                     END;

    MBInc2datatype = RECORD
                     delay_1      : integer;
                     delay_2      : integer;
                     delay_3      : integer;
                     delay_4      : integer;
                     delay_5      : integer;
                     Time_Base_1  : integer;
                     Time_Base_2  : integer;
                     END;

    SMSdatatype = RECORD
                     Steps        : longint;  (* 600 = 2.4mm           *)
                     TimeBase     : byte;     (* 8   = 200 kHz         *)
                     FrqDiv       : word;     (* 200 = Start_FrqDiv    *)
                     PulseLen     : word;     (* 200 = Start_PulseLen  *)
                     RampSteps    : longint;  (* Zahl der RampenStufen *)
                     RampDstps    : real;     (* Stufenbreite Rampe integer! *)
                     END;

     DVMDatatype = RECORD
                     Range        : real;
                     Aperture     : real;
                     Readings     : integer;
                     END;

     PAR32Datatype = RECORD
                     PAR1Trig     : integer;
                     PAR2Trig     : integer;
                     PAR3Trig     : integer;
                     PAR4Trig     : integer;
                     END;

    ScaleDatatype  = RECORD
                     xminzero     : real;
                     xmaxzero     : real;
                     yminzero     : real;
                     ymaxzero     : real;
                     xminninety   : real;
                     xmaxninety   : real;
                     yminninety   : real;
                     ymaxninety   : real;
                     dispchannel0 : integer;
                     dispchannel90: integer;
                     autoscale0   : integer;
                     autoscale90  : integer;
                     resfactor0   : real;
                     resfactor90  : real;
                     END;

    ServoParDatatype = RECORD
                       c0    : RECORD
                               SIL   : longint;
                               SVA   : longint;
                               SXEA  : longint;
                               SXER  : longint;
                               SXGA  : longint;
                               SXGF  : longint;
                               SXLV  : longint;
                               SXLA  : longint;
                               SXZE  : longint;
                               SXKZ  : longint;
                               SXPO  : longint;
                               SYEA  : longint;
                               SYER  : longint;
                               SYGA  : longint;
                               SYGF  : longint;
                               SYLV  : longint;
                               SYLA  : longint;
                               SYZE  : longint;
                               SYKZ  : longint;
                               SYPO  : longint;
                               END;
                       c1    : RECORD
                               SIL   : longint;
                               SVA   : longint;
                               SXEA  : longint;
                               SXER  : longint;
                               SXGA  : longint;
                               SXGF  : longint;
                               SXLV  : longint;
                               SXLA  : longint;
                               SXZE  : longint;
                               SXKZ  : longint;
                               SXPO  : longint;
                               SYEA  : longint;
                               SYER  : longint;
                               SYGA  : longint;
                               SYGF  : longint;
                               SYLV  : longint;
                               SYLA  : longint;
                               SYZE  : longint;
                               SYKZ  : longint;
                               SYPO  : longint;
                               END;
                       c2    : RECORD
                               SIL   : longint;
                               SVA   : longint;
                               SXEA  : longint;
                               SXER  : longint;
                               SXGA  : longint;
                               SXGF  : longint;
                               SXLV  : longint;
                               SXLA  : longint;
                               SXZE  : longint;
                               SXKZ  : longint;
                               SXPO  : longint;
                               SYEA  : longint;
                               SYER  : longint;
                               SYGA  : longint;
                               SYGF  : longint;
                               SYLV  : longint;
                               SYLA  : longint;
                               SYZE  : longint;
                               SYKZ  : longint;
                               SYPO  : longint;
                               END;
                    END;
    Servodatatype = RECORD
                     i_rel_abs    : integer;
                     i_JS         : integer;
                     act_position : RECORD
                                    x     : real;
                                    y     : real;
                                    z     : real;
                                    theta : real;
                                    END;
                     end_position : RECORD
                                    x     : real;
                                    y     : real;
                                    z     : real;
                                    theta : real;
                                    END;
                     distance     : RECORD
                                    x     : real;
                                    y     : real;
                                    z     : real;
                                    theta : real;
                                    END;
                     velocity     : RECORD
                                    x     : real;
                                    y     : real;
                                    z     : real;
                                    theta : real;
                                    END;
                     anorad_c     : RECORD
                                    x     : real;
                                    xa    : real;
                                    y     : real;
                                    z     : real;
                                    theta : real;
                                    END;
                     JS_velocity  : RECORD
                                    x     : real;
                                    y     : real;
                                    z     : real;
                                    theta : real;
                                    END;
                     JS_c         : RECORD
                                    x     : real;
                                    y     : real;
                                    z     : real;
                                    theta : real;
                                    END;
                     END;

    STWIdatatype = RECORD
                   Par : RECORD
                         Modus : ARRAY[1..2,1..4] OF longint;
                         Ist   : ARRAY[1..2,1..4] OF longint;
                         Soll  : ARRAY[1..2,1..4] OF longint;
                         AC    : ARRAY[1..2,1..4] OF longint;
                         VL    : ARRAY[1..2,1..4] OF longint;
                         VH    : ARRAY[1..2,1..4] OF longint;
                         Time  : ARRAY[1..2,1..4] OF longint;
                         KD    : ARRAY[1..2,1..4] OF longint;
                         KP    : ARRAY[1..2,1..4] OF longint;
                         KI    : ARRAY[1..2,1..4] OF longint;
                         IL    : ARRAY[1..2,1..4] OF longint;
                         F_VL  : ARRAY[1..2,1..4] OF longint;
                         F_VH  : ARRAY[1..2,1..4] OF longint;
                         L_VL  : ARRAY[1..2,1..4] OF longint;
                         L_VH  : ARRAY[1..2,1..4] OF longint;
                         END;
                     ref_offset   : RECORD
                                    x1     : real;
                                    x2     : real;
                                    y1     : real;
                                    y2     : real;
                                    z1     : real;
                                    z2     : real;
                                    theta1 : real;
                                    theta2 : real;
                                    END;
                     Meas         : RECORD
                                    IntBy12     : integer;
                                    IntBz12     : integer;
                                    IntBtheta12 : integer;
                                    Dz2I        : real;
                                    Dy2I        : real;
                                    END;
                     END;

(****** OLD CONFIGURATION FILE ********)
    CFGDatatype0  = RECORD
                     DefineData   : Definedatatype;
                     GridData     : GridDataType;
                     AnalysisData : Analysisdatatype;
                     DACData      : DacDataType;
                     MBInc1Data   : MBInc1DataType;
                     MBInc2Data   : MBInc2DataType;
                     SMS1Data     : SMSDataType;
                     SMS2Data     : SMSDataType;
                     DVM1Data     : DVMDataType;
                     DVM2Data     : DVMDataType;
                     DVM3Data     : DVMDataType;
                     PAR32Data    : PAR32DataType;
                     ScaleData    : ScaleDataType;
                     ServoData    : ServoDataType;
                     ServoParData : ServoParDataType;
                     END;

    STWICFGDatatype0  = RECORD
                     STWIData     : STWIDataType;
                     END;

(****** NEW CONFIGURATION FILE ********)
    CFGDatatype  = RECORD
                     DefineData   : Definedatatype;
                     GridData     : GridDataType;
                     AnalysisData : Analysisdatatype;
                     DACData      : DacDataType;
                     MBInc1Data   : MBInc1DataType;
                     MBInc2Data   : MBInc2DataType;
                     SMS1Data     : SMSDataType;
                     SMS2Data     : SMSDataType;
                     DVM1Data     : DVMDataType;
                     DVM2Data     : DVMDataType;
                     DVM3Data     : DVMDataType;
                     PAR32Data    : PAR32DataType;
                     ScaleData    : ScaleDataType;
                     ServoData    : ServoDataType;
                     ServoParData : ServoParDataType;
                     END;

    STWICFGDatatype  = RECORD
                     STWIData     : STWIDataType;
                     END;

(****** the following bloc may be different if the configuration
        file has to be changed; ********)


  plotdatatype   =  PosRecType;
    datatype256  = array[1..256] of real;
    datatype2_256= array[1..2,1..256] of real;
    datatype4_4_256= array[1..4,1..4,1..256] of real;
    datatype4_1024= array[1..4,1..1024] of real;

    SW2_FileNr=array[1..8] of word;
    Varianz   =array[1..4,1..2] of single;
    HHS_Results=Array[1..2,1..8] of real;  (*2x: Mx0,Mx90,My0,My90,Mz0,Mz90,speed0,speed90 *)CONST

(******* STWI constants ************)
     STWI_scal_vel_xyz    = 0.39;
     STWI_scal_vel_theta  = 1.0;  (* not yet calibrated *)
     STWI_length = 6400.0;
  (*   STWI_amplifier = 90.0;   *)    (* Vorverst�rker  ist jetzt Variable *)
  (*   SMS_Ramp_Hight = 0.2;    *)   (* jetzt eine Variable: Erh�hung von f um 20% der Startfrequenz *)
     SMS_factor     = 250;       (* Kalibrierungsfaktor 1mm = 250 Steps *)
     STWI2_Length   = 690;
(******* Laser constants ***********)
     straightness_cal = 356.4;
     length_cal       = 0.9997296;
     length_cal_1     = 0.9890490;
     istart           = 50;
                       (* 21 Grad, 1013 mbar, 30% Luftfeuchte *)

     maxlen     = 10;

(******* SMS constants *************)

     SMS1DefData : SMSDataType = (Steps : 1000; TimeBase : 15;
                              FrqDiv : 1000; PulseLen : 10;
                              RampSteps : 100; RampDstps : 1);
     SMS2DefData : SMSDataType = (Steps : 1000; TimeBase : 15;
                              FrqDiv : 1000; PulseLen : 10;
                              RampSteps : 100; RampDstps : 1);
     DefineDefData : DefineDataType = (speed_x : 20.0;
                              speed_y : 1.0;speed_z : 1.0;
                              speedctrl : 0; autoreverse : 0;
                              zero : 1; ninety : 1;
                              delay_1 : 1000; delay_2 : 1000; delay_3 : 1000;
                              delay_4 : 1000;
                              SaveStack : 1; SaveLogFile : 1; Red_factor : 1);
     GridDefData : GridDataType = (xmin : 0; xmax : 0; ianzx : 1;
                                   ymin : 0; ymax : 0; ianzy : 1;
                                   zmin : 0; zmax : 0; ianzz : 1;
                                   xoff1 : 0.0; xoff2 : 0.0);
     AnalysisDefData : AnalysisDataType = (SpeedCorr : 1;
                              PtsPerRev : 100.0; NrOfRev : 10;
                              OrdOfFA : 3; Hall_Pr_Resp : 0.1);
     DACDefData : DACDataType = (StartVoltage : 0; EndVoltage : 0;
                              Time : 1; Steps : 1; MaxDiff : 0.2;
                              MinDT : 0.1; Speed_const : 1.0);
     MBInc1DefData : MBInc1DataType = (delay_1 : 3; delay_2 : 20;
                              delay_3 : 3; delay_4 : 3; delay_5 : 10;
                              time_base : 1);
     MBInc2DefData : MBInc2DataType = (Delay_1 : 3; Delay_2 : 20;
                              Delay_3 : 3; Delay_4 : 3; Delay_5 : 10;
                              Time_Base_1: 1;Time_Base_2: 1);
     DVM1DefData : DVMDataType = (Range : 10.0; Aperture : 1.0;
                              Readings : 1000);
     DVM2DefData : DVMDataType = (Range : 10.0; Aperture : 1.0;
                              Readings : 1000);
     DVM3DefData : DVMDataType = (Range : 10.0; Aperture : 1.0;
                              Readings : 1000);
     PAR32DefData : PAR32DataType = (PAR1Trig : 1; PAR2Trig : 1;
                                     PAR3Trig : 1; PAR4Trig : 1);
     ScaleDefData : ScaleDataType = (xminzero : 0; xmaxzero : 10;
                                     yminzero :-0.1 ; ymaxzero : 0.1;
                                     xminninety : 0; xmaxninety : 10;
                                     yminninety : -0.1; ymaxninety : 0.1;
                                     dispchannel0 : 1; dispchannel90 : 1;
                                     autoscale0 : 1; autoscale90 : 1;
                                     resfactor0 : 1.0; resfactor90 : 1.0);

     ServoParDefData : ServoParDataType = (
                                           c0 : (SIL  : 28;
                                                 SVA  : 100000;
                                                 SXEA : 32000;
                                                 SXER : 32000;
                                                 SXGA : 20;
                                                 SXGF : -3;
                                                 SXLV : 1500000;
                                                 SXLA : 100000;
                                                 SXZE : 225;
                                                 SXKZ : 27;
                                                 SXPO : 0;
                                                 SYEA : 32000;
                                                 SYER : 32000;
                                                 SYGA : 10;
                                                 SYGF : 0;
                                                 SYLV : 15000;
                                                 SYLA : 15000;
                                                 SYZE : 237;
                                                 SYKZ : 1;
                                                 SYPO : 0);
                                           c1 : (SIL  : 31;
                                                 SVA  : 100000;
                                                 SXEA : 32000;
                                                 SXER : 32000;
                                                 SXGA : 80;
                                                 SXGF : -1;
                                                 SXLV : 1000;
                                                 SXLA : 1000;
                                                 SXZE : 234;
                                                 SXKZ : 27;
                                                 SXPO : 0;
                                                 SYEA : 32000;
                                                 SYER : 32000;
                                                 SYGA : 80;
                                                 SYGF : -2;
                                                 SYLV : 1000;
                                                 SYLA : 1000;
                                                 SYZE : 249;
                                                 SYKZ : 27;
                                                 SYPO : 0);
                                           c2 : (SIL  : 28;
                                                 SVA  : 100000;
                                                 SXEA : 32000;
                                                 SXER : 32000;
                                                 SXGA : 10;
                                                 SXGF : -2;
                                                 SXLV : 1000;
                                                 SXLA : 1000;
                                                 SXZE : 246;
                                                 SXKZ : 27;
                                                 SXPO : 0;
                                                 SYEA : 32000;
                                                 SYER : 32000;
                                                 SYGA : 10;
                                                 SYGF : -2;
                                                 SYLV : 1000;
                                                 SYLA : 1000;
                                                 SYZE : 246;
                                                 SYKZ : 27;
                                                 SYPO : 0)
                                                 );

     ServoDefData : ServoDataType = (i_rel_abs : 1; i_JS : 0;
                                     act_position : (x : 0;
                                                     y : 0;
                                                     z : 0;
                                                     theta : 0);
                                     end_position : (x : 0;
                                                     y : 0;
                                                     z : 0;
                                                     theta : 0);
                                     distance : (x : 100;
                                                 y : 10;
                                                 z : 10;
                                                 theta : 1);
                                     velocity : (x : 20;
                                                 y : 1;
                                                 z : 1;
                                                 theta : 1);
                                     anorad_c : (x : -4298.90;
                                                 xa: -0.047;
                                                 y : 2000;
                                                 z : 2000;
                                                 theta : 3655);
                                     JS_velocity : (x : 10;
                                                    y : 1;
                                                    z : 1;
                                                    theta : 1);
                                     JS_c : (x : 3655;
                                             y : 3655;
                                             z : 3655;
                                             theta : 3655)
                                    );

     STWIDefData : STWIDataType = (
                 Par : (
                       Modus : ((0, 0, 0, 0), (0, 0, 0, 0));
                       Ist :   ((0, 0, 0, 0), (0, 0, 0, 0));
                       Soll:   ((0, 0, 0, 0), (0, 0, 0, 0));
                       AC  :   ((0, 0, 0, 0), (0, 0, 0, 0));
                       VL  :   ((0, 0, 0, 0), (0, 0, 0, 0));
                       VH  :   ((0, 0, 0, 0), (0, 0, 0, 0));
                       Time :  ((0, 0, 0, 0), (0, 0, 0, 0));
                       KD   :  ((0, 0, 0, 0), (0, 0, 0, 0));
                       KP   :  ((0, 0, 0, 0), (0, 0, 0, 0));
                       KI   :  ((0, 0, 0, 0), (0, 0, 0, 0));
                       IL   :  ((0, 0, 0, 0), (0, 0, 0, 0));
                       F_VL :  ((0, 0, 0, 0), (0, 0, 0, 0));
                       F_VH :  ((0, 0, 0, 0), (0, 0, 0, 0));
                       L_VL :  ((0, 0, 0, 0), (0, 0, 0, 0));
                       L_VH :  ((0, 0, 0, 0), (0, 0, 0, 0))   );
                                    ref_offset : (x1 : 0;
                                                  x2 : 0;
                                                  y1 : 0;
                                                  y2 : 0;
                                                  z1 : 0;
                                                  z2 : 0;
                                                  theta1 : 0;
                                                  theta2 : 0) ;
                                    Meas        : (IntBy12 : 0;
                                                   IntBz12 : 0;
                                                   IntBtheta12 : 0;
                                                   Dz2I : 0.0;
                                                   Dy2I : 0.0 ) );

     BlocName              : string80 = 'TEST';

     CalDataFileDefName    : string80 = 'Test.DAT';
     RawDataFileDefName    : string80 = 'Test.Raw';
     ResDataFileDefName    : string80 = 'Test.RES';
     LasDataFileDefName    : string80 = 'Test.LAS';
     LasDataFileDefName90  : string80 = 'Test.LA1';
     LOGDataFileDefName    : string80 = 'Test.LOG';

     Path                  : string80 = 'C:\PLL\NEU\TMP\';

(****** the following name may be different if the configuration
        file has to be changed ********)
(*     CFGfileDefName0  : string80 = 'MFM0.CFG';  *)
     CFGfileDefName0  : string80 = 'MFM.CFG';
     CFGfileDefName  : string80 = 'MFM.CFG';

  ExitID    = 9000;
  StartID   = 9001;
  StopID    = 9002;
  StoreID   = 9003;
  LoadID    = 9004;
  SetUpID   = 9005;
  CalID     = 9006;
  ManualID  = 9007;
  AnalyseID = 9008;
  CommentID       = 9009;
  BlocNameID      = 9010;
  DataFileNameID  = 9011;
  ch1zeroID       = 9012;
  ch2zeroID       = 9013;
  ch1ninetyID     = 9014;
  ch2ninetyID     = 9015;

  small_value = 1.e-5;
  encoder_const = 10240;

  MBINC_Rate_1 = 2.5e6;
  MBINC_Rate_2 = 2.5e5;
  MBINC_Rate_3 = 2.5e4;
  MBINC_Rate_4 = 2.5e3;
  MBINC_Rate_5 = 2.5e2;

var

(******** Stack variables *************************************************)
    Top_Las   : PointPtrType_Las;	{pointer to start of list}
    Top_STWI  : PointPtrType_STWI;	{pointer to start of list}
    Top_DVM   : PointPtrType_DVM;	{pointer to start of list}
    END_LAS   : PointPtrType_LAS;       {pointer to end of list}
    END_STWI  : PointPtrType_STWI;      {pointer to end of list}
    END_DVM   : PointPtrType_DVM;       {pointer to end of list}
(**SW2****** DVM Identification  with program PARAMETER ********************)
   DVM_ident  : char;                  (* HP3458: h; Keithley 2001: k  *)
   SMS_RAMP_HIGHT:REAL;               (* Erh�hung von f um 20% der Startfrequenz*)
   STWI_amplifier:real;                (* =90 Bank (P.St.), =50 (MultiWire *)
   vier       :integer;                (* Zahl der Block-Positionen <=4 *)
   MesRep     :integer;                (* Zahl der Wiederrholungen, <=2 *)
   number_of_scans:integer;            (* Zahl der Durchl�ufe der Messroutine*)
   SMS_Pause  :word;                   (* Zus�tzliche Pause in SW2-Routine *)
   SMS1_Referenz, SMS2_Referenz:real;  (* Abstand zum Endlagenschalter in mm*)
   SMSREF     :integer;                (* Ref-Fahrt w�hrend der Messroutine *)
   SMS_RAMP_TYP:Char;                  (* L=linear, S=SINUS *)
   SW2_Num0 :String;                    (* aktueller Wert der sw2_num.lst*)
(** HHS *******)
   HHSBlocPos:integer;                  (*=1 wie fr�her,=2 automatisch Messung _1 und _2*)
   CheckDelta:real;                     (*Toleranz beim Vergleich der Momente*)
   HHSR      :HHS_Results;              (*1. und 2. Messung, je x0,x90,y0,y90,z0,z90*)
(************ IEEE variables ************************************************)
    bdname  : nbuf;		       (* Board name buffer.  nbuf is       *)
                                       (* defined in TPDECL.PAS as a        *)
                                       (* character array.		    *)
    hpname  : nbuf;		       (* Board name buffer.  nbuf is       *)
    liname  : nbuf;		       (* Board name buffer.  nbuf is       *)
    SWname  : nbuf;		       (* Board name buffer.  nbuf is       *)
                                       (* defined in TPDECL.PAS as a        *)
                                       (* character array.		    *)
    cmd     : cbuf;                    (* Array of commands.  cbuf is       *)
                                       (* defined in TPDECL.PAS as a        *)
                                       (* character array.                  *)
    wrt        : cbuf;                 (* Data written to the Fluke 45.     *)
    rd,rd1,rd2 : cbuf;                 (* Data received from the Fluke 45.  *)
    buffer  : string[maxlen];          (* Assigned the value of RD.         *)
                                       (* Will be converted to numeric.     *)
    sendstr : string[40];              (* GPIB command string.              *)
    sendstr1: string[40];              (* GPIB command string.              *)
    sendstr2: string[40];              (* GPIB command string.              *)
    sendstr3: string[40];              (* GPIB command string.              *)
    mask    : integer;                 (* Wait mask.                        *)
    bd      : integer;                 (* Board number.                     *)
    hp1,hp2,hp3,hp : integer;          (* Device number.                    *)
    sw1,sw2 : integer;                 (* Device number.                    *)
    li      : integer;                 (* Device number.                    *)
    code    : integer;                 (* Proced. VAL parameter.  VAL is a  *)
                                       (* Turbo Pascal conversion proced.   *)
    num     : real;                    (* Numeric conversion of RD.         *)
    sum     : real;                    (* Accumulator of measurements.      *)

(***** RS232 Variables ******************************************************)
    rd_text  : string;
    wr_text  : string;
(******* SMS_SW2 Variablen *************************************************)
   SMSR1,SMSR2      : ^SMSRAMPE;
(******* Status Variables **************************************************)
   i_log            : integer;
   ihhs             : integer;
   ifirst           : integer;
   ifirstSTWI       : integer;
   iOneTwo          : integer;
   setupini         : integer;
   ifirstdac        : integer;
   ifirstservo      : integer;
   ifirstlaser      : integer;
   ifirstpar32      : integer;
   ifirstrs232      : integer;
   ifirstDVM        : integer;
   iterhos          : integer;
   ifirstDMM        : integer;
   ijoystick        : integer;
   iblabla          : integer;
(******* Measurement Variables *********************************************)
  SekOld            :String;
   SW2_NUM          : SW2_FileNr;
   xdata0            : ^datatype256;

   yrawdata0         : ^datatype256;
   ydata0            : ^datatype2_256;
   yresdata0         : ^datatype2_256;
   xdata90           : ^datatype256;
   yrawdata90        : ^datatype256;
   ydata90           : ^datatype2_256;
   yresdata90        : ^datatype2_256;
   xData_Byz          : ^datatype4_4_256;
   yData_Byz          : ^datatype4_4_256;
   xResData          : ^datatype256;
   yResData_Byz       : ^datatype4_4_256;
   ianzdata0         : ARRAY[1..2] OF integer;
   ianzdata90        : ARRAY[1..2] OF integer;
   gbspeed           : ^datatype256;  {for granite bench}
   L1,L2,L3,L4	     : longint;
   DeltaT	     : longint;
   Laser_val	     :  ^datatype4_1024;
   xbuf              : ARRAY[1..1024] OF REAL;
   ybuf              : ARRAY[1..1024] OF REAL;
   temperature       : real;
   trigger_rate_r    : real;
   speed_averaged    : real;
   total_travel      : real;
   iplpath           : integer;
   imeasure          : integer; (* measurement is running *)
   AZERO             : integer; (* spaeter in DVM-structures einbauen *)
   i_more_scans      : integer; (* used for STWI measurements *)
   i_more_fieldcomp  : integer; (* used for STWI measurements *)
   i_more_integrals  : integer; (* used for STWI measurements *)
   ibybz             : integer; (* -------- '' -------------- *)
   i_wake_up         : integer;
   MessVar,MessMax   : Varianz; (*SW2: Standardabweichung der Doppelmessungen und Extremwerte*)

(*********** Analysis Variables ********************************************)
   phi0                            : array[1..64] of real;
   amp0                            : array[1..64] of real;
   phi90                           : array[1..64] of real;
   amp90                           : array[1..64] of real;
   mx,my_0,my_90,mz                : real;
   data0_rms, data90_rms           : real;
   speed0,speed90                  : real;
   speed0_rms, speed90_rms         : real;
   rmsmx,rmsmy                     : real;
   dipole,dipole_error             : real;
   angle_xy,angle_zy               : real;
   angle_xy_error,angle_zy_error   : real;

   npole,spole                     : real;
(******* DAC Variables ******************************************************)
   aktdacvoltage    : real;

(******* Configuration Variables ********************************************)
   cfgfile0     : File of CFGDataType0;
   cfgfile      : File of CFGDataType;
   STWIcfgfile0 : File of STWICFGDataType0;
   STWIcfgfile  : File of STWICFGDataType;
   DefineData   : DefineDataType;
   GridData     : GridDataType;
   AnalysisData : AnalysisDataType;
   DACdata      : DACdatatype;
   MBInc1data   : MBInc1datatype;
   MBInc1aData  : MBInc2DataType;
   MBInc2data   : MBInc2datatype;
   SMS1data     : SMSdatatype;
   SMS2data     : SMSdatatype;
   DVM1Data     : DVMDatatype;
   DVM2Data     : DVMDatatype;
   DVM3Data     : DVMDatatype;
   PAR32Data    : PAR32Datatype;
   ScaleData    : ScaleDataType;
   ServoData    : ServoDataType;
   ServoParData : ServoParDataType;
   STWIData     : STWIDataType;
   CFGData0     : CFGDataType0;
   CFGData      : CFGDataType;
   STWICFGData0 : STWICFGDataType0;
   STWICFGData  : STWICFGDataType;

(********** Window Variables ***********************************************)
   MainScreen                               : WinCtrlPtrType;
   event, KommandEvent,InWinEvent,SetUpEv   : WORD;
   fill, flagStart, FlagStop                : BOOLEAN;
  {Fensterbezeichner der Anzeigefenster}
   DACValueWin, TemperatureWin,
   DataFileNameWin,CFGFileNameWin,
   SpeedWin,CommentWin,BlocNameWin          : WinCtrlPtrType;
   gFont                                    : WORD;
  {Fenster und Achsenbezeichner der Me�datenfenster}
   zerowin,ninetywin,win3,win4,DateTimeWin  : WinCtrlPtrType;
   zeroanalysiswin,ninetyanalysiswin        : WinCtrlPtrType;
   analysiswin3,analysiswin4                : WinCtrlPtrType;
   resultwin                                : WinCtrlPtrType;
   warningswin                              : WinCtrlPtrType;
   aktMousePtr                              : MouseObjectPtrType;
   Axis0LayOut,Axis1LayOut,
   Axis2LayOut,Axis3LayOut                  : ScaleXYRecType;

(************ IO Variables *************************************************)
   BlocNameTmp                     : String80;
   FileNr                          : integer;
   PathData                        : string80;
   fehler                          : integer;  (*** error flag for IO*)
   ireplot                         : integer;
   outfile                         : text;
   plotdatarec                     : PosRecType;
   comment                         : string80;
   RawDataFileName,CalDataFileName : string80;
   ResDataFileName                 : string80;
   LasDataFileName, LasDataFileName90 : string80;
   DVMDataFileName, LOGDataFileName   : string80;
   CfgFileName,CfgFileName0        : string80;
   FileName,FileName0,Filekurz      : string80;
   STWICfgFileName,STWICfgFileName0 : string80;
   STWIFileName,STWIFileName0       : string80;
   ioutput                         : integer;

IMPLEMENTATION

END.
