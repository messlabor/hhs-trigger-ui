import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import h5py

from scipy.optimize import curve_fit
from scipy.fft import fft, fftfreq
from scipy.signal import find_peaks
from scipy.integrate import cumulative_trapezoid
from datetime import datetime
from mpl_toolkits.mplot3d import Axes3D


'''
The code below is a collection of functions used to analyse and manipulate the data of the Old and New helmholtz coil.
'''


def readOldHHS(path, degree, columns=None):
    '''
    The old HHS uses the first 100 lines to write the data of the 0 degrees measurement and other 100 points to the 90 degrees measurements.
    The columns depend on the data file: .DAT or .RAW, the former has the value of corrected voltage and the value of the error (speed or voltage, I don't remember now). The RAW data has the original voltage measured and the speed of the motor. The values used to calculate the magnetizations are the ones from the .DAT files.
    '''
    if degree == 0:
        first_row = 1
        last_row = 101
    elif degree == 90:
        first_row = 102
        last_row = False
    
    df = pd.read_csv(path, header=None, skiprows=first_row, skipfooter=last_row)
    for i in range(len(df)):
        df.loc[i,0] = df.loc[i,0].replace(" ",";").replace(";;",";")
    df = df[0].str.split(';', n=-1, expand=True)
    df.columns = ["test","turns", "idk1", "idk2"]
    df = df.drop("test",axis=1)
    for i in df.columns:
        df[i]=pd.to_numeric(df[i])
    if columns is not None:
        df.columns = columns
    return df

def writeH5File(filename, arr, step_size, 
                jog_velocity, jog_accel, 
                n_total, multiturn, home, 
                zero_degree_pointer=192203464.625, 
                DVM_Aperture=0.02, radgra=np.pi / 180):
    """
    Inputs: filename, the array of measurements, and the attributes to be saved as a dictionary
    Output: HDF5 file with dataset and the configurations of the experiment, including magnetization calculations
    """
    from scipy.fft import fft, fftfreq

    today = datetime.today().strftime("%Y/%m/%d %H:%M")

    # Fourier Analysis and Magnetization Calculations
    resolution = 65535  # Multiturn encoder resolution
    ydata = arr[:, 1]  # Voltage data
    positions = arr[:, 0]  # Positions in counts
    freq = jog_velocity / 360  # Average frequency based on jog velocity in Hz

    N = len(ydata)
    T = (positions[1] - positions[0]) / resolution / freq

    fy = fft(ydata)
    xf = fftfreq(N, T)[:N // 2]

    idx = np.argmax(np.abs(fy[0:N // 2]))
    ampc = 2 * np.real(fy[idx]) / N
    amps = 2 * np.imag(fy[idx]) / N
    phi = np.arctan2(-amps, ampc)

    amp = ampc / np.cos(phi)

    # Phase corrections
    Delta_phi = 0.5 * DVM_Aperture * 2.0 * np.pi * freq
    phi -= Delta_phi

    # Magnetization components
    mv = amp * np.cos(phi / radgra)
    mh = -amp * np.sin(phi / radgra)

    # Open and write to HDF5 file
    with h5py.File(f"{filename}", "w") as f:
        # Measurement group
        grp = f.create_group("Measurement")
        
        # Add column names before saving the dataset
        rec_arr = np.rec.fromarrays(
            [arr[:, 0], arr[:, 1], arr[:, 2]],
            dtype=np.dtype({
                "names": ["position", "voltage", "time"], 
                "formats": [(float)] * 3
            })
        )
        dset = grp.create_dataset("Results", data=rec_arr)

        # Experiment settings as attributes
        grp.attrs["Step size [ct]"] = step_size
        grp.attrs["Jog Velocity [°/s]"] = jog_velocity
        grp.attrs["Jog Acceleration [°/s²]"] = jog_accel
        grp.attrs["Number Samples"] = n_total
        grp.attrs["Multiturn [ct]"] = multiturn
        grp.attrs["Date"] = today
        grp.attrs["Home Position [ct]"] = home
        grp.attrs["Zero Degree Pointer [ct]"] = zero_degree_pointer

        # Add magnetization results as attributes
        grp.attrs["Mv [mV/Hz]"] = 1000 * mv / abs(freq)
        grp.attrs["Mh [mV/Hz]"] = 1000 * mh / abs(freq)
        grp.attrs["Amplitude [mV/Hz]"] = 1000 * amp / freq
        grp.attrs["Phase [rad]"] = phi


def positions_to_degrees(positions, reference_position = 192203464.625):
    """
    Convert encoder positions to degrees.

    The reference position was measured manually on the system and added here. It representats the position in counts of the motor when the magnet is parallel to the surface. One turn of the motor is 65535 counts.

    Parameters
    ----------
    positions : numpy.ndarray
        Array of encoder positions.
    reference_position : float
        Reference position in encoder counts.
    """
    # Given data
    counts_per_revolution = 65535
    degrees_per_revolution = 360
    counts_per_degree = counts_per_revolution / degrees_per_revolution

    # Calculate the differences in counts
    differences_in_counts = positions - reference_position

    # Convert the differences to degrees
    angle_differences = differences_in_counts / counts_per_degree

    # Normalize the angles within [0°, 360°]
    normalized_angles = np.mod(angle_differences, 360)
    normalized_angles = (normalized_angles + 360) % 360

    # Calculate the number of turns
    n_turns = (np.max(positions) - np.min(positions)) / (360 * counts_per_degree)

    return normalized_angles, n_turns

def read_data(file_path):
    '''
    This function reads the h5 file generated from the run_measurement.py routine. 
    Remember to change this function whenever you add more features to the data analysis.

    '''
    with h5py.File(file_path, 'r') as f:
        res_attrs = list(f["Measurement"].attrs.keys())
        attrs_values = list(f["Measurement"].attrs.values())
        data = dict(zip(res_attrs, attrs_values))
        data["Home Position [ct]"] = round(data["Home Position [ct]"], 2)
        data["Speed [ct/s]"] = data["Jog Velocity [°/s]"] * 65536 / 360 # Convert to Hz

        dset = f["Measurement"]["Results"]
        voltage = dset["voltage"][:]
        position = dset["position"][:]
        degrees, n_turns = positions_to_degrees(position, data["Home Position [ct]"])
        times = dset["time"][:]

        # Create DataFrame
        df = pd.DataFrame({"Position": position, 
                           "Voltage": voltage, 
                           "Degrees": degrees, 
                           "Time": times})

        # Calculate Turn numbers more accurately
        turn_changes = np.diff(df['Degrees']) < 0  # Detect resets in the degree count
        turn_indices = np.insert(turn_changes, 0, True)  # True at the start and wherever a reset is detected
        df['Turn'] = np.cumsum(turn_indices)  # Cumulative sum to increment the turn count

        # Calculate the time on each turn
        df['Turn Time'] = df.groupby('Turn')['Time'].transform(lambda x: x - x.min())
        
        # Calculate the speed
        df['Speed_cts'] = np.insert(np.abs(np.diff(df['Position']) / np.diff(df['Time'])), 0, 0)  # Speed in position units per second
        df["Speed_deg"] = df["Speed_cts"] * 360 / (65535)  # Speed in degrees per second
        df["Speed_Hz"] = df["Speed_cts"] / 65535  # Speed in Hz
        df.loc[0, "Speed_Hz"] = df["Speed_Hz"].iloc[1]  # Replace the first value with the second value

    return data, df

def fft_analysis(data, df):

    '''
    this function calculates the fourier transform of the code and returns the amplitude yf for each frequency xf.
    This can be used to calculate the magnetization based on the old system methods. As well as to validate if the sampling and the measurement is good. 
    '''
    
    resolution = 65535
    
    freq = data["Jog Velocity [°/s]"] * resolution / 360

    y = np.array(df["Voltage"])
    N = len(y)
    T = (df["Position"][1] - df["Position"][0]) / freq
    fy = fft(y)

    xf = fftfreq(N, T)[:N//2]

    return xf, fy

# Xu, J. Z., & Vasserman, I. (2012). 
# A new Helmholtz coil permanent magnet measurement system.
def compute_moments(df):

    time        = df["time_mean"]
    time_err    = df["time_std"]
    angle       = df["degree_mean"]
    angle_err   = df["degree_std"]
    voltage     = df["voltage_mean"]
    voltage_err = df["voltage_std"]

    # constants
    mu_0 = 4 * np.pi * 1e-7  # permeability of free space [T m/A]
    N = 349  # number of coil turns
    R = 0.305 # radius of the coil [m]
    k = (5/4)**(3/2) * (R / (mu_0 * N)) # coil constant [A/T]

    theta = np.radians(angle)
    theta_err = np.radians(angle_err)

    flux = -cumulative_trapezoid(voltage, time, initial=0)
    flux_err = -cumulative_trapezoid(voltage_err, time_err, initial=0)

    def model(theta, A, B, C):
        return A * np.sin(theta) + B * np.cos(theta) + C
    
    params, params_covariance = curve_fit(model, theta, flux)
    
    A, B, C = params
    A_err, B_err, C_err = np.sqrt(np.diag(params_covariance))

    # Calculate the moments
    Mv = k * A
    Mh = k * B
    Mv_err = k * A_err
    Mh_err = k * B_err

    # Round values based on error
    Mv, Mv_err = round_sig_figs(Mv, Mv_err)
    Mh, Mh_err = round_sig_figs(Mh, Mh_err)

    # compile into array
    radians     = np.array(theta)
    time        = np.array(time)
    flux        = np.array(flux)
    fitted_flux = model(radians, A, B, C)
    # put into dataframe
    df["flux"]          = flux
    df["flux_err"]      = flux_err
    df["fitted_flux"]   = fitted_flux

    return df, Mv, Mh, Mv_err, Mh_err

# Xu, J. Z., & Vasserman, I. (2012). 
# A new Helmholtz coil permanent magnet measurement system.
def compute_moments_v2(df):

    angle   = df["Degrees"]
    time    = df["Time"]
    voltage = df["Voltage"]

    # constants
    mu_0 = 4 * np.pi * 1e-7  # permeability of free space [T m/A]
    N = 349  # number of coil turns
    R = 0.305 # radius of the coil [m]
    k = (5/4)**(3/2) * (R / (mu_0 * N)) # coil constant [A/T]

    theta = np.radians(angle)

    flux = -cumulative_trapezoid(voltage, time,)

    def model(theta, A, B, C):
        return A * np.sin(theta) + B * np.cos(theta) + C
    
    params, params_covariance = curve_fit(model, theta, flux)
    
    A, B, C = params
    A_err, B_err, C_err = np.sqrt(np.diag(params_covariance))

    # Calculate the moments
    Mv = k * A
    Mh = k * B
    Mv_err = k * A_err
    Mh_err = k * B_err

    # Round values based on error
    Mv, Mv_err = round_sig_figs(Mv, Mv_err)
    Mh, Mh_err = round_sig_figs(Mh, Mh_err)

    # put into dataframe
    df["flux"]          = np.array(flux)
    df["fitted_flux"]   = model(theta, A, B, C)

    return df, Mv, Mh, Mv_err, Mh_err


def compute_binned_avg_stats(df, bin_width=0.1):
    # Define the bin edges for every 'bin_width' interval
    bin_edges = np.arange(df["Degrees"].min(), df["Degrees"].max() + bin_width, bin_width)

    # Assign each degree value to a bin
    df["Degrees Bin"] = pd.cut(df["Degrees"], bins=bin_edges, include_lowest=True)

    # Group by the created bins and compute mean and standard deviation
    binned_stats = df.groupby("Degrees Bin").agg(
        degree_mean     =("Degrees", "mean"),
        degree_std      =("Degrees", "std"),
        time_mean       =("Turn Time", "mean"),
        time_std        =("Turn Time", "std"),
        voltage_mean    =("Voltage", "mean"),
        voltage_std     =("Voltage", "std"),
    ).reset_index()

    # Drop NaN rows that might occur due to empty bins
    binned_stats.dropna(inplace=True)
    
    return binned_stats


def round_sig_figs(val, val_err, sig_figs=2):
    '''
    Round a value and its error estimate to a certain number 
    of significant figures (on the error estimate).  By default 2
    significant figures are used.
    '''

    n = int(np.log10(val_err))  # displacement from ones place
    if val_err >= 1:
        n += 1

    scale = 10 ** (sig_figs - n)
    val = round(val * scale) / scale
    val_err = round(val_err * scale) / scale

    return val, val_err

def plot_0degrees_axis(fig, gs):
    ax = fig.add_subplot(gs, projection='3d')
    ax.axis("off")
    ax.set_xlim([-4, 4])
    ax.set_ylim([-4, 4])
    ax.set_zlim([-4, 4])

    ax.quiver(0, 0, 0, 4, 0, 0, arrow_length_ratio=0.1)  # X-axis
    ax.quiver(0, 0, 0, 0, 0, 4, arrow_length_ratio=0.1)  # Y-axis
    ax.quiver(0, 0, 0, 0, -4, 0, arrow_length_ratio=0.1)  # Z-axis

    ax.text(5, 0, 0, "X", color='red', fontsize=12)
    ax.text(-2, -4, 0, "Z", color='red', fontsize=12)
    ax.text(0, 0, 5, "Y", color='red', fontsize=12)

    ax.grid(False)
    return ax

def plot_90degrees_axis(fig, gs):
    ax = fig.add_subplot(gs, projection='3d')
    ax.axis("off")
    ax.set_xlim([-4, 4])
    ax.set_ylim([-4, 4])
    ax.set_zlim([-4, 4])

    ax.quiver(0, 0, 0, 4, 0, 0, arrow_length_ratio=0.1)  # Y-axis
    ax.quiver(0, 0, 0, 0, 0, -4, arrow_length_ratio=0.1)  # X-axis
    ax.quiver(0, 0, 0, 0, -4, 0, arrow_length_ratio=0.1)  # Z-axis

    ax.text(4, 0, 0, "Y", color='red', fontsize=12)
    ax.text(-2, -4, 0, "Z", color='red', fontsize=12)
    ax.text(0, 0, -7, "X", color='red', fontsize=12)

    ax.grid(False)
    return ax
