USES Graph,CRT,STDLIB,MouseLib,VESALib,WinLib;

CONST
  ButtonList = '1.Button a' + ZeroChr +
               '2.Button ab' + ZeroChr +
               '3.Button abc' + ZeroChr +
               '4.Button abcd' + ZeroChr;
  objListNr  = $100;

VAR
  buttonNr : WORD;
  outText  : STRING;

BEGIN
  WinInit(VGA);
  MouseInstall(MouseAllEvent,MousePoll);

  WinSetHeader(WinDefDes,Blue,'Liste');
  ButtonNr := WinListBox (100,150,objListNr,
                         {ObjButtonType} ObjTextType OR Local OR VertDir OR PosCenter,
                          Font8x16,ButtonList,NilEvProc,outText);

  WRITELN('Button ',ButtonNr,' ausgew�hlt mit Namen ',outText);
  WRITELN('Taste dr�cken f�r Programmende');
  REPEAT
  UNTIL(KeyPressed);
END.

