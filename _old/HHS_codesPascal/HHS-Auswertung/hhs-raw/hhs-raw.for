
      integer i,ii,k,k1,inum,imin(1500),imax(1500),orientation
      character*80 xline,namei,nameo,nameo1,nameo2,dfile,stra,strb,str0,
     & strmag,s1,s2,s3,s4,xnum
		
      integer clen1
      
c------------------------------------------------------------------
c
c     einlesen des Parameter-Files data.par
c
c     - orientierung (1 oder 2)
c     - Anzahl (inum) der Zeilen mit Magnetnummern
c     - inum Zeilen mit Start- und EndMagnetnummer
c     - Output-Name (nameo) f�r die Messergebnisse
c
c------------------------------------------------------------------
1     format(a80)
2     format(a1)
3     format(2i7,6f11.4)
4     format(2i6,7f9.3)            
       
      str0='0'      

      open(unit=10,file='data.par')
      read(10,1)dfile
        ifile=clen1(dfile)
        nameo=''
        nameo(1:ifile)=dfile(1:ifile)    
        if(nameo(ifile:ifile).eq.'1')orientation=1
        if(nameo(ifile:ifile).eq.'2')orientation=2
        write(6,*)' orientation of measurement = ',orientation
        strmag=''
        strmag(1:2)=nameo(6:7)
c      check whether magnet is reference magnet
        iref=1
        if(((strmag(2:2).ge.'A').and.(strmag(2:2).le.'Z')).or.
     &     ((strmag(2:2).ge.'a').and.(strmag(2:2).le.'z')))iref=0
      read(10,*)inum
      do k=1,inum
        read(10,*)imin(k),imax(k)
        write(6,*)k,imin(k),imax(k)
      enddo
        nameo1=nameo
        nameo2=nameo
        stra='.dat'
        strb='.rms'
        call strcat_for(nameo1,stra)
        call strcat_for(nameo2,strb)       
      close(10)

      ii=0
      open(unit=10,file=nameo1)
      open(unit=11,file=nameo2)
      do k=1,inum
	  do k1=imin(k),imax(k)
          ii=ii+1
          
c------------ build magnet name
	namei=''
      call strcat_for(namei,strmag)
      if(iref.eq.1)then
        stra='-'
        call strcat_for(namei,stra)
      endif  
      if(iref.eq.1)then
        i1=0
        else
      i1=k1/1000
      endif
      i2=(k1-i1*1000)/100
      i3=(k1-i1*1000-i2*100)/10
      i4=k1-i1*1000-i2*100-i3*10

      if(iref.eq.0)call digi(i1,s1)
      call digi(i2,s2)      
      call digi(i3,s3)
      call digi(i4,s4)   
      
      if(iref.eq.0)call strcat_for(namei,s1)     
      call strcat_for(namei,s2) 	
      call strcat_for(namei,s3) 
      call strcat_for(namei,s4)   

      if(orientation.eq.1)stra='_1.res'
	if(orientation.eq.2)stra='_2.res'
      call strcat_for(namei,stra)
      
      write(6,*)'  '
      write(6,1)namei
      open(unit=12,file=namei)
      read(12,1)xline
      read(12,1)xline
      len=clen1(namei)-4
      
c--------- check for consistency
      do m=1,len
        if(namei(m:m).ne.xline(11+m:11+m))then
          write(6,*)' file name inconsistent with block name'
c         stop
c         end
        endif
      enddo
        
      read(12,1)xline      
      read(12,1)xline
      read(12,1)xline

c-------- read magnetic data      
c     read mz
      read(12,1)xline
      xnum=''
      do m=1,69
        xnum(m:m)=xline(11+m:11+m)
      enddo
      read(xnum,*)amz
      write(6,*)' mz = ',amz     
 
c     read my0
      read(12,1)xline
      xnum=''
      do m=1,69
        xnum(m:m)=xline(11+m:11+m)
      enddo
      read(xnum,*)amy0
      write(6,*)' my0 = ',amy0

c     read my90
      read(12,1)xline
      xnum=''
      do m=1,69
        xnum(m:m)=xline(11+m:11+m)
      enddo
      read(xnum,*)amy90
      write(6,*)' my90 = ',amy90      

c     read mx
      read(12,1)xline
      xnum=''
      do m=1,69
        xnum(m:m)=xline(11+m:11+m)
      enddo
      read(xnum,*)amx
      write(6,*)' mx = ',amx      

      read(12,1)xline
      read(12,1)xline     

c     read north pole effect
      read(12,1)xline
      xnum=''
      do m=1,58
        xnum(m:m)=xline(22+m:22+m)
      enddo
      read(xnum,*)anp
      write(6,*)' north pole effect = ',anp      

c     read south pole effect
      read(12,1)xline
      xnum=''
      do m=1,58
        xnum(m:m)=xline(22+m:22+m)
      enddo
      read(xnum,*)asp
      write(6,*)' south pole effect = ',asp          
      
c-------- read statistical data 
c     temperature
c     speed0
c     data0_rms
c     speed0_rms
c     speed90
c     data90_rms
c     speed90_rms

c     read temperature
      read(12,1)xline
      read(12,1)xline   
      ioff=22
      xnum=''
      do m=1,80-ioff
        xnum(m:m)=xline(ioff+m:ioff+m)
      enddo
      read(xnum,*)atemp
      write(6,*)' temperature ',atemp     
 
c     read speed0
      do i=1,6
        read(12,1)xline
      enddo
      ioff=21
      xnum=''
      do m=1,80-ioff
        xnum(m:m)=xline(ioff+m:ioff+m)
      enddo
      read(xnum,*)speed0
      write(6,*)' speed0 = ',speed0

c     read data0 rms
      read(12,1)xline
      read(12,1)xline   
      ioff=40
      xnum=''
      do m=1,80-ioff
        xnum(m:m)=xline(ioff+m:ioff+m)
      enddo
      read(xnum,*)data0rms
      write(6,*)' data0rms ',data0rms        
      
c     read speed0 rms
      read(12,1)xline   
      ioff=40
      xnum=''
      do m=1,80-ioff
        xnum(m:m)=xline(ioff+m:ioff+m)
      enddo
      read(xnum,*)speed0rms
      write(6,*)' speed0rms ',speed0rms        
               
c     read speed90
      do i=1,10
        read(12,1)xline
      enddo
      ioff=21
      xnum=''
      do m=1,80-ioff
        xnum(m:m)=xline(ioff+m:ioff+m)
      enddo
      read(xnum,*)speed90
      write(6,*)' speed90 = ',speed90

c     read data90 rms
      read(12,1)xline
      read(12,1)xline   
      ioff=41
      xnum=''
      do m=1,80-ioff
        xnum(m:m)=xline(ioff+m:ioff+m)
      enddo
      read(xnum,*)data90rms
      write(6,*)' data90rms ',data90rms        
      
c     read speed90 rms
      read(12,1)xline   
      ioff=41
      xnum=''
      do m=1,80-ioff
        xnum(m:m)=xline(ioff+m:ioff+m)
      enddo
      read(xnum,*)speed90rms
      write(6,*)' speed90rms ',speed90rms        
            
      close(12)

c------- write data
      write(6,*)' '
      write(6,*)' '      
      write(6,*)' writing summary files of magnetics and statistics: '
      write(6,*)' '      
      write(6,1)nameo1
      write(6,1)nameo2
       
      write(10,3)ii,k1,amz,amy0,amy90,amx,anp,asp
      write(11,4)ii,k1,atemp,speed0,data0rms,speed0rms,
     &  speed90,data90rms,speed90rms
        enddo
      enddo    
      
      close(11)
      close(10)

      stop
      end
          
c----------------------------------------------------------
      integer function clen1(str)
c----------------------------------------------------------
      character*(*) str
      do 23000 i=1,80
      if(.not.(ichar(str(i:i)) .eq. 32))goto 23002
      clen1 = i-1
      goto 23001
23002 continue
      clen1 = i
23003 continue
23000 continue
23001 continue
      return
      end
      
c----------------------------------------------------------
      subroutine strcat_for(str1,str2)
c----------------------------------------------------------
c
c     this routine adds string b to string a
c
c----------------------------------------------------------
      character*80 str1,str2
      
1     format(a80)
      i1=clen1(str1)
      i2=clen1(str2)
      do i=1,i2
      str1(i1+i:i1+i)=str2(i:i)
      enddo
      
      return
      end
      
c----------------------------------------------------------
      subroutine digi(ii,str)
c----------------------------------------------------------
      character*80 str
      
      str=''
      if(ii.eq.0)str(1:1)='0'
      if(ii.eq.1)str(1:1)='1'      
      if(ii.eq.2)str(1:1)='2' 
      if(ii.eq.3)str(1:1)='3'      
      if(ii.eq.4)str(1:1)='4'  
      if(ii.eq.5)str(1:1)='5' 
      if(ii.eq.6)str(1:1)='6'
      if(ii.eq.7)str(1:1)='7'     
      if(ii.eq.8)str(1:1)='8'      
      if(ii.eq.9)str(1:1)='9'      

      return
      end