rem Extraktion der *.res-Daten 
rem Aufruf: hhsres.exe -t [TYP]  -p [ORD]  
rem [TYP]=max. 2 Characters, z.b. AN, AS (Magnet-Typ) oder A1, A2 (Ref-Magnete) 
rem [ORD]=max. 1 Character, = Ordner-Buchstabe des Tagesverzeichnisses(A,B,C,...)
rem erzeugt: data[TYP][ORD]_1.dat, data[TYP][ORD]_1.rms, data[TYP][ORD]_2.dat, data[TYP][ORD]_2.rms
rem *.dat-Spalten: lfd.Nr MagNr Mz My0 My90 Mx Nordpol suedpol | Magnet-Name
rem *.rms-Spalten: lfd.Nr MagNr Temp. Speed0 Data0rms Speed0rms Speed90 data90rms speed90rms | Magnetname


set  ORD=B
set  TYP=AN

hhsres.exe -t %TYP% -p %ORD%

