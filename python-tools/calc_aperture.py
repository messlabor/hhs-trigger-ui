import numpy as np
import pandas as pd
import math
import matplotlib.pyplot as plt
from matplotlib import gridspec
from scipy.fft import fft, fftfreq

from hhs_tools import *

'''
This code was an attempt to calculate the aperture based on the phase shift used in the old pascal code. The results showed that the aperture was not exactly the one showed in the LOG files and my idea was to find which factor was being added or multiplied to the phase shift, maybe a factor of pi or pi/2 was missing. I did not find anything with the amount of time I put into it.
'''



# Assuming readOldHHS and other imports are already available
df_raw = readOldHHS("../_old/N444/N444_A_1.RAW", degree=0, columns=["turns", "voltage", "Hz"])
df_dat = readOldHHS("../_old/N444/N444_A_1.DAT", degree=0, columns=["turns", "voltage", "residual"])

# Initialize variables
radgra = 180 / math.pi
yrawdata = np.array(df_raw["voltage"])
ycorrdata = np.array(df_dat["voltage"])
N = len(ycorrdata)
freq = df_raw["Hz"].mean()
T = (df_dat["turns"][1] - df_dat["turns"][0]) / freq

# Fourier Analysis
fy = fft(ycorrdata)
xf = fftfreq(N, T)[:N // 2]
ampc = 2 * np.real(fy[np.argmax(np.abs(fy))]) / N
amps = 2 * np.imag(fy[np.argmax(np.abs(fy))]) / N
phi = math.atan(-amps / ampc)
amp = ampc / math.cos(phi)

# Target phase and tolerance
target_phi = -3.49943139293752E-0001 / radgra  # Target phase in rad
tolerance = 1e-6

# Iteratively adjust DVM1Data_Aperture
DVM1Data_Aperture = 0.002  # Starting value
step_size = 0.00001  # Increment step size
max_iterations = 100000  # Prevent infinite loops

for _ in range(max_iterations):
    # Phase correction
    Delta_phi = 0.5 * DVM1Data_Aperture * 2.0 * math.pi * freq
    adjusted_phi = phi - Delta_phi  # Apply the phase correction

    if abs(adjusted_phi - target_phi) <= tolerance:
        break

    # Adjust DVM1Data_Aperture
    if adjusted_phi < target_phi:
        DVM1Data_Aperture -= step_size
    else:
        DVM1Data_Aperture += step_size

# Results
print(f"Calculated DVM1Data_Aperture: {DVM1Data_Aperture}")
print(f"Adjusted Phase (rad): {adjusted_phi}")
print(f"Target Phase (rad): {target_phi}")
print(f"Delta_phi: {Delta_phi}")
