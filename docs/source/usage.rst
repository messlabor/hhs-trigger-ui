Usage 
====================

Inside the repository you can find a step-by-step explaining how to connect the devices and run measurements -  `Step by Step Instructions. <https://codebase.helmholtz.cloud/messlabor/helmholtzcoil/hhs-trigger-ui/-/blob/main/HHS_Instructions_EN.docx?ref_type=heads>`_ . The step-by-step document together with this documentation and the IOCs / PowerPMAC project documentations should help the user to understand the project as a whole and further develop it.

Python automation
----------------------

We use the file ./python-tools/run_measurement.py in the parent folder to run our measurement. This can be used as a reference for custom measurements. It makes use of the ophyd classes created in the respective projects of the IOCs and use a simple routine to send the commands to the IOCs to run the measurement, collect the data, and save as hdf5 file. 

We run the file below inside the User interface by pressing the button **Start**. The procedure sets already all the important parameters for the measurement.

.. literalinclude:: ../../python-tools/run_measurement.py
    :language: python


User Interface 
---------------

The User interface can be opened normally from the Phoebus interface, or via terminal commands, for example.

.. code-blox::console
    /opt/epics/phoebus-4.7.3-SNAPSHOT/phoebus.sh -resource file:/opt/epics/hhs-trigger-ui/hhs.bob?

.. note::
    Refer to `Trigger IOC: <https://codebase.helmholtz.cloud/messlabor/helmholtzcoil/ioc-pmac-trigger>`_  for details on the IOC controlling the PLC.

.. note::
    Refer to `Power PMAC Project: <https://codebase.helmholtz.cloud/messlabor/helmholtzcoil/powerpmac-trigger>`_ for details on how the PLC works.

.. figure:: ./phoebus-ui.png
   :width: 800px
   :align: center

.. note::
    The button **START** will be responsible for running the run_measurement.py file, which contains the necessary automations for the procedure.

.. note::
    If the Step Size is too small and the rotation speed is too fast, the multimeter will throw  **TRIGGER TOO FAST ERROR**. Reseting the IOC might help. If not, try using the Interactive IO from Keysight to clear the device and send a reset command.

On the top the user can start and close the communication with the devices (Multimeter and PMAC). By pressing the buttons, a new terminal will open and the ioc will be started. Just close the terminal window to stop communication.

On the left side we can see the settings of the measurement. The buttons ABORT PLC , RESET PLC are used to control the State Machine in the PLC and only the PLC, the motor is movement is not affected by these buttons. Additionally, one can turn the PLC on/off using the respective buttons.

The multimeter box will allow the user to reset the multimeter, trigger a measurement using "AUTO" and stop the measurement using "HOLD". This is in case one wants to check what values are being sampled by the multimeter.

The Motor Overview window on the right (in blue) is a summary of some of the motor information. It is based on the EPICS Motor Record, and it allows the user to change its position, start, stop and jog (forward or backward). Check the `EPICS Motor Record Documentation. <https://epics.anl.gov/bcda/synApps/motor/motorRecord.html>`_ 

.. note::

    The blue Buttons "Motor Controls" and "Multimeter" will open the respective phoebus interfaces. This might be necessary in case of errors of custom measurements.

