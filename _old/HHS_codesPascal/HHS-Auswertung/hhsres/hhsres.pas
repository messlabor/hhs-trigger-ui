program hhsres;
{20.05.2015 wf
extrahiert data[TYP]1.dat, data[TYP]2.dat, data[TYP]1.rms, data[TYP]2.rms
aus HHS *.res-Dateien
sucht vorher �ber die Masken [TYP]*_1.res und [TYP]*_2.res die filenamen raus
Programm im Pfad starten.  }

{$A+,B-,D-,E-,F+,G-,I+,L-,N+,O-,R-,S+,V-,X-}
uses crt,dos;
const maxlist=2048;
fsuffix='.*';
TYPE
   str16=String[16];
   str1 =STRING[1];
   str2 =STRING[2];
   str4 =STRING[4];
   str8 =STRING[8];
FileL=Array[1..maxlist] of str16;
RESULT =RECORD
          BlockName:string;
          MagNr    :str4;
          MagNrWert:integer;
          MZ,MY0,MY90,MX,NPo,SPo,Temp,Sp0,Darms0,Sprms0,Amp0,Pha0,Sp90,Darms90,Sprms90,Amp90,Pha90:single;
        end;

var ix,iy,iz,lauf:word;
    fl3name,fl1name,fl2name,fl2,strdir0,strdir,fzeile,fff :string;
    wert1:single;
    f1,f2,f3:text;
    fl,fl1:FILEL;
    error:integer;
    res:Result;
    Pref:Str2;
    Ordner:Str1;

PROCEDURE info;
begin
  writeln;
  writeln('hhsres.exe  20.05.2015wf');
  writeln('extrahiert Daten aus HHS *.res-Dateien ');
  writeln('speichert data[TYP][ORD]_1.dat, data[TYP][ORD]_1.rms');
  writeln('          data[TYP][ORD]_2.dat, data[TYP][ORD]_2.rms');
  writeln('File-Suchmasken: [TYP]*_1.res und [TYP]*_2.res');
  writeln('Programm in Daten-Pfad kopieren und dort starten');
  writeln(' hhsres.exe -t TYP  -p ORD   Suchmaske ueber Parameteraufruf');
  writeln('               [TYP] =max. 2char. z.B. AN,BS, oder A1, A2');
  writeln('               [ORD] =Verzeichnisbuchstabe A,B,C...');
  writeln(' hhres.exe             Suchmaske ohne Parameteraufruf: alle *.res');
end;

PROCEDURE FindMagNr;
var ixx,iyy,izz,err:integer;
    StrTmp,StrTmp1:Str4;
    strt1:Str1;
    ChrTmp:Char;
begin
  StrTmp1:='';
  izz:=length(res.BlockName);
  StrTmp:=Copy(res.BlockName,izz-3,4);
  for ixx:=1 to 4 do
  begin
    strt1:=copy(StrTmp,ixx,1);
    ChrTmp:=strt1[1];
    if ((ord(ChrTmp)>47) and (ord(ChrTmp)<58)) then  Strtmp1:=StrTmp1 + strt1;
  end;
  res.MagNr:=StrTmp1;
  val(StrTmp1,res.MagNrWert,err);

end;

PROCEDURE ResLesen;
var txt1,txt2,txt3:string;
    l1:integer;
    err:integer;
begin
  assign(f3,fl3name);
  reset(f3);
  repeat
    readln(f3,txt1);
    l1:=length(txt1);
    txt2:=copy(txt1,1,9);
    if (txt2='Bloc Name') then
    begin
      res.BlockName:=copy(txt1,12,l1-13);
      FindMagNr;
    end;
    if (txt2=' M_z     ') then
    begin
      txt3:=copy(txt1,13,l1-12);
      Val(txt3,res.MZ,err);
    end;
    if (txt2=' M_y (0) ') then
    begin
      txt3:=copy(txt1,13,l1-12);
      Val(txt3,res.MY0,err);
    end;
    if (txt2=' M_y (90)') then
    begin
      txt3:=copy(txt1,13,l1-12);
      Val(txt3,res.MY90,err);
    end;
    if (txt2=' M_x     ') then
    begin
      txt3:=copy(txt1,13,l1-12);
      Val(txt3,res.MX,err);
    end;
    if (txt2=' north po') then
    begin
      txt3:=copy(txt1,42,l1-41);
      Val(txt3,res.NPo,err);
    end;
    if (txt2=' south po') then
    begin
      txt3:=copy(txt1,42,l1-41);
      Val(txt3,res.SPo,err);
    end;
    if (txt2=' temperat') then
    begin
      txt3:=copy(txt1,24,l1-23);
      Val(txt3,res.Temp,err);
    end;
    if (txt2=' averaged') then
    begin
      txt3:=copy(txt1,22,l1-21);
      Val(txt3,res.SP0,err);
    end;
    if (txt2='  normali') then
    begin
      txt3:=copy(txt1,42,l1-41);
      Val(txt3,res.Darms0,err);
    end;
    if (txt2=' normaliz') then
    begin
      txt3:=copy(txt1,42,l1-41);
      Val(txt3,res.Sprms0,err);
    end;
    if (txt2=' 1 amplit') then
    begin
      txt3:=copy(txt1,27,l1-26);
      Val(txt3,res.Amp0,err);
    end;
    if (txt2=' 1 phase ') then
    begin
      txt3:=copy(txt1,21,l1-20);
      Val(txt3,res.Pha0,err);
    end;
  until (txt2=' ninety d');
  repeat
    readln(f3,txt1);
    l1:=length(txt1);
    txt2:=copy(txt1,1,9);
    if (txt2=' averaged') then
    begin
      txt3:=copy(txt1,22,l1-21);
      Val(txt3,res.Sp90,err);
    end;
    if (txt2='  normali') then
    begin
      txt3:=copy(txt1,42,l1-41);
      Val(txt3,res.Darms90,err);
    end;
    if (txt2=' normaliz') then
    begin
      txt3:=copy(txt1,42,l1-41);
      Val(txt3,res.Sprms90,err);
    end;
    if (txt2=' 1 amplit') then
    begin
      txt3:=copy(txt1,27,l1-26);
      Val(txt3,res.Amp90,err);
    end;
    if (txt2=' 1 phase ') then
    begin
      txt3:=copy(txt1,21,l1-20);
      Val(txt3,res.Pha90,err);
    end;
  until eof(f3);
  close(f3);
end;

PROCEDURE ResPrint;
begin
writeln;
writeln('### ',res.BlockName,' ###');
writeln('  M_z        = ',res.MZ);
writeln('  M_y0       = ',res.MY0);
writeln('  M_y90      = ',res.MY90);
writeln('  M_x        = ',res.MX);
writeln('  north pole = ',res.NPo);
writeln('  south pole = ',res.SPo);
writeln('  temp       = ',res.Temp);
writeln('  Speed0     = ',res.Sp0);
writeln('  DataRms0   = ',res.Darms0);
writeln('  SpeedRms0  = ',res.Sprms0);
writeln('  Amplitude0 = ',res.Amp0);
writeln('  Phase0     = ',res.Pha0);
writeln('  Speed90     = ',res.Sp90);
writeln('  DataRms90   = ',res.Darms90);
writeln('  SpeedRms90  = ',res.Sprms90);
writeln('  Amplitude90 = ',res.Amp90);
writeln('  Phase90     = ',res.Pha90)

end;

PROCEDURE DateiName;
var srec                                      :SearchRec;
    punkt,l1,dlauf,dlh                        :Integer;
    Name,Spsize,Dst,Mst,Yst,suffix        :String;
    DT                                        :Datetime;
    fl_name                                   :string[30];
begin
  for dlauf:=1 to maxlist do  Fl[dlauf]:='';
  FindFirst(fff,anyfile,SRec);
  dlauf:=1;
  lauf:=1;
  while doserror=0 do
  begin
    Name:=Srec.name;
    fl[dlauf]:=Name;
    dlauf:=dlauf+1;
{    writeln(dlauf,' ',fl[dlauf]);}
    if dlauf>maxlist then dlauf:=maxlist;
    findnext(Srec);
  end;    {while}
  lauf:=dlauf-1;
end;

PROCEDURE DataFill;
var ixx:integer;
begin
  assign(f1,fl1name);
(*  {$I}
  Reset(f1);
  {$I+}
  if IOResult <>0 then*)
  Rewrite(f1);
  writeln(fl1name);

  assign(f2,fl2name);
  Rewrite(f2);
  writeln(fl2name);
  for ixx:=1 to lauf do
  begin
    fl3name:=fl[ixx];
    ResLesen;
    writeln(ixx,' ',fl3name);
    writeln(f1,ixx:5,res.MagNrWert:6,res.MZ:14:6,res.MY0:12:6,res.MY90:12:6,res.Mx:12:6,res.NPo:12:6,res.SPo:12:6,' | ',res.BlockName);
    writeln(f2,ixx:5,res.MagNrWert:6,res.Temp:8:2,res.Sp0:12:4,res.Darms0:12:4,res.Sprms0:12:4,res.Sp90:12:4,res.Darms90:12:4,res.Sprms90:12:4,' | ',res.BlockName);
  end;
  close(f2);
  close(f1);

end;

PROCEDURE ReadParameter;
var ipx:integer;
begin
{  write('xyp ');
  for ipx:=1 to paramcount do
  begin
    write(paramstr(ipx),' ');
  end;
}
  for ipx:=1 to paramcount do
  begin

    if (paramstr(ipx)='-t') then Pref:=paramstr(ipx+1);
    if (paramstr(ipx)='-p') then Ordner:=paramstr(ipx+1);
    if (paramstr(ipx)='-?') then  info;
  end;
end;

begin
  Pref:='';
  Ordner:='';
  ReadParameter;
  fl1name:='data'+Pref+Ordner+'_1.dat';
  fl2name:='data'+Pref+Ordner+'_1.rms';
  fff:=Pref+'*_1.res';
  DateiName;
  DataFill;
  fl1name:='data'+Pref+Ordner+'_2.dat';
  fl2name:='data'+Pref+Ordner+'_2.rms';
  fff:=Pref+'*_2.res';
  DateiName;
  DataFill;
  writeln('fertig');
{  readln;}
end.
