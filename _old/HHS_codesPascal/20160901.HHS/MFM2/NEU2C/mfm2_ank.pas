{$N+}
unit mfm2_anK;
{04.04.2008}
INTERFACE

USES DOS, CRT, Graph, SCALELIB, MBBASE, BESLIB, SDLIB,
          STDLIB, MOUSELIB, VESALIB, WINLIB,
          TPDECL, AUXIO1,
          MFM2_TCK,MFM2_HWK,MFM2_UFK;
PROCEDURE DateTimeTempActualieren;
procedure get_Date_Time(var Date_str : string; var Time_str : string);

procedure RestoreRawData;
procedure SaveRawData;
procedure Write_LogFile_GB;
procedure Write_LogFile_STWI;
PROCEDURE Write_LogFile_STWI2;
procedure Write_LogFile_HHS;
PROCEDURE StoreDataProc_SW2;
procedure StoreDataProc_HHS(var ixx:integer);
procedure StoreDataProc;
procedure loaddataProc;
procedure fa;
procedure AnalysisProc;
procedure SpeedCorrection;
procedure AnalyseProc;
procedure SaveStack_Las(var fname:String);
procedure CreatePoint_Las(var aPointPtr_Las:PointPtrType_Las);
procedure DisposeOldData_Las;
procedure STWI_write_data_to_stack(var AktPoint_STWI : PointPtrType_STWI);
procedure SaveStack_STWI(var fname: String);
procedure CreatePoint_STWI(var aPointPtr_STWI:PointPtrType_STWI);
procedure DisposeOldData_STWI;
procedure SaveStack_DVM(var fname:String);
procedure CreatePoint_DVM(var aPointPtr_DVM:PointPtrType_DVM);
procedure DisposeOldData_DVM;
procedure Get_north_south_pole;
PROCEDURE WarningText(wtxt:string);
IMPLEMENTATION

uses MFM2_SCK;

PROCEDURE DateTimeTempActualieren;
var   OldWin  : WinCtrlPtrType;
     Date_str,Time_Str:String;
begin
  OldWin:=WinActCtrl;
  get_Date_Time(Date_str,Time_str);
  if (SekOld<>Copy(Time_Str,7,2)) then
  begin
    DMM_Value(1,'T');
    Changewin(DateTimeWin);
    clearwin;
    write(Date_str,' ',Time_str);
    SekOld:=Copy(Time_Str,7,2);
{    ChangeWin(MainScreen);}
    ChangeWin(Oldwin);
  end;
end;

(************* initialize stack for Laser Data ********************)
 PROCEDURE CreatePoint_Las(var aPointPtr_Las:PointPtrType_Las);
  BEGIN
   NEW(aPointPtr_Las);    {reserviert Platz auf dem Heap}
   WITH aPointPtr_Las^ DO {Belegen mit Initialisierungswerten}
    BEGIN
     Next:=NIL;
     DeltaT:=0; Laser.Ch1:=0; Laser.Ch2:=0; Laser.Ch3:=0; Laser.Ch4:=0;
    END;
  END;

(******************************************************************)
 PROCEDURE DisposeOldData_Las;
   {deletes old data and releases storage space}
  VAR ADataPtr_Las, NextDataPtr_Las    : PointPtrType_Las;
  BEGIN
    ADataPtr_Las := TOP_Las;
    REPEAT
     NextDataPtr_Las := ADataPtr_Las^.next;
     DISPOSE(ADataPtr_Las);                  {Heap s�ubern}
     ADataPtr_Las := NextDataPtr_Las;
    UNTIL (ADataPtr_Las=NIL);  {bis zum letzten Bildschirmpunkt}
   Top_Las:=NIL;
   END;

(************* initialize stack for STWI Data ********************)
 PROCEDURE CreatePoint_STWI(var aPointPtr_STWI:PointPtrType_STWI);
  BEGIN
   NEW(aPointPtr_STWI);    {reserviert Platz auf dem Heap}
   WITH aPointPtr_STWI^ DO {Belegen mit Initialisierungswerten}
    BEGIN
     Next:=NIL;
     zory:=0; yint1:=0; yint2:=0;
    END;
  END;

(******************************************************************)
 PROCEDURE DisposeOldData_STWI;
   {deletes old data and releases storage space}
  VAR ADataPtr_STWI, NextDataPtr_STWI    : PointPtrType_STWI;
  BEGIN
    ADataPtr_STWI := TOP_STWI;
    REPEAT
     NextDataPtr_STWI := ADataPtr_STWI^.next;
     DISPOSE(ADataPtr_STWI);                  {Heap s�ubern}
     ADataPtr_STWI := NextDataPtr_STWI;
    UNTIL (ADataPtr_STWI=NIL);  {bis zum letzten Bildschirmpunkt}
   Top_STWI:=NIL;
   END;

(******************************************************************)
procedure STWI_write_data_to_stack(var AktPoint_STWI : PointPtrType_STWI);
var
   ii   : integer;
   OldWin         : WinCtrlPtrType;

begin
  OldWin:=WinActCtrl;
  changewin(WarningsWin);

     for ii:=1 to ianzdata0[1] do
     begin

     with AktPoint_STWI^ DO
     begin
	zory:=xdata0^[ii];
	yint1:=ydata0^[1,ii];
	yint2:=ydata90^[1,ii];
        if (ii < ianzdata0[1]) or (i_more_scans = 1) or
           (i_more_fieldcomp = 1) or (i_more_integrals = 1) then
	begin
           CreatePoint_STWI(AktPoint_STWI^.Next);
           AktPoint_STWI:=AktPoint_STWI^.Next;
	end;
     end;
     writeln(memavail);
     end;    (** ii **)
     changewin(OldWin);
end;

(************* initialize stack for DVM data *********************)
 PROCEDURE CreatePoint_DVM(var aPointPtr_DVM:PointPtrType_DVM);
  BEGIN
   NEW(aPointPtr_DVM);    {reserviert Platz auf dem Heap}
   WITH aPointPtr_DVM^ DO {Belegen mit Initialisierungswerten}
    BEGIN
     Last:=NIL;           {aktuellen pointer eintragen}
     DVM.Ch1:=0; DVM.Ch2:=0; DVM.Ch3:=0;
     Next:=NIL;
    END;
  END;

(******************************************************************)
 PROCEDURE DisposeOldData_DVM;
   {deletes old data and releases storage space}
  VAR ADataPtr_DVM, NextDataPtr_DVM    : PointPtrType_DVM;
  BEGIN
    ADataPtr_DVM := TOP_DVM;
    REPEAT
     NextDataPtr_DVM := ADataPtr_DVM^.next;
     DISPOSE(ADataPtr_DVM);                  {Heap s�ubern}
     ADataPtr_DVM := NextDataPtr_DVM;
    UNTIL (ADataPtr_DVM=NIL);  {bis zum letzten Bildschirmpunkt}
   Top_DVM:=NIL;
   END;


(**********************************************************)
  procedure get_Date_Time(var Date_str : string; var Time_str : string);

var
  Jahr, Monat, Tag, Wochentag    : word;
  Stunde, Minute, Sekunde,Sek100 : word;
  s1,s2,s3                       : string;

  begin

  getDate(Jahr,Monat,Tag,Wochentag);
  getTime(Stunde,Minute,Sekunde,Sek100);

  str(Jahr,s1);
  str(Monat,s2);
  str(Tag,s3);
  Date_str:=s3+'.';
  if (Monat < 10) then
     Date_str:=Date_str+'0';
  Date_str:=Date_str+s2+'.'+s1;

  str(Stunde,s1);
  str(Minute,s2);
  str(Sekunde,s3);
  Time_str:=s1+':';
  if (Minute < 10) then
     Time_str:=Time_str+'0';
  Time_str:=Time_str+s2+':';
  if (Sekunde < 10) then
     Time_str:=Time_str+'0';
  Time_str:=Time_str+s3;

  end;

(****************************************************************)
procedure Write_LogFile_GB;
var
   Date_str, Time_str  : string;

   begin
   filename := path+LOGDataFileName;
   openoutfile(0,0);

   writeln(outfile,'  ');
   get_Date_Time(Date_str, Time_str);
   writeln(outfile,' *** Date *** ',Date_str);
   writeln(outfile,' *** Time *** ',Time_str);
   writeln(outfile,'  ');
   writeln(outfile,' temperature = ',temperature:8:2);
   writeln(outfile,' time interval between two datapoints (ms) = ',
                     trigger_rate_r*1000:10:4);
   writeln(outfile,' averaged speed (mm/s) = ',speed_averaged:8:2);
   writeln(outfile,' total travel (m) = ',total_travel/1000:10:3);
   writeln(outfile,'  ');
   writeln(outfile,' LI: straightness calibration factor = ',straightness_cal);
   writeln(outfile,' LI: length calibration factor       = ',length_cal);
   writeln(outfile,'  ');
   writeln(outfile,'***** DefineData. **********');
   writeln(outfile,'      speedctrl    ',DefineData.speedctrl);
   writeln(outfile,'      autoreverse  ',DefineData.autoreverse);
   writeln(outfile,'      scan speed x ',DefineData.speed_x);
   writeln(outfile,'      scan speed y ',DefineData.speed_y);
   writeln(outfile,'      scan speed z ',DefineData.speed_z);
   writeln(outfile,'      zero         ',DefineData.zero);
   writeln(outfile,'      ninety       ',DefineData.ninety);
   writeln(outfile,'      delay_1      ',DefineData.delay_1);
   writeln(outfile,'      delay_2      ',DefineData.delay_2);
   writeln(outfile,'      delay_3      ',DefineData.delay_3);
   writeln(outfile,'      SaveStack    ',DefineData.SaveStack);
   writeln(outfile,'      Red_Factor   ',DefineData.Red_Factor);
   writeln(outfile,' ');

   writeln(outfile,'*** GridData. ***************');
   writeln(outfile,'      xmin    ',GridData.xmin);
   writeln(outfile,'      xmax    ',GridData.xmax);
   writeln(outfile,'      ianzx   ',GridData.ianzx);
   writeln(outfile,'      ymin    ',GridData.ymin);
   writeln(outfile,'      ymax    ',GridData.ymax);
   writeln(outfile,'      ianzy   ',GridData.ianzy);
   writeln(outfile,'      zmin    ',GridData.zmin);
   writeln(outfile,'      zmax    ',GridData.zmax);
   writeln(outfile,'      ianzz   ',GridData.ianzz);
   writeln(outfile,'  ');

   writeln(outfile,'*** AnalysisData. **********');
   writeln(outfile,'      SpeedCorr    ',AnalysisData.SpeedCorr);
   Writeln(outfile,'      PtsPerRev    ',AnalysisData.PtsPerRev);
   writeln(outfile,'      NrOfRev      ',AnalysisData.NrOfRev);
   writeln(outfile,'      OrdOfFA      ',AnalysisData.OrdOfFA);
   writeln(outfile,'      Hall_Pr_Resp ',AnalysisData.Hall_Pr_Resp);
   writeln(outfile,'  ');

   writeln(outfile,'*** MBInc1Data. ************');
   writeln(outfile,'      delay_1      ',MBInc1Data.delay_1);
   writeln(outfile,'      delay_2      ',MBInc1Data.delay_2);
   writeln(outfile,'      delay_3      ',MBInc1Data.delay_3);
   writeln(outfile,'      delay_4      ',MBInc1Data.delay_4);
   writeln(outfile,'      delay_5      ',MBInc1Data.delay_5);
   writeln(outfile,'      Time_Base    ',MBInc1Data.Time_Base);
   writeln(outfile,'  ');

   writeln(outfile,'*** MBInc2Data. ************');
   writeln(outfile,'      delay_1      ',MBInc2Data.delay_1);
   writeln(outfile,'      delay_2      ',MBInc2Data.delay_2);
   writeln(outfile,'      delay_3      ',MBInc2Data.delay_3);
   writeln(outfile,'      delay_4      ',MBInc2Data.delay_4);
   writeln(outfile,'      delay_5      ',MBInc2Data.delay_5);
   writeln(outfile,'      Time_Base_1  ',MBInc2Data.Time_Base_1);
   writeln(outfile,'      Time_Base_2  ',MBInc2Data.Time_Base_2);
   writeln(outfile,'  ');

   writeln(outfile,'*** DVM1Data. **************');
   writeln(outfile,'      Range        ',DVM1Data.Range);
   writeln(outfile,'      Aperture     ',DVM1Data.Aperture);
   writeln(outfile,'      Readings     ',DVM1Data.Readings);
   writeln(outfile,'  ');

   writeln(outfile,'*** Par32Data. *************');
   writeln(outfile,'      Par1Trig     ',Par32Data.Par1Trig);
   writeln(outfile,'      Par2Trig     ',Par32Data.Par2Trig);
   writeln(outfile,'      Par3Trig     ',Par32Data.Par3Trig);
   writeln(outfile,'      Par4Trig     ',Par32Data.Par4Trig);
   writeln(outfile,'  ');

   writeln(outfile,'*** ServoData. *************');
   writeln(outfile,'      i_rel_abs          ',ServoData.i_rel_abs);
   writeln(outfile,'      i_JS               ',ServoData.i_JS);
   writeln(outfile,'      act_position.x     ',ServoData.act_position.x);
   writeln(outfile,'      act_position.y     ',ServoData.act_position.y);
   writeln(outfile,'      act_position.z     ',ServoData.act_position.z);
   writeln(outfile,'      act_position.theta ',ServoData.act_position.theta);
   writeln(outfile,'      end_position.x     ',ServoData.end_position.x);
   writeln(outfile,'      end_position.y     ',ServoData.end_position.y);
   writeln(outfile,'      end_position.z     ',ServoData.end_position.z);
   writeln(outfile,'      end_position.theta ',ServoData.end_position.theta);
   writeln(outfile,'      distance.x         ',ServoData.distance.x);
   writeln(outfile,'      distance.y         ',ServoData.distance.y);
   writeln(outfile,'      distance.z         ',ServoData.distance.z);
   writeln(outfile,'      distance.theta     ',ServoData.distance.theta);
   writeln(outfile,'      velocity.x         ',ServoData.velocity.x);
   writeln(outfile,'      velocity.y         ',ServoData.velocity.y);
   writeln(outfile,'      velocity.z         ',ServoData.velocity.z);
   writeln(outfile,'      velocity.theta     ',ServoData.velocity.theta);
   writeln(outfile,'      anorad_c.x         ',ServoData.anorad_c.x);
   writeln(outfile,'      anorad_c.xa        ',ServoData.anorad_c.xa);
   writeln(outfile,'      anorad_c.y         ',ServoData.anorad_c.y);
   writeln(outfile,'      anorad_c.z         ',ServoData.anorad_c.z);
   writeln(outfile,'      anorad_c.theta     ',ServoData.anorad_c.theta);
   writeln(outfile,'      JS_velocity.x      ',ServoData.JS_velocity.x);
   writeln(outfile,'      JS_velocity.y      ',ServoData.JS_velocity.y);
   writeln(outfile,'      JS_velocity.z      ',ServoData.JS_velocity.z);
   writeln(outfile,'      JS_velocity.theta  ',ServoData.JS_velocity.theta);
   writeln(outfile,'      JS_c.x             ',ServoData.JS_c.x);
   writeln(outfile,'      JS_c.y             ',ServoData.JS_c.y);
   writeln(outfile,'      JS_c.z             ',ServoData.JS_c.z);
   writeln(outfile,'      JS_c.theta         ',ServoData.JS_c.theta);

   writeln(outfile,'*** ServoParData. *************');
   writeln(outfile,' Controller 1             ',
                   ' Controller 2             ',
                   ' Controller 3             ');
   writeln(outfile,'   x           y          ',
                   '   x           y          ',
                   '   x           y          ');
   writeln(outfile,'   SIL',
          '   ',ServoParData.c0.SIL,'   ',ServoParData.c0.SIL,
          '   ',ServoParData.c1.SIL,'   ',ServoParData.c1.SIL,
          '   ',ServoParData.c2.SIL,'   ',ServoParData.c2.SIL);
   writeln(outfile,'   SVA',
          '   ',ServoParData.c0.SVA,'   ',ServoParData.c0.SVA,
          '   ',ServoParData.c1.SVA,'   ',ServoParData.c1.SVA,
          '   ',ServoParData.c2.SVA,'   ',ServoParData.c2.SVA);
   writeln(outfile,'   SEA',
          '   ',ServoParData.c0.SXEA,'   ',ServoParData.c0.SYEA,
          '   ',ServoParData.c1.SXEA,'   ',ServoParData.c1.SYEA,
          '   ',ServoParData.c2.SXEA,'   ',ServoParData.c2.SYEA);
   writeln(outfile,'   SER',
          '   ',ServoParData.c0.SXER,'   ',ServoParData.c0.SYER,
          '   ',ServoParData.c1.SXER,'   ',ServoParData.c1.SYER,
          '   ',ServoParData.c2.SXER,'   ',ServoParData.c2.SYER);
   writeln(outfile,'   SGA',
          '   ',ServoParData.c0.SXGA,'   ',ServoParData.c0.SYGA,
          '   ',ServoParData.c1.SXGA,'   ',ServoParData.c1.SYGA,
          '   ',ServoParData.c2.SXGA,'   ',ServoParData.c2.SYGA);
   writeln(outfile,'   SGF',
          '   ',ServoParData.c0.SXGF,'   ',ServoParData.c0.SYGF,
          '   ',ServoParData.c1.SXGF,'   ',ServoParData.c1.SYGF,
          '   ',ServoParData.c2.SXGF,'   ',ServoParData.c2.SYGF);
   writeln(outfile,'   SLV',
          '   ',ServoParData.c0.SXLV,'   ',ServoParData.c0.SYLV,
          '   ',ServoParData.c1.SXLV,'   ',ServoParData.c1.SYLV,
          '   ',ServoParData.c2.SXLV,'   ',ServoParData.c2.SYLV);
   writeln(outfile,'   SLA',
          '   ',ServoParData.c0.SXLA,'   ',ServoParData.c0.SYLA,
          '   ',ServoParData.c1.SXLA,'   ',ServoParData.c1.SYLA,
          '   ',ServoParData.c2.SXLA,'   ',ServoParData.c2.SYLA);
   writeln(outfile,'   SZE',
          '   ',ServoParData.c0.SXZE,'   ',ServoParData.c0.SYZE,
          '   ',ServoParData.c1.SXZE,'   ',ServoParData.c1.SYZE,
          '   ',ServoParData.c2.SXZE,'   ',ServoParData.c2.SYZE);
   writeln(outfile,'   SKZ',
          '   ',ServoParData.c0.SXKZ,'   ',ServoParData.c0.SYKZ,
          '   ',ServoParData.c1.SXKZ,'   ',ServoParData.c1.SYKZ,
          '   ',ServoParData.c2.SXKZ,'   ',ServoParData.c2.SYKZ);
   writeln(outfile,'   SPO',
          '   ',ServoParData.c0.SXPO,'   ',ServoParData.c0.SYPO,
          '   ',ServoParData.c1.SXPO,'   ',ServoParData.c1.SYPO,
          '   ',ServoParData.c2.SXPO,'   ',ServoParData.c2.SYPO);

   closeoutfile(0,0);

   end;

(****************************************************************)
procedure Write_LogFile_STWI;
var
   Date_str, Time_str  : string;
   i,j                 : integer;
   begin
   filename := path+LOGDataFileName;
   openoutfile(0,0);

   writeln(outfile,'  ');
   get_Date_Time(Date_str, Time_str);
   writeln(outfile,' *** Date *** ',Date_str);
   writeln(outfile,' *** Time *** ',Time_str);
   writeln(outfile,'  ');
   writeln(outfile,' temperature = ',temperature:8:2);
   writeln(outfile,' time interval between two datapoints (ms) = ',
                     trigger_rate_r*1000:10:4);
   writeln(outfile,' averaged speed (mm/s) = ',speed_averaged:8:2);
   writeln(outfile,' total travel (m) = ',total_travel/1000:10:3);
   writeln(outfile,'  ');
   writeln(outfile,' LI: straightness calibration factor = ',straightness_cal);
   writeln(outfile,' LI: length calibration factor       = ',length_cal);
   writeln(outfile,'  ');
   writeln(outfile,'***** DefineData. **********');
   writeln(outfile,'      speedctrl    ',DefineData.speedctrl);
   writeln(outfile,'      autoreverse  ',DefineData.autoreverse);
   writeln(outfile,'      scan speed theta ',DefineData.speed_x);
   writeln(outfile,'      scan speed y ',DefineData.speed_y);
   writeln(outfile,'      scan speed z ',DefineData.speed_z);
   writeln(outfile,'      zero         ',DefineData.zero);
   writeln(outfile,'      ninety       ',DefineData.ninety);
   writeln(outfile,'      delay_1      ',DefineData.delay_1);
   writeln(outfile,'      delay_2      ',DefineData.delay_2);
   writeln(outfile,'      delay_3      ',DefineData.delay_3);
   writeln(outfile,'      SaveStack    ',DefineData.SaveStack);
   writeln(outfile,'      Red_Factor   ',DefineData.Red_Factor);
   writeln(outfile,' ');

   writeln(outfile,'*** STWIData. ***************');
   writeln(outfile,' ref_offset x1, x2 ',
                     STWIData.ref_offset.x1,STWIData.ref_offset.x2);
   writeln(outfile,' ref_offset y1, y2 ',
                     STWIData.ref_offset.y1,STWIData.ref_offset.y2);
   writeln(outfile,' ref_offset z1, z2 ',
                     STWIData.ref_offset.z1,STWIData.ref_offset.z2);
   writeln(outfile,' ref_offset theta1, theta2 ',
                     STWIData.ref_offset.theta1,STWIData.ref_offset.theta2);
   writeln(outfile,'          IntBy12, IntBz12, IntBtheta12 ',
                     STWIData.Meas.IntBy12,'  ',STWIData.Meas.IntBz12,
                     '  ',STWIData.Meas.IntBtheta12);

   writeln(outfile,'*** GridData. ***************');
   writeln(outfile,'      xmin    ',GridData.xmin);
   writeln(outfile,'      xmax    ',GridData.xmax);
   writeln(outfile,'      ianzx   ',GridData.ianzx);
   writeln(outfile,'      ymin    ',GridData.ymin);
   writeln(outfile,'      ymax    ',GridData.ymax);
   writeln(outfile,'      ianzy   ',GridData.ianzy);
   writeln(outfile,'      zmin    ',GridData.zmin);
   writeln(outfile,'      zmax    ',GridData.zmax);
   writeln(outfile,'      ianzz   ',GridData.ianzz);
   writeln(outfile,'  ');

   writeln(outfile,'*** MBInc1Data. ************');
   writeln(outfile,'      delay_1      ',MBInc1Data.delay_1);
   writeln(outfile,'      delay_2      ',MBInc1Data.delay_2);
   writeln(outfile,'      delay_3      ',MBInc1Data.delay_3);
   writeln(outfile,'      delay_4      ',MBInc1Data.delay_4);
   writeln(outfile,'      delay_5      ',MBInc1Data.delay_5);
   writeln(outfile,'      Time_Base    ',MBInc1Data.Time_Base);
   writeln(outfile,'  ');

   writeln(outfile,'*** MBInc2Data. ************');
   writeln(outfile,'      delay_1      ',MBInc2Data.delay_1);
   writeln(outfile,'      delay_2      ',MBInc2Data.delay_2);
   writeln(outfile,'      delay_3      ',MBInc2Data.delay_3);
   writeln(outfile,'      delay_4      ',MBInc2Data.delay_4);
   writeln(outfile,'      delay_5      ',MBInc2Data.delay_5);
   writeln(outfile,'      Time_Base_1  ',MBInc2Data.Time_Base_1);
   writeln(outfile,'      Time_Base_2  ',MBInc2Data.Time_Base_2);
   writeln(outfile,'  ');

   writeln(outfile,'*** DVM1Data. **************');
   writeln(outfile,'      Range        ',DVM1Data.Range);
   writeln(outfile,'      Aperture     ',DVM1Data.Aperture);
   writeln(outfile,'      Readings     ',DVM1Data.Readings);
   writeln(outfile,'  ');

   writeln(outfile,'*** Par32Data. *************');
   writeln(outfile,'      Par1Trig     ',Par32Data.Par1Trig);
   writeln(outfile,'      Par2Trig     ',Par32Data.Par2Trig);
   writeln(outfile,'      Par3Trig     ',Par32Data.Par3Trig);
   writeln(outfile,'      Par4Trig     ',Par32Data.Par4Trig);
   writeln(outfile,'  ');

   writeln(outfile,'*** ServoData. *************');
   writeln(outfile,'      i_rel_abs          ',ServoData.i_rel_abs);
   writeln(outfile,'      i_JS               ',ServoData.i_JS);
   writeln(outfile,'      act_position.x     ',ServoData.act_position.x);
   writeln(outfile,'      act_position.y     ',ServoData.act_position.y);
   writeln(outfile,'      act_position.z     ',ServoData.act_position.z);
   writeln(outfile,'      act_position.theta ',ServoData.act_position.theta);
   writeln(outfile,'      end_position.x     ',ServoData.end_position.x);
   writeln(outfile,'      end_position.y     ',ServoData.end_position.y);
   writeln(outfile,'      end_position.z     ',ServoData.end_position.z);
   writeln(outfile,'      end_position.theta ',ServoData.end_position.theta);
   writeln(outfile,'      distance.x         ',ServoData.distance.x);
   writeln(outfile,'      distance.y         ',ServoData.distance.y);
   writeln(outfile,'      distance.z         ',ServoData.distance.z);
   writeln(outfile,'      distance.theta     ',ServoData.distance.theta);
   writeln(outfile,'      velocity.x         ',ServoData.velocity.x);
   writeln(outfile,'      velocity.y         ',ServoData.velocity.y);
   writeln(outfile,'      velocity.z         ',ServoData.velocity.z);
   writeln(outfile,'      velocity.theta     ',ServoData.velocity.theta);
   writeln(outfile,'      anorad_c.x         ',ServoData.anorad_c.x);
   writeln(outfile,'      anorad_c.xa        ',ServoData.anorad_c.xa);
   writeln(outfile,'      anorad_c.y         ',ServoData.anorad_c.y);
   writeln(outfile,'      anorad_c.z         ',ServoData.anorad_c.z);
   writeln(outfile,'      anorad_c.theta     ',ServoData.anorad_c.theta);
   writeln(outfile,'      JS_velocity.x      ',ServoData.JS_velocity.x);
   writeln(outfile,'      JS_velocity.y      ',ServoData.JS_velocity.y);
   writeln(outfile,'      JS_velocity.z      ',ServoData.JS_velocity.z);
   writeln(outfile,'      JS_velocity.theta  ',ServoData.JS_velocity.theta);
   writeln(outfile,'      JS_c.x             ',ServoData.JS_c.x);
   writeln(outfile,'      JS_c.y             ',ServoData.JS_c.y);
   writeln(outfile,'      JS_c.z             ',ServoData.JS_c.z);
   writeln(outfile,'      JS_c.theta         ',ServoData.JS_c.theta);
   for i:=1 to 2 do
   begin
   for j:=1 to 4 do
   begin
   writeln(outfile,'  ');
   writeln(outfile,' Controller ',i,' Axis ',j);
   writeln(outfile,STWIData.Par.Modus[i,j],'  ',STWIData.Par.Ist[i,j],
              '  ',STWIData.Par.Soll[i,j]);
   writeln(outfile,STWIData.Par.AC[i,j],'  ',STWIData.Par.VL[i,j],
              '  ',STWIData.Par.VH[i,j]);
   writeln(outfile,STWIData.Par.Time[i,j],'  ',STWIData.Par.KD[i,j],
              '  ',STWIData.Par.KP[i,j],'  ',STWIData.Par.KI[i,j]);
   writeln(outfile,STWIData.Par.IL[i,j],'  ',STWIData.Par.F_VL[i,j],
              '  ',STWIData.Par.F_VH[i,j],'  ',STWIData.Par.L_VL[i,j],
              '  ',STWIData.Par.L_VH[i,j]);

   writeln(outfile,' ');
   writeln(outfile,' STWI_amplifier ',STWI_amplifier);
   writeln(outfile,' STWI_length ',STWI_length);
   end;
   end;


   closeoutfile(0,0);
end;

procedure Write_LogFile_STWI2;
var
   Date_str, Time_str  : string;
   i,j                 : integer;
   begin
   filename := path+filekurz+'.log';
   openoutfile(0,0);


   get_Date_Time(Date_str, Time_str);

   writeln(outfile,' *** Date *** ',Date_str);
   writeln(outfile,' *** Time *** ',Time_str);
   writeln(outfile,'  ');
   writeln(outfile,Comment);
   for i:=1 to vier do
   begin
     for j:=1 to Mesrep do
     begin
       write(outfile,blocname,SW2_NUM[2*(i-1)+j],'.dvm ');
     end;
     writeln(outfile);
   end;
   writeln(outfile,'  ');
   writeln(outfile,' STWI_amplifier ',STWI_amplifier);
{   writeln(outfile,' STWI_length ',STWI_length);
}   writeln(outfile,' Number of Sample Positions         :',vier);
   writeln(outfile,' Number of Measurements per Position:',MesRep);
{   writeln(outfile,' time interval between two datapoints (ms) = ',
                     trigger_rate_r*1000:10:4);
   writeln(outfile,' averaged speed (mm/s) = ',speed_averaged:8:2);
   writeln(outfile,' total travel (m) = ',total_travel/1000:10:3);
   writeln(outfile,'  ');
   writeln(outfile,' LI: straightness calibration factor = ',straightness_cal);
   writeln(outfile,' LI: length calibration factor       = ',length_cal);
}   writeln(outfile,'  ');
   writeln(outfile,'***** DefineData. **********');
   writeln(outfile,'      speedctrl    ',DefineData.speedctrl);
   writeln(outfile,'      autoreverse  ',DefineData.autoreverse);
   writeln(outfile,'      scan speed theta ',DefineData.speed_x);
   writeln(outfile,'      scan speed y ',DefineData.speed_y);
   writeln(outfile,'      scan speed z ',DefineData.speed_z);
   writeln(outfile,'      zero         ',DefineData.zero);
   writeln(outfile,'      ninety       ',DefineData.ninety);
   writeln(outfile,'      delay_1      ',DefineData.delay_1);
   writeln(outfile,'      delay_2      ',DefineData.delay_2);
   writeln(outfile,'      delay_3      ',DefineData.delay_3);
   writeln(outfile,'      SaveStack    ',DefineData.SaveStack);
   writeln(outfile,'      Red_Factor   ',DefineData.Red_Factor);
   writeln(outfile,' ');

 {  writeln(outfile,'*** STWIData. ***************');
   writeln(outfile,' ref_offset x1, x2 ',
                     STWIData.ref_offset.x1,STWIData.ref_offset.x2);
   writeln(outfile,' ref_offset y1, y2 ',
                     STWIData.ref_offset.y1,STWIData.ref_offset.y2);
   writeln(outfile,' ref_offset z1, z2 ',
                     STWIData.ref_offset.z1,STWIData.ref_offset.z2);
   writeln(outfile,' ref_offset theta1, theta2 ',
                     STWIData.ref_offset.theta1,STWIData.ref_offset.theta2);
   writeln(outfile,'          IntBy12, IntBz12, IntBtheta12 ',
                     STWIData.Meas.IntBy12,'  ',STWIData.Meas.IntBz12,
                     '  ',STWIData.Meas.IntBtheta12);
}
   writeln(outfile,'*** GridData. ***************');
   writeln(outfile,'      xmin    ',GridData.xmin);
   writeln(outfile,'      xmax    ',GridData.xmax);
   writeln(outfile,'      ianzx   ',GridData.ianzx);
   writeln(outfile,'      ymin    ',GridData.ymin);
   writeln(outfile,'      ymax    ',GridData.ymax);
   writeln(outfile,'      ianzy   ',GridData.ianzy);
   writeln(outfile,'      zmin    ',GridData.zmin);
   writeln(outfile,'      zmax    ',GridData.zmax);
   writeln(outfile,'      ianzz   ',GridData.ianzz);
   writeln(outfile,'  ');

   writeln(outfile,'*** MBInc1Data. ************');
   writeln(outfile,'      delay_1      ',MBInc1Data.delay_1);
   writeln(outfile,'      delay_2      ',MBInc1Data.delay_2);
   writeln(outfile,'      delay_3      ',MBInc1Data.delay_3);
   writeln(outfile,'      delay_4      ',MBInc1Data.delay_4);
   writeln(outfile,'      delay_5      ',MBInc1Data.delay_5);
   writeln(outfile,'      Time_Base    ',MBInc1Data.Time_Base);
   writeln(outfile,'  ');

   writeln(outfile,'*** MBInc2Data. ************');
   writeln(outfile,'      delay_1      ',MBInc2Data.delay_1);
   writeln(outfile,'      delay_2      ',MBInc2Data.delay_2);
   writeln(outfile,'      delay_3      ',MBInc2Data.delay_3);
   writeln(outfile,'      delay_4      ',MBInc2Data.delay_4);
   writeln(outfile,'      delay_5      ',MBInc2Data.delay_5);
   writeln(outfile,'      Time_Base_1  ',MBInc2Data.Time_Base_1);
   writeln(outfile,'      Time_Base_2  ',MBInc2Data.Time_Base_2);
   writeln(outfile,'  ');
   writeln(outfile,' DVM-Type ',DVM_ident);
   writeln(outfile,'*** DVM1Data. **************');
   writeln(outfile,'      Range        ',DVM1Data.Range);
   writeln(outfile,'      Aperture     ',DVM1Data.Aperture);
   writeln(outfile,'      Readings     ',DVM1Data.Readings);
   writeln(outfile,'  ');
   if DVM_ident='k' then
   begin
     writeln(outfile,'*** DVM3Data. **************');
     writeln(outfile,'      Range(=Filter)        ',DVM1Data.Range);
     writeln(outfile,'  ');
   end;
{   writeln(outfile,'*** Par32Data. *************');
   writeln(outfile,'      Par1Trig     ',Par32Data.Par1Trig);
   writeln(outfile,'      Par2Trig     ',Par32Data.Par2Trig);
   writeln(outfile,'      Par3Trig     ',Par32Data.Par3Trig);
   writeln(outfile,'      Par4Trig     ',Par32Data.Par4Trig);
   writeln(outfile,'  ');

   writeln(outfile,'*** ServoData. *************');
   writeln(outfile,'      i_rel_abs          ',ServoData.i_rel_abs);
   writeln(outfile,'      i_JS               ',ServoData.i_JS);
   writeln(outfile,'      act_position.x     ',ServoData.act_position.x);
   writeln(outfile,'      act_position.y     ',ServoData.act_position.y);
   writeln(outfile,'      act_position.z     ',ServoData.act_position.z);
   writeln(outfile,'      act_position.theta ',ServoData.act_position.theta);
   writeln(outfile,'      end_position.x     ',ServoData.end_position.x);
   writeln(outfile,'      end_position.y     ',ServoData.end_position.y);
   writeln(outfile,'      end_position.z     ',ServoData.end_position.z);
   writeln(outfile,'      end_position.theta ',ServoData.end_position.theta);
   writeln(outfile,'      distance.x         ',ServoData.distance.x);
   writeln(outfile,'      distance.y         ',ServoData.distance.y);
   writeln(outfile,'      distance.z         ',ServoData.distance.z);
   writeln(outfile,'      distance.theta     ',ServoData.distance.theta);
   writeln(outfile,'      velocity.x         ',ServoData.velocity.x);
   writeln(outfile,'      velocity.y         ',ServoData.velocity.y);
   writeln(outfile,'      velocity.z         ',ServoData.velocity.z);
   writeln(outfile,'      velocity.theta     ',ServoData.velocity.theta);
   writeln(outfile,'      anorad_c.x         ',ServoData.anorad_c.x);
   writeln(outfile,'      anorad_c.xa        ',ServoData.anorad_c.xa);
   writeln(outfile,'      anorad_c.y         ',ServoData.anorad_c.y);
   writeln(outfile,'      anorad_c.z         ',ServoData.anorad_c.z);
   writeln(outfile,'      anorad_c.theta     ',ServoData.anorad_c.theta);
   writeln(outfile,'      JS_velocity.x      ',ServoData.JS_velocity.x);
   writeln(outfile,'      JS_velocity.y      ',ServoData.JS_velocity.y);
   writeln(outfile,'      JS_velocity.z      ',ServoData.JS_velocity.z);
   writeln(outfile,'      JS_velocity.theta  ',ServoData.JS_velocity.theta);
   writeln(outfile,'      JS_c.x             ',ServoData.JS_c.x);
   writeln(outfile,'      JS_c.y             ',ServoData.JS_c.y);
   writeln(outfile,'      JS_c.z             ',ServoData.JS_c.z);
   writeln(outfile,'      JS_c.theta         ',ServoData.JS_c.theta);
   for i:=1 to 2 do
   begin
   for j:=1 to 4 do
   begin
   writeln(outfile,'  ');
   writeln(outfile,' Controller ',i,' Axis ',j);
   writeln(outfile,STWIData.Par.Modus[i,j],'  ',STWIData.Par.Ist[i,j],
              '  ',STWIData.Par.Soll[i,j]);
   writeln(outfile,STWIData.Par.AC[i,j],'  ',STWIData.Par.VL[i,j],
              '  ',STWIData.Par.VH[i,j]);
   writeln(outfile,STWIData.Par.Time[i,j],'  ',STWIData.Par.KD[i,j],
              '  ',STWIData.Par.KP[i,j],'  ',STWIData.Par.KI[i,j]);
   writeln(outfile,STWIData.Par.IL[i,j],'  ',STWIData.Par.F_VL[i,j],
              '  ',STWIData.Par.F_VH[i,j],'  ',STWIData.Par.L_VL[i,j],
              '  ',STWIData.Par.L_VH[i,j]);

   writeln(outfile,' ');

   end;
   end;
}

   closeoutfile(0,0);
end;



(****************************************************************)
procedure Write_LogFile_HHS;
var
   Date_str, Time_str  : string;

   begin
   filename := path+LOGDataFileName;
   openoutfile(0,0);

   writeln(outfile,'  ');
   get_Date_Time(Date_str, Time_str);
   writeln(outfile,' Bloc-Name   = ',BlocNameTmp);

   writeln(outfile,' *** Date ***  ',Date_str);
   writeln(outfile,' *** Time ***  ',Time_str);
   writeln(outfile,'  ');
   writeln(outfile,' temperature = ',temperature:8:2);
   writeln(outfile,'  ');
   writeln(outfile,'***** DefineData. **********');
   writeln(outfile,'      speedctrl   ',DefineData.speedctrl);
   writeln(outfile,'      autoreverse ',DefineData.autoreverse);
   writeln(outfile,'      zero        ',DefineData.zero);
   writeln(outfile,'      ninety      ',DefineData.ninety);
   writeln(outfile,'      delay_1     ',DefineData.delay_1);
   writeln(outfile,'      delay_2     ',DefineData.delay_2);
   writeln(outfile,'      delay_3     ',DefineData.delay_3);
   writeln(outfile,'      SaveStack   ',DefineData.SaveStack);
   writeln(outfile,'      Red_Factor  ',DefineData.Red_Factor);
   writeln(outfile,' ');

   writeln(outfile,'*** AnalysisData. **********');
   writeln(outfile,'      SpeedCorr    ',AnalysisData.SpeedCorr);
   Writeln(outfile,'      PtsPerRev    ',AnalysisData.PtsPerRev);
   writeln(outfile,'      NrOfRev      ',AnalysisData.NrOfRev);
   writeln(outfile,'      OrdOfFA      ',AnalysisData.OrdOfFA);
   writeln(outfile,'      Hall_Pr_Resp ',AnalysisData.Hall_Pr_Resp);
   writeln(outfile,'  ');

   writeln(outfile,'*** DACData. ***************');
   writeln(outfile,'      StartVoltage ',DACData.StartVoltage);
   writeln(outfile,'      EndVoltage   ',DACData.EndVoltage);
   writeln(outfile,'      Time         ',DACData.Time);
   writeln(outfile,'      Steps        ',DACData.Steps);
   writeln(outfile,'      MaxDiff      ',DACData.MaxDiff);
   writeln(outfile,'      MinDT        ',DACData.MinDT);
   writeln(outfile,'      Speed_const  ',DACData.Speed_const);
   writeln(outfile,'  ');

   writeln(outfile,'*** MBInc1Data. ************');
   writeln(outfile,'      delay_1      ',MBInc1Data.delay_1);
   writeln(outfile,'      delay_2      ',MBInc1Data.delay_2);
   writeln(outfile,'      delay_3      ',MBInc1Data.delay_3);
   writeln(outfile,'      delay_4      ',MBInc1Data.delay_4);
   writeln(outfile,'      delay_5      ',MBInc1Data.delay_5);
   writeln(outfile,'      Time_Base    ',MBInc1Data.Time_Base);
   writeln(outfile,'  ');

   writeln(outfile,'*** SMS1Data. **************');
   writeln(outfile,'      Steps        ',SMS1Data.Steps);
   writeln(outfile,'      TimeBase     ',SMS1Data.TimeBase);
   writeln(outfile,'      FrqDiv       ',SMS1Data.FrqDiv);
   writeln(outfile,'      PulseLen     ',SMS1Data.PulseLen);
   writeln(outfile,'      RampSteps    ',SMS1Data.RampSteps);
   writeln(outfile,'      RampDstps     ',SMS1Data.RampDstps);
   writeln(outfile,'  ');

   writeln(outfile,'*** DVM1Data. **************');
   writeln(outfile,'      Range        ',DVM1Data.Range);
   writeln(outfile,'      Aperture     ',DVM1Data.Aperture);
   writeln(outfile,'      Readings     ',DVM1Data.Readings);
   writeln(outfile,'  ');

(*   writeln(outfile,'*** ScaleData. *************');
   writeln(outfile,'      xminzero      ',ScaleData.xminzero);
   writeln(outfile,'      xmaxzero      ',ScaleData.xmaxzero);
   writeln(outfile,'      yinzero       ',ScaleData.yminzero);
   writeln(outfile,'      yaxzero       ',ScaleData.ymaxzero);
   writeln(outfile,'      xminninety    ',ScaleData.xminninety);
   writeln(outfile,'      xmaxninety    ',ScaleData.xmaxninety);
   writeln(outfile,'      yinninety     ',ScaleData.yminninety);
   writeln(outfile,'      yaxninety     ',ScaleData.ymaxninety);
   writeln(outfile,'      dispchannel0  ',ScaleData.dispchannel0);
   writeln(outfile,'      dispchannel90 ',ScaleData.dispchannel90);
   writeln(outfile,'      autoscale0    ',ScaleData.autoscale0);
   writeln(outfile,'      autoscale90   ',ScaleData.autoscale90);
   writeln(outfile,'      resfactor0    ',ScaleData.resfactor0);
   writeln(outfile,'      resfactor90   ',ScaleData.resfactor90);
   writeln(outfile,'  '); *)

   closeoutfile(0,0);

   end;

(*************** Analysis procedures ******************)
procedure Get_north_south_pole;

  CONST
       npoleID             = 1001;
       spoleID             = 1002;
       firstID             = npoleID;
       lastID              = spoleID;

 VAR NoSoWinLayOut                    : WinDesType;
     OldWin,NoSoWin                   : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent                          : WORD;

 BEGIN

  OldWin:=WinActCtrl;

  NoSoWinLayOut := WinDefDes;
  WinSetHeader(NoSoWinLayOut,LightGray,
               C_LightBlue+'North South Pole Effect');
  WinSetFillColor(NoSoWinLayOut, yellow);
  WinSetOpenMode(NoSoWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=(3*width) div 4; ypos:=50;
  WinHeight:= 6*height;
  NoSowin:=OpenWin(xpos,ypos, xpos+2*width,
                                  ypos+WinHeight,NoSoWinLayOut);

  ChangeWin(NoSoWin);
  SetMargin(5,LeftMargin);
  SetCursor(0,0);
  ShowLineObject('    North Pole (KGauss) = ',34, npoleID,
                     buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(npole:8:2);

  ShowLineObject('    South Pole (KGauss) = ',34, spoleID,
                        buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(spole:8:2);

  repeat
   locEvent:=WaitOfEvent(firstID,lastID);
   case locEvent of
      npoleID   : BEGIN
            SetCursor(26,0);
            npole:= Editreal(npole,-10000,10000,8,2);
            SetCursor(26,0);
            WRITELN(npole:8:2);
                  END;
      spoleID   : BEGIN
            SetCursor(26,1);
            spole:= Editreal(spole,-10000,10000,8,2);
            SetCursor(26,1);
            WRITELN(spole:8:2);
                  END;
   END;
  until locEvent=0;;
  ChangeWin(OldWin);
  CloseWin(NoSowin);

end;

(*******************************************************)
PROCEDURE SW2_FileIndex(var SW2_NUM:string);
var fs:text;
    SW2_Num_Value:Word;
    fname:string;
    error_code:integer;
begin
  fname:=Path_to_datafiles+'\mfm_sw2.num';
  assign(fs,fname);
  reset(fs);
  readln(fs,SW2_Num);
  close(fs);
  val(SW2_NUM,SW2_NUM_VALUE,error_code);
  SW2_Num_Value:=SW2_Num_Value+1;
  if SW2_NUM_Value>999 then SW2_Num_Value:=0;

  rewrite(fs);
  writeln(fs,SW2_Num_Value);
  close(fs);
end;


PROCEDURE StoreDataProc_SW2;
var
   i,iord,j  : Integer;
   radgra    : Real;
   SW2_NUM,Com_S   : String;
   fname:string;

PROCEDURE SW2_Lst;
var fl:text;
    Date_str, Time_str  : string;
begin
  get_Date_Time(Date_str, Time_str);
  fname:=Path_to_Datafiles + '\mfm_sw2.lst';
  assign(fl,fname);
  {$I-}
  reset(fl);
  close(fl);
  {$I+}
  if IORESULT<>0 then
  begin
    rewrite(fl);
    close(fl);
  end;
  {$I-}
  append(fl);
  writeln(fl,Comment,' ',FileKurz,'.dat    ',Date_str,'  ',Time_Str);
  close(fl);
  {I+}
end;
begin
(*  filename := path+CalDataFileName;*)
  SW2_FileIndex(SW2_NUM);
  Com_S:=Copy(COMMENT,1,2);
{  filename:=path + comment+'.dat';}
  filekurz:=Com_S+SW2_Num;
  filename:=path + filekurz;
  openoutfile(0,0);
(*  write(outfile,'#Magnet Name: ',Comment,'  ',BlocName ,':');*)
   { for j:= vier downto 1 do
    begin
      write(outfile,(FileNr-j+1):6);
    end;}
  for i:=1 to GridData.ianzz do
  begin
    write(outfile,xResdata^[i]:6:2);
    for j:=1 to vier do
    begin
      write(outfile,' ',yresdata_Byz^[j,1,i]:8:5,' ',yresdata_Byz^[j,2,i]:8:5);
    end;
    writeln(outfile);
  end;

  closeoutfile(0,0);
  SW2_Lst;
end;               (*ihhs = 6*)


PROCEDURE StoreDataProc_HHS(var ixx:integer);
var
   i,iord,j  : integer;
   radgra    : real;
begin
  (**** store configuration parameters ***)
  if (DefineData.SaveLogFile = 1) then Write_Logfile_HHS;

  (***** store calibrated data **)
    filename := path+CalDataFileName;
    openoutfile(0,0);

    writeln(outfile,ianzdata0[1]);
    for i := 1 to ianzdata0[1] do
        writeln(outfile,xdata0^[i],' ',ydata0^[1,i],' ',
                        yresdata0^[1,i]);

    writeln(outfile,ianzdata90[1]);
    for i := 1 to ianzdata90[1] do
        writeln(outfile,xdata90^[i],' ',ydata90^[1,i],' ',
                        yresdata90^[1,i]);
    closeoutfile(0,0);


  (***** store raw data **)
    filename := path+RawDataFileName;
    openoutfile(0,0);

    writeln(outfile,ianzdata0[1]);
    for i := 1 to ianzdata0[1] do
        writeln(outfile,xdata0^[i],' ',yrawdata0^[i],' ',ydata0^[2,i]);

    writeln(outfile,ianzdata90[1]);
    for i := 1 to ianzdata90[1] do
        writeln(outfile,xdata90^[i],' ',yrawdata90^[i],' ',ydata90^[2,i]);

    closeoutfile(0,0);


  (***** store results *****)
(*    get_north_south_pole; *)
    radgra:=180/pi;
    if definedata.zero = 0 then
    begin
      speed0:=9999;
      amp0[1]:=9999;
    end;
    if definedata.ninety = 0 then
    begin
      speed90:=9999;
      amp90[1]:=9999;
    end;

    filename := path+ResDataFileName;
    openoutfile(0,0);

    writeln(outfile,'**********************');
    writeln(outfile,'Bloc Name: ',BlocNameTmp);
    writeln(outfile,'**********************');
    writeln(outfile, '  ');
    writeln(outfile,'-------------------------');
    writeln(outfile,' M_z      = ',1000.0*(mz/abs(speed0)));
    writeln(outfile,' M_y (0)  = ',1000.0*(my_0/abs(speed0)));
    writeln(outfile,' M_y (90) = ',1000.0*(my_90/abs(speed90)));
    writeln(outfile,' M_x      = ',1000.0*(mx/abs(speed90)));
    writeln(outfile,'-------------------------');
    writeln(outfile,'  ');
    writeln(outfile,' north pole (KGauss) = ',npole);
    writeln(outfile,' south pole (KGauss) = ',spole);
    writeln(outfile,'  ');
    writeln(outfile,' temperature (deg. C): ',Temperature);
    writeln(outfile,'  ');
    writeln(outfile,'-------------------------');
    writeln(outfile,' zero degree position');
    writeln(outfile,'-------------------------');
    writeln(outfile,'  ');
    writeln(outfile,' averaged speed (Hz): ',speed0);
    writeln(outfile,'  ');
    writeln(outfile,'  normalized data_rms (0 degree) (0/00): ',
                       1000.0*(data0_rms/amp0[1]));
    writeln(outfile,' normalized speed_rms (0 degree) (0/00): ',
                      1000.0*(speed0_rms/speed0));
    writeln(outfile,'  ');
    iord:=AnalysisData.OrdOfFA;
    writeln(outfile,' order of Fourier analysis: ',iord);

    for i:=1 to iord do
    begin
      writeln(outfile,' ',i,' amplitude (0) (mV/Hz): ',
                            1000.0*(amp0[i]/speed0));
      writeln(outfile,' ',i,' phase (0) (rad): ',phi0[i]*radgra);
    end;

    writeln(outfile,'  ');
    writeln(outfile,'-------------------------');
    writeln(outfile,' ninety degree position');
    writeln(outfile,'-------------------------');
    writeln(outfile,'  ');
    writeln(outfile,' averaged speed (Hz): ',speed90);
    writeln(outfile,'  ');
    writeln(outfile,'  normalized data_rms (90 degree) (0/00): ',
                       1000.0*(data90_rms/amp90[1]));
    writeln(outfile,' normalized speed_rms (90 degree) (0/00): ',
                      1000.0*(speed90_rms/speed90));
    writeln(outfile,'  ');
    iord:=AnalysisData.OrdOfFA;
    writeln(outfile,' order of Fourier analysis: ',iord);

    for i:=1 to iord do
    begin
      writeln(outfile,' ',i,' amplitude (90) (mV/Hz): ',
                        1000.0*(amp90[i]/speed90));
      writeln(outfile,' ',i,' phase (90) (rad): ',phi90[i]*radgra);
    end;

    closeoutfile(0,0);

end;



PROCEDURE StoreDataProc;
var
   i,iord,j  : integer;
   radgra    : real;

begin

  (**** store configuration parameters ***)
  if (DefineData.SaveLogFile = 1) then
  begin
    if (ihhs = 1) then Write_Logfile_HHS;
    if (ihhs = 2) then Write_Logfile_GB;
  end;

  (***** store calibrated data **)
  if (ihhs = 1) then
  begin
    filename := path+CalDataFileName;
    openoutfile(0,0);

    writeln(outfile,ianzdata0[1]);
    for i := 1 to ianzdata0[1] do
        writeln(outfile,xdata0^[i],' ',ydata0^[1,i],' ',
                        yresdata0^[1,i]);

    writeln(outfile,ianzdata90[1]);
    for i := 1 to ianzdata90[1] do
        writeln(outfile,xdata90^[i],' ',ydata90^[1,i],' ',
                        yresdata90^[1,i]);
    closeoutfile(0,0);
  end;              (* ihhs = 1 *)


  (***** store raw data **)
  if (ihhs = 1) then
  begin
    filename := path+RawDataFileName;
    openoutfile(0,0);

    writeln(outfile,ianzdata0[1]);
    for i := 1 to ianzdata0[1] do
        writeln(outfile,xdata0^[i],' ',yrawdata0^[i],' ',ydata0^[2,i]);

    writeln(outfile,ianzdata90[1]);
    for i := 1 to ianzdata90[1] do
        writeln(outfile,xdata90^[i],' ',yrawdata90^[i],' ',ydata90^[2,i]);

    closeoutfile(0,0);

  end;              (* ihhs = 1 *)

  if (ihhs = 2) then
  begin
    filename := path+RawDataFileName;
    openoutfile(0,0);

    for i := 1 to ianzdata0[1] do
      if( abs(xdata0^[1]-xdata0^[ianzdata0[1]]) > 1.0) then
            writeln(outfile,xdata0^[i],' ',ydata0^[1,i])
      else
            writeln(outfile,i,' ',ydata0^[1,i]);

    closeoutfile(0,0);

  end;              (* ihhs = 2 *)

  (***** store results *****)
  if (ihhs = 1) then
  begin
(*    get_north_south_pole; *)
    radgra:=180/pi;
    if definedata.zero = 0 then
    begin
      speed0:=9999;
      amp0[1]:=9999;
    end;
    if definedata.ninety = 0 then
    begin
      speed90:=9999;
      amp90[1]:=9999;
    end;

    filename := path+ResDataFileName;
    openoutfile(0,0);

    writeln(outfile,'**********************');
    writeln(outfile,'Bloc Name: ',blocname);
    writeln(outfile,'**********************');
    writeln(outfile, '  ');
    writeln(outfile,'-------------------------');
    writeln(outfile,' M_z      = ',1000.0*(mz/abs(speed0)));
    writeln(outfile,' M_y (0)  = ',1000.0*(my_0/abs(speed0)));
    writeln(outfile,' M_y (90) = ',1000.0*(my_90/abs(speed90)));
    writeln(outfile,' M_x      = ',1000.0*(mx/abs(speed90)));
    writeln(outfile,'-------------------------');
    writeln(outfile,'  ');
    writeln(outfile,' north pole (KGauss) = ',npole);
    writeln(outfile,' south pole (KGauss) = ',spole);
    writeln(outfile,'  ');
    writeln(outfile,' temperature (deg. C): ',Temperature);
    writeln(outfile,'  ');
    writeln(outfile,'-------------------------');
    writeln(outfile,' zero degree position');
    writeln(outfile,'-------------------------');
    writeln(outfile,'  ');
    writeln(outfile,' averaged speed (Hz): ',speed0);
    writeln(outfile,'  ');
    writeln(outfile,'  normalized data_rms (0 degree) (0/00): ',
                       1000.0*(data0_rms/amp0[1]));
    writeln(outfile,' normalized speed_rms (0 degree) (0/00): ',
                      1000.0*(speed0_rms/speed0));
    writeln(outfile,'  ');
    iord:=AnalysisData.OrdOfFA;
    writeln(outfile,' order of Fourier analysis: ',iord);

    for i:=1 to iord do
    begin
      writeln(outfile,' ',i,' amplitude (0) (mV/Hz): ',
                            1000.0*(amp0[i]/speed0));
      writeln(outfile,' ',i,' phase (0) (rad): ',phi0[i]*radgra);
    end;

    writeln(outfile,'  ');
    writeln(outfile,'-------------------------');
    writeln(outfile,' ninety degree position');
    writeln(outfile,'-------------------------');
    writeln(outfile,'  ');
    writeln(outfile,' averaged speed (Hz): ',speed90);
    writeln(outfile,'  ');
    writeln(outfile,'  normalized data_rms (90 degree) (0/00): ',
                       1000.0*(data90_rms/amp90[1]));
    writeln(outfile,' normalized speed_rms (90 degree) (0/00): ',
                      1000.0*(speed90_rms/speed90));
    writeln(outfile,'  ');
    iord:=AnalysisData.OrdOfFA;
    writeln(outfile,' order of Fourier analysis: ',iord);

    for i:=1 to iord do
    begin
      writeln(outfile,' ',i,' amplitude (90) (mV/Hz): ',
                        1000.0*(amp90[i]/speed90));
      writeln(outfile,' ',i,' phase (90) (rad): ',phi90[i]*radgra);
    end;

    closeoutfile(0,0);

  end;              (* ihhs = 1 *)

  (***** store data **)
  if ihhs = 3 then
  begin

    filename := path+RawDataFileName;
    write(filename);
    openoutfile(0,0);
    for i := 1 to ianzdata90[1] do
        writeln(outfile,xdata90^[i],' ',ydata90^[1,i]);
    closeoutfile(0,0);
  end;

  if (ihhs = 4) or (ihhs = 6) then
  begin
  (***** store data left window **)
    filename := path+RawDataFileName;
    openoutfile(0,0);
    for i := 1 to ianzdata0[1] do
        writeln(outfile,xdata0^[i],' ',ydata0^[1,i]);
    closeoutfile(0,0);

  (***** store data right window **)
    filename := path+CalDataFileName;
    openoutfile(0,0);
    for i := 1 to ianzdata90[1] do
        writeln(outfile,xdata90^[i],' ',ydata90^[1,i]);
    closeoutfile(0,0);

  end;

end;

(*******************************************************)
procedure LoadDataProc;
var
   i,ii,iord    : integer;

    begin
    filename := CalDataFileName;

    openoutfile(1,0);

    if(fehler = 0)then
    begin

    readln(outfile,ianzdata0[1]);
    for i := 1 to ianzdata0[1] do
        begin
        readln(outfile,xdata0^[i],yrawdata0^[i]);
        readln(outfile,ydata0^[1,i],yresdata0^[1,i]);
        end;

    readln(outfile,ianzdata0[2]);
    for i := 1 to ianzdata0[2] do
        readln(outfile,xdata0^[i],ydata0^[2,i],yresdata0^[2,i]);

    readln(outfile,ianzdata90[1]);
    for i := 1 to ianzdata90[1] do
        begin
        readln(outfile,xdata90^[i],yrawdata90^[i]);
        readln(outfile,ydata90^[1,i],yresdata90^[1,i]);
        end;

    readln(outfile,ianzdata90[2]);
    for i := 1 to ianzdata90[2] do
        readln(outfile,xdata90^[i],ydata90^[2,i],yresdata90^[2,i]);

    closeoutfile(0,0);

    filename := ResDataFileName;
    openoutfile(1,0);
    iord:=AnalysisData.OrdOfFA;

    readln(outfile,iord);
    for i:=1 to iord do
        readln(outfile,amp0[i],phi0[i]);
    readln(outfile,speed0);
    readln(outfile,data0_rms,speed0_rms);

    readln(outfile,iord);
    for i:=1 to iord do
        readln(outfile,amp90[i],phi90[i]);
    readln(outfile,speed90);
    readln(outfile,data90_rms,speed90_rms);

    closeoutfile(0,0);


    if analysisdata.speedcorr = 1 then
        SpeedCorrection;

    fa;             (* Fourier Analysis*)

    replotdata(3);

    end;

    end;

(******************************************************)
procedure SaveRawData;
var
   ii : integer;
begin
  for ii :=1 to ianzdata0[1] do
  begin
    yrawdata0^[ii]:=ydata0^[1,ii];
  end;
  for ii :=1 to ianzdata90[1] do
  begin
    yrawdata90^[ii]:=ydata90^[1,ii];
  end;
end;

(****************************************************************)
procedure RestoreRawData;
var
   ii : integer;
begin
  for ii :=1 to ianzdata0[1] do
  begin
    ydata0^[1,ii]:=yrawdata0^[ii];
  end;
  for ii :=1 to ianzdata90[1] do
  begin
  ydata90^[1,ii]:=yrawdata90^[ii];
  end;
end;

(****************************************************************)
procedure fa;
var
(*   pi   : real;                *)
   npts,npts1: integer;
   ampc           : array[1..64] of real;
   amps           : array[1..64] of real;
   i,j,k,iord     : integer;
   xk             : real;
   xi,xiperiod,xnperiod : real;
   OldWin         : WinCtrlPtrType;
   Delta_phi0     : real;
   Delta_phi90    : real;
   xsign          : real;
begin
      OldWin:=WinActCtrl;

(*      pi:=4.*arctan(1.0);  *)

      xiperiod:=analysisData.PtsPerRev;
      xnperiod:=analysisData.NrOfRev;
      iord    :=analysisData.OrdOfFA;

      if dacdata.endvoltage < 0 then
          xsign := -1.0
          else
          xsign:=1.0;

      npts1:=round(xnperiod*xiperiod);

      if DefineData.zero = 1 then
      begin
(*********** zero degree data *)
      changewin(zeroanalysiswin);

      for i:=1 to iord do
      begin
       ampc[i]:=0.;
       amps[i]:=0.;
       xi:=i;
       xk:=(2.*pi*xi)/xiperiod;


       for j:=1 to npts1 do
       begin
        ampc[i]:=ampc[i] + cos(xsign*xk*realval(j))*ydata0^[1,j];
        amps[i]:=amps[i] + sin(xsign*xk*realval(j))*ydata0^[1,j];
       end;

      ampc[i]:=(2.*ampc[i])/(xnperiod*xiperiod);
      amps[i]:=(2.*amps[i])/(xnperiod*xiperiod);

      phi0[i]:=arctan(-amps[i]/ampc[i]);

      amp0[i]:=ampc[i]/cos(phi0[i]);
      end;

      for j:=1 to ianzdata0[1] do
      begin
        yresdata0^[1,j]:=ydata0^[1,j];
        for i:=1 to iord do
        begin
            xi:=i;
            xk:=(2.*pi*xi)/xiperiod;
            yresdata0^[1,j]:=yresdata0^[1,j] -
                      amp0[i]*cos(xsign*xk*realval(j) + phi0[i]);
        end;
      end;

      end;

      if DefineData.ninety = 1 then
      begin
(*********** ninety degree data *)
      changewin(ninetyanalysiswin);

      for i:=1 to iord do
      begin
       ampc[i]:=0.;
       amps[i]:=0.;
       xi:=i;
       xk:=(2.*pi*xi)/xiperiod ;

       for j:=1 to npts1 do
       begin
        ampc[i]:=ampc[i]+cos(xsign*xk*realval(j))*ydata90^[1,j];
        amps[i]:=amps[i]+sin(xsign*xk*realval(j))*ydata90^[1,j];
       end;

      ampc[i]:=(2.*ampc[i])/(xnperiod*xiperiod);
      amps[i]:=(2.*amps[i])/(xnperiod*xiperiod);
      phi90[i]:=arctan(-amps[i]/ampc[i]);
      amp90[i]:=ampc[i]/cos(phi90[i]);
      writeln(i,' ',amp90[i]:8:5,phi90[i]*(180/pi):8:2);
      end;

      for j:=1 to ianzdata90[1] do
      begin
        yresdata90^[1,j]:=ydata90^[1,j];
        for i:=1 to iord do
        begin
            xi:=i;
            xk:=(2.*pi*xi)/xiperiod;
            yresdata90^[1,j]:=yresdata90^[1,j]-
                    amp90[i]*cos(xsign*xk*realval(j) + phi90[i]);
        end;
      end;

      end;


(********** correction for finite measurement time ********)

if dacdata.endvoltage < 0 then
   xsign:=-1.0
   else
   xsign:=1.0;

Delta_phi0:=0.5*(DVM1Data.Aperture/1000.0)*speed0*2.0*pi;
Delta_phi90:=0.5*(DVM1Data.Aperture/1000.0)*speed90*2.0*pi;

for i := 1 to iord do
    begin
    phi0[i]:=phi0[i]-realval(i)*Delta_phi0;
    phi90[i]:=phi90[i]-realval(i)*Delta_phi90;
    end;

       changewin(ninetyanalysiswin);

(********* amplitudes and phases shall be positive ********)
for i := 1 to iord do
    begin
    if amp0[i] < 0.0 then
       begin
       amp0[i]:=-amp0[i];
       phi0[i]:=phi0[i]+pi;
       end;
(*    if phi0[i] < 0 then
       phi0[i]:=phi0[i]+2.0*pi;  *)
    if phi0[i] > 2.0*pi then
       phi0[i]:=phi0[i]-2.0*pi;
    end;

for i := 1 to iord do
    begin
    if amp90[i] < 0.0 then
       begin
       amp90[i]:=-amp90[i];
       phi90[i]:=phi90[i]+pi;
       end;
(*    if phi90[i] < 0 then
       phi90[i]:=phi90[i]+2.0*pi; *)
    if phi90[i] > 2.0*pi then
       phi90[i]:=phi90[i]-2.0*pi;
    end;

      changewin(oldwin);

      mz:=amp0[1]*cos(phi0[1]);
      my_0:=amp0[1]*sin(phi0[1]);
(* Vorzeichen von Mx geaendert 18.2.1997
   bei der Vermessung der U49 Magnete stand hier ein (+)
   und das Vorzeichen wurde in COMPARE.FOR korrigiert
     mx:=+amp90[1]*cos(phi90[1]); *)
      mx:=-amp90[1]*cos(phi90[1]);
      my_90:=amp90[1]*sin(phi90[1]);

end;

(******************************************************)
procedure AnalysisProc;
var
   a1_loc, a2_loc, a3_loc, a4_loc : real;
   ii      : integer;
   OldWin         : WinCtrlPtrType;
   aa       ,bb      : real;
   f:text;
begin

  OldWin:=WinActCtrl;

  a1_loc:=0.0;
  a2_loc:=0.0;
  a3_loc:=0.0;
  a4_loc:=0.0;
{TEST}  assign(f,'test_ank.txt');
{TEST}  rewrite(f);
  if definedata.zero = 1 then
  begin
    for ii:=1 to ianzdata0[1] do
    begin
{Test}      writeln(f,ianzdata0[1],' ',ii,' ',a1_loc,' ',ydata0^[1,ii],' ',yresdata0^[1,ii]);
      a1_loc:=a1_loc+yresdata0^[1,ii]*yresdata0^[1,ii];
    end;
{TEST}  close(f);
    for ii:=1 to ianzdata0[2] do
    begin
      a2_loc:=a2_loc+yresdata0^[2,ii]*yresdata0^[2,ii];
    end;
    if ianzdata0[1] > 1 then
    begin
      data0_rms:=sqrt(a1_loc)/sqrt(realval(ianzdata0[1]-1));
      speed0_rms:=sqrt(a2_loc)/sqrt(realval(ianzdata0[2]-1));
    end;
  end;

  if ianzdata90[1] > 1 then
  begin
    if definedata.ninety = 1 then
    begin
      for ii:=1 to ianzdata90[1] do
      begin
        a3_loc:=a3_loc+yresdata90^[1,ii]*yresdata90^[1,ii];
      end;
    end;

    for ii:=1 to ianzdata90[2] do
    begin
      a4_loc:=a4_loc+yresdata90^[2,ii]*yresdata90^[2,ii];
    end;
    data90_rms:=sqrt(a3_loc)/sqrt(realval(ianzdata90[1]-1));
    speed90_rms:=sqrt(a4_loc)/sqrt(realval(ianzdata90[2]-1));
  end;

end;

(******************************************************)
procedure SpeedCorrection;
var
   ii         : integer;
   yres_local : real;
   Delta_T1   : real;
   Delta_T2   : real;
   factor     : real;
begin
Delta_T1:=dvm1data.aperture/1000.0;
Delta_T2:=1.0/(AnalysisData.PtsPerRev*speed0);
factor:=Delta_T1/(2.0*Delta_T2);

if definedata.zero = 1 then
begin
ydata0^[1,ianzdata0[1]] :=yrawdata0^[ianzdata0[1]] /
      (1.0 + yresdata0^[2,ianzdata0[1]]/speed0);
for ii :=1 to ianzdata0[1]-1 do
begin
yres_local:=yresdata0^[2,ii]*(1.0-factor) + yresdata0^[2,ii+1]*factor;
ydata0^[1,ii] :=yrawdata0^[ii] / (1.0 + yres_local/speed0);
end;
end;

if definedata.ninety = 1 then
begin
ydata90^[1,ianzdata90[1]] :=yrawdata90^[ianzdata90[1]] /
      (1.0 + yresdata90^[2,ianzdata90[1]]/speed90);
for ii :=1 to ianzdata90[1]-1 do
begin
yres_local:=yresdata90^[2,ii]*(1.0-factor) + yresdata90^[2,ii+1]*factor;
ydata90^[1,ii] :=yrawdata90^[ii] / (1.0 + yres_local/speed90);
end;
end;

end;

(*****************************************************)
procedure AnalyseProc;
          begin

   ChangeWin(MainScreen);

          end;


(*****************************************************)
 PROCEDURE SaveStack_LAS(var fname: String);
   VAR AktDataPtr_Las, NextDataPtr_Las     : PointPtrType_Las;
       f,f2                        : TEXT;
       fErr                        : WORD;
       Ppos,fnumber                : INTEGER;
   BEGIN
    IF Top_Las<>NIL {Sind daten vorhanden?}
     THEN
      BEGIN
       if (DefineData.SaveStack = 1) and (ihhs = 2) then
          begin
          ASSIGN(f, path+fname);
          {$I-}
          REWRITE(f);
          fErr := IOResult;
          end;

         {store data}
         AktDataPtr_Las := Top_Las;   {start at list head}

          REPEAT
           WITH AktDataPtr_Las^ DO
            BEGIN
             if (DefineData.SaveStack = 1) then
                begin
                (*if abs(Laser.Ch1) > 1.e-9 then*)
                    if (ihhs = 2) then
                       WRITELN(f , Temp:12:8,' ',
                       Laser.Ch1:12:8,' ',
                       Laser.CH2:12:8,' ',
                       Laser.Ch3:12:8,' ',
                       Laser.CH4:12:8);
                end;
             NextDataPtr_Las := next;
            END;
           DISPOSE(AktDataPtr_Las);               {clean up stack}
           AktDataPtr_Las := NextDataPtr_Las;
          UNTIL (AktDataPtr_Las=NIL);      {until last data point}
       if (DefineData.SaveStack = 1) and (ihhs = 2) then
              begin
              CLOSE(f);
              fErr:=IOResult;
              end;
       Top_Las := NIL;

      END;
   END;

(*****************************************************)
 PROCEDURE SaveStack_STWI(var fname: String);
   VAR AktDataPtr_STWI, NextDataPtr_STWI  : PointPtrType_STWI;
       f,f2                        : TEXT;
       fErr                        : WORD;
       Ppos,fnumber                : INTEGER;
   BEGIN
    IF Top_STWI<>NIL {Sind daten vorhanden?}
     THEN
      BEGIN
       if (DefineData.SaveStack = 1) then
          begin
          ASSIGN(f, path+fname);
          {$I-}
          REWRITE(f);
          fErr := IOResult;
          end;

         WRITELN(f ,ibybz);

         {store data}
         AktDataPtr_STWI := Top_STWI;   {start at list head}

          REPEAT
           WITH AktDataPtr_STWI^ DO
            BEGIN
             if (DefineData.SaveStack = 1) then
                begin
                       WRITELN(f ,
                       zory:12:8,' ',
                       yint1,' ',
                       yint2);
                end;
             NextDataPtr_STWI := next;
            END;
           DISPOSE(AktDataPtr_STWI);               {clean up stack}
           AktDataPtr_STWI := NextDataPtr_STWI;
          UNTIL (AktDataPtr_STWI=NIL);      {until last data point}
       if (DefineData.SaveStack = 1) then
              begin
              CLOSE(f);
              fErr:=IOResult;
              end;
       Top_STWI := NIL;

      END;
   END;

(*****************************************************)
 PROCEDURE SaveStack_DVM(var fname: String);
   VAR AktDataPtr_DVM, NextDataPtr_DVM, LastDataPtr: PointPtrType_DVM;
       f,f2                        : TEXT;
       fErr,f2Err                  : WORD;
       Ppos,fnumber                : INTEGER;
       b_old,b_new,b2_old,b2_new   : real;
       a,bb,bbb,bbb2,c,dx          : real;
       ii                          : integer;

   BEGIN
    IF END_DVM<>NIL {Sind Daten vorhanden?}
     THEN
      BEGIN
       if (DefineData.SaveStack = 1) then
          begin
          ASSIGN(f2, path+LasDataFilename);
          ASSIGN(f, path+fname);
          {$I-}
          RESET(f2);
          f2Err := IOResult;
          REWRITE(f);
          fErr := IOResult;
          end;

         {store data}
         AktDataPtr_DVM := END_DVM;   {start at list end}

          READLN(f2,a,b_old,b2_old);
          bbb:=b_old;
          bbb2:=b2_old;

          ii:=1;

          REPEAT
           WITH AktDataPtr_DVM^ DO
            BEGIN
             if (DefineData.SaveStack = 1) then
                begin
                       ii:=ii+1;
                       if (ii <=DVM1Data.Readings) then
                          begin
                          readln(f2,a,b_new,b2_new);
                          c:=(b_new-b_old)/trigger_rate_r;
                          dx:=-AnalysisData.Hall_Pr_Resp*0.001*c;
                          if AnalysisData.SpeedCorr = 1 then
                             dx:=dx+0.5e-3*dvm1data.aperture*c;
                          bb:=b_old+dx;
                          b_old:=b_new;
                          end;
                       if (ii > DVM1Data.Readings) then
                          (* same speed as before *)
                          begin
                          dx:=-AnalysisData.Hall_Pr_Resp*0.001*c;
                          if AnalysisData.SpeedCorr = 1 then
                             dx:=dx+0.5e-3*dvm1data.aperture*c;
                          bb:=b_old+dx;
                          end;

  if abs(DefineData.speed_x) > 0.00001 then
                       begin
                       WRITELN(f,bb:12:8,
                       DVM.Ch1:12:8,DVM.CH2:12:8);
       (* spaetere Erweiterung auf 3D Hall-Probe *)
       (*                DVM.CH3:12:8);          *)
                       end;

  if abs(DefineData.speed_x) < 0.00001 then
                       begin
                       WRITELN(f,bbb:12:8,'  ',bbb2:12:8,'  ',
                       DVM.Ch1:12:8,DVM.CH2:12:8);
       (* spaetere Erweiterung auf 3D Hall-Probe *)
       (*                DVM.CH3:12:8);          *)
                       bbb:=b_new;
                       bbb2:=b2_new;
                       end;

                       NextDataPtr_DVM := last;
                end;
             DISPOSE(AktDataPtr_DVM);               {clean up stack}
             AktDataPtr_DVM := NextDataPtr_DVM;
            END;
          UNTIL (AktDataPtr_DVM=NIL);      {until last data point}

       if (DefineData.SaveStack = 1) then
              begin
              CLOSE(f);
              fErr:=IOResult;
              CLOSE(f2);
              f2Err:=IOResult;
              end;
       Top_DVM := NIL;
       End_DVM := NIL;

      END;
   END;
PROCEDURE WarningText(wtxt:string);
begin
  changewin(warningswin);
  writeln(wtxt);
  ChangeWin(MainScreen);
end;
END.
