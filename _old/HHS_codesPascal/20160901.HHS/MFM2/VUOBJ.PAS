{ -------------------------------------------------------------------------- }
{                       VUOBJ Library                                        }
{                       -------------                                        }
{  Autor    : Michael Pietsch                                                }
{  Date     : 03.07.94                                                       }
{  Update   : 09.02.95(mp) Fensterrahmen nicht mehr lokal                    }
{                                                                            }
{  Version  : 1.1                                                            }
{  Unitname : VUOBJ.PAS                                                      }
{  Units    : CRT,MathLib,Graph,StdLib,MouseLib,ScaleLib,VesaLib,WinLib      }
{                                                                            }
{ -------------------------------------------------------------------------- }


UNIT VUOBJ;

INTERFACE

USES CRT,MathLib,Graph,StdLib,MouseLib,ScaleLib,VesaLib,WinLib;

CONST
  LimitMin   = $100;
  LimitMax   = $400;
  BarTyp     = $2000;
  PeakTyp    = $4000;
  ButtonArea = $8000;
  minYsize   : WORD = 48;

  OverFlow   = 1;
  UnderFlow  = 2;

TYPE
  MeterBase = OBJECT
     winCtrl    : WinCtrlPtrType;
     b_Height   : WORD;
     ySize      : WORD;
     bColor     : WORD;
     xLast      : WORD;
     style      : WORD;
     newFlag    : BOOLEAN;
     upDate     : BOOLEAN;
     error      : WORD;

     PROCEDURE Init    (title       : String79;
                        colorH      : WORD;
                        colorB      : WORD;
                        styleT      : WORD;
                        x1,y1       : WORD;
                        sizeX,sizeY : WORD);
     PROCEDURE Buttons (buttonStr   : STRING;
                        mouseId     : WORD;
                        eventProc   : eventProcType);
     PROCEDURE Done;
  END;

  Bar_Meter = OBJECT(MeterBase)
     scaleRec   : ScaleXYRecType;
     PROCEDURE Init    (title       : String79;
                        colorH      : WORD;
                        colorB      : WORD;
                        styleT      : WORD;
                        x1,y1       : WORD;
                        sizeX,sizeY : WORD);
     PROCEDURE ShowAxis(min, max    : DOUBLE;
                        title       : String20);
     PROCEDURE Show    (value       : DOUBLE;
                        color       : WORD);
  END;

  Limit_Meter = OBJECT(Bar_Meter)
     vMin  : DOUBLE;
     vMax  : DOUBLE;
     xMin  : WORD;
     xMax  : WORD;
     PROCEDURE Init     (title       : String79;
                         colorH      : WORD;
                         colorB      : WORD;
                         styleT      : WORD;
                         x1,y1       : WORD;
                         sizeX,sizeY : WORD);
     PROCEDURE DrawLimit(     value  : DOUBLE;
                          VAR vOld   : DOUBLE;
                          VAR xO     : WORD;
                              color  : WORD);
     PROCEDURE Show     (value       : DOUBLE;
                         color       : WORD);
     PROCEDURE LimitClear(typ : WORD);
  END;

  Dig_Meter = OBJECT(MeterBase)
     width   : WORD;
     decimal : WORD;
     size    : WORD;
     vLast   : DOUBLE;
     lStr    : STRING[16];
     xP,yP   : WORD;
     PROCEDURE Init       (title       : String79;
                           colorH      : WORD;
                           colorB      : WORD;
                           styleT      : WORD;
                           x1,y1       : WORD;
                           sizeX,sizeY : WORD);
     PROCEDURE ShowDigits(stellen,nachkomma : WORD);
     PROCEDURE Show      (value       : DOUBLE;
                          color       : WORD);{VIRTUAL;}
     PROCEDURE Refresh;
 END;


IMPLEMENTATION

{ *********************** }
{ *** Basisdefinition *** }
{ *********************** }

PROCEDURE MeterBase.Init ( title       : String79;
                           colorH      : WORD;
                           colorB      : WORD;
                           styleT      : WORD;
                           x1,y1       : WORD;
                           sizeX,sizeY : WORD);

VAR
  oldWinId  : WinCtrlPtrType;
  locWinDes : WinDesType;
  width     : WORD;

BEGIN
  oldWinId  := WinActCtrl;                       {aktuelle Fenster merken}
  bColor    := colorB;                           {Farbe f�r Buttonliste merken}
  ySize     := sizeY;                            {Y - Fenstergr��e}
  style     := styleT;
  locWinDes := WinDefDes;                        {lokale Variable und Frame mit}

  WITH locWinDes DO                              {aktuelle Buttondimensionen}
  BEGIN
    Font     := DefaultFont;                     {8x8 Font setzen}
    GetButtonDim  (Font,FontSize,5,width,b_height);  {abfragen}
    b_Height := b_Height + 2;
  END;

  IF (style AND ButtonArea > 0)                  {mit Buttonliste}
  THEN ySize    := ySize + b_Height              {Fensterl�nge erweitern}
  ELSE b_Height := 0;

  WinSetHeader  (locWinDes,colorH,title);        {�berschrift setzen}
  winCtrl   := OpenWin (x1 OR PosRel,y1 OR PosRel,sizeX,ySize,locWinDes);
  upDate    := FALSE;                            {derzeit Wert nicht neu}
  newFlag   := TRUE;                             {neuerzeugt}
  error     := 0;                                {derzeit kein Fehler}
  ChangeWin (oldWinId);                          {alte Fenster aktivieren}
END;


PROCEDURE MeterBase.Done;
BEGIN
  CloseWin(winCtrl);
END;


PROCEDURE MeterBase.Buttons ( buttonStr   : STRING;
                              mouseId     : WORD;
                              eventProc   : eventProcType);
VAR
  oldWinId : WinCtrlPtrType;

BEGIN
  IF b_height = 0 THEN EXIT;
  oldWinId  := WinActCtrl;                       {aktuelle Fenster merken}
  ChangeWin   (winCtrl);                         {Fenster aktivieren}
  SetViewPort (0,0,GetMaxX,GetMaxY,TRUE);        {ViewPort zur�cksetzen}
  SetFillStyle(XHatchFill,bColor);               {F�llmuster setzen}

  HideMouse;                                     {Maus abschalten}

  WITH winCtrl^.ViewInfo DO                      {Viewport f�r Buttonbereich}
  BEGIN                                          {setzen und diesen Bereich}
    SetViewPort (x1,y1,x2,y2,TRUE);              {mit F�llmuster f�llen}
    Bar         (0,ySize - b_height,x2-x1,ySize);
  END;

  MoveTo      (0,ySize - b_height + 2);             {an Position Buttons ausgeben}
  ListButtons (buttonStr,mouseId,PosRel OR HorizDir OR ObjButtonType OR
               WinBackFill OR ObjActiv OR Global,eventProc);
  ShowMouse;                                     {Maus einschalten}
  WITH WinCtrl^.ViewInfo                         {alten ViewPort setzen}
    DO SetViewPort (x1,y1,x2,y2,Clip);
  ChangeWin   (oldWinId);                        {alte Fenster aktivieren}
END;


{ ************************ }
{ *** BAR - Definition *** }
{ ************************ }


PROCEDURE Bar_Meter.Init ( title       : String79;
                           colorH      : WORD;
                           colorB      : WORD;
                           styleT      : WORD;
                           x1,y1       : WORD;
                           sizeX,sizeY : WORD);
BEGIN
  IF sizeX < 160      THEN sizeX := 160;         {mindestens 160 Punkte f�r X}
  IF sizeY < minYSize THEN sizeY := MinYsize;    {mindestens 68 Punkte f�r Y}
  MeterBase.Init(title,colorH,colorB,styleT,x1,y1,sizeX,sizeY);
END;


PROCEDURE Bar_Meter.ShowAxis (min, max : DOUBLE; title : String20);

VAR
  oldWinId : WinCtrlPtrType;
  oldView  : ViewPortType;
  oldAC    : WORD;
  oldTC    : WORD;

BEGIN
  oldWinId := WinActCtrl;
  ChangeWin  (winCtrl);                          {Fenster aktivieren}
  oldView  := winCtrl^.ViewInfo;                 {Viewport merken}
  SetViewPort(0,0,GetMaxX,GetMaxY,TRUE);         {ViewPort zur�cksetzen}
  HideMouse;

  WITH oldView                                   {ViewPort setzen und}
  DO BEGIN                                       {Fensterbereich l�schen}
       SetViewPort(x1,y1,x2,y1 + ySize - b_height,TRUE);
       DelArea    (0,0,x2 - x1,y2 - y1);
     END;
                                                 {Achse zeichnen}

  oldAC     := AxisColor;                        {Achsenfarbe merken}
  oldTC     := TextColor;                        {Textfarbe merken}
  AxisColor := Black;                            {Achsenfarbe und}
  TextColor := Black;                            {Textfarbe setzen}

  Axis (min,max,title,0,0,'',scaleRec,AxisX);

  AxisColor := oldAC;                            {alte Achsenfarbe und}
  TextColor := oldTC;                            {Textfarbe setzen}

  WITH scaleRec                                  {Nullposition bestimmen}
  DO BEGIN
       zero.X := null.X + ScalePos(0,max.x - null.x,ScaleX);
       Rectangle(null.x-1,null.y+1,max.x+1,max.y-1);
       xLast := zero.x;                          {letzte Zeichenposition}
     END;

  ShowMouse;

  WITH oldView                                   {alten ViewPort setzen}
  DO SetViewPort(x1,y1,x2,y2,Clip);

  ChangeWin (oldWinId);                          {alte Fenster aktivieren}
END;



PROCEDURE Bar_Meter.Show (value : DOUBLE; color : WORD);
VAR
  oldWinId : WinCtrlPtrType;
  fillInfo : FillSettingsType;
  x,y	   : WORD;
  zX,nX    : WORD;

BEGIN
  WITH scaleRec DO
  BEGIN
    x := null.x + ScalePos (value,max.x  - null.x,scaleX);

    upDate := (x <> xLast);
    IF NOT upDate THEN EXIT;                          {wenn neue= alte EXIT}

    oldWinId := WinActCtrl;
    ChangeWin(winCtrl);                               {Fenster aktivieren}

    WITH winCtrl^.ViewInfo                            {Maus innerhalb des}
    DO ConditionalOff(x1,y1,x2,y1+ySize-b_height);    {Fensters abschalten}

    IF (style AND BarTyp) > 0
    THEN BEGIN
           zX := zero.X;                              {Var. schneller als Rec.}

           IF ((xLast < zX) AND (x > zX)) OR          {wenn Nulldurchgang}
              ((xLast > zX) AND (x < zX))             {dann bis Null l�schen}
           THEN BEGIN
                  SetFillStyle(SolidFill,winCtrl^.WinBackColor);
                  BAR(xLast,null.y,zx,max.y);
                  xLast := zx;
                END;

           IF ((x < xLast) AND (x >= zx)) OR          {wenn neuer Wert kleiner}
              ((x > xLast) AND (x <= zx))             {als der alte Wert}
           THEN BEGIN                                 {dann l�schen}
                  SetFillStyle(SolidFill,winCtrl^.WinBackColor);
                  nx := x;
                  IF (((x > Null.X) AND (x < Max.X)) OR (x = zx))
                  THEN BEGIN
                         IF x > xLast
                         THEN nX := PRED(x)
                         ELSE nx := SUCC(x);
                       END;
                  BAR(nx,null.y,xLast,max.y)
                END
           ELSE BEGIN
                  SetFillStyle(SolidFill,color);      {sonst um neuen}
                  BAR(xLast,null.y,x,max.y);          {Wert erweitern}
                END;
         END;
    IF (style AND PeakTyp) > 0
    THEN BEGIN
           SetColor (winCtrl^.WinBackColor);
           Line (xLast,null.y,xLast,max.y);           {alte Line l�schen}

           SetColor (color);
           Line(x,null.y,x,max.y);                    {neue Line ausgeben}
         END;
  END; {END WITH}
  xLast   := x;                                       {letzten Wert merken}
  newFlag := FALSE;
  IF (MouseCount = 0)                                 {Maus einschalten ?}
  THEN ShowMouse;                                     {Maus einschalten}
{  ChangeWin (oldWinId); ??? ben�tigt Zeit und verhindert derzeit ObjMove }
END;


{ ************************** }
{ *** LIMIT - Definition *** }
{ ************************** }


PROCEDURE Limit_Meter.DrawLimit (    value : DOUBLE;
                                 VAR vOld  : DOUBLE;
                                 VAR xO    : WORD;
                                     color : WORD);
VAR
  oldWinId : WinCtrlPtrType;
  xN	   : WORD;
  oldMode  : WORD;

BEGIN
  vOld := value;

  WITH scaleRec DO
  BEGIN                                               {x-Position berechnen}
    xN := null.x + ScalePos (value,max.x  - null.x,scaleX);
    IF (xN = xO) THEN EXIT;                           {akt. = alte Position}

    oldWinId := WinActCtrl;                           {akt. Fenster merken}
    ChangeWin(winCtrl);                               {Fenster aktivieren}

    oldMode := GetWriteMode;                          {akt. Writemode merken}
    SetWriteMode (XorPut);                            {XOR-Mode setzen}

    WITH winCtrl^.ViewInfo                            {innerhalb des Fensters}
    DO ConditionalOff(x1,y1,x2,y1+ySize-b_height);    {Maus abschalten}

    SetColor (color XOR winCtrl^.WinBackColor);       {Farbe setzen}
    IF xo < $FFFF                                     {test ob alte Position}
    THEN BEGIN                                        {g�ltig ist; wenn ja}
           Line (xO-3,null.y+8,xO,null.y+3);          {dann Pfeil l�schen}
           Line (xO+1,null.y+4,xO+3,null.y+8);
           Line (xO-2,null.y+8,xO+2,null.y+8);
         END;
    Line (xN-3,null.y+8,xN,null.y+3);                 {Pfeil zeichnen}
    Line (xN+1,null.y+4,xN+3,null.y+8);
    Line (xN-2,null.y+8,xN+2,null.y+8);
  END; {END WITH}

  xO := xN;                                           {aktuelle Position merken}

  SetWriteMode (oldMode);                             {alten Writemode setzen}
  IF (MouseCount = 0)                                 {Maus einschalten ?}
  THEN ShowMouse;                                     {Maus einschalten}
END;


PROCEDURE Limit_Meter.Init ( title       : String79;
                             colorH      : WORD;
                             colorB      : WORD;
                             styleT      : WORD;
                             x1,y1       : WORD;
                             sizeX,sizeY : WORD);
BEGIN
  Bar_Meter.Init(title,colorH,colorB,styleT,x1,y1,sizeX,sizeY);
  vMin := 1.7E308;
  vMax := -1.0E308;
  xMin := $FFFF;
  xMax := $FFFF;
END;


PROCEDURE Limit_Meter.Show (value : DOUBLE; color : WORD);

BEGIN
  IF (value < vMin) AND ((style AND LimitMin) > 0)
  THEN DrawLimit (value,vMin,xMin,BLUE);

  IF (value > vMax) AND ((style AND LimitMax) > 0)
  THEN DrawLimit (value,vMax,xMax,RED);
  Bar_Meter.Show (value,color);
END;


PROCEDURE Limit_Meter.LimitClear (typ : WORD);
BEGIN
  xMin := $FFFF;
  xMax := $FFFF;

  IF (typ AND LimitMax) > 0 THEN DrawLimit (vMax,vMax,xMax,RED);
  IF (typ AND LimitMin) > 0 THEN DrawLimit (vMin,vMin,xMin,BLUE);

  xMin := $FFFF;
  xMax := $FFFF;

  vMin := 1.7E308;
  vMax := -1.0E308;
END;


PROCEDURE Dig_Meter.Init ( title       : String79;
                           colorH      : WORD;
                           colorB      : WORD;
                           styleT      : WORD;
                           x1,y1       : WORD;
                           sizeX,sizeY : WORD);
BEGIN
  IF sizeX < 40 THEN sizeX := 40;                {mindestens 40 Punkte f�r X}
  IF sizeY < 16  THEN sizeY := 16;               {mindestens 16 Punkte f�r Y}
  MeterBase.Init(title,colorH,colorB,styleT,x1,y1,sizeX,sizeY);
END;


PROCEDURE Dig_Meter.ShowDigits (stellen,nachkomma : WORD);

VAR
  xS,ys : WORD;

BEGIN
  width   := stellen;                            {Anzahl der Zeichen und}
  decimal := nachkomma;                          {der Nachkommastellen merken}

  IF (width < 3)  THEN width := 3;
  IF (width > 16) THEN width := 16;              {wenn zuviele Stellen oder}
  IF (decimal >= (width-2)) THEN decimal := width-2; {Nachkommastellen ?}

  ChangeWin (winCtrl);
  WITH winCtrl^.ViewInfo                         {aus dem ViewPort die X und}
  DO BEGIN                                       {Y- Dimensionen berechenen}
       xS := PRED(x2-x1);
       yS := PRED(y2-y1 - b_Height - 3);         {Buttonliste ber�cksichtigen}
       DelArea (0,0,x2,ys-2);
     END;

  size := xS DIV (8*width);                      {Vergr��erungsfaktor}

  WHILE (yS < (8*size)) AND (size > 0)           {teste ob Ausgabe auch in}
     DO DEC(size);                               {Y-Richtung passt sonst kleiner}

  IF (size = 0)                                  {wenn Vergr��erung = 0 dann}
  THEN BEGIN
         stellen := xs DIV 8;                    {Stellenanzahl verringern}
         size    := 1;                           {keine Vergr��erung}
       END;

  vLast := 1.7e308;                              {unm�glichen Startwert setzen}
  FillChar(lStr,16,' ');                         {Merkstring mit Blank f�llen}

  WITH winCtrl^.ViewInfo                         {aus dem ViewPort die Topposition}
  DO BEGIN                                       {f�r das erste Zeichen berechnen}
       xP := (x2 - x1 - (width * 8 * size)) DIV 2;
       yP := (y2 - y1 - b_Height - (8 * size)) DIV 2
     END;
END;


PROCEDURE Dig_Meter.Show (value : DOUBLE; color : WORD);

VAR
  aStr     : String79;
  i        : WORD;
  ts       : WORD;

BEGIN
  value := RoundReal (value,-decimal);           {Wert runden}
  upDate:= (value <> vLast);

  IF NOT upDate                                  {letzte gleich aktuelle Wert}
  THEN EXIT;                                     {wenn ja dann EXIT}
(*
  aStr := FormReal (value,width,decimal);       {Real -> String wandeln}
*)
  Str(value : width : decimal,aStr);             {Real -> String wandeln}

  IF (Length(aStr) > width)                      {wenn String l�nger WIDTH}
  THEN BEGIN
         IF (Length(aStr) - decimal - 1) <= width
         THEN astr[0] := CHR(width)              {dann begrenzen}
         ELSE BEGIN
                aStr := FormReal (value,width,decimal);
                IF (LENGTH(aStr) > width)
                THEN aStr := 'OVERFLOW';
                IF (LENGTH(aStr) > width)
                THEN astr[0] := CHR(width);
              END;
         error   := OverFlow;
       END
  ELSE BEGIN
         error := 0;
       END;

  ChangeWin(winCtrl);                            {Fenster aktivieren}
  SetTextStyle(DefaultFont,HorizDir,size);       {Font und Gr��e setzen}

  SetColor(color);
  ts := size * 8;
  FOR i := 1 TO width                            {nun alle Zeichen bearbeiten}
   DO IF (aStr[i] <> lStr[i])                    {akt. Zeichen <> neu Zeichen}
      THEN BEGIN                                 {dann Position des Zeichens}
             MoveTo (xP + PRED(i) * ts,yP);      {berechenen und positionieren}
             WRITE(aStr[i]);                     {Zeichen ausgeben}
           END;

  vLast   := value;                              {letzen Wert als Real und}
  lStr    := aStr;                               {als String merken}
  newFlag := FALSE;

  WITH WinCtrl^.TextInfo                         {alten Font und Gr��e laden}
  DO SetTextStyle(Font,Direction,CharSize);
END;


PROCEDURE Dig_Meter.Refresh;
BEGIN
  FillChar(lStr,16,' ');                         {Merkstring mit Blank f�llen}
END;


END.
