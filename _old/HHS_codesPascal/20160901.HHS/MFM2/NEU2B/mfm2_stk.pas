Unit mfm2_stk;
{03.02.2015 HHS+SW2}
{27.01.2009 Mittelwertbestimmung: Division durch "2" ersetzt durch "MesRep"}
{27.02.2008 ckk:='e' auskommentiert, damit vier=1 und MesRep=1 speichert}
{22.10.2007 delay(400) bei horizontalfeld eingef�hrt, fr�her delay(100)}
{18.10.2006 SW_wdhlg eingebaut}
{10.10.2007 GoParkPos bei Abfrage 'n'}
{26.06.2007 ParkPos in ihhs6 eingebaut}
{12.04.2005 Stack-Prozeduren auskommentiert}
{06.04.2005 Einzelmessungs-Speicherung r�ckg�ngig gemacht}
{29.10.2003 Speicherung der Einzelmessungen als .dat}
{15.12.2002 Wiederholung Ref-Messung modifiziert}
{02.10.2001 Start_Prozeduren=eigentliche Messung}
{$N+}
Interface

USES DOS, CRT, Graph, SCALELIB, MBBASE, BESLIB, SDLIB,
          STDLIB, MOUSELIB, VESALIB, WINLIB,MBADTLIB,
          TPDECL, AUXIO1,
          MFM2_TCK,MFM2_HWK,MFM2_UFK,MFM2_MAK,MFM2_ANK,MFM2_SCK,MFM2_SUK,
          MFM2_CAK;


procedure Start_measurementproc;
Implementation

PROCEDURE MESSFELD_LEER;
var ix,iy:integer;
begin
  for ix:=1 to 256 do
  begin
    ydata0^[1,ix]:=0.0;
    ydata0^[2,ix]:=0.0;
    xdata0^[ix]:=0.0;
  end;
  ydata90^:=ydata0^;
  yresdata0^:=ydata0^;
  yresdata90^:=ydata0^;
  xdata90^:=xdata0^;
end;
(************* measurement ****************************)
(******** HELMHOLTZ COIL MEASUREMENT *****************)
PROCEDURE ihhs1(var OldWin : WinCtrlPtrType;var delay1,delay2,delay3    : integer;
                var  reduction_factor: real);

var
  scale_x,xsign           : real;
  startvolt, endvolt      : real;
  i,ilauf       :integer;
  chx:char;

Procedure HHSR_Null;
var iix,iiy :integer;
begin
  for iix:=1 to 2 do
  begin
    for iiy:=1 to 8 do
    begin
      HHSR[iix,iiy]:=0;
    end;
  end;
end;

begin
  HHSR_Null;
  for ilauf:=1 to HHSBlocPos do
  begin
    DateTimeTempActualieren;
    HHS_BlocPosFileName(ilauf);

    changewin(BlocNamewin);
    clearwin;
    write(BlocNameTmp);

    ChangeWin(DataFileNameWin);
    clearwin;
    write(CalDatafilename);

    ChangeWin(MainScreen);

    MESSFELD_LEER;
    WarningLeer;
    HHSini;
{delay(2000);}
    DateTimeTempActualieren;
    ifirst:=0;
    MBInc1Stop;

    if ifirstDMM = 1 then dmmini;
    DateTimeTempActualieren;

(********************************************************************)
    if definedata.zero = 1 then
    begin
      ChangeWin(MainScreen);

      dvmstart(1);
    DateTimeTempActualieren;

{WarningText('dvmstart');}
    (********* DAC Hochfahren *************)
    WarningText('DREHEN');
      startvolt:=DACData.StartVoltage;
      endvolt:=DACData.EndVoltage;
      DACDrive(startvolt,endvolt,1);
    DateTimeTempActualieren;

      MBINC1Ini;
      MBINC1Start;
      PlSpeedProc(1);
    DateTimeTempActualieren;
      delay(delay2);
{WarningText('delay2 ',delay2);}
    (********* DAC wieder runterfahren ****)
      startvolt:=DACData.EndVoltage;
      endvolt:=DACData.StartVoltage;
      DACDrive(startvolt,endvolt,1);
    DateTimeTempActualieren;

WarningText('jetzt DVMREADOUT');
      dvmreadout(1,0);
WarningText('DVMREADOUT ende');
    DateTimeTempActualieren;

      MBInc1Stop;
      DateTimeTempActualieren;
    end;                  (*** zero degree position ***)

(**********************************************)
    if definedata.ninety = 1 then
    begin
      (* Magnet flippen *)
WarningText('Magnet Flippen');
      SMS1Drive;
    DateTimeTempActualieren;
      delay(delay3);
    DateTimeTempActualieren;

    {  readkey;}
      delay(15000);
    DateTimeTempActualieren;
      dvmstart(1);
    DateTimeTempActualieren;

    (******** DAC Hochfahren **********)
WarningText('Drehen');
      startvolt:=DACData.StartVoltage;
      endvolt:=DACData.EndVoltage;
      DACDrive(startvolt,endvolt,1);
    DateTimeTempActualieren;

      MBINC1ini;
      MBInc1Start;
    DateTimeTempActualieren;
      PlSpeedProc(2);
    DateTimeTempActualieren;
      delay(delay2);
    DateTimeTempActualieren;

      MBInc1Stop;

    (********* DAC wieder runterfahren ****)
      startvolt:=DACData.EndVoltage;
      endvolt:=DACData.StartVoltage+0.5*DACData.MaxDiff;
      DACDrive(startvolt,endvolt,1);
    DateTimeTempActualieren;

WarningText('jetzt DVMreadout');
      dvmreadout(1,90);
WarningText('DVMreadout ende');
    DateTimeTempActualieren;

    (******* Magnet zurueck flippen *********)
      sms1data.steps:=-sms1data.steps;
WarningText('Zur�ck flippen');
      SMS1Drive;
      delay(delay3);
    DateTimeTempActualieren;
      delay(15000);

      sms1data.steps:=-sms1data.steps;

      DateTimeTempActualieren;
    end;                  (*** ninety degree ***)

    MBINC1aIni;
    MBINC1Start;
    AdjustProc;           (*** Kopf in Ausgangsstellung drehen ***)
    DateTimeTempActualieren;

    if ( (definedata.zero = 1) OR (definedata.ninety = 1) ) then
    dvmreset(1);
    DateTimeTempActualieren;

  (******** Save Raw Data of Ch 1 **********)
    SaveRawData;

WarningText('SaveRawData');
    DateTimeTempActualieren;

  (******** Create Absissa Values **********)
    scale_x:=MbInc1Data.Delay_3/encoder_const;

    if dacdata.endvoltage < 0 then
      xsign:=-1.0
    else
      xsign:=1.0;

    for i:=1 to ianzdata0[1] do
    begin
      xdata0^[i]:=xsign*realval(i)*scale_x;
    end;
    for i:=1 to ianzdata90[1] do
    begin
      xdata90^[i]:=xsign*realval(i)*scale_x;
    end;

  (******** Speed Correction ***************)
    if analysisdata.speedcorr = 1 then SpeedCorrection;

  (******** Analysis ***********************)

(*  for i:=1 to ianzdata0[1] do
  begin
      writeln(i,'  ',xdata0^[i],' ',ydata0^[i,1]);
  end;
  readkey;
*)
    fa;    (* Fourier analysis and Residuals Determination *)
    DateTimeTempActualieren;

    analysisproc;     (* more data analysis *)

  (******** Display ************************)

    DateTimeTempActualieren;

    HHSOutputScreen(ilauf);
    DateTimeTempActualieren;
    delay(2000);
    StoreDataProc_HHS(ilauf);
    DateTimeTempActualieren;
    delay(2000);
    DateTimeTempActualieren;
    changeWin(WarningsWin);
    if ((HHSBlocPos=2) and (ilauf=1)) then
    begin
      write('Magnet in 180� Position einlegen!');
      write('Freigabe! Dann > c < dr�cken!');
      chx:=#255;
      repeat chx:=readkey
{     DateTimeTempActualieren;}

      until (chx='c')
    end;
    DateTimeTempActualieren;

    if (ilauf=HHSBlocPos) then
    begin
      writeln('Fertig! Neuer Magnet!');
      write('Freigabe! Dann START!');
    end;
    DateTimeTempActualieren;

    changeWin(MainScreen);
  end; {ilauf}
  ilauf:=1;
end;
 {ihhs1}


{************ Stretched Wire Grosse Messbank *********************}
{
PROCEDURE ihhs4(var OldWin : WinCtrlPtrType;var delay1,delay2,delay3:integer;
                var  reduction_factor: real);
var
  ifc,i,j,k,n,iiax,idir,idir_new,ifieldcomp,i_field_comp          :integer;
  axisnr,axisnr_a,axisnr_b,SW_ianz_a,SW_ianz_b,iintegrals,iinteg  :integer;
  AktPoint_STWI                              : PointPtrType_STWI;
  SW_START_a,SW_END_a,SW_DIST_a,SW_VELO_a,SW1_VELO_a              :real;
  SW_START_b,SW_END_b,SW_DIST_b,SW_VELO_b,SW1_VELO_b              :real;
  dist_loc,vel_loc,xx,xsign,scal_fact_1,scal_fact_2               :real;
  s_dist, s_vel                                                   : string;
  go1b_plus,go1b_minus,go1b_zero,go2b_plus,go2b_minus,go2b_zero   : string;
  go1a_plus,go1a_minus,go1a_zero,go2a_plus,go2a_minus,go2a_zero   : string;

begin
  imeasure:=0;

  if ifirstSTWI = 1 then
  begin
    STWIini;
    ifirstSTWI:=0;
  end;

  ifieldcomp:=0;
  if (STWIData.Meas.IntBy12 > 0) then ifieldcomp:=ifieldcomp+1;
  if (STWIData.Meas.IntBz12 > 0) then ifieldcomp:=ifieldcomp+1;
  if (STWIData.Meas.IntBtheta12 > 0) then ifieldcomp:=1;  (* spaeter *)

  if ((STWIData.Meas.IntBy12 > 0) and (STWIData.Meas.IntBz12 = 0)) then
    ibybz:=1;
  if ((STWIData.Meas.IntBy12 = 0) and (STWIData.Meas.IntBz12 > 0)) then
    ibybz:=2;
  if ((STWIData.Meas.IntBy12 > 0) and (STWIData.Meas.IntBz12 > 0)) then
    ibybz:=3;

  (*initialize stack for DVM data*)
  if(DefineData.SaveStack = 1)then
  begin
    CreatePoint_STWI(Top_STWI);
    AktPoint_STWI:=Top_STWI;
  end;

  axisnr_a:=3;
  SW_Start_a:=GridData.zmin;
  SW_End_a:=GridData.zmax;
  SW_ianz_a:=GridData.ianzz;
  SW_dist_a:=(SW_End_a-SW_Start_a)/realval(SW_ianz_a-1);
  SW_velo_a:=DefineData.Speed_z;      (* Scan Geschwindigkeit *)
  SW1_velo_a:=ServoData.velocity.z;   (* Geschw. zwischen Scans *)
  axisnr_b:=2;
  SW_Start_b:=GridData.ymin;
  SW_End_b:=GridData.ymax;
  SW_ianz_b:=GridData.ianzy;
  if (SW_ianz_b > 1) then
    SW_dist_b:=(SW_End_b-SW_Start_b)/realval(SW_ianz_b-1)
  else
    SW_dist_b:=SW_End_b-SW_Start_b;

  SW_velo_b:=DefineData.Speed_y;
  SW1_velo_b:=ServoData.velocity.y;

  axisnr:=2;
  dist_loc:=SW_Dist_b;
  vel_loc:=0;
  get_dist_vel(axisnr, dist_loc, vel_loc, s_dist, s_vel);
  go1b_plus:='SET1='+s_dist;
  go2b_plus:='SET2='+s_dist;
  dist_loc:=-SW_Dist_b;
  get_dist_vel(axisnr, dist_loc, vel_loc, s_dist, s_vel);
  go1b_minus:='SET1='+s_dist;
  go2b_minus:='SET2='+s_dist;
  go1b_zero:='SET1=0';
  go2b_zero:='SET2=0';

  axisnr:=3;
  dist_loc:=SW_Dist_a;
  vel_loc:=0;
  get_dist_vel(axisnr, dist_loc, vel_loc, s_dist, s_vel);
  go1a_plus:='SET1='+s_dist;
  go2a_plus:='SET2='+s_dist;
  dist_loc:=-SW_Dist_a;
  get_dist_vel(axisnr, dist_loc, vel_loc, s_dist, s_vel);
  go1a_minus:='SET1='+s_dist;
  go2a_minus:='SET2='+s_dist;
  go1a_zero:='SET1=0';
  go2a_zero:='SET2=0';

  FileCounter(1);   (* get new data file name *)
  MBINC1Stop;       (* initialize Counter *)
  MBINC2Stop;
  delay(100);
  MBINC1Ini;
  MBINC2aIni;
  delay(100);
  MBINC1Start;
  delay(100);

  (*----------------------------------------------------------*)
  for ifc:=1 to ifieldcomp do
  begin
    if (ifc < ifieldcomp ) then
      i_more_fieldcomp := 1
    else
      i_more_fieldcomp := 0;

    for j:=1 to 256 do
    begin
      ydata0^[1,j]:=0.0;
      yresdata0^[1,j]:=0.0;
      ydata90^[1,j]:=0.0;
      yresdata90^[1,j]:=0.0;
    end;

    if ((ifc = 1) and (STWIData.Meas.Intby12 > 0))  then
    begin
      i_field_comp:=1;  (* vertical fields *)
      DVM1Data.Readings:=2*GridData.ianzz+1;
    end;
    if ( (ifc = 2) or ( (ifc = 1) and (STWIData.Meas.Intby12 = 0) ) ) then
    begin
      i_field_comp:=2;  (* horizontal fields *)
      DVM1Data.Readings:=3*GridData.ianzz;
    end;

    if (i_field_comp = 1) then iintegrals:=STWIData.Meas.Intby12;
    if (i_field_comp = 2) then iintegrals:=STWIData.Meas.Intbz12;

    for iinteg:=1 to iintegrals do
    begin
      if (iinteg < iintegrals ) then i_more_integrals := 1
      else  i_more_integrals := 0;

      (*** go to start position in direction a ***)

      if (i_field_comp = 1) then   (* vertical fields *)
      begin
        ServoDriveSTWI(axisnr_a, 1, 3,
        SW_Start_a-0.5*SW_Dist_a, SW1_velo_a);
        if (iinteg = 2) then
        begin
          repeat n:=STWI_ask_motion; until n=0;
            ServoDriveSTWI(axisnr_a, 1, 1,0.5*SW_Dist_a, SW1_velo_a);
        end;
      end;

      if (i_field_comp = 2) then   (* horizontal fields *)
        ServoDriveSTWI(axisnr_a, 1, 3, SW_Start_a, SW1_velo_a);

      repeat n:=STWI_ask_motion; until n=0;

      (**** go to start position in direction b ****)

      if ( (i_field_comp = 1) and (SW_ianz_b > 1) ) then
        ServoDriveSTWI(axisnr_b, 1, 3, SW_Start_b, SW1_velo_b);

      if ( (i_field_comp = 2) ) then
      begin
        if (SW_ianz_b = 1) then
        begin
          if (iinteg = 1) then
            ServoDriveSTWI(axisnr_b, 1, 3, SW_Start_b, SW1_velo_b);
          if (iinteg = 2) then
            ServoDriveSTWI(axisnr_b, 1, 2, SW_Start_b, SW1_velo_b);
        end;

        if (SW_ianz_b > 1) then
        begin
          ServoDriveSTWI(axisnr_b, 1, 3,
          SW_Start_b-0.5*SW_Dist_b, SW1_velo_b);
          if (iinteg = 2) then
          begin
            repeat n:=STWI_ask_motion; until n=0;
            ServoDriveSTWI(axisnr_b, 1, 1,+0.5*SW_Dist_b, SW1_velo_b);
          end;
        end;       (* SW_ianz_b >  *)
      end;       (* i_field_comp = 2 *)

      repeat n:=STWI_ask_motion; until n=0;

      (***** start measurement *******)
      i_more_scans:=1;
      idir:=1;

      DVMStart(1);
      delay(200);

      for i := 1 to SW_ianz_b do    (* loop over scans *)
      begin
        if (i = SW_ianz_b) then i_more_scans:=0;

        (* initialize Controller *)
        (** nicht zu fahrende Achsen deaktivieren !! *)
        imeasure:=1;
        if (iinteg = 1)then
        begin
          ServoDriveSTWI(axisnr_a, 1, 3, SW_dist_a, SW_velo_a);
          delay(500);
          ServoDriveSTWI(axisnr_b, 1, 3, SW_dist_b, SW_velo_b);
          delay(500);
        end;
        if (iinteg = 2)then
        begin
          if (i_field_comp = 1) then
            ServoDriveSTWI(axisnr_a, 1, 2, SW_dist_a, SW_velo_a);
          if (i_field_comp = 2) then
            ServoDriveSTWI(axisnr_a, 1, 3, SW_dist_a, SW_velo_a);
          delay(500);
          ServoDriveSTWI(axisnr_b, 1, 2, SW_dist_b, SW_velo_b);
          delay(500);
        end;
        imeasure:=0;
        IEEE_write(SW1,'CLBUF');
        delay(200);
        IEEE_write(SW2,'CLBUF');
        delay(200);

(*----------------------------------------------------------*)
        if (i_field_comp = 1) then (* vertical fields *)
        begin
          (** first data point **)
          k:=1;
          MBINC2Start;
          delay(round(DVM1Data.aperture)+round(1.65*MBINC2data.Delay_2));

          for j := 1 to SW_ianz_a do        (* start single loop *)
          begin
            (* take data point during movement*)
            k:=k+1;
            MBINC2Start;
            delay(DefineData.delay_1);
            (* start axes A and B at the same time *)
            (* hier Fahrgeschwindigkeit an Apertur anpassen *)
            ibtrg(SW1);  (* wr_text:= 'START'; IEEE_write(SW1,wr_text); *)
            delay(round(DVM1Data.aperture)+round(1.65*MBINC2data.Delay_2));

            (* take data point before next movement*)
            k:=k+1;
            MBINC2Start;
            delay(round(DVM1Data.aperture)+round(1.65*MBINC2data.Delay_2));

            if (iinteg = 2) then  (*second field integral *)
            begin
              ServoDriveSTWI(axisnr_a, 1, 1, SW_dist_a, SW_velo_a);
              repeat n:=STWI_ask_motion; until n=0;  (* all motors stopped ?*)
              wr_text:=go1a_zero;
              IEEE_write(SW1,wr_text);
              delay(200);
              wr_text:=go2a_plus;
              IEEE_write(SW1,wr_text);
              delay(200);
            end;
          end;       (* end loop over j (end single scan) *)
        end;       (* vertical fields *)
        (*----------------------------------------------------------*)
        if (i_field_comp = 2) then (* horizontal fields *)
        begin
          k:=0;
          for j := 1 to SW_ianz_a do        (* start single loop *)
          begin
            repeat n:=STWI_ask_motion; until n=0;  (* all motors stopped ?*)
            (* take data point before movement*)
            k:=k+1;
            MBINC2Start;
            delay(round(DVM1Data.aperture)+round(1.65*MBINC2data.Delay_2));

            (* take data point during movement*)
            k:=k+1;
            MBINC2Start;
            delay(DefineData.delay_1);
            (* start axes A and B at the same time *)
            (* hier Fahrgeschwindigkeit an Apertur anpassen *)
            ibtrg(SW2);  (* wr_text:= 'START'; IEEE_write(SW2,wr_text); *)

            delay(round(DVM1Data.aperture)+round(1.65*MBINC2data.Delay_2));

            (* take data point after movement*)
            k:=k+1;
            MBINC2Start;
            delay(round(DVM1Data.aperture)+round(1.65*MBINC2data.Delay_2));

            (* reverse y-direction for next data point *)
            if (idir=1) then
            begin
              idir_new:=-1;
              delay(200);
              if (iinteg = 1) then
                wr_text:=go1b_plus;   (* y-Achse zaehlt verkehrt herum *)
              if (iinteg = 2) then
                wr_text:=go1b_zero;   (* y-Achse zaehlt verkehrt herum *)
              IEEE_write(SW2,wr_text);
              delay(200);
              wr_text:=go2b_plus;
              IEEE_write(SW2,wr_text);
              delay(200);
            end;
            if (idir=-1) then
            begin
              idir_new:=1;
              delay(200);
              if (iinteg = 1) then wr_text:=go1b_minus;
              if (iinteg = 2) then wr_text:=go1b_zero;
              IEEE_write(SW2,wr_text);
              delay(200);
              wr_text:=go2b_minus;
              IEEE_write(SW2,wr_text);
              delay(200);
            end;
            idir:=idir_new;

            if (j < SW_ianz_a) then
            begin
              repeat n:=STWI_ask_motion; until n=0;
              ibtrg(SW1);  (* drive one step in z-direction *)
            end;
          end;       (* end loop over j (end single scan) *)
        end;       (* horizontal fields *)
        (*----------------------------------------------------------*)

        (* readout dvm *)
        if (iinteg = 1) then dvmreadout(1,0);
        if (iinteg = 2) then dvmreadout(1,90);
        dvmreset(1);

        ianzdata0[1]:=k;
        ianzdata90[1]:=k;
        ianzdata0[2]:=k;
        ianzdata90[2]:=k;

        if (i_field_comp = 1) then
        begin
          xdata0^[1]:=SW_start_a-0.5*SW_dist_a;
          xdata90^[1]:=SW_start_a-0.5*SW_dist_a;
          for j:=1 to SW_ianz_a do
          begin
            xx:=j;
            xdata0^[2*j]:=SW_start_a+(xx-1.0)*SW_dist_a;
            xdata90^[2*j]:=SW_start_a+(xx-1.0)*SW_dist_a;
            xdata0^[2*j+1]:=SW_start_a+(xx-0.5)*SW_dist_a;
            xdata90^[2*j+1]:=SW_start_a+(xx-0.5)*SW_dist_a;
          end;
          for j:=1 to k div 2 do
          begin
            yresdata0^[1,2*j]   :=ydata0^[1,2*j]
                   -0.5*(ydata0^[1,2*j-1]+ydata0^[1,2*j+1]);
            yresdata0^[1,2*j-1] :=yresdata0^[1,2*j];
            yresdata90^[1,2*j]  :=ydata90^[1,2*j]
                   -0.5*(ydata90^[1,2*j-1]+ydata90^[1,2*j+1]);
            yresdata90^[1,2*j-1]:=yresdata90^[1,2*j];
          end;
          yresdata0^[1,k] :=yresdata0^[1,k-1];
          yresdata90^[1,k]:=yresdata90^[1,k-1];
        end;    (* i_field_comp = 1 *)

        if (i_field_comp = 2) then
        begin
          for j:=1 to SW_ianz_a do
          begin
            xx:=j-1;
            xdata0^[3*j-2]:=SW_start_a+(xx-(1.0/3.0))*SW_dist_a;
            xdata0^[3*j-1]:=SW_start_a+(xx)*SW_dist_a;
            xdata0^[3*j]  :=SW_start_a+(xx+(1.0/3.0))*SW_dist_a;
            xdata90^[3*j-2]:=SW_start_a+(xx-(1.0/3.0))*SW_dist_a;
            xdata90^[3*j-1]:=SW_start_a+(xx)*SW_dist_a;
            xdata90^[3*j]:=SW_start_a+(xx+(1.0/3.0))*SW_dist_a;
          end;
          for j:=1 to k div 3 do
          begin
            xsign:=1.0;
            if ((j mod 2) = 0) then
            begin
              xsign:=-1.0;
              if (iinteg = 1) then ydata0^[1,3*j-1]:=-ydata0^[1,3*j-1];
              if (iinteg = 2) then ydata90^[1,3*j-1]:=-ydata90^[1,3*j-1];
            end;
            if (iinteg = 1) then
            begin
              yresdata0^[1,3*j-1]:=ydata0^[1,3*j-1]
                        -xsign*0.5*(ydata0^[1,3*j-2]+ydata0^[1,3*j]);
              yresdata0^[1,3*j-2]:=yresdata0^[1,3*j-1];
              yresdata0^[1,3*j]  :=yresdata0^[1,3*j-1];
            end;
            if (iinteg = 2) then
            begin
              yresdata90^[1,3*j-1]:=ydata90^[1,3*j-1]
                             -xsign*0.5*(ydata90^[1,3*j-2]+ydata90^[1,3*j]);
              yresdata90^[1,3*j-2]:=yresdata90^[1,3*j-1];
              yresdata90^[1,3*j]  :=yresdata90^[1,3*j-1];
            end;
          end;
        end;    (* i_field_comp = 2 *)

        scaledata.dispchannel0:=1;
        scaledata.dispchannel90:=1;
        scaledata.autoscale0:=1;
        scaledata.autoscale90:=1;

        (* Scaling of data changed from Gauss m and Gauss m**2
            to Tesla mm and Tesla mm**2, 14.12.1998  *)
        if (i_field_comp = 1) then
        begin
          (*scal_fact_1:=10000.0*(DVM1Data.aperture/SW_Dist_a);
             scal_fact_2:=1000.0*(DVM1Data.aperture/SW_Dist_a)*STWI_length;*)
          scal_fact_1:=(1000.0*DVM1Data.aperture)/
                         (STWI_amplifier*SW_Dist_a);
          scal_fact_2:=(1000.0*DVM1Data.aperture*STWI_length)/
                         (SW_Dist_a*STWI_amplifier);
        end;
        if (i_field_comp = 2) then
        begin
          (*scal_fact_1:=10000.0*(DVM1Data.aperture/SW_Dist_b);
             scal_fact_2:=1000.0*(DVM1Data.aperture/SW_Dist_b)*STWI_length*)
          scal_fact_1:=(1000.0*DVM1Data.aperture)/
                         (SW_Dist_b*STWI_amplifier);
          scal_fact_2:=(1000.0*DVM1Data.aperture*STWI_length)/
                         (SW_Dist_b*STWI_amplifier);
        end;
        for j:=1 to k do
        begin
          if (iinteg = 1) then
          begin
            ydata0^[1,j]:=ydata0^[1,j]*scal_fact_1;
            yresdata0^[1,j]:=yresdata0^[1,j]*scal_fact_1;
          end;
          if (iinteg = 2) then
          begin
            ydata90^[1,j]:=ydata90^[1,j]*scal_fact_2;
            yresdata90^[1,j]:=yresdata90^[1,j]*scal_fact_2;
          end;
        end;

        replotdata(3);

        if(DefineData.SaveStack = 1) and (SW_ianz_b > 1) then
          STWI_write_data_to_stack(AktPoint_STWI);

        (* go to start position in direction a *)
        if (i<SW_ianz_b)then   (* next scan *)
        begin
          delay(200);

          if (i_field_comp = 1) then
            ServoDriveSTWI(axisnr_a, 1, 3,
                   -(SW_END_a-SW_Start_a)-SW_Dist_a, SW1_velo_a);
          if (i_field_comp = 2) then
            ServoDriveSTWI(axisnr_a, 1, 3,
                   -(SW_END_a-SW_Start_a), SW1_velo_a);
          repeat n:=STWI_ask_motion; until n=0;

          (* go to start position in direction b *)
          delay(200);
          if ((i_field_comp =1) or ((i_field_comp =2) and (idir =1))) then
            ServoDriveSTWI(axisnr_b, 1, 3, SW_dist_b, SW_velo_b);
          repeat n:=STWI_ask_motion; until n=0;
        end;
      end;       (* i; end loop over scans *)
      (* go to home position in direction a *)
      delay(200);
      if (i_field_comp = 1) then
      begin
        ServoDriveSTWI(axisnr_a, 1, 3,SW_START_a-0.5*SW_Dist_a, SW1_velo_a);
        if (iinteg = 2) then
        begin
          repeat n:=STWI_ask_motion; until n=0;
          ServoDriveSTWI(axisnr_a, 1, 1,-0.5*SW_Dist_a, SW1_velo_a);
        end;
      end;
      if (i_field_comp = 2) then
        ServoDriveSTWI(axisnr_a, 1, 3, SW_START_a, SW1_velo_a);
      repeat n:=STWI_ask_motion; until n=0;

      (* go to home position in direction b *)
      delay(200);
      if ( (i_field_comp = 1) and (SW_ianz_b > 1) ) then
        ServoDriveSTWI(axisnr_b, 1, 3, SW_Start_b, SW1_velo_b);

      if ( (i_field_comp = 2) ) then
      begin
        if (iinteg = 1) then iiax:=3;
        if (iinteg = 2) then iiax:=2;

        if (SW_ianz_b = 1) then
        begin
          if (idir = 1) then
            ServoDriveSTWI(axisnr_b, 1, iiax, -SW_Start_b, SW1_velo_b);
          if (idir = -1) then
            ServoDriveSTWI(axisnr_b, 1, iiax, SW_Start_b, SW1_velo_b);
        end;

        if (SW_ianz_b > 1) then
        begin
          if (idir = 1) then
            ServoDriveSTWI(axisnr_b, 1, 3,
                      SW_Start_b+0.5*SW_Dist_b, SW1_velo_b);
          if (idir = -1) then
            ServoDriveSTWI(axisnr_b, 1, 3,
                      SW_Start_b-0.5*SW_Dist_b, SW1_velo_b);
          if (iinteg = 2) then
          begin
            repeat n:=STWI_ask_motion; until n=0;
            if (idir = 1) then
              ServoDriveSTWI(axisnr_b, 1, 1,-0.5*SW_Dist_b, SW1_velo_b);
            if (idir = -1) then
              ServoDriveSTWI(axisnr_b, 1, 1,0.5*SW_Dist_b, SW1_velo_b);
          end;
        end; (*SW_ianz_b>1*)
      end;   (*i_field_comp=2*)

      repeat n:=STWI_ask_motion; until n=0;

    end;          (* iinteg; end loop over integrals (1st and 2nd) *)

    if(DefineData.SaveStack = 1) and (SW_ianz_b = 1) then
      STWI_write_data_to_stack(AktPoint_STWI);
      (* N O T I C E !!!
          different data format for SW_ianz_b = 1 and SW_ianz_b > 1)
         SW.EXE on VMS works only for the case SW_ianz_b = 1 *)

  end;          (* ifc; end loop over field components *)
  DVMReset(1);
  filename:=DVMDataFilename;
  SaveStack_STWI(filename);
  Write_logfile_STWI;

end;
} {ihhs4 **** Stretched Wire an der grossen Bank****}

{************ Stretched Wire SYTEM 2 *********************}
PROCEDURE ihhs6(var OldWin : WinCtrlPtrType;var delay1,delay2,delay3:integer;
                var  reduction_factor: real;var vier:integer);
Label LOOP1,LOOP2,ZURUECK,DasEnde;
var
  SW_wdhlg,wdhlg:integer;
  ifc,i,j,k,l,n,iiax,idir,idir_new,ifieldcomp,i_field_comp,iv,iyz :integer;
  axisnr,axisnr_a,axisnr_b,SW_ianz_a,SW_ianz_b,iintegrals,iinteg  :integer;
  AktPoint_STWI                              : PointPtrType_STWI;
  SW_PARK_a,SW_PARK_b                                             :real;
  SW_START_a,SW_END_a,SW_DIST_a,SW_VELO_a,SW1_VELO_a,SMSFAKTOR    :real;
  SW_START_b,SW_END_b,SW_DIST_b,SW_VELO_b,SW1_VELO_b              :real;
  xx,xsign,scal_fact_1,scal_fact_2                                :real;
  rweg                                                            :longint;
  chp,ckk,chab:char;

Procedure GoParkPos;
begin
  rweg:=round(SMS_Factor*(SW_Park_a));
  SMSRampeDo(1,rweg,SMSR1^,0);
end;
Procedure GoNegParkPos;
begin
  rweg:=round(-SMS_Factor*(SW_Park_a));
  SMSRampeDo(1,rweg,SMSR1^,0);
end;

begin
  chbreak:=#255;
  ckk:='v';
  DateTimeTempActualieren;
  vier:=AnalysisData.SpeedCorr; {vier;, Zahl der Blockpositionen}
  MesRep:=AnalysisData.NrOfRev; {MesRep; Zahl der Messungen/Posit.}
  SMS_Pause:=ROUND(AnalysisData.PtsPerRev);{SMS_Pause}
  SMSRef:=AnalysisData.OrdOfFA;  {Ref-Fahrt in der Messroutine}

{  if (vier=1) and (MesRep=1) then ckk:='e';}
  SMSRampINI;
  SMSRampeFill(SMSR1^,SMS1Data);
  SMSRampeFill(SMSR2^,SMS2Data);
  SMSFaktor:=SMS_FACTOR*(-1); (* hier Bewegung der Probe, bei SW1 Draht !*)
  imeasure:=0;

  for i:=1 to 256 do    (*Messpunkte*)
  begin
    for j:=1 to 4 do    (*1,2=1.Messung, 3,4=2.Messung, 1,3=By, 2,4=Bz*)
    begin
      for l:=1 to 4     (*Probenseite*)
      do
      begin
        xdata_Byz^[l,j,i]:=0;
        ydata_Byz^[l,j,i]:=0;
        yresdata_Byz^[l,j,i]:=0;
      end;
    end;
  end;

  if ifirstSTWI = 1 then
  begin
    DVMIni(1);
    ifirstSTWI:=0;
  end;
  DateTimeTempActualieren;
    ifieldcomp:=0;
    if (STWIData.Meas.IntBy12 > 0) then ifieldcomp:=ifieldcomp+1;
    if (STWIData.Meas.IntBz12 > 0) then ifieldcomp:=ifieldcomp+1;

    if ((STWIData.Meas.IntBy12 > 0) and (STWIData.Meas.IntBz12 = 0)) then
      ibybz:=1;      {nur vertikale Felder}
    if ((STWIData.Meas.IntBy12 = 0) and (STWIData.Meas.IntBz12 > 0)) then
      ibybz:=2;      {nur horizontale Felder}
    if ((STWIData.Meas.IntBy12 > 0) and (STWIData.Meas.IntBz12 > 0)) then
      ibybz:=3;      {horizontale und vertikale Felder}

    SW_Park_a:=GridData.xmin;           {Parkposition zur entnahme der magnete}

    SW_wdhlg:=GridData.ianzx;              {Anzahl der Wiederholungen eines Satzes}
    SW_Start_a:=GridData.zmin;          {Startposition vertikales Feld}
    SW_End_a:=GridData.zmax;            {EndPosition vertikales Feld}
    SW_ianz_a:=GridData.ianzz;          {Maschen f�r vertikales Feld}
    if SW_ianz_a>1 then
      SW_dist_a:=(SW_End_a-SW_Start_a)/realval(SW_ianz_a-1) {Weg vert. Feld}
    else
      SW_dist_a:=(SW_End_a-SW_Start_a);
    SW_velo_a:=DefineData.Speed_z;      (* vert.Feld Scan Geschwindigkeit *)
    SW1_velo_a:=ServoData.velocity.z;   (* Geschw. zwischen Scans *)

    SW_Start_b:=GridData.ymin;          {Startposition horizontales Feld}
    SW_End_b:=GridData.ymax;            {Endposition horizontales Feldd}
    SW_ianz_b:=GridData.ianzy;          {Maschen f. horiz. Feld}
    if (SW_ianz_b > 1) then
      SW_dist_b:=(SW_End_b-SW_Start_b)/realval(SW_ianz_b-1) {Weg horiz.feld}
    else
      SW_dist_b:=SW_End_b-SW_Start_b;

  for wdhlg:=1 to SW_wdhlg do
  begin
    DateTimeTempActualieren;
  if (SMSREF=1) then SMSREFERENCE;
  for iv:=1 to vier do  (* Messung �ber max. vier Blockpositionen des Magneten*)
  begin
    LOOP1:
    DateTimeTempActualieren;
    if (SMSREF=2) then SMSREFERENCE;
    DateTimeTempActualieren;
    if (iv>1) and (SMSREF<2) then GoNegParkPos;
    DateTimeTempActualieren;
    for l:=1 to MesRep do    (* max 2 S�tze = Messungen in By u. Bz*)
    begin
      LOOP2:
      DateTimeTempActualieren;
 (*     if l=2 then
      begin
        ChangeWin(WarningsWin);
        writeln;
        writeln('Magnet noch einmal einlegen!');
        write('Dann dr�cke >c<');
        repeat chp:=readkey;
          chp:=upcase(chp);
        until chp='C';
        changeWin(MainScreen);
      end;
*)
      if (SMSREF=3) then SMSREFERENCE;
      DateTimeTempActualieren;
      {initialize stack for DVM data}
(*      if(DefineData.SaveStack = 1) then
      begin
        CreatePoint_STWI(Top_STWI);
        AktPoint_STWI:=Top_STWI;
      end;
*)      if (ckk='v') then
      begin
        changeWin(DACValueWin);
        writeln;
{        writeln(iv:2,'. Bloc Position');
        writeln(l:2,'. Measurement');   }
        writeln(iv:2,'. Block Position');
        writeln(l:2,'. Satz');
             writeln('------------------');
        write('Halt: Strg C');
        changeWin(MainScreen);
      end;
      DateTimeTempActualieren;
      FileCounter(1);   (* get new data file name *)
      MBINC1Stop;       (* initialize Counter *)

      delay(200);
      MBINC1bIni;
      delay(200);

  (*----------------------------------------------------------*)
      for ifc:=1 to ifieldcomp do    (*Messung �ber die By, Bz-Felder*)
      begin
        if (ifc < ifieldcomp ) then
          i_more_fieldcomp := 1
        else
          i_more_fieldcomp := 0;

        for j:=1 to 256 do
        begin
          xResData^[j]:=0;
          ydata0^[1,j]:=0.0;
          yresdata0^[1,j]:=0.0;
          ydata90^[1,j]:=0.0;
          yresdata90^[1,j]:=0.0;
        end;

        if ((ifc = 1) and (STWIData.Meas.Intby12 > 0))  then
        begin
          i_field_comp:=1;  (* vertical fields *)
          DVM1Data.Readings:=2*GridData.ianzz+1;
        end;
        if ( (ifc = 2) or ( (ifc = 1) and (STWIData.Meas.Intby12 = 0) ) ) then
        begin
          i_field_comp:=2;  (* horizontal fields *)
          DVM1Data.Readings:=3*GridData.ianzz;
        end;

        if (i_field_comp = 1) then iintegrals:=STWIData.Meas.Intby12;
        if (i_field_comp = 2) then iintegrals:=STWIData.Meas.Intbz12;

        iinteg:=1;
        i_more_integrals := 0;

(*  SKALIERUNG ********************************************************* *)
        scaledata.dispchannel0:=1;
        scaledata.dispchannel90:=1;
        scaledata.autoscale0:=1;
        scaledata.autoscale90:=1;

        (* Scaling of data changed from Gauss m and Gauss m**2
            to Tesla mm and Tesla mm**2, 14.12.1998  *)
        if (i_field_comp = 1) then
        begin
          scal_fact_1:=(1000.0*DVM1Data.aperture)/(STWI_amplifier*SW_Dist_a);
        end;
        if (i_field_comp = 2) then
        begin
          scal_fact_1:=(1000.0*DVM1Data.aperture)/(SW_Dist_b*STWI_amplifier);
        end;

    (*** go to start position ***)
        ChangeWin(WarningsWin);
        writeln('Goto StartPos.');
        DateTimeTempActualieren;
        if (i_field_comp = 1) then   (* vertical fields *)
        begin
          rweg:=round(SMSFaktor*(SW_Start_a-0.5*SW_Dist_a));
          SMSRampeDo(1,rweg,SMSR1^,0);
          rweg:=round(SMSFaktor*(SW_Start_b));
          if (SW_ianz_b > 1) then  SMSRampeDo(2,rweg,SMSR2^,0);
        end;
        DateTimeTempActualieren;
        if (i_field_comp = 2) then   (* horizontal fields *)
        begin
          rweg:=round(SMSFaktor*SW_Start_a);
          SMSRampeDo(1,rweg,SMSR1^,0);
          rweg:=round(SMSFaktor*SW_Start_b);
          delay(400);
          if (SW_ianz_b = 1) then  SMSRampeDo(2,rweg,SMSR2^,0);
          rweg:=round(SMSFaktor*(SW_Start_b-0.5*SW_Dist_b));
          if (SW_ianz_b > 1) then  SMSRampeDo(2,rweg,SMSR2^,0);
          delay(400);
        end;
        DateTimeTempActualieren;
        (***** start measurement *******)
        i_more_scans:=1;
        idir:=1;
        ChangeWin(WarningsWin);
        writeln('Start!');
        changeWin(MainScreen);

        DVMStart(1);
        delay(400);
        DateTimeTempActualieren;
        for i := 1 to SW_ianz_b do    (* loop over scans *)
        begin
          if (i = SW_ianz_b) then i_more_scans:=0;
          delay(1000);
(*----------------------------------------------------------*)
          DateTimeTempActualieren;
          if (i_field_comp = 1) then (* vertical fields *)
          begin
            (** first data point **)
            k:=1;
            MBINC1Start;
            delay(round(DVM1Data.aperture)+round(1.65*MBINC1data.Delay_2));
            delay(200);
{neu} delay(SMS_Pause);
            DateTimeTempActualieren;
            for j := 1 to SW_ianz_a do        (* start single loop *)
            begin
              (* take data point during movement*)
              k:=k+1;
              ChangeWin(WarningsWin);
              writeln('IBy ',j,'(',sw_ianz_a,')');
              changeWin(MainScreen);

              rweg:=round(SMSFaktor*SW_Dist_a);
              MBINC1Start;
  {?}        { delay(DefineData.delay_1);}
              SMSRampeDo(1,rweg,SMSR1^,1);
              delay(round(DVM1Data.aperture)+round(1.65*MBINC1data.Delay_2));
              delay(200);
              (* take data point before next movement*)
{neu} delay(SMS_Pause);
              DateTimeTempActualieren;
              k:=k+1;
              MBINC1Start;
              delay(round(DVM1Data.aperture)+round(1.65*MBINC1data.Delay_2));
              delay(200);
{neu} delay(SMS_Pause);
              DateTimeTempActualieren;
            if chbreak=#3 then goto DasEnde;
            end;       (* end loop over j (end single scan) *)
          end;       (* vertical fields *)
        (*----------------------------------------------------------*)
          if (i_field_comp = 2) then (* horizontal fields *)
          begin
            k:=0;
            for j := 1 to SW_ianz_a do        (* start single loop *)
            begin
              (* take data point before movement:*)
              k:=k+1;
              MBINC1Start;
              delay(round(DVM1Data.aperture)+round(1.65*MBINC1data.Delay_2));
              delay(400);
{neu} delay(SMS_Pause);
              (* take data point during movement:*)
              ChangeWin(WarningsWin);
              writeln('IBz ',j,'(',sw_ianz_a,')');
              changeWin(MainScreen);

              DateTimeTempActualieren;
              k:=k+1;
              rweg:=round(SMSFaktor*SW_dist_b*idir);
              MBINC1Start;
  {?}   {      delay(DefineData.delay_1);}
              SMSRampeDo(2,rweg,SMSR2^,1);
              delay(round(DVM1Data.aperture)+round(1.65*MBINC1data.Delay_2));
              delay(400);
              (* take data point after movement:*)
{neu} delay(SMS_Pause);
              DateTimeTempActualieren;
              k:=k+1;
              MBINC1Start;
              delay(round(DVM1Data.aperture)+round(1.65*MBINC1data.Delay_2));
              delay(400);
              (* reverse y-direction for next data point *)
              idir:=idir*(-1);
              DateTimeTempActualieren;
              if (j < SW_ianz_a) then
              begin
                rweg:=round(SMSfaktor*SW_dist_a);
                SMSRampeDo(1,rweg,SMSR1^,0);
              end;
              delay(400);
{neu} delay(SMS_pause);
              DateTimeTempActualieren;
            if chbreak=#3 then goto DasEnde;
            end;       (* end loop over j (end single scan) *)
          end;       (* horizontal fields *)
        (*----------------------------------------------------------*)
ChangeWin(WarningsWin);
writeln('lese aus');
         dvmreadout(1,0);
        (*   ChangeWin(WarningsWin);  *)
writeln('readout done');
          DateTimeTempActualieren;
          dvmreset(1);
          DateTimeTempActualieren;
          ianzdata0[1]:=k;
          ianzdata90[1]:=k;
          ianzdata0[2]:=k;
          ianzdata90[2]:=k;

          if (i_field_comp = 1) then
          begin
            xdata0^[1]:=SW_start_a-0.5*SW_dist_a;
            xdata_Byz^[iv,2*l-1,1]:=xdata0^[1];

            for j:=1 to SW_ianz_a do
            begin
              xx:=j;
              xdata0^[2*j+1]:=SW_start_a+(xx-0.5)*SW_dist_a;
              xdata0^[2*j]:=SW_start_a+(xx-1.0)*SW_dist_a;

              xdata_Byz^[iv,2*l-1,2*j]  := xdata0^[2*j];
              xdata_Byz^[iv,2*l-1,2*j+1]:= xdata0^[2*j+1];
              xResData^[j] :=xdata0^[2*j];
            end;

            for j:=1 to k div 2 do
            begin
              yresdata0^[1,2*j]   :=ydata0^[1,2*j]
                     -0.5*(ydata0^[1,2*j-1]+ydata0^[1,2*j+1]);
              yresdata0^[1,2*j-1] :=yresdata0^[1,2*j];
            end;
            yresdata0^[1,k] :=yresdata0^[1,k-1];

            for j:=1 to k do
            begin
              ydata0^[1,j]:=ydata0^[1,j]*scal_fact_1;
              ydata_Byz^[iv,2*l-1,j]   :=ydata0^[1,j];
            end;
            for j:=1 to SW_ianz_a do
            begin
              yresdata_Byz^[iv,2*l-1,j]:=yresdata0^[1,2*j]*scal_fact_1*(-1);
            end;

            iyz:=1;
          end;    (* i_field_comp = 1 *)

          if (i_field_comp = 2) then
          begin
            for j:=1 to SW_ianz_a do
            begin
              xx:=j-1;
              xdata0^[3*j-2]:=SW_start_a+(xx-(1.0/3.0))*SW_dist_a;
              xdata0^[3*j-1]:=SW_start_a+(xx)*SW_dist_a;
              xdata0^[3*j]  :=SW_start_a+(xx+(1.0/3.0))*SW_dist_a;

              xdata_Byz^[iv,2*l,3*j-2]:=xdata0^[3*j-2];
              xdata_Byz^[iv,2*l,3*j-1]:=xdata0^[3*j-1];
              xdata_Byz^[iv,2*l,3*j]  :=xdata0^[3*j];
              xResData^[j]:=xdata0^[3*j-1];
            end;
            for j:=1 to k div 3 do
            begin
              xsign:=1.0;
              if ((j mod 2) = 0) then
              begin
                xsign:=-1.0;
                ydata0^[1,3*j-1]:=-ydata0^[1,3*j-1];
              end;
              yresdata0^[1,3*j-1]:=ydata0^[1,3*j-1]
                        -xsign*0.5*(ydata0^[1,3*j-2]+ydata0^[1,3*j]);
              yresdata0^[1,3*j-2]:=yresdata0^[1,3*j-1];
              yresdata0^[1,3*j]  :=yresdata0^[1,3*j-1];
            end;

            for j:=1 to k do
            begin
              ydata0^[1,j]:=ydata0^[1,j]*scal_fact_1;
              ydata_Byz^[iv,2*l,j]:=ydata0^[1,j];
            end;
            for j:=1 to SW_ianz_a do
            begin
              yresdata_Byz^[iv,2*l,j]:=yresdata0^[1,3*j-1]*scal_fact_1;
            end;
            iyz:=2;
          end;    (* i_field_comp = 2 *)

         { for j:=1 to k do
          begin
            for i:=1 to 2*iv do
            begin
          (*  ydata0^[1,j]:=ydata0^[1,j]*scal_fact_1;
              yresdata0^[1,j]:=yresdata0^[1,j]*scal_fact_1;*)
              ydata_Byz^[i,j]:=ydata_Byz^[i,j]*scal_fact_1;
              yresdata_Byz^[i,j]:=yresdata_Byz^[i,j]*scal_fact_1;
            end;
          end;
          }
          DateTimeTempActualieren;
ChangeWin(WarningsWin);
writeln('plotte aus');

          RePlotDataSW2(iv,iyz,l);
          {REPLOTDATA(3);}
ChangeWin(WarningsWin);
writeln('replotted done');
(*          if (DefineData.SaveStack = 1) and (SW_ianz_b > 1) then
            STWI_write_data_to_stack(AktPoint_STWI);
*)
 ZURUECK:
          (* go to start position in direction a *)
          if (i<SW_ianz_b)then   (* next scan *)
          begin
            delay(200);
            rweg:=round(-SMSFaktor*(SW_END_a-SW_Start_a)-SW_Dist_a);
            if (i_field_comp = 1) then  SMSRampeDo(1,rweg,SMSR1^,0);
            rweg:=round(-SMSFaktor*(SW_END_a-SW_Start_a));
            if (i_field_comp = 2) then  SMSRampeDo(1,rweg,SMSR1^,0);
            DateTimeTempActualieren;
            (* go to start position in direction b *)
            delay(200);
            rweg:=round(SMSFaktor*SW_dist_b);
            if ((i_field_comp =1) or ((i_field_comp =2) and (idir =1))) then
              SMSRampeDo(2, rweg, SMSR2^,0);
          end;
          DateTimeTempActualieren;
        end;       (* i; end loop over scans *)
      (* go to home position in direction a *)

        delay(200);
        rweg:=round(SMSFaktor*(SW_START_a-0.5*SW_Dist_a));
        if (i_field_comp = 1) then  SMSRampeDo(1,rweg, SMSR1^,0);
          rweg:=round(SMSFaktor*SW_START_a);
        if (i_field_comp = 2) then  SMSRampeDo(1,rweg, SMSR1^,0);
        (* go to home position in direction b *)
        DateTimeTempActualieren;
        delay(200);
        rweg:=round(SMSFaktor*SW_Start_b);
        if ( (i_field_comp = 1) and (SW_ianz_b > 1) ) then
          SMSRampeDo(2,rweg , SMSR2^,0);
        if ( (i_field_comp = 2) ) then
        begin
          if (iinteg = 1) then iiax:=3;
          if (SW_ianz_b = 1) then
          begin
            rweg:= round(-SMSFaktor*SW_Start_b);
            if (idir = 1) then  SMSRampeDo(2,rweg,SMSR2^,0);
            rweg:=round(SMSFaktor* SW_Start_b);
            if (idir = -1) then SMSRampeDo(2,rweg, SMSR2^,0);
          end;
          delay(200);
          if (SW_ianz_b > 1) then
          begin
            rweg:=round(SMSFaktor*(SW_Start_b+0.5*SW_Dist_b));
            if (idir = 1) then SMSRampeDo(2,rweg,SMSR2^,0);
            rweg:=round(SMSFaktor*( SW_Start_b-0.5*SW_Dist_b));
            if (idir = -1) then SMSRampeDo(2,rweg,SMSR2^,0);
          end; {SW_ianz_b>1}
        end;   {i_field_comp=2}
        DateTimeTempActualieren;
{?}(*     if(DefineData.SaveStack = 1) and (SW_ianz_b = 1) then
          STWI_write_data_to_stack(AktPoint_STWI);
*)         (* N O T I C E !!!
          different data format for SW_ianz_b = 1 and SW_ianz_b > 1)
         SW.EXE on VMS works only for the case SW_ianz_b = 1 *)

      end;          (* ifc; end loop over field components *)
      filename:=DVMDataFilename;    (* Speicherung der Einzelfiles ?*)
{      SaveStack_STWI(filename);}   (* 29.10.2003 Keine Speicherung der SW-files mehr*)

      {if (ckk='e') then Write_logfile_STWI;}
    end;            (* l, loop �ber die beiden Messungen *)
    if ckk='v' then
    begin
      ChangeWin(WarningsWin);
      chp:=#255;
      { writeln('Both Data o.k. ?  >y<  >n<  >r<');}
      if (SW_wdhlg=1) then
      begin
        writeln('Beide Daten o.k.? >j<  >n<  >r<');
        repeat chp:=readkey;
          chp:=upcase(chp);
        until (chp='Y') or (chp='J') or (chp='N') or (chp='R');
      end;
      if SW_wdhlg>1 then chp:='Y';
        case chp of
        'Y','J' : begin
              SW2_NUM[2*(iv-1)+MesRep]:=FileNr;
              if (MesRep=2) then SW2_NUM[2*(iv-1)+1]:=FileNr-1;

{29.10.2003}  {StoreDataSingleMeasurement_SW2(iv,MesRep);}

              for j:=1 to SW_ianz_a do
              begin
                yresdata_Byz^[iv,1,j]:=
                   (yresdata_Byz^[iv,1,j]+yresdata_Byz^[iv,3,j])/MesRep;
                yresdata_Byz^[iv,2,j]:=
                   (yresdata_Byz^[iv,2,j]+yresdata_Byz^[iv,4,j])/MesRep;
              end;
            end;
      'N' : begin
              GoParkPos;
              ChangeWin(WarningsWin);
              writeln('�berpr�fe aktuelle Pos.');
              write('dann dr�cke >c<');
              repeat chp:=readkey;
                chp:= upcase(chp);
              until chp='C';
              changeWin(MainScreen);
              goto LOOP1;
            end;
      'R' : begin
              GoParkPos;
              ChangeWin(WarningsWin);
              writeln('�berpr�fe aktuelle Pos.');
              write('dann dr�cke >c<');
              repeat chp:=readkey;
                chp:= upcase(chp);
              until chp='C';
              changeWin(MainScreen);
              if (SMSREF<>2) then SMSREFERENCE;
              goto LOOP1;
            end;
      end;
    end;
    DateTimeTempActualieren;
    if (iv < vier) and (SW_wdhlg=1) then
    begin
      GoParkPos;
      ChangeWin(WarningsWin);
{     writeln(' Turn the Bloc to the ',iv+1,'.Pos');
      write('then press >c<');}
      writeln('Bringe Block in die ',iv+1,'.Pos');
      write('dann dr�cke >c<');
      repeat chp:=readkey;
        chp:=upcase(chp);
      until chp='C';
      changeWin(MainScreen);
    end;  (*iv < vier*)
    DateTimeTempActualieren;
  end;            (* for iv:=1 to vier*)

  if (ckk='v') then
  begin
    StoreDataProc_SW2;
    Write_logfile_STWI2;
  end;
  end; {wdhlg}
  DateTimeTempActualieren;
  GoParkPos;
  changeWin(WarningsWin);
{ writeln(' Ready ! Try a new sample !');
  write(' Sample name ! = >COMMENT< ');}
  writeln('Fertig ! Der n�chste Magnet !');
  write('Eintragen: Neuer Magnet-Name !');
  DateTimeTempActualieren;
DasEnde:
  if chbreak=#3 then
  begin
    chab:=#255;
    changeWin(WarningsWin);
{    writeln(' Measurement aborted !');}
    writeln('Halt !   Abbruch: >A<');
    write('Wdhlg:Satz >S<   Block >B<');
    repeat chab:=readkey;
        chab:=upcase(chab);
    until (chab='A') or (chab='S') or (chab='B');
    changeWin(MainScreen);
    DateTimeTempActualieren;
    DVMReset(1);
    DVMIni(1);
    DateTimeTempActualieren;
    chbreak:=#255;
(*    if chab<>'A' then SMSREFERENCE;*)
    if chab='S' then
    begin
      if (SMSREF<>3) then SMSREFERENCE;
      goto LOOP2;
    end;
    if chab='B' then
    begin
      if (SMSREF<>2) then SMSREFERENCE;
      goto LOOP1;
    end;
    if chab='A' then WarningLeer;
  end;

  DVMReset(1);
  SMSRampClose;
  changeWin(MainScreen);
  DateTimeTempActualieren;
end;   {ihhs6 **** Stretched Wire System 2 ****}


PROCEDURE Start_measurementproc;
var
   delay1,delay2,delay3   : integer;
   OldWin                 : WinCtrlPtrType;
   reduction_factor       : real;

begin

  oldwin:=WinActCtrl;

  delay1:=definedata.delay_1;
  delay2:=definedata.delay_2;
  delay3:=definedata.delay_3;

  reduction_factor:=DefineData.Red_Factor;

  if ihhs = 1 then  ihhs1(OldWin,delay1,delay2,delay3,reduction_factor);
  if ihhs = 6 then  ihhs6(OldWin,delay1,delay2,delay3,reduction_factor,vier);
end;
end.