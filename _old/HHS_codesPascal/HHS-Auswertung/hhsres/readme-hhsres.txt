Extraktion von HelmholtzSpulen-Messdaten aus *.res
wf20.05.2015

!! Im Normalfall sollte jedoch hhs-raw verwendet werden.
!! Darauf sind die Auswerteprozeduren von Johannes abgestimmt.

hhsres.bat:
	Ruft mit Parametern [TYP] und [ORD] das Programm hhsres.exe auf.
	Mehrere Aufrufe von hhsres.exe mit neuen Parametern sind m�glich

hhsres.exe:
	Sucht alle Files der Suchmasken [TYP]*_1.res und [TYP]*_2.res
	Extrahiert daraus die Messwerte,
	Speichert die Output-Files.


1. kopiere hhsres.exe und hhsres.bat in das Tagesverzeichnis

2. editiere hhsres.bat: 
   setze [ORD]=Tagesverzeichnis-Buchstabe und [TYP]=Magnet-Typ
   [TYP]=max. 2 Characters, z.b. AN, AS (Magnet-Typ) oder A1, A2 (Ref-Magnete) 
   [ORD]=max. 1 Character, = Ordner-Buchstabe des Tagesverzeichnisses(A,B,C,...)

   z.B. so:
	set  ORD=B
	set  TYP=AN

3. f�hre hhsres.bat aus: 
	Extrahierung durch Aufruf von: hhsres.exe -t %TYP% -p %ORD%


Output-Files:
	data[TYP][ORD]_1.dat, data[TYP][ORD]_1.rms, 
	data[TYP][ORD]_2.dat, data[TYP][ORD]_2.rms

Spalten in data[TYP][ORD]_1.dat und data[TYP][ORD]_2.dat:
	lfd.Nr 
	MagnetNr 
	Mz 
	My0 
	My90 
	Mx 
	Nordpol effekt
	Suedpol effekt
	| Magnet-Name	

Spalten in data[TYP][ORD]_1.rms und data[TYP][ORD]_2.rms:
	lfd.Nr 
	MagnetNr 
	Temperatur
	Speed0 
	Data0rms 
	Speed0rms 
	Speed90 
	data90rms 
	speed90rms 
	| Magnetname
