.. hhs-tests documentation master file, created by
   sphinx-quickstart on Tue Mar 12 08:37:26 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Welcome to hhs-tests's documentation!
=====================================

This project shows an example of usage of the trigger system using the PMAC together with the Multimeter HP34581A. The idea is to show how to use the trigger system to synchronize the acquisition of the multimeter with the PMAC position.

For that, one needs to understand the projects created in the following repositories:

* `DVM IOC <https://codebase.helmholtz.cloud/messlabor/helmholtzcoil/hp3458a-gpib>`_
* `Trigger IOC <https://codebase.helmholtz.cloud/messlabor/helmholtzcoil/ioc-pmac-trigger>`_
* `Power PMAC Trigger <https://codebase.helmholtz.cloud/messlabor/helmholtzcoil/powerpmac-trigger>`_

Below is the scheme of the project.

.. figure:: ./scheme.png
   :width: 800px
   :align: center

   Project Scheme.

- On the bottom we have the Hardware part with the Unidrive M700, which runs the motor, and the multimeter Hp3458A. The commisioning of the Unidrive M700 with the motor is done via commercial software from the Control Techniques, and it is not explained in the project.
- The Omron CK3E which is PMAC device is the controller of the Unidrive M700, and it is connected to the multimeter via Digital I/O for the TTL 5-0 V trigger. The trigger is done via a PLC program shown in `Power PMAC Trigger: <https://codebase.helmholtz.cloud/messlabor/powerpmac-trigger>`_.
- The Epics box consists of the two IOCs, explained in the projects `DVM IOC: <https://codebase.helmholtz.cloud/messlabor/hp3458a-gpib>`_ and `Trigger IOC: <https://codebase.helmholtz.cloud/messlabor/ioc-pmac-trigger>`_.
- Inside each of these projects, we have the Control Studio User interfaces for monitoring, as well as the ophyd objects for the control of the devices via python, which the current project aims to show.
- Additionally, we have Gitlab CI for the continuous integration of the projects.

Contents
--------

.. toctree:: 
   installation
   usage
   data-analysis
