{$N+}
unit mfm2_ufk;
{03.02.2015 HHS+SW2}
{20.12.1999}
INTERFACE

USES DOS, CRT, Graph, SCALELIB, MBBASE, BESLIB, SDLIB,
          STDLIB, MOUSELIB, VESALIB, WINLIB,
          TPDECL, AUXIO1;

procedure clear_screen(nlines: integer; ncolumns: integer);

function minval(x : real; y : real) : real;
function maxval(x : real; y : real) : real;
function realval (value : integer) : real;
function string2real(s1 : string) : real;

IMPLEMENTATION
(******* some useful procedures ******************************)
function realval (value : integer) : real;
begin
     REALVAL := value;
     end;

(******************************************************)
procedure clear_screen(nlines: integer; ncolumns: integer);
var
   i,j  : integer;
   s1   : char;
   s2   : string;
begin
   s1:=' ';
   s2:='';
   for i:=1 to ncolumns do
     s2:=s2+s1;
   setcursor(0,0);
   for j:=1 to nlines do
     writeln(s2);
end;

(***************************************************)
function minval(x : real; y : real) : real;
begin
minval :=x;
if y < x then
minval := y;
end;
(***************************************************)
function maxval(x : real; y : real) : real;
begin
maxval :=x;
if y > x then
maxval := y;
end;

(****************************************************************)
Function string2real(s1 : string) : real;
var
    i,n,ierr,ic1,isume                : integer;
    c1                                : char;
    isign,itens,itenths,iexp          : integer;
    sign,signe,sum,fact               : real;

begin

    n:=length(s1);

    isign:=1;
    itens:=1;
    itenths:=1;
    iexp:=1;

    sign:=1.0;
    signe:=1.0;
    sum:=0.0;
    isume:=0;
    fact:=1.0;

    for i:=1 to n do
    begin
      c1:=s1[i];
      val(c1,ic1,ierr);

(***** search for leading sign: isign = 1 *)
      if isign = 1 then
      begin
        if (c1 = '+') then
        begin
          sign:=1.0;
          isign:=0;
        end;
        if (c1 = '-') then
        begin
          sign:=-1.0;
          isign:=0;
        end;
        if ( ierr = 0 ) then
        begin
          sign:=1.0;
          isign:=0;
        end;
      end;

(***** digits (tens): itens = 1 **********************)
    if (isign = 0) and (itens = 1) then
    begin
      if (c1 = '.') then
        itens:=0
      else
        if ( ierr = 0 ) then
          sum:=sum*10.0+ic1;
    end;

(***** digits (tenths): iexp = 1 **************************)
    if (itens = 0) and (iexp = 1) then
    begin
      if ( ierr = 0 ) then
        begin
          fact:=fact*0.1;
          sum:=sum+fact*ic1;
        end;
      if (c1 = 'e') or (c1 = 'E') or (c1 = 'd') or (c1 = 'D') then
      begin
        iexp:=0;
      end;
    end;  (* iexp *)

(***** sign of exponent: isigne *******)

    if (iexp = 0) then
    begin

      if (ierr <> 0)then
      begin
        if (c1 = '+') then
          signe:=1.0;
        if (c1 = '-') then
          signe:=-1.0;
      end;

      if (ierr = 0) then
          isume:=isume*10+ic1
    end;

    end;  (** loop over characters **)

    fact:=10.0;
    if isume > 0 then
       begin
       if signe < 0.0 then
           fact:=0.1;
       for i:=1 to isume do
          sum:=sum*fact;
    end;

    string2real:=sign*sum;

    end;




END.