{$N+}
unit mfm2_mak;
{29.06.2000 Manuelle Operationen}
INTERFACE

USES DOS, CRT, Graph, SCALELIB, MBBASE, BESLIB, SDLIB,
          STDLIB, MOUSELIB, VESALIB, WINLIB,
          TPDECL, AUXIO1,
          MFM2_TCK,MFM2_HWK,MFM2_UFK,MFM2_ANK,MFM2_SCK;


procedure ManualLaserProc;
procedure ManualservoProc;

procedure ManualDVMProc;

procedure ManualDACProc;
procedure ManualPrSpeedProc;
procedure PlSpeedProc(iwindow : integer);
procedure ManualPlSpeedProc;
procedure ManualPrPathProc;

procedure PlPathProc(iwindow : integer);
procedure ManualPlPathProc;

procedure ManualSMS1Proc;
procedure ManualSMS2Proc;
procedure ManualMBInc1Proc;
procedure ManualMBInc2Proc;
procedure ManualADCProc;
procedure ManualIntegratorProc;
procedure ManualProc;

IMPLEMENTATION

(************* Manual procedures *********************)

procedure ManualLaserProc;
  CONST ReadStatusID = 1001;
        ReadPosID    = 1002;
        ResetID      = 1003;
        TMID         = 1004;
        firstID      = ReadStatusID;
        lastID       = TMID;
var  ManualLaserWinLayOut              : WinDesType;
     OldWin,ManualLaserWin             : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent       : WORD;
     ystring        : string[20];
     s1             : string[1];
     n,i            : integer;
     nlines,ncolumns : integer;
begin

  nlines:=11;
  ncolumns:=37;

  OldWin:=WinActCtrl;

  ManualLaserWinLayOut := WinDefDes;
  WinSetHeader(ManualLaserWinLayOut,LightGray,
               C_LightBlue+'Read Out Laser Interferometer');
  WinSetFillColor(ManualLaserWinLayOut, yellow);
  WinSetOpenMode(ManualLaserWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=3*width div 2; ypos:=110;
  WinHeight:= 9*height;
  ManualLaserwin:=OpenWin(xpos,ypos, xpos+2*width,
                                  ypos+WinHeight,ManualLaserWinLayOut);

  ChangeWin(ManualLaserWin);
  SetMargin(5,LeftMargin);

  xpos:=15;
  ypos:=180;
  MoveTo(xpos,ypos);
  ButtonDefDes.Frame^.ActivBack := Green;
  ShowButtonObject('Position',8, ReadPosID, ButtonLeftEvent, ALT_S,
                                 local, NilEvProc);

  xpos:=100;
  ypos:=180;
  MoveTo(xpos,ypos);
  ButtonDefDes.Frame^.ActivBack := Green;
  ShowButtonObject('Status',8, ReadStatusID, ButtonLeftEvent, ALT_S,
                               local, NilEvProc);

  xpos:=185;
  ypos:=180;
  MoveTo(xpos,ypos);
  ButtonDefDes.Frame^.ActivBack := Red;
  ShowButtonObject('Reset',6, ResetID, ButtonLeftEvent, ALT_S,
                              local, NilEvProc);

  xpos:=255;
  ypos:=180;
  MoveTo(xpos,ypos);
  ButtonDefDes.Frame^.ActivBack := LightBlue;
  ShowButtonObject('TM',3, TMID, ButtonLeftEvent, ALT_S, local, NilEvProc);

  repeat
   locEvent:=WaitOfEvent(firstID,lastID);
   case locEvent of
      ReadPosID    : BEGIN
            if ifirstlaser = 1 then
            begin
            laserini;
            ifirstlaser:=0;
            end;

    clear_screen(nlines,ncolumns);
    SetCursor(0,1);

    wr_text := 'XPOS?';
    IEEE_write(LI,wr_text);

    n:=20;
    IEEE_read(LI,n);
    writeln(' Xpos (mm) = ',(length_cal*string2real(rd_text)):12:6);
    writeln(' ');

(**************************************)
    wr_text := 'YPOS?';
    IEEE_write(LI,wr_text);

    n:=20;
    IEEE_read(LI,n);
    writeln(' Ypos (mm) = ',(length_cal*string2real(rd_text)):12:6);
    writeln(' ');

(***********************************)
    wr_text := 'ZPOS?';
    IEEE_write(LI,wr_text);

    n:=20;
    IEEE_read(LI,n);
    writeln(' Zpos (mm) = ',(length_cal*string2real(rd_text)):12:6);
    writeln(' ');

(***********************************)
    wr_text := 'WPOS?';
    IEEE_write(LI,wr_text);

    n:=20;
    IEEE_read(LI,n);
    writeln(' Wpos (mm) = ',(length_cal*string2real(rd_text)*
                                       straightness_cal):12:6);
    writeln(' ');

(************************************)
   end; (* read Position *)
(************************************)
   ReadStatusID    : BEGIN
            if ifirstlaser = 1 then
            begin
            laserini;
            ifirstlaser:=0;
            end;

    clear_screen(nlines,ncolumns);
    SetCursor(0,1);

    wr_text := 'XSTA?';
    IEEE_write(LI,wr_text);

    n:=3;
    IEEE_read(LI,n);
    writeln(' XStatus = ',rd_text);
    writeln(' ');

(**********************************)
    wr_text := 'YSTA?';
    IEEE_write(LI,wr_text);

    n:=3;
    IEEE_read(LI,n);
    writeln(' YStatus = ',rd_text);
    writeln(' ');

(**************************************)
    wr_text := 'ZSTA?';
    IEEE_write(LI,wr_text);

    n:=3;
    IEEE_read(LI,n);
    writeln(' ZStatus = ',rd_text);
    writeln(' ');

(**************************************)
    wr_text := 'WSTA?';
    IEEE_write(LI,wr_text);

    n:=3;
    IEEE_read(LI,n);
    writeln(' WStatus = ',rd_text);
    writeln(' ');

(******************************************)
    end;  (* ReadStatus *)
(******************************************)

    ResetID: Begin
          laserini;
          end;

    TMID: Begin
          TransparentMode(2);
          end;

    end;

  until locEvent=0;

  ChangeWin(OldWin);
  CloseWin(ManualLaserwin);
END;

(*****************************************************)
procedure JoyStickProc;

  CONST LinVel1ID = 1001;
        LinVel2ID = 1002;
        LinVel3ID = 1003;
        LinVel4ID = 1004;
        applyID   = 1005;
        firstID   = LinVel1ID;
        lastID    = applyID;

 VAR ManualJSWinLayOut                : WinDesType;
     OldWin,ManualJSWin               : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent                    : WORD;
     SaveSteps, ManSteps         : longint;

 BEGIN

  OldWin:=WinActCtrl;

  ManualJSWinLayOut := WinDefDes;
  WinSetHeader(ManualJSWinLayOut,LightGray,
               C_LightBlue+'Define Joy Stick Velocities');
  WinSetFillColor(ManualJSWinLayOut, LightGreen);
  WinSetOpenMode(ManualJSWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=7*width div 10; ypos:=80;
  WinHeight:= 6*height;
  ManualJSwin:=OpenWin(xpos,ypos, xpos+18*width div 8,
                                  ypos+WinHeight,ManualJSWinLayOut);
  ChangeWin(ManualJSWin);
  SetMargin(5,LeftMargin);

  SetCursor(0,1);
  ShowLineObject('     Linear Velocity x (mm/s) = ',42, LinVel1ID,
                       ButtonLeftEvent, NoKey, Local,NilEvProc);
  WRITELN(ServoData.JS_velocity.x:8:2);
  ShowLineObject('     Linear Velocity y (mm/s) = ',42, LinVel2ID,
                       ButtonLeftEvent, NoKey, Local,NilEvProc);
  WRITELN(ServoData.JS_velocity.y:8:2);
  ShowLineObject('     Linear Velocity z (mm/s) = ',42, LinVel3ID,
                       ButtonLeftEvent, NoKey, Local,NilEvProc);
  WRITELN(ServoData.JS_velocity.z:8:2);
  ShowLineObject('Linear Velocity theta (deg/s) = ',42, LinVel4ID,
                       ButtonLeftEvent, NoKey, Local,NilEvProc);
  WRITELN(ServoData.JS_velocity.theta:8:2);

  xpos:=60;
  ypos:=100;

  MoveTo(xpos,ypos);
  ButtonDefDes.Frame^.ActivBack := LightBlue;
  ShowButtonObject('apply',20, applyID, ButtonLeftEvent, ALT_S,
                               local, NilEvProc);

  repeat
   locEvent:=WaitOfEvent(firstID,lastID);
   case locEvent of
      LinVel1ID    : BEGIN
            SetCursor(32,1);
            ServoData.JS_velocity.x :=
                     Editreal(ServoData.JS_velocity.x,-200,200,8,2);
            SetCursor(32,1);
            WRITELN(ServoData.JS_velocity.x:8:2);
                  END;
      LinVel2ID    : BEGIN
            SetCursor(32,2);
            ServoData.JS_velocity.y :=
                     Editreal(ServoData.JS_velocity.y,-20,20,8,2);
            SetCursor(32,2);
            WRITELN(ServoData.JS_velocity.y:8:2);
                  END;
      LinVel3ID    : BEGIN
            SetCursor(32,3);
            ServoData.JS_velocity.z :=
                     Editreal(ServoData.JS_velocity.z,-20,20,8,2);
            SetCursor(32,3);
            WRITELN(ServoData.JS_velocity.z:8:2);
                  END;
      LinVel4ID    : BEGIN
            SetCursor(32,4);
            ServoData.JS_velocity.theta :=
                     Editreal(ServoData.JS_velocity.theta,-5,5,8,2);
            SetCursor(32,4);
            WRITELN(ServoData.JS_velocity.theta:8:2);
                  END;
      applyID : BEGIN
            JoyStick(1);
                  END;
   END;
  until locEvent=0;;
  ChangeWin(OldWin);
  CloseWin(ManualJSWin);
END;

(*****************************************************)

PROCEDURE ManualservoProc;
  CONST DistanceID = 1001;
        VelocityID = 1002;
        TMID       = 1003;
        TERHOSID   = 1004;
        POSID      = 1005;
        GoID       = 1006;
        ReAbID     = 1007;
        xID        = 1008;
        yID        = 1009;
        zID        = 1010;
        thetaID    = 1011;
        JSID       = 1012;
        FlipID     = 1013;
        OneID      = 1014;
        TwoID      = 1015;
        OnePTwoID  = 1016;
        OneMTwoID  = 1017;
        StopID     = 1018;
        RefxID     = 1019;
        RefyID     = 1020;
        RefzID     = 1021;
        RefthetaID = 1022;
        firstID    = DistanceID;
        lastID     = RefthetaID;

 VAR ManualservoWinLayOut              : WinDesType;
     OldWin,ManualServoWin             : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent                          : WORD;
     read_li,n                         : integer;
     nlines,ncolumns                   : integer;
     onoff                             : integer;
     axisnr                            : integer;
     dista,velo                        : real;
     i                                 : integer;
     wr_string                         : string;
 BEGIN
  if (ihhs = 6) then EXIT;
  if (ihhs = 2) or (ihhs = 3) or (ihhs = 5) then
  begin
  iterhos:=1;
  SetTerHos;
  axisnr:=1;
  dista:=ServoData.distance.x;
  velo:=ServoData.velocity.x;
  end;

  if (ihhs = 4) then
  begin
    iOneTwo := 3;   (* move two axes *)
    axisnr:=3;      (* default is z-axis *)
    dista:=ServoData.distance.z;
    velo:=ServoData.velocity.z;
  end;


  read_li:=0;
  nlines:=6;
  ncolumns:=37;

  OldWin:=WinActCtrl;

  ManualServoWinLayOut := WinDefDes;
  WinSetHeader(ManualServoWinLayOut,LightGray,
               C_LightBlue+'Manual Servo Motor Operation');
  WinSetFillColor(ManualServoWinLayOut, yellow);
  WinSetOpenMode(ManualServoWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=3*width div 2 - 30 ; ypos:=90;
  WinHeight:= 11*height;
  ManualServowin:=OpenWin(xpos,ypos, xpos+2*width,
                                  ypos+WinHeight,ManualServoWinLayOut);

  ChangeWin(ManualServoWin);
  SetMargin(5,LeftMargin);

  SetCursor(0,1);
  writeln('     x-position : ');
  writeln('     y-position : ');
  writeln('     z-position : ');
  writeln(' theta-position : ');
  writeln(' ');

  ShowLineObject('   rel. distance (mm) = ',35, distanceID,
                     ButtonLeftEvent, NoKey, Local,NilEvProc);
  WRITELN(dista:11:3);

  ShowLineObject('      velocity (mm/s) = ',35, velocityID,
                     ButtonLeftEvent, NoKey, Local,NilEvProc);
  WRITELN(velo:11:3);

  xpos:=40;
  ypos:=145;
  MoveTo(xpos,ypos);
  ButtonDefDes.Frame^.ActivBack := red;
  ShowButtonObject('X',4, xID, ButtonLeftEvent, ALT_S, local, NilEvProc);

  xpos:=90;
  ypos:=145;
  MoveTo(xpos,ypos);
  ButtonDefDes.Frame^.ActivBack := red;
  ShowButtonObject('Y',4, yID, ButtonLeftEvent, ALT_S, local, NilEvProc);

  xpos:=140;
  ypos:=145;
  MoveTo(xpos,ypos);
  ButtonDefDes.Frame^.ActivBack := red;
  ShowButtonObject('Z',4, zID, ButtonLeftEvent, ALT_S, local, NilEvProc);

  xpos:=190;
  ypos:=145;
  MoveTo(xpos,ypos);
  ButtonDefDes.Frame^.ActivBack := red;
  ShowButtonObject('TH',4, thetaID, ButtonLeftEvent, ALT_S, local, NilEvProc);

  xpos:=240;
  ypos:=145;
  MoveTo(xpos,ypos);
  ButtonDefDes.Frame^.ActivBack := red;
  ShowButtonObject('JS',4, JSID, ButtonLeftEvent, ALT_S, local, NilEvProc);

  xpos:=40;
  ypos:=180;
  MoveTo(xpos,ypos);
  ButtonDefDes.Frame^.ActivBack := Green;
  ShowButtonObject('Go',4, GoID, ButtonLeftEvent, ALT_S, local, NilEvProc);

  xpos:=90;
  ypos:=180;
  MoveTo(xpos,ypos);
  ButtonDefDes.Frame^.ActivBack := LightBlue;
  ShowButtonObject('R-A',4, ReAbID, ButtonLeftEvent, ALT_S, local, NilEvProc);

  xpos:=140;
  ypos:=180;
  MoveTo(xpos,ypos);
  ButtonDefDes.Frame^.ActivBack := LightBlue;
  ShowButtonObject('POS',4, POSID, ButtonLeftEvent, ALT_S, local, NilEvProc);

  xpos:=190;
  ypos:=180;
  MoveTo(xpos,ypos);
  ButtonDefDes.Frame^.ActivBack := LightBlue;
  ShowButtonObject('T-H',4, TERHOSID, ButtonLeftEvent, ALT_S, local, NilEvProc);

  xpos:=240;
  ypos:=180;
  MoveTo(xpos,ypos);
  ButtonDefDes.Frame^.ActivBack := LightBlue;
  ShowButtonObject('TM',4, TMID, ButtonLeftEvent, ALT_S, local, NilEvProc);

  xpos:=40;
  ypos:=215;
  MoveTo(xpos,ypos);
  ButtonDefDes.Frame^.ActivBack := Green;
  ShowButtonObject('Flip',4, FlipID, ButtonLeftEvent, ALT_S, local,
  NilEvProc);

  if (ihhs = 4) then
  begin
    xpos:=90;
    ypos:=215;
    MoveTo(xpos,ypos);
    ButtonDefDes.Frame^.ActivBack := Magenta;
    ShowButtonObject('1',4, OneID, ButtonLeftEvent, ALT_S, local, NilEvProc);

    xpos:=140;
    ypos:=215;
    MoveTo(xpos,ypos);
    ButtonDefDes.Frame^.ActivBack := Magenta;
    ShowButtonObject('2',4, TwoID, ButtonLeftEvent, ALT_S, local, NilEvProc);

    xpos:=190;
    ypos:=215;
    MoveTo(xpos,ypos);
    ButtonDefDes.Frame^.ActivBack := Magenta;
    ShowButtonObject('1+2',4, OnePTwoID, ButtonLeftEvent, ALT_S, local,
    NilEvProc);

    xpos:=240;
    ypos:=215;
    MoveTo(xpos,ypos);
    ButtonDefDes.Frame^.ActivBack := Magenta;
    ShowButtonObject('1-2',4, OneMTwoID, ButtonLeftEvent, ALT_S, local,
    NilEvProc);

    xpos:=40;
    ypos:=250;
    MoveTo(xpos,ypos);
    ButtonDefDes.Frame^.ActivBack := red;
    ShowButtonObject('STOP',4, StopID, ButtonLeftEvent, ALT_S, local,
    NilEvProc);

    xpos:=90;
    ypos:=250;
    MoveTo(xpos,ypos);
    ButtonDefDes.Frame^.ActivBack := red;
    ShowButtonObject('R-x',4, RefxID, ButtonLeftEvent, ALT_S, local,
    NilEvProc);

    xpos:=140;
    ypos:=250;
    MoveTo(xpos,ypos);
    ButtonDefDes.Frame^.ActivBack := red;
    ShowButtonObject('R-y',4, RefyID, ButtonLeftEvent, ALT_S, local,
    NilEvProc);

    xpos:=190;
    ypos:=250;
    MoveTo(xpos,ypos);
    ButtonDefDes.Frame^.ActivBack := red;
    ShowButtonObject('R-z',4, RefzID, ButtonLeftEvent, ALT_S, local,
    NilEvProc);

    xpos:=240;
    ypos:=250;
    MoveTo(xpos,ypos);
    ButtonDefDes.Frame^.ActivBack := red;
    ShowButtonObject('R-t',4, RefthetaID, ButtonLeftEvent, ALT_S, local,
    NilEvProc);
  end;

  if (ihhs = 2) or (ihhs = 3) or (ihhs = 5) then
  begin
    SetCursor(10,5);
    write(' x ');
  end;
  if (ihhs = 4) then
  begin
    SetCursor(10,5);
    write(' z ');
    SetCursor(20,5);
    if(ionetwo = 1) then write(' 1 ');
    if(ionetwo = 2) then write(' 2 ');
    if(ionetwo = 3) then write('1+2');
    if(ionetwo = 4) then write('1-2');
  end;

  repeat
    locEvent:=WaitOfEvent(firstID,lastID);
    case locEvent of
      xID     : BEGIN
                  if (ihhs = 2) or (ihhs = 3) or (ihhs = 5) then
                  begin
                    if ifirstservo = 1 then
                    begin
                      ServoIni;
                      ifirstservo:=0;
                    end;
                    ijoystick:=0;
                    Joystick(0);

                    wr_string:='@W1';
                    RS232_write(COM1,wr_string,iterhos,0);
                    wr_string:='SXMO0';
                    RS232_write(COM1,wr_string,iterhos,0);
                    wr_string:='SYMO0';
                    RS232_write(COM1,wr_string,iterhos,0);

                    wr_string:='@W2';
                    RS232_write(COM1,wr_string,iterhos,0);
                    wr_string:='SXMO0';
                    RS232_write(COM1,wr_string,iterhos,0);

                    wr_string:='@W0';
                    RS232_write(COM1,wr_string,iterhos,0);
                  end;

                  if (ihhs = 4) then
                  begin
                    if ifirstSTWI = 1 then
                    begin
                      STWIIni;
                      ifirstSTWI:=0;
                    end;
                  end;

                  axisnr := 1;
                  SetCursor(24,6);
                  writeln(ServoData.distance.x:10:2);
                  SetCursor(24,7);
                  writeln(ServoData.velocity.x:10:2);
                  dista:=ServoData.distance.x;
                  velo:=ServoData.velocity.x;

                  SetCursor(10,5);
                  write(' x ');
                END;   (*xiD*)

      yID     : BEGIN
                  if (ihhs = 2) or (ihhs = 3) or (ihhs = 5) then
                  begin
                    if ifirstservo = 1 then
                    begin
                      ServoIni;
                      ifirstservo:=0;
                    end;
                    ijoystick:=0;
                    Joystick(0);
                  end;

                  if (ihhs = 4) then
                  begin
                    if ifirstSTWI = 1 then
                    begin
                      STWIIni;
                      ifirstSTWI:=0;
                    end;
                  end;

                  axisnr := 2;
                  SetCursor(24,6);
                  writeln(ServoData.distance.y:10:2);
                  SetCursor(24,7);
                  writeln(ServoData.velocity.y:10:2);
                  dista:=ServoData.distance.y;
                  velo:=ServoData.velocity.y;
                  SetCursor(10,5);
                  write(' y ');
                END; (*yiD*)

      zID     : BEGIN
                  if (ihhs = 2) or (ihhs = 3) or (ihhs = 5) then
                  begin
                    if ifirstservo = 1 then
                    begin
                      ServoIni;
                      ifirstservo:=0;
                    end;
                    ijoystick:=0;
                    Joystick(0);
                  end;

                  if (ihhs = 4) then
                  begin
                    if ifirstSTWI = 1 then
                    begin
                      STWIIni;
                      ifirstSTWI:=0;
                    end;
                  end;
                  axisnr := 3;
                  SetCursor(24,6);
                  writeln(ServoData.distance.z:10:2);
                  SetCursor(24,7);
                  writeln(ServoData.velocity.z:10:2);
                  dista:=ServoData.distance.z;
                  velo:=ServoData.velocity.z;
                  SetCursor(10,5);
                  write(' z ');
                END; (*ziD*)

      thetaID : BEGIN
                  if (ihhs = 2) or (ihhs = 3) or (ihhs = 5) then
                  begin
                    if ifirstservo = 1 then
                    begin
                      ServoIni;
                      ifirstservo:=0;
                    end;
                    ijoystick:=0;
                    Joystick(0);
                  end;

                  if (ihhs = 4) then
                  begin
                    if ifirstSTWI = 1 then
                    begin
                      STWIIni;
                      ifirstSTWI:=0;
                    end;
                  end;

                  axisnr := 4;
                  SetCursor(24,6);
                  writeln(ServoData.distance.theta:10:2);
                  SetCursor(24,7);
                  writeln(ServoData.velocity.theta:10:2);
                  dista:=ServoData.distance.theta;
                  velo:=ServoData.velocity.theta;
                  SetCursor(10,5);
                  write(' th');
                END; (*thetaiD*)

      JSID    : BEGIN
                  if (ihhs = 2) or (ihhs = 3) or (ihhs = 5) then
                  begin
                    if ifirstservo = 1 then
                    begin
                      ServoIni;
                      ifirstservo:=0;
                    end;
                    ijoystick:=1;
                    JoyStick(1);
                    JoyStickProc;
                     (*     Joystick(0);  *)
                  end;

                  if (ihhs = 4) then
                  begin
                    (* not yet implemented *)
                  end;

                END; (*JSID*)
      ReAbID  : BEGIN
                (* 1 : relative positioning
                   2 : absolute positioning
                   not yet implemented *)
                  if (ihhs=2) or (ihhs=3) or (ihhs=5) then
                  begin
                    if ServoData.i_rel_abs = 1 then
                      ServoData.i_rel_abs:=2;
                    if ServoData.i_rel_abs = 2 then
                      ServoData.i_rel_abs:=1;
                  end;

                  if (ihhs=4) then
                  begin
                    if(ServoData.i_rel_abs = 1)then
                    begin
                      ServoData.i_rel_abs:=2;
                      wr_text:='ABSOL';
                    end;
                    if(ServoData.i_rel_abs = 2)then
                    begin
                      ServoData.i_rel_abs:=1;
                      wr_text:='RELAT';
                    end;
                    IEEE_write(SW1,wr_text);
                    delay(100);
                    IEEE_write(SW2,wr_text);
                    delay(100);
                  end;
                END;  (*ReAbID*)

    distanceID: BEGIN
                  SetCursor(24,6);
                  dista := Editreal(dista,-5400,5400,11,3);
                  if axisnr = 1 then ServoData.distance.x:=dista;
                  if axisnr = 2 then  ServoData.distance.y:=dista;
                  if axisnr = 3 then ServoData.distance.z:=dista;
                  if axisnr = 4 then ServoData.distance.theta:=dista;
                  SetCursor(24,6);
                  WRITELN(dista:11:3);
                END;

    velocityID: BEGIN
                  SetCursor(24,7);
                  velo := Editreal(velo,-200,200,11,3);
                  if axisnr = 1 then  ServoData.velocity.x:=velo;
                  if axisnr = 2 then  ServoData.velocity.y:=velo;
                  if axisnr = 3 then  ServoData.velocity.z:=velo;
                  if axisnr = 4 then  ServoData.velocity.theta:=velo;
                  SetCursor(24,7);
                  WRITELN(velo:11:3);
                END;

      GoID    : BEGIN
                  if (ihhs = 2) or (ihhs = 3) or (ihhs = 5) then
                  begin
                    if ifirstservo = 1 then
                    begin
                      ServoIni;
                      ifirstservo:=0;
                    end;
                    ServoDrive(axisnr,1,1,dista,velo);
                  end;

                  if (ihhs = 4) or (ihhs = 6) then
                    ServoDriveSTWI(axisnr,1,iOneTwo,dista,velo);

                  if definedata.autoreverse = 1 then
                  begin
                    dista:=-dista;
                    SetCursor(24,6);
                    WRITELN(dista:10:2);
                    if axisnr = 1 then
                      ServoData.distance.x:=-ServoData.distance.x;
                    if axisnr = 2 then
                      ServoData.distance.y:=-ServoData.distance.y;
                    if axisnr = 3 then
                      ServoData.distance.z:=-ServoData.distance.z;
                    if axisnr = 4 then
                      ServoData.distance.theta:=-ServoData.distance.theta;
                  end;     (* autoreverse *)

                END;  (*GOID*)

      FlipID  : BEGIN
                  if (ihhs = 2) or (ihhs = 3) or (ihhs = 5) then
                  begin
                    if ifirstservo = 1 then
                    begin
                      ServoIni;
                      ifirstservo:=0;
                    end;
                    ijoystick:=0;
                    Joystick(0);

                    axisnr := 2;
                    SetCursor(24,6);
                    writeln(ServoData.distance.y:10:2);
                    SetCursor(24,7);
                    writeln(ServoData.velocity.y:10:2);
                    dista:=ServoData.distance.y;
                    velo:=ServoData.velocity.y;

                    ServoDrive(axisnr,1,1,dista,velo);
                    if definedata.autoreverse = 1 then
                    begin
                      dista:=-dista;
                      SetCursor(24,6);
                      WRITELN(dista:10:2);
                      ServoData.distance.y:=-ServoData.distance.y;
                    end;

                    delay(20000);
                    delay(20000);

                    axisnr := 4;
                    SetCursor(24,6);
                    writeln(ServoData.distance.theta:10:2);
                    SetCursor(24,7);
                    writeln(ServoData.velocity.theta:10:2);
                    dista:=ServoData.distance.theta;
                    velo:=ServoData.velocity.theta;

                    ServoDrive(axisnr,1,1,dista,velo);
                    if definedata.autoreverse = 1 then
                    begin
                      dista:=-dista;
                      SetCursor(24,6);
                      WRITELN(dista:10:2);
                      ServoData.distance.theta:=-ServoData.distance.theta;
                    end;
                  end;
                END;     (*FLIPID*)

      TMID    : BEGIN
                  if (ihhs = 2) or (ihhs = 3) or (ihhs = 5) then
                  begin
                    if ifirstservo = 1 then
                    begin
                      ServoIni;
                      ifirstservo:=0;
                    end;
                    TransparentMode(1);
                  end;

                  if (ihhs = 4) then
                  begin
                    if (ifirstSTWI=1) then
                    begin
                      SWini(1);
                      SWini(2);
                      ifirstSTWI:=0;
                    end;
                    if (axisnr=1) or (axisnr=4) then
                      TransparentMode(7);
                    if (axisnr=2) or (axisnr=3) then
                      TransparentMode(6);
                  end;
                END;   (*TMID*)

      TERHOSID: BEGIN
                  if (ihhs = 2) or (ihhs = 3) or (ihhs = 5) then
                  begin
                    if ifirstservo = 1 then ServoIni;
                    if iterhos = 1 then
                    begin
                      iterhos :=0;
                      SetTerHos;
                    end
                    else
                    begin
                      iterhos := 1;
                      SetTerHos;
                    end;
                    writeln(iterhos);
                  end;
                END;  (*TERHOSID*)

        POSID : BEGIN
                  SetCursor(0,1);
                  writeln('     x-position : ');
                  writeln('     y-position : ');
                  writeln('     z-position : ');
                  writeln(' theta-position : ');
                  writeln(' ');
                END;

        OneID : BEGIN
                  iOneTwo :=1;
                  SetCursor(20,5);
                  write(' 1 ');
                END;
        TwoID : BEGIN
                  iOneTwo :=2;
                  SetCursor(20,5);
                  write(' 2 ');
                END;
    OnePTwoID : BEGIN
                  iOneTwo :=3;
                  SetCursor(20,5);
                  write('1+2');
                END;
    OneMTwoID : BEGIN
                  iOneTwo :=4;
                  SetCursor(20,5);
                  write('1-2');
                END;
       StopID : BEGIN
                  wr_text:='STOP';
                  IEEE_write(SW1,wr_text);
                  delay(100);
                  IEEE_write(SW2,wr_text);
                END;
       RefxID : BEGIN
                  ServoRef(3,1);
                END;
       RefyID : BEGIN
                  ServoRef(3,2);
                END;
       RefzID : BEGIN
                  ServoRef(3,3);
                END;
   RefthetaID : BEGIN
                  ServoRef(3,4);
                END;
    END;   (*case local event*)
  until locEvent=0;

  ChangeWin(OldWin);
  if ijoystick = 1 then
  begin
    ChangeWin(WarningsWin);
    writeln;
    write(' stay in JS-Mode (1) ? ');
    readln(i);
    if i <> 1 then
    begin
      joystick(0);
      ijoystick:=0;
    end;
  end;
  ChangeWin(OldWin);
  CloseWin(ManualServowin);
END;

(*****************************************************)
procedure ManualDVMProc;
  CONST TM1ID       = 1001;
        TM2ID       = 1002;
        TM3ID       = 1003;
        firstID    = TM1ID;
        lastID     = TM3ID;

 VAR ManualDVMWinLayOut              : WinDesType;
     OldWin,ManualDVMWin             : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent                          : WORD;

 BEGIN

  OldWin:=WinActCtrl;

  ManualDVMWinLayOut := WinDefDes;
  WinSetHeader(ManualDVMWinLayOut,LightGray,
               C_LightBlue+'Manual DVM Operation');
  WinSetFillColor(ManualDVMWinLayOut, yellow);
  WinSetOpenMode(ManualDVMWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=3*width div 2; ypos:=150;
  WinHeight:= 5*height;
  ManualDVMwin:=OpenWin(xpos,ypos, xpos+2*width,
                                  ypos+WinHeight,ManualDVMWinLayOut);

  ChangeWin(ManualDVMWin);
  SetMargin(5,LeftMargin);
  SetCursor(0,1);

  xpos:=70;
  ypos:=80;
  MoveTo(xpos,ypos);
  ButtonDefDes.Frame^.ActivBack := LightBlue;
  ShowButtonObject('TM1',4, TM1ID,
                            ButtonLeftEvent, ALT_S, local, NilEvProc);

  xpos:=150;
  ypos:=80;
  MoveTo(xpos,ypos);
  ButtonDefDes.Frame^.ActivBack := LightBlue;
  ShowButtonObject('TM2',4, TM2ID,
                            ButtonLeftEvent, ALT_S, local, NilEvProc);

  xpos:=230;
  ypos:=80;
  MoveTo(xpos,ypos);
  ButtonDefDes.Frame^.ActivBack := LightBlue;
  ShowButtonObject('TM3',4, TM3ID,
                            ButtonLeftEvent, ALT_S, local, NilEvProc);

  repeat
   locEvent:=WaitOfEvent(firstID,lastID);
   case locEvent of
      TM1ID : BEGIN
             TransparentMode(3);
             END;
      TM2ID : BEGIN
             TransparentMode(4);
             END;
      TM3ID : BEGIN
             TransparentMode(5);
             END;
   END;
  until locEvent=0;
  ChangeWin(OldWin);
  CloseWin(ManualDVMwin);
  end;

(*****************************************************)

procedure ManualDACProc;
  CONST EndVoltageID = 1001;
        GoID      = 1002;
        firstID  = EndVoltageID;
        lastID   = GoID;

 VAR ManualDACWinLayOut                    : WinDesType;
     OldWin,ManualDACWin                   : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent                          : WORD;
     startvolt                         : real;
     endvolt                           : real;

 BEGIN

  OldWin:=WinActCtrl;

  ManualDACWinLayOut := WinDefDes;
  WinSetHeader(ManualDACWinLayOut,LightGray,
               C_LightBlue+'Manual DAC Operation');
  WinSetFillColor(ManualDACWinLayOut, yellow);
  WinSetOpenMode(ManualDACWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=3*width div 2; ypos:=150;
  WinHeight:= 5*height;
  ManualDACwin:=OpenWin(xpos,ypos, xpos+2*width,
                                  ypos+WinHeight,ManualDACWinLayOut);

  ChangeWin(ManualDACWin);
  SetMargin(5,LeftMargin);
  SetCursor(3,1);
  ShowLineObject('   End Voltage (V) = ',29, EndVoltageID, ButtonLeftEvent,
                     NoKey, Local,NilEvProc);
  WRITELN(aktdacvoltage:6:2);

  xpos:=60;
  ypos:=50;

  MoveTo(xpos,ypos);
  ButtonDefDes.Frame^.ActivBack := Green;
  ShowButtonObject('Go',20, GoID, ButtonLeftEvent, ALT_S, local, NilEvProc);

  endvolt:=aktdacvoltage;

  repeat
   locEvent:=WaitOfEvent(firstID,lastID);
   case locEvent of
      EndVoltageID    : BEGIN
            SetCursor(24,1);
            EndVolt := Editreal(EndVolt,0,2,6,2);
            SetCursor(24,1);
            WRITELN(EndVolt:6:2);
                  END;
      GoID : BEGIN
             startvolt:=aktdacvoltage;
             if ifirstdac = 1 then
             begin
             mbopen(slave1);
             DACini;
             ifirstdac:=0;
             end;
             DACDrive(StartVolt,EndVolt,1);
             END;
   END;
  until locEvent=0;;
  ChangeWin(OldWin);
  CloseWin(ManualDACwin);
  end;


(**********************************************************)

procedure ManualPrSpeedProc;

 VAR ManualSpeedWinLayOut              : WinDesType;
     OldWin,ManualSpeedWin               : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent                          : WORD;
     value      : word;
     loctime    : real;
     locfreq    : real;
     out4 	:	Boolean;
     overrun	:	Boolean;
     ii         : integer;
     scaling_factor : real;

 BEGIN

  OldWin:=WinActCtrl;

  ManualSpeedWinLayOut := WinDefDes;
  WinSetHeader(ManualSpeedWinLayOut,LightGray,
               C_LightBlue+'Speed Measurement');
  WinSetFillColor(ManualSpeedWinLayOut, yellow);
  WinSetOpenMode(ManualSpeedWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=3*width div 2; ypos:=150;
  WinHeight:= 5*height;
  ManualSpeedwin:=OpenWin(xpos,ypos, xpos+2*width,
                                  ypos+WinHeight,ManualSpeedWinLayOut);

  ChangeWin(ManualSpeedWin);
  SetMargin(5,LeftMargin);

  SetCursor(0,0);
  writeln(' press any key to stop measurement');
  writeln(' (except RETURN)');

  loctime:=0;
  MBInc1Ini;
  MBInc1Start;

  out4:=TRUE;
  ii:=0;
  locfreq:=0.0;

  scaling_factor:=(encoder_const * MBInc1_timebase_factor) /
                  (MBInc1Data.Delay_3 * MBInc1Data.Delay_4);

     Repeat

     if out4 = FALSE then
        begin
        if STC_Status(INC1,STC_Counter4) = TRUE then
           begin
           (** kein neuer Messwert*)
           out4 := TRUE;
           end;
      end;
     if out4 = TRUE then
        begin
        if STC_Status(INC1,STC_Counter4) = FALSE then
           begin
           out4 := FALSE;
           ii:=ii+1;
	   value:=STC_Read(INC1,STC_HoldReg,STC_counter5);
           loctime:=value*scaling_factor;
           if loctime > small_value then
           locfreq:=1.0/loctime;
           writeln(ii,'   ',value);
           WRITELN('      Time (s) ',loctime:6:4);
           WRITELN('Frequency (Hz) ',locfreq:6:4);
           end;
      end;

        SetCursor(0,2);

  until keypressed;

  ChangeWin(OldWin);
  CloseWin(ManualSpeedwin);
  end;

(**********************************************************)

procedure PlSpeedProc(iwindow : integer);

 VAR
     value                : word;
     out4 	          : Boolean;
     overrun	          : Boolean;
     i,ii,jj,iii          : integer;
     a_loc,b_loc          : real;
     ianz_loc             : integer;
     delta_i              : integer;
     factor               : real;
     ierr                 : integer;
     OldWin               : WinCtrlPtrType;
     scaling_factor       : real;
     offset               : integer;
     speed_local          : real;
     scale_x              : real;
BEGIN
  OldWin:=WinActCtrl;
  changewin(warningswin);

  scale_x:=MbInc1Data.Delay_3/encoder_const;

  scaling_factor:=(encoder_const * MBInc1_timebase_factor) /
                  (MBInc1Data.Delay_3 * MBInc1Data.Delay_4);

  speed_local:=aktdacvoltage*DacData.speed_const;

  out4:=FALSE;
  ii:=0;
  iii:=0;
  if iwindow = 1 then
  begin
    ianz_loc:=ianzdata0[2];
    for i:=1 to ianz_loc do xdata0^[i]:=realval(i)*scale_x;
    end
    else
    begin
       ianz_loc:=ianzdata90[2];
       for i:=1 to ianz_loc do xdata90^[i]:=realval(i)*scale_x;
    end;

  Repeat
{writeln('repeat PlSpeedProc ii: ',ii,' ',ianz_loc);}
    if out4 = FALSE then
    begin
      if STC_Status(INC1,STC_Counter4) = TRUE then
      begin
           out4:=TRUE;
           (* kein neuer Messwert *)
      end;
    end;

    if out4 = TRUE then
    begin
      if STC_Status(INC1,STC_Counter4) = FALSE then
      begin
        out4 := FALSE;
        if iii<1 then    (*skip first data point*)
          iii:=iii+1
        else
        begin
          ii:=ii+1;
	  value:=STC_Read(INC1,STC_HoldReg,STC_counter5);
          if iwindow = 1 then
          begin
            if value > small_value then
              ydata0^[2,ii]:=1.0/(value*scaling_factor)
            else
              ydata0^[2,ii]:=speed_local;
          end
          else
          begin
            if value > small_value then
              ydata90^[2,ii]:=1.0/(value*scaling_factor)
            else
              ydata90^[2,ii]:=speed_local;
          end;
        end;
      end;
    end; {out4=true}
  until (ii=ianz_loc);

  if dacdata.endvoltage < 0 then
  begin
    if iwindow = 1 then
    begin
      for ii:=1 to ianz_loc do
        ydata0^[2,ii]:=-ydata0^[2,ii];
      end
    else
    begin
      for ii:=1 to ianz_loc do
        ydata90^[2,ii]:=-ydata90^[2,ii];
      end
    end;

    a_loc:=0;
    b_loc:=ianz_loc;
    for ii:=1 to ianz_loc do
    begin
      if iwindow = 1 then
        a_loc:=a_loc+ydata0^[2,ii]
      else
        a_loc:=a_loc+ydata90^[2,ii];
    end;
    a_loc:=a_loc/b_loc;

    if iwindow = 1 then
      speed0:=a_loc
    else
      speed90 :=a_loc;

    for ii:=1 to ianz_loc do
    begin
      if iwindow = 1 then
      begin
        yresdata0^[2,ii]:=ydata0^[2,ii]-a_loc;
      end
      else
      begin
        yresdata90^[2,ii]:=ydata90^[2,ii]-a_loc;
      end;
    end;

    changewin(oldwin);

    changewin(speedwin);
    setcursor(1,1);
    if iwindow = 1 then
    begin
      writeln;write(speed0:5:3);
    end
    else
    begin
      writeln;write(speed90:5:3);
    end;

    changewin(oldwin);

(******** expand data set to get equidistant grid with channel 1***)

    ierr:=0;

    delta_i:=MBInc1Data.Delay_4;

    for ii:=ianz_loc downto 1 do
    begin
      if iwindow = 1 then
      begin
        for jj:=(delta_i*ii) downto (delta_i*ii-delta_i+1) do
        begin
          ydata0^[2,jj]:=ydata0^[2,ii];
          yresdata0^[2,jj]:=yresdata0^[2,ii];
        end;
      end
      else
      begin
        for jj:=(delta_i*ii) downto (delta_i*ii-delta_i+1) do
        begin
          ydata90^[2,jj]:=ydata90^[2,ii];
          yresdata90^[2,jj]:=yresdata90^[2,ii];
        end;
      end;
    end;  {ii}

    ianzdata0[2]:=delta_i*ianzdata0[2];
    if ianzdata0[2] <> ianzdata0[2] then ierr := 1;
    ianzdata90[2]:=delta_i*ianzdata90[2];
    if ianzdata90[2] <> ianzdata90[2] then ierr := 1;

    if ierr = 1 then
    begin
      OldWin:=WinActCtrl;
      changewin(WarningsWin);
      writeln('ianzdata0[1/2] =',ianzdata0[1],' ',ianzdata0[2]);
      writeln('ianzdata90[1/2]=',ianzdata90[1],' ',ianzdata90[2]);
      changewin(oldwin);
    end;
    analysisproc;
    changewin(oldwin);
end;

(**********************************************************)
procedure ManualPlSpeedProc;
begin
     MBInc1Ini;
     MBInc1start;
     PlSpeedProc(1);
     scaledata.autoscale0:=1;
     scaledata.dispchannel0:=2;
     replotdata(1);
end;

(**********************************************************)
procedure ManualSMS1Ini;
begin
     SMS1Ini;
end;

(**********************************************************)
procedure ManualSMS2Ini;
begin
     SMS2Ini;
end;

(**********************************************************)
procedure ManualPrPathProc;

 VAR ManualPathWinLayOut                : WinDesType;
     OldWin,ManualPathWin               : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent                           : WORD;
     value          : word;
     value_1,value_2,value_3 : longint;
     loctime        : real;
     locfreq        : real;
     out4 	    : Boolean;
     overrun	    : Boolean;
     ii             : integer;
     scaling_factor : real;
     itrigger       : integer;

 BEGIN

  OldWin:=WinActCtrl;

  ManualPathWinLayOut := WinDefDes;
  WinSetHeader(ManualPathWinLayOut,LightGray,
               C_LightBlue+'Path Measurement');
  WinSetFillColor(ManualPathWinLayOut, yellow);
  WinSetOpenMode(ManualPathWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=3*width div 2; ypos:=150;
  WinHeight:= 10*height;
  ManualPathwin:=OpenWin(xpos,ypos, xpos+2*width,
                                  ypos+WinHeight,ManualPathWinLayOut);

  ChangeWin(ManualPathWin);
  SetMargin(5,LeftMargin);

  SetCursor(0,0);
  writeln(' press any key to stop measurement');
  writeln(' (except RETURN)');
  writeln('  ');
  writeln(' external Trigger (1) ');
  write(' internal Trigger (0) : ');
  read(itrigger);
  writeln('  ');

  loctime:=0;
  MBInc1Ini;
  MBInc1Start;
{  par32ini;}

  out4:=TRUE;
  ii:=0;
  locfreq:=0.0;

  scaling_factor:=(encoder_const * MBInc1_timebase_factor) /
                  (MBInc1Data.Delay_3 * MBInc1Data.Delay_4);

     Repeat

     if out4 = FALSE then
        begin
        if STC_Status(INC1,STC_Counter4) = TRUE then
           begin
           (** kein neuer Messwert*)
           out4 := TRUE;
           end;
      end;
     if out4 = TRUE then
        begin
        if STC_Status(INC1,STC_Counter4) = FALSE then
           begin

           if itrigger = 1 then
             out4 := FALSE;

           ii:=ii+1;
	   value:=STC_Read(INC1,STC_HoldReg,STC_counter5);
           loctime:=value*scaling_factor;
           if loctime > small_value then
           locfreq:=1.0/loctime;

           writeln(ii,'   ',value);
           value_1:=parin(par1,LongTyp);
           value_2:=parin(par2,LongTyp);
           value_3:=parin(par3,LongTyp);
           WRITELN('      Time (s) ',loctime:6:4);
           WRITELN('Frequency (Hz) ',locfreq:6:4);
           Writeln(' Channel 1 : ',value_1);
           Writeln(' Channel 2 : ',value_2);
           Writeln(' Channel 3 : ',value_3);

           end;
      end;

        SetCursor(0,5);

  until keypressed;

  ChangeWin(OldWin);
  CloseWin(ManualPathwin);
  end;

(***********************************************************)

procedure PlPathProc(iwindow : integer);
(* Routine is called only if ihhs = 2 *)
 VAR
     new_val              : integer;
     L1,L2,L3,L4	  : longint;
     L11,L22,L33,L44      : real;
     gbsp		  : real;
     AktPoint_Las         : PointPtrType_Las;
     value                : word;
     value_long           : longint;
     out4 	          : Boolean;
     overrun	          : Boolean;
     i,ii,jj,iii          : integer;
     a_loc,b_loc          : real;
     ianz_loc             : integer;
     delta_i              : integer;
     factor               : real;
     ierr                 : integer;
     OldWin               : WinCtrlPtrType;
     scaling_factor       : real;
     offset               : integer;
     speed_local          : real;
     scale_x              : real;
     itrigger             : integer;
     itrigext             : integer;
     trig                 : byte;
     trig1                : byte;
     ired,iired		  : integer;
     s1,s2                : string;
     reduction_factor     : integer;
     temp_loc             : real;
     dmmval              : longint;

 BEGIN

  changewin(zerowin);

  if(DefineData.SaveStack = 1)then
  begin
    (*initialize stack for laser data*)
    CreatePoint_Las(Top_Las);
    AktPoint_Las:=Top_Las;
  end;

  if ifirst = 1 then
  begin
    hhsini;
    ifirst:=0;
  end;

{  if ifirstpar32 = 1 then
  begin
    par32ini;
    MBINC1ini;
    MBINC1START;
    MBINC2ini;
    MBINC2START;
    ifirstpar32 := 0;
  end; }

  if ifirstlaser = 1 then
  begin
    laserini;
    ifirstlaser:=0;
  end;

  OldWin:=WinActCtrl;
  ChangeWin(WarningsWin);

  scale_x:=MbInc1Data.Delay_3/encoder_const;

  scaling_factor:=(encoder_const * MBInc1_timebase_factor) /
                  (MBInc1Data.Delay_3 * MBInc1Data.Delay_4);

  if ihhs = 1 then
     speed_local:=aktdacvoltage*DacData.speed_const;
  if ihhs = 2 then
  begin
    if MBINC2Data.Time_base_2 = 1 then factor:=MBINC_Rate_1;
    if MBINC2Data.Time_base_2 = 2 then factor:=MBINC_Rate_2;
    if MBINC2Data.Time_base_2 = 3 then factor:=MBINC_Rate_3;
    if MBINC2Data.Time_base_2 = 4 then factor:=MBINC_Rate_4;
    if MBINC2Data.Time_base_2 = 5 then factor:=MBINC_Rate_5;
    speed_local:= realval(MBINC1Data.Delay_3)*
                   realval(MBINC2Data.Delay_2)*
                   realval(MBINC2Data.Delay_5);
    speed_local:=speed_local/factor;
  end;

  itrigger:=1;  (***** in Menue aufnehmen *)

  out4:=FALSE;
  ii:=0;
  iii:=0;

  itrigext:=1;
  if itrigext = 0 then
  begin
  (******** PAR 32: interner Trigger *************)
    dmmstartconvertion(dmm1);
    Repeat
      if out4 = FALSE then
      begin
        if STC_Status(INC1,STC_Counter4) = TRUE then
        begin
          out4:=TRUE;
            (* kein neuer Messwert *)
        end;
      end;

      if out4 = TRUE then
      begin
        if STC_Status(INC1,STC_Counter4) = FALSE then
        begin
          if itrigger = 1 then out4 := FALSE;

          if iii<1 then    (*skip first data point*)
            iii:=iii+1
          else
          begin
            ii:=ii+1;
	    value:=STC_Read(INC1,STC_HoldReg,STC_counter5);
            if value > small_value then
              gbsp:=1.0/(value*scaling_factor)
            else
              gbsp:=speed_local;

{            read_par32_1234(L1, L2, L3, L4);}
          end; (*iii<1*)

        end;   (*STC_Status(INC1,STC_Counter4) = FALSE*)
      end;     (* if out4 = TRUE*)
    until (ii=DVM1Data.Readings);
  end;        (* end internal trigger *)

  if itrigext = 1 then
  begin
    ii:=0;
    jj:=0;
    (******** PAR 32: externer Trigger *************)
    iired:=0;
    reduction_factor:=definedata.red_factor;
    ired:=reduction_factor-1;

    if abs(DefineData.speed_x) > 0.00001 then
    begin
      Repeat
        if out4 = FALSE then
        begin
          if STC_Status(INC1,STC_Counter4) = TRUE then
          begin
            out4:=TRUE;
            (* kein neuer Messwert *)
          end;
        end;

        gbsp:=speed_local;
        trigger_rate_r:=gbsp;
        (******** folgender Programmteil wird nicht in jedem Fall an-
         gesprungen, wenn gueltige Daten am PAR32 Interface anliegen.
         Dadurch kann es vorkommen, das gbsp den Wert null hat, obwohl
         die Laser-Daten vernuenftig aussehen ***********************)

{      if out4 = TRUE then
        begin
          if STC_Status(INC1,STC_Counter4) = FALSE then
          begin
            if itrigger = 1 then out4 := FALSE;

     (*      if iii<1 then *)   (*skip first data point*)
            if iii<0 then    (* do not skip first data point*)
              iii:=iii+1
            else
            begin
              jj:=jj+1;
	              value:=STC_Read(INC1,STC_HoldReg,STC_counter5);
                   if value > small_value then
                      gbsp:=(2.0*value)/5.0e5
                      else
                      gbsp:=speed_local;
            end;
          end;
        end;
}

        new_val:=0;

        if ihhs = 2 then (* field measurements *)
           if (parstatus(par1) = FALSE) and
              (parstatus(par2) = FALSE) and
              (parstatus(par3) = FALSE) then
(*              (parstatus(par3) = FALSE) and  *)
(*              (parstatus(par4) = FALSE) then *)
                  new_val:=1;
        if (ihhs = 3) then (* straightness measuremnet *)
           if (parstatus(par4) = FALSE) then
                  new_val:=1;
        if (ihhs = 4) or (ihhs =6) then (* Hall probe char. *)
           if (parstatus(par1) = FALSE) and
              (parstatus(par2) = FALSE) and
              (parstatus(par3) = FALSE) then
                  new_val:=1;

        if new_val = 1 then
        begin

        ii:=ii+1;
{        read_par32_1234(L1, L2, L3, L4);}

	L11:=L1*1.e-5*length_cal*length_cal_1;
	L22:=L2*1.e-5*length_cal*length_cal_1;
	L33:=L3*1.e-5*length_cal*length_cal_1;
	L44:=L4*1.e-5*length_cal*length_cal_1;

(*        writeln(l4,l44,length_cal,length_cal_1);  *)

        if dmmeoc(dmm1) then
           begin
           dmmval:=dmmgetdata(dmm1);
           temp_loc:=DMMConvVal(dmm1,dmmval);
           dmmstartconvertion(dmm1);
           end;

  if(DefineData.SaveStack = 1)then
  begin
  (*write data to stack*)
(***** checken *****)
(*      if (ihhs = 2) or (ihhs = 3) then  *)
      if (ihhs = 2) then
        begin
        if gbsp < 1.e-5 then
           begin
           ChangeWin(WarningsWin);
           writeln(gbsp);
           readkey;
           writeln(speed_local);
           readkey;
           writeln(MBINC1Data.Delay_3);
           readkey;
           writeln(MBINC2Data.Delay_2);
           readkey;
           writeln(MBINC2Data.Delay_5);
           readkey;
           writeln(factor);
           readkey;
           writeln('******');
           readkey;
           end;
        end;

	with AktPoint_Las^ DO                (*In Pointervariable eintragen*)
     	  begin
		Temp:=temp_loc;
		Laser.Ch1:=L11;
		Laser.Ch2:=L22;
		Laser.Ch3:=L33;
		Laser.Ch4:=L44;

                if (ii < DVM1Data.Readings) then
	        begin
	  	CreatePoint_Las(AktPoint_Las^.Next);  (*generate new pointer
                                                      variable*)
		AktPoint_Las := AktPoint_Las^.Next;   (*new actual element*)
	        end;

          end;
  end;

  writeln(memavail);

  (* reduce data set *)

	ired:=ired+1;
	if (ired = reduction_factor )then
		begin
		iired:=iired+1;
		if(iired < 256)then
			begin
			gbspeed^[iired]:=gbsp;
			Laser_val^[1,iired]:=L11;
			Laser_val^[2,iired]:=L22;
			Laser_val^[3,iired]:=L33;
			Laser_val^[4,iired]:=L44;
			end;
		ired:=0;
		end;
        end;

  until (ii=DVM1Data.Readings);
  end; (* speed > 0 *)

  if abs(DefineData.speed_x) < 0.00001 then

  IncEncMode(INC1,$00);
  IncEncWrite(INC1,$FFFF);
  IncEncMode(INC2,$00);
  IncEncWrite(INC2,$FFFF);

  begin
  Repeat

     if out4 = FALSE then
     begin
        if STC_Status(INC1,STC_Counter4) = TRUE then
        begin
           out4:=TRUE;
           (* kein neuer Messwert *)
        end;
     end;

     gbsp:=speed_local;
     trigger_rate_r:=gbsp;

        new_val:=0;

        if (parstatus(par1) = FALSE) then
                  new_val:=1;

        if new_val = 1 then
        begin

        ii:=ii+1;

{        read_par32_1234(L1, L2, L3, L4); (* reset data flag *)}
	L11:=IncEncRead(INC1);
	L22:=IncEncRead(INC2);

  if(DefineData.SaveStack = 1)then
  begin   (*write data to stack*)
	with AktPoint_Las^ DO                (*In Pointervariable eintragen*)
     	  begin
		Temp:=temp_loc;
		Laser.Ch1:=L11;
		Laser.Ch2:=L22;
		Laser.Ch3:=L33;
		Laser.Ch4:=L44;

                if (ii < DVM1Data.Readings) then
	        begin
	  	CreatePoint_Las(AktPoint_Las^.Next);  (*generate new pointer
                                                      variable*)
		AktPoint_Las := AktPoint_Las^.Next;   (*new actual element*)
	        end;

          end;
  end;

  (* reduce data set *)

	ired:=ired+1;
	if (ired = reduction_factor )then
		begin
		iired:=iired+1;
		if(iired < 256)then
			Laser_val^[1,iired]:=L11;
		ired:=0;
		end;
        end;

  until (ii=DVM1Data.Readings);

  end; (* speed_local = 0 *)

  end; (* external trigger *)

  ianzdata0[1]:=iired;
  ianzdata90[1]:=iired;
  ianzdata0[2]:=iired;
  ianzdata90[2]:=iired;

  (*move laser data from stack to file*)
  if(DefineData.SaveStack = 1) then
  begin
  DataFileNameProc(0);
  if (iwindow = 0)then
     s1:=LasDataFileName;
  if (iwindow = 90)then
     s1:=LasDataFileName90;
  SaveStack_LAS(s1);
  end;

(***********************************************)

  if (ihhs = 1) or (ihhs = 2) then
  begin

  for i:=1 to iired do
  begin

  if iplpath=1 then
  begin
  xdata0^[i]:=Laser_val^[1,i];
  xdata90^[i]:=Laser_val^[1,i];
  end;
  if iplpath=0 then
  begin
  xdata0^[i]:=realval(i);
  xdata90^[i]:=realval(i);
  end;
  ydata0^[1,i]:=Laser_val^[1,i];
  ydata0^[2,i]:=Laser_val^[2,i];
  ydata90^[1,i]:=Laser_val^[3,i];
  ydata90^[2,i]:=Laser_val^[4,i];

  end;

  end;

if (ihhs = 2) then
  if abs(DefineData.speed_x) > 0.00001 then
  begin
  while not dmmeoc(dmm1) do;
  dmmval:=dmmgetdata(dmm1);
  temp_loc:=DMMConvVal(dmm1,dmmval);
  dmmstartconvertion(dmm1);
  end;

  changewin(oldwin);

  end;

(**********************************************************)

procedure ManualPlPathProc;

VAR ManualPathWinLayOut                : WinDesType;
     OldWin,ManualPathWin               : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent                           : WORD;
     delay1                             : integer;
     L1,L2,L3,L4			: longint;

begin
  OldWin:=WinActCtrl;

  ManualPathWinLayOut := WinDefDes;
  WinSetHeader(ManualPathWinLayOut,LightGray,
               C_LightBlue+'Path Measurement');
  WinSetFillColor(ManualPathWinLayOut, yellow);
  WinSetOpenMode(ManualPathWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=3*width div 2; ypos:=150;
  WinHeight:= 4*height;
  ManualPathwin:=OpenWin(xpos,ypos, xpos+2*width,
                                  ypos+WinHeight,ManualPathWinLayOut);

  ChangeWin(ManualPathWin);
  SetMargin(5,LeftMargin);

  SetCursor(0,0);
  writeln('  ');
  writeln('     x-axis: time scale (0) ');
  write(' x-axis: chanel 1 of LI (1) ');
  readln(iplpath);
  writeln('  ');

  ChangeWin(OldWin);
  CloseWin(ManualPathwin);

  delay1:=500;

  (*dvmini(1);*)

{  par32ini;}
  DMMIni;
  MBINC1Ini;
  MBINC2Ini;

  MBINC1START;
  MBINC2START;

  (*dvmstart(1);*)

  delay(delay1);

  PlPathProc(1);

  ireplot:=1;

  delay(delay1);

(*  dvmreadout(0);  *)

  delay(delay1);

  MBInc1Stop;
  MBInc2Stop;

(*  dvmreset(1);  *)

end;

(***********************************************************)
procedure ManualSMS1Proc;

  CONST StepsID = 1001;
        GoID    = 1002;
        RefID   = 1003;
        firstID = StepsID;
        lastID  = RefID;

 VAR ManualSMS1WinLayOut                : WinDesType;
     OldWin,ManualSMS1Win               : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent                    : WORD;
     SaveSteps, ManSteps         : longint;
     RelWeg                      : Real;

 BEGIN
  chbreak:=#255;
  OldWin:=WinActCtrl;
  savesteps:=SMS1Data.Steps;

  ManualSMS1WinLayOut := WinDefDes;
  WinSetHeader(ManualSMS1WinLayOut,LightGray,
               C_LightBlue+'Manual SMS-1 Operation');
  WinSetFillColor(ManualSMS1WinLayOut, yellow);
  WinSetOpenMode(ManualSMS1WinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=3*width div 2; ypos:=150;
  WinHeight:= 5*height;
  ManualSMS1win:=OpenWin(xpos,ypos, xpos+2*width,
                                  ypos+WinHeight,ManualSMS1WinLayOut);

  mansteps:=0;
  RelWeg:=0;
  ChangeWin(ManualSMS1Win);
  SetMargin(5,LeftMargin);
  SetCursor(4,1);
  if ihhs <> 6 then
  begin
    ShowLineObject('  Number of Steps = ',29, StepsID, ButtonLeftEvent,
                    NoKey, Local,NilEvProc);
    WRITELN(mansteps:6);
  end;
  if ihhs = 6 then
  begin
    ShowLineObject('  Distance (mm)   = ',29, StepsID, ButtonLeftEvent,
                    NoKey, Local,NilEvProc);
    WRITELN(RelWeg:8:3);
  end;

  xpos:=60;
  ypos:=50;

  MoveTo(xpos,ypos);
  ButtonDefDes.Frame^.ActivBack := Green;
  ShowButtonObject('Go',20, GoID, ButtonLeftEvent, ALT_S, local, NilEvProc);

  if ihhs = 6 then
  begin
    xpos:=60;
    ypos:=80;
    MoveTo(xpos,ypos);
    ButtonDefDes.Frame^.ActivBack := Green;
    ShowButtonObject('Ref',20, RefID, ButtonLeftEvent, ALT_S, local,
                      NilEvProc);
  end;

  repeat
   locEvent:=WaitOfEvent(firstID,lastID);
   case locEvent of
      StepsID    : BEGIN
                     SetCursor(24,1);
                     if ihhs <> 6 then
                     begin
                       ManSteps := Editint(ManSteps,-40000,40000,6);
                       SetCursor(24,1);
                       WRITELN(ManSteps:6);
                     end;
                     if ihhs = 6 then
                     begin
                       RelWeg := Editreal(RelWeg,-120,120,8,3);
                       SetCursor(24,1);
                       Writeln(RelWeg:8:3);
                       ManSteps:=round(SMS_Factor*RelWeg);
                     end;
                   END;
      GoID : BEGIN
               if ifirst = 1 then
               begin
                 if ihhs <> 6 then SMS1Ini;
                 if ihhs=6 then
                 begin
                   SMSRampeFill(SMSR1^,SMS1Data);
                   SMSRampINI;
                 end;
               end;
               if ihhs <> 6 then
               begin
                 SMS1Data.Steps:=ManSteps;
                 SMS1Drive;
               end;
               if ihhs = 6 then SMSRampeDo(1,ManSteps,SMSR1^,0);
               delay(1000);
             END;
     RefID : if ihhs = 6 then SMS1Ref;
   END;
  until locEvent=0;
  SMS1Data.Steps:=SaveSteps;
  ChangeWin(OldWin);
  CloseWin(ManualSMS1Win);
  if (ihhs = 6) then SMSRampClose;
END;

(***********************************************************)
procedure ManualSMS2Proc;

  CONST StepsID = 1001;
        GoID    = 1002;
        RefID   = 1003;
        firstID = StepsID;
        lastID  = RefID;

 VAR ManualSMS2WinLayOut                : WinDesType;
     OldWin,ManualSMS2Win               : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent                    : WORD;
     SaveSteps, ManSteps         : longint;
     RelWeg                      : real;
BEGIN
  chbreak:=#255;
  OldWin:=WinActCtrl;
  savesteps:=SMS2Data.Steps;

  ManualSMS2WinLayOut := WinDefDes;
  WinSetHeader(ManualSMS2WinLayOut,LightGray,
               C_LightBlue+'Manual SMS-2 Operation');
  WinSetFillColor(ManualSMS2WinLayOut, yellow);
  WinSetOpenMode(ManualSMS2WinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=3*width div 2; ypos:=150;
  WinHeight:= 5*height;
  ManualSMS2win:=OpenWin(xpos,ypos, xpos+2*width,
                                  ypos+WinHeight,ManualSMS2WinLayOut);

  mansteps:=0;
  RelWeg := 0;
  ChangeWin(ManualSMS2Win);
  SetMargin(5,LeftMargin);
  SetCursor(4,1);
  if ihhs <> 6 then
  begin
    ShowLineObject('  Number of Steps = ',29, StepsID, ButtonLeftEvent,
                    NoKey, Local,NilEvProc);
    WRITELN(mansteps:6);
  end;
  if ihhs = 6 then
  begin
    ShowLineObject('  Distance (mm)   = ',29, StepsID, ButtonLeftEvent,
                    NoKey, Local,NilEvProc);
    WRITELN(RelWeg:8:3);
  end;

  xpos:=60;
  ypos:=50;

  MoveTo(xpos,ypos);
  ButtonDefDes.Frame^.ActivBack := Green;
  ShowButtonObject('Go',20, GoID, ButtonLeftEvent, ALT_S, local, NilEvProc);

  if ihhs = 6 then
  begin
    xpos:=60;
    ypos:=80;
    MoveTo(xpos,ypos);
    ButtonDefDes.Frame^.ActivBack := Green;
    ShowButtonObject('Ref',20, RefID, ButtonLeftEvent, ALT_S, local,
                      NilEvProc);
  end;

  repeat
   locEvent:=WaitOfEvent(firstID,lastID);
   case locEvent of
      StepsID    : BEGIN
                     SetCursor(24,1);
                     if ihhs <> 6 then
                     begin
                       ManSteps := Editint(ManSteps,-40000,40000,6);
                       SetCursor(24,1);
                       WRITELN(ManSteps:6);
                     end;
                     if ihhs = 6 then
                     begin
                       RelWeg := Editreal(RelWeg,-120,120,8,3);
                       SetCursor(24,1);
                       Writeln(RelWeg:8:3);
                       ManSteps:=round(SMS_Factor*RelWeg);
                     end;
                  END;
      GoID : BEGIN
               if ifirst = 1 then
               begin
                 if ihhs <> 6 then SMS2Ini;
                 if ihhs=6 then
                 begin
                   SMSRampeFill(SMSR2^,SMS2Data);
                   SMSRampINI;
                 end;
               end;
               if ihhs <> 6 then
               begin
                 SMS2Data.Steps:=ManSteps;
                 SMS2Drive;
               end;
               if ihhs = 6 then SMSRampeDo(2,ManSteps,SMSR2^,0);
               delay(1000);
             END;
     RefID : if ihhs = 6 then SMS2Ref;
   END;
  until locEvent=0;;
  SMS2Data.Steps:=SaveSteps;
  ChangeWin(OldWin);
  CloseWin(ManualSMS2Win);
  if ihhs = 6 then SMSRampClose;
end;

(**********************************************************)
procedure ManualMBInc1Proc;
begin
MBInc1Ini;
MBInc1Start;
end;

(**********************************************************)
procedure ManualMBInc2Proc;
begin
if(ihhs = 2) or (ihhs = 3) or (ihhs = 5)then
  MBInc2Ini;
if(ihhs = 4) or (ihhs = 6) then
  MBInc2aIni;
  MBInc2Start;
end;

(**********************************************************)
(**********************************************************)
procedure ManualADCProc;
begin

end;

(**********************************************************)
procedure ManualIntegratorProc;
begin

end;

(**********************************************************)
PROCEDURE ManualProc;
 CONST ServoIniID    = 4000;
       ServoID       = 4001;
       SMS1ID        = 4002;
       SMS2ID        = 4003;
       DACID         = 4004;
       MBInc1ID      = 4005;
       MBInc2ID      = 4006;
       MBInc1sID     = 4007;
       MBInc2sID     = 4008;
       Par32ID       = 4009;
       ADCID         = 4010;
       IntegratorID  = 4011;
       DVMID         = 4012;
       LaserID       = 4013;
       PrSpeedID     = 4014;
       PlSpeedID     = 4015;
       PrPathID      = 4016;
       PlPathID      = 4017;
       RS232ID       = 4018;
       SMS1IniID     = 4019;
       SMS2IniID     = 4020;
       firstID       = ServoIniID;
       lastID        = SMS2IniID;

 VAR ManualWinLayOut                    : WinDesType;
     ManualWin,OldWin                   : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent, infoEvent               : WORD;
     dirStr                            : String[8];
     ii                                : integer;

begin

  OldWin:=WinActCtrl;

  ManualWinLayOut := WinDefDes;
  WinSetHeader(ManualWinLayOut,LightGray,
               C_LightBlue+'Manual Operation');
  WinSetFillColor(ManualWinLayOut, lightred);
  WinSetOpenMode(ManualWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);

  xpos:=round(3.0*width+0.021*realval(GetMaxX));
  ypos:=round(realval(GetMaxY)*0.055);
  WinHeight:= 14*height;
  ManualWin:=OpenWin(xpos,ypos, xpos+width, ypos+WinHeight,ManualWinLayOut);

  ChangeWin(ManualWin);
  SetMargin(5,LeftMargin);
  SetCursor(0,0);
  if (ihhs <> 1) then ShowLineObject('Servo M. Ini ',18, ServoIniID, ButtonLeftEvent, NoKey,
                  Local,NilEvProc);
  writeln(' ');
  if (ihhs <> 1) then   ShowLineObject('Servo Motors ',18, ServoID, ButtonLeftEvent, NoKey, Local,
                  NilEvProc);
  writeln(' ');
  if (ihhs <> 1) then   ShowLineObject('Initialize RS232',18, RS232ID, ButtonLeftEvent, NoKey,
                  Local,NilEvProc);
  writeln(' ');
  ShowLineObject('DAC  ',18, DACID, ButtonLeftEvent, NoKey, Local,
                  NilEvProc);
  writeln(' ');
  ShowLineObject('Initialize SMS 1',18,SMS1IniID, ButtonLeftEvent, NoKey,
                  Local,NilEvProc);
  writeln(' ');
  if (ihhs <> 1) then   ShowLineObject('Initialize SMS 2',18,SMS2IniID, ButtonLeftEvent, NoKey,
                  Local,NilEvProc);
  writeln(' ');
  ShowLineObject('SMS 1',18,SMS1ID, ButtonLeftEvent, NoKey, Local,
                  NilEvProc);
  writeln(' ');
  if (ihhs <> 1) then   ShowLineObject('SMS 2',18,SMS2ID, ButtonLeftEvent, NoKey, Local,
                  NilEvProc);
  writeln(' ');
  ShowLineObject('Initialize MBInc1',18, MBInc1ID, ButtonLeftEvent, NoKey,
                  Local,NilEvProc);
  writeln(' ');
  ShowLineObject('Initialize MBInc2',18, MBInc2ID, ButtonLeftEvent, NoKey,
                  Local,NilEvProc);
  writeln(' ');
  ShowLineObject('DVM 1-3',18,DVMID, ButtonLeftEvent, NoKey, Local,
                  NilEvProc);
  writeln(' ');

  if (ihhs <> 1) then   ShowLineObject('Read PAR32 1-4',18, Par32ID, ButtonLeftEvent, NoKey, Local,
                  NilEvProc);
  writeln(' ');
  ShowLineObject('Read ADC',18, ADCID, ButtonLeftEvent, NoKey, Local,
                  NilEvProc);
  writeln(' ');
  if (ihhs <> 1) then   ShowLineObject('Integrator',18, IntegratorID, ButtonLeftEvent, NoKey, Local,
                  NilEvProc);
  writeln(' ');
  if (ihhs <> 1) then   ShowLineObject('Laser Interfer.',18,LaserID, ButtonLeftEvent, NoKey, Local,
                  NilEvProc);
  writeln(' ');
  ShowLineObject('Print Speed',18, PrSpeedID, ButtonLeftEvent, NoKey, Local,
                  NilEvProc);
  writeln(' ');
  ShowLineObject('Plot Speed',18, PlSpeedID, ButtonLeftEvent, NoKey, Local,
                  NilEvProc);
  writeln(' ');
  ShowLineObject('Print Path',18, PrPathID, ButtonLeftEvent, NoKey, Local,
                  NilEvProc);
  writeln(' ');
  ShowLineObject('Plot Path',18, PlPathID, ButtonLeftEvent, NoKey, Local,
                  NilEvProc);
  writeln(' ');

  repeat
    locEvent:=WaitOfEvent(firstID,lastID);
    case locEvent of
      ServoIniID    : Begin
                        if (ihhs = 2) or (ihhs = 3) or (ihhs = 5) then
                        begin
                          ii:=iterhos;
                          iterhos:=1;
                          SetTerHos;
                          ServoIni;
                          iterhos:=ii;
                          SetTerHos;
                        end;
                        if (ihhs = 4) then STWIini;
                      END;
      ServoID       : ManualservoProc;
      SMS1ID        : ManualSMS1Proc;
      SMS2ID        : ManualSMS2Proc;
      SMS1IniID     : ManualSMS1Ini;
      SMS2IniID     : ManualSMS2Ini;
      DACID         : ManualDACProc;
      MBInc1ID      : ManualMBInc1Proc;
      MBInc2ID      : ManualMBInc2Proc;
      ADCID         : ManualADCProc;
      RS232ID       : begin
                        RS232_CLOSE(COM1);
                        delay(100);
                        RS232_OPEN(COM1);
                        delay(100);
                      end;
      LaserID       : ManualLaserProc;
      PrSpeedID     : ManualPrSpeedProc;
      PlSpeedID     : ManualPlSpeedProc;
      PrPathID      : ManualPrPathProc;
      PlPathID      : ManualPlPathProc;
      IntegratorID  : ManualIntegratorProc;
      DVMID         : ManualDVMProc;
    END;
  until (locEvent=0) or (ireplot=1);
  ChangeWin(OldWin);

  CloseWin(ManualWin);

  if ireplot = 1 then
  begin
    scaledata.autoscale0:=1;
    scaledata.dispchannel0:=2;
    replotdata(1);
    scaledata.autoscale90:=1;
    scaledata.dispchannel90:=2;
    replotdata(2);
    ireplot:=0;
  end;

END;

END.
