{$N+}
unit mfm2_sck;
{15.05.2001  Bildschirmfenster ..
22.10.2007 HHSOutputScreen}
INTERFACE

USES DOS, CRT, Graph, SCALELIB, MBBASE, BESLIB, SDLIB,
          STDLIB, MOUSELIB, VESALIB, WINLIB,
          TPDECL, AUXIO1,
          MFM2_TCK,MFM2_UFK,MFM2_HWK;
var Path_to_Datafiles:string;
    Erstellungsdate:string;
procedure displaychannel(iposition : integer; ichannel : integer);
procedure CommentProc;
procedure BlocNameProc;
Procedure HHS_BlocPosFileName(var ixx:integer);
procedure DataFileNameProc(ediname : integer);
procedure FileCounter(incr : integer);
procedure FileCounter1;
procedure CreateMainScreen;
procedure CreateKommandoMenue;
procedure CreateSteuerButtons;
procedure CreateAnzeigeFenster;
procedure CreateMessFenster;
procedure replotdata(inew : integer);
procedure RePlotDataSW2(var iv,iyz,ilz:integer);
procedure HHSoutputscreen(var imsr:integer);
procedure exit_routine;

IMPLEMENTATION

uses MFM2_ANK;

(************* Screen Procedures ********************************)
procedure displaychannel(iposition : integer; ichannel : integer);
var
   inew : integer;
begin
if iposition = 1 then
   begin
     inew := 1;
     scaledata.dispchannel0 := ichannel;
   end;
if iposition = 2 then
   begin
     inew := 2;
     scaledata.dispchannel90 := ichannel;
   end;
replotdata(inew);
end;

(***********************************************************)
procedure CommentProc;
begin
  changewin(commentwin);
  clearwin;
  comment := editstr(comment,0,12);
  changewin(mainscreen);
end;

(******************************************************)
procedure FileCounter(incr : integer);
var
   number       : integer;
   s1,s2        : string;

begin
  if (ihhs = 1) then filename:= Path_to_Datafiles + '\mfm_hhs.num';
  if (ihhs = 2) then filename:= Path_to_Datafiles + '\mfm_mag.num';
  if (ihhs = 3) then filename:= Path_to_Datafiles + '\mfm_str.num';
  if (ihhs = 4) then filename:= Path_to_Datafiles + '\mfm_SW.num';
  if (ihhs = 6) then filename:= Path_to_Datafiles + '\mfm_SW.num';

  openoutfile(1,0);
  readln(outfile,number);
  readln(outfile,path);
  closeoutfile(1,0);

  if (incr < 0)then
  begin
    Filenr:=number;
    PathData:=path;
  end;
  if incr = 0 then
  begin
    openoutfile(0,0);
    writeln(outfile,number);
    writeln(outfile,path);
    closeoutfile(0,0);
  end;
  if incr = 1 then
  begin
    openoutfile(0,0);
    writeln(outfile,number+1);
    writeln(outfile,path);
    closeoutfile(0,0);
  end;
  if (incr = 2) then
  begin
    openoutfile(0,0);
    writeln(outfile,FileNr);
    writeln(outfile,path);
    number:=FileNr;
    closeoutfile(0,0);
  end;
  if (incr = 3) then
  begin
    openoutfile(0,0);
    writeln(outfile,number);
    writeln(outfile,PathData);
    path:=PathData;
    closeoutfile(0,0);
  end;

  if (incr = 0) or (incr = 1) then
  begin
    s1:='';
    if number < 1000 then s1:=s1+'0';
    if number < 100  then s1:=s1+'0';
    if number < 10   then s1:=s1+'0';
    str(number,s2);
    s1:=s1+s2;
    RawDataFileName:=BlocName+s1+'.RAW';
    CalDataFileName:=BlocName+s1+'.DAT';
    LasDataFileName:=BlocName+s1+'.LAS';
    LasDataFileName90:=BlocName+s1+'.LA1';
    DVMDataFileName:=BlocName+s1+'.DVM';
    LOGDataFileName:=BlocName+s1+'.LOG';
    changewin(DataFilenameWin);
    clearwin;
    write(RawDatafilename);
    changewin(mainscreen);
    FileNr:=number;
  end;
END;


(******************************************************)
procedure FileCounter1;
var
   number       : integer;
   s1,s2        : string;
   num          : integer;

begin
  if (ihhs = 2)then
  begin
    filename:='mfm_mag.num';
    openoutfile(1,0);
    readln(outfile,number);
    readln(outfile,path);
    closeoutfile(1,0);

    if (path[1] = 'c')then
    begin
      filename:='mfm_magl.num';
      openoutfile(0,0);
      writeln(outfile,number);
      writeln(outfile,path);
      closeoutfile(0,0);
    end;

    if (path[1] = 'n')then
    begin
      filename:='mfm_magn.num';
      openoutfile(0,0);
      writeln(outfile,number);
      writeln(outfile,path);
      closeoutfile(0,0);
    end;
  end;

  if (ihhs = 3)then
  begin
    filename:='mfm_str.num';
    openoutfile(1,0);
    readln(outfile,number);
    readln(outfile,path);
    closeoutfile(1,0);

    if (path[1] = 'c')then
    begin
      filename:='mfm_strl.num';
      openoutfile(0,0);
      writeln(outfile,number);
      writeln(outfile,path);
      closeoutfile(0,0);
    end;

    if (path[1] = 'n')then
    begin
      filename:='mfm_strn.num';
      openoutfile(0,0);
      writeln(outfile,number);
      writeln(outfile,path);
      closeoutfile(0,0);
    end;
  end;

  if (ihhs = 4) or (ihhs = 6) then
  begin
    filename:='mfm_SW.num';
    openoutfile(1,0);
    readln(outfile,number);
    readln(outfile,path);
    closeoutfile(1,0);

    if (path[1] = 'c')then
    begin
      filename:='mfm_SWl.num';
      openoutfile(0,0);
      writeln(outfile,number);
      writeln(outfile,path);
      closeoutfile(0,0);
    end;

    if (path[1] = 'n')then
    begin
      filename:='mfm_SWn.num';
      openoutfile(0,0);
      writeln(outfile,number);
      writeln(outfile,path);
      closeoutfile(0,0);
    end;
  end;
END;

(******************************************************)
PROCEDURE exit_routine;

begin
  FileCounter1;
end;

(******************************************************)
PROCEDURE BlocNameProc;   (* Startwert bei Beginn des Hauptprogramms *)
var
ii        : integer;
number    : integer;
s1,s2     : string;

begin
  changewin(BlocNamewin);
  clearwin;

  BlocName := editstr(BlocName,0,14);
  BlocNameTmp:=BlocName;
  if (ihhs = 1) or (ihhs = 4) or (ihhs = 6) then  (** Helmholtz Coil **)
  begin
    CalDatafilename:=BlocName+'.DAT';
    RawDataFileName:=BlocName+'.RAW';
    ResDataFileName:=BlocName+'.RES';
    LOGDataFileName:=BlocName+'.LOG';
    changewin(DataFilenameWin);
    clearwin;
    write(CalDatafilename);
    changewin(mainscreen);
  end;

  if (ihhs = 2) or (ihhs = 3)       (** granite bench **)
  or (ihhs = 4) or (ihhs = 6) then  (** stretched wire **)
  begin
    filecounter(0);
  end;
end;

Procedure HHS_BlocPosFileName(var ixx:integer);
begin
  BlocNameTmp:=BlocName;
  if (HHSBlocPos=2) then
  begin

    if ixx=1 then
    begin
      BlocNameTmp:=BlocName+'_1';
      CalDatafilename:=BlocNameTmp+'.DAT';
      RawDataFileName:=BlocNameTmp+'.RAW';
      ResDataFileName:=BlocNameTmp+'.RES';
      LOGDataFileName:=BlocNameTmp+'.LOG';
    end;
    if ixx=2 then
    begin
      BlocNameTmp:=BlocName+'_2';
      CalDatafilename:=BlocNameTmp+'.DAT';
      RawDataFileName:=BlocNameTmp+'.RAW';
      ResDataFileName:=BlocNameTmp+'.RES';
      LOGDataFileName:=BlocNameTmp+'.LOG';
    end;
  end;
end;
(******************************************************)
procedure DataFileNameProc(ediname : integer);
var
   ii   : integer;
begin
  if(ediname = 1)then
  begin
    changewin(DataFileNamewin);
    clearwin;
    CalDataFileName := editstr(CalDataFileName,0,14);
  end;
  ii:=length(CalDataFileName);
  RawDataFileName:=CalDataFileName;
  delete(RawDataFileName,ii-3,ii);
  RawDataFileName:=RawDataFileName+'.RAW';
  ResDataFileName:=CalDataFileName;
  delete(ResDataFileName,ii-3,ii);
  ResDataFileName:=ResDataFileName+'.RES';
  LasDataFileName:=CalDataFileName;
  delete(LasDataFileName,ii-3,ii);
  LasDataFileName:=LasDataFileName+'.LAS';
  LasDataFileName90:=LasDataFileName;
  delete(LasDataFileName90,ii-3,ii);
  LasDataFileName90:=LasDataFileName90+'.LA1';
  DVMDataFileName:=CalDataFileName;
  delete(DVMDataFileName,ii-3,ii);
  DVMDataFileName:=DVMDataFileName+'.DVM';
  LOGDataFileName:=LOGDataFileName;
  delete(LOGDataFileName,ii-3,ii);
  LOGDataFileName:=LOGDataFileName+'.LOG';

  changewin(mainscreen);
END;

(**********    CreateMainScreen ********************************)
procedure CreateMainScreen;
 {AufBau des Hauptbildschirms mit Headline}
 VAR MainScreenLayOut : WinDesType;
     Ueberschrift:string;

BEGIN
  MainScreenLayOut := WinDefDes;
  if ihhs = 1 then
  begin
    ueberschrift:= 'Helmholtz Coil Measurement   version '+ErstellungsDate;
    WinSetHeader(MainScreenLayOut,blue,ueberschrift);
  end;
{  WinSetHeader(MainScreenLayOut,blue,
        'Helmholtz Coil Measurement             28.01.2005');

  if ihhs = 2 then
  WinSetHeader(MainScreenLayOut,blue,
        '5m Granite Bench, Hall Probe         JB 28.9.1998');
  if ihhs = 3 then
  WinSetHeader(MainScreenLayOut,blue,
        '5m Granite Bench, Straightness       JB 28.9.1998');
  if ihhs = 4 then
  WinSetHeader(MainScreenLayOut,blue,
        'Stretched Wire System                JB 28.9.1998');
  if ihhs = 5 then
  WinSetHeader(MainScreenLayOut,blue,
        'Hall Probe Response                  JB 28.9.1998');
}
  if ihhs = 6 then
  begin
  ueberschrift:= 'Stretched Wire System 2  Bloc-Measurement   version '+ErstellungsDate;
  WinSetHeader(MainScreenLayOut,blue,ueberschrift);
  end;

  WinSetOpenMode(MainScreenLayOut, WinHeadText, NoSave);
  WinSetFillColor(MainScreenLayOut,DarkGray);
  MainScreen := OpenWin(0,0, GetMaxX, GetMaxY, MainScreenLayOut);
 END;

(*************************   CreateKommandomenue    *************************)

procedure CreateKommandoMenue;
 CONST KommandString = 'Set-Up'+ ZeroChr + 'Calibration' + ZeroChr +
                       'Measurement' + ZeroChr + 'Analysis'+ ZeroChr;
       dist = 4;
 VAR width,height,xpos:word;

 BEGIN
  ChangeWin(MainScreen);
  ButtonDefDes.Frame^.ActivBack := LightBlue;

  xpos:=0;
  GetButtonDim(FONT8x16,1,18,width,height);

  MoveTo(xpos,0);
  ShowButtonObject('Set-Up', 18, SetUpID, ButtonLeftEvent, NoKey, Global,
                    NilEvProc);
  xpos:= xpos+width+dist;

  MoveTo(xpos,0);
  if (ihhs <>1) then ShowButtonObject('Calibration',18, CalID, ButtonLeftEvent,NoKey, Global,
                    NilEvProc);
{  xpos:= xpos+width+dist;

  MoveTo(xpos,0);
  ShowButtonObject('Analyse',18,AnalyseID, ButtonLeftEvent,NoKey, Global,
                   NilEvProc);

  xpos:= xpos+width+dist;
}
  xpos:= xpos+2*(width+dist);

  MoveTo(xpos,0);
  ShowButtonObject('Manual Operation',18, ManualID, ButtonLeftEvent,NoKey,
                   Global,NilEvProc);

END;

(**********************  CreateSteuerButtons  *******************************)
procedure CreateSteuerButtons;
  {Start, Stop, Store, Load und Exit Button}
 VAR xpos, ypos, deltax : real;

BEGIN
  ChangeWin(MainScreen);
  {Anfangspositionen setzen}
  xpos:= WinGetMaxX*0.588;
  deltax:=WinGetMaxX*0.082;
  ypos:= WinGetMaxY*0.935;
  {Start Button}
  MoveTo(round(xpos),round(ypos));
  ButtonDefDes.Frame^.ActivBack := Green;
  ShowButtonObject('Start', 5, StartID, ButtonLeftEvent, ALT_S,
                            local, NilEvProc);
if ihhs<>6 then
begin
  flagStart := false;
  {Stop Button}
  MoveTo(round(xpos+deltax),round(ypos));
  ButtonDefDes.Frame^.ActivBack := Red;
  ShowButtonObject('Stop', 5, StopID, ButtonLeftEvent, ALT_H,
                           local, NilEvProc);
(*
  flagStop := false;
  {Store Button}
  MoveTo(round(xpos+2.0*deltax),round(ypos));
  ButtonDefDes.Frame^.ActivBack := brown;
  ShowButtonObject('Store', 5, StoreID, ButtonLeftEvent, ALT_W,
                            local, NilEvProc);
  flagStop := false;
  {Load Button}
  MoveTo(round(xpos+3.0*deltax),round(ypos));
  ButtonDefDes.Frame^.ActivBack := lightblue;
  ShowButtonObject('Load', 5, LoadID, ButtonLeftEvent, ALT_R,
                           local, NilEvProc);
*)
end;

  flagStop := false;
  {Exit Button}
  MoveTo(round(xpos+4.0*deltax),round(ypos));
  ButtonDefDes.Frame^.ActivBack := magenta;
  ShowButtonObject('Exit', 5, ExitID, ButtonLeftEvent, ALT_X,
                           local, NilEvProc);

  {Kanalwahlbuttons zero degree window}
  MoveTo(0,round(realval(GetMaxY)*0.07));
  aktMousePtr:=ShowButtonObject('1', 1, Ch1zeroID, ButtonLeftEvent,
                                   ALT_1, Local, NilEvProc);
  MoveTo(0,round(realval(GetMaxY)*0.132));
  aktMousePtr:=ShowButtonObject('2', 1, Ch2zeroID, ButtonLeftEvent,
                                   ALT_2, Local, NilEvProc);

  if (ihhs = 1) or (ihhs = 4) or (ihhs = 5) {or (ihhs = 6)} then
  begin
    {Kanalwahlbuttons ninety degree window}
    MoveTo(round(realval(GetMaxX)*0.5),round(realval(GetMaxY)*0.07));
    aktMousePtr:=ShowButtonObject('1', 1, Ch1ninetyID, ButtonLeftEvent,
                                   ALT_1, Local, NilEvProc);
    MoveTo(round(realval(GetMaxX)*0.5),round(realval(GetMaxY)*0.132));
    aktMousePtr:=ShowButtonObject('2', 1, Ch2ninetyID, ButtonLeftEvent,
                                   ALT_2, Local, NilEvProc);
  end;

  if (ihhs = 2) or (ihhs = 3) then
  begin
     {Kanalwahlbuttons ninety degree window}
    MoveTo(0,round(realval(GetMaxY)*0.427));
    aktMousePtr:=ShowButtonObject('1', 1, Ch1ninetyID, ButtonLeftEvent,
                                   ALT_1, Local, NilEvProc);
    MoveTo(0,round(realval(GetMaxY)*0.489));
    aktMousePtr:=ShowButtonObject('2', 1, Ch2ninetyID, ButtonLeftEvent,
                                   ALT_2, Local, NilEvProc);
  end;

END;

(*************************   CreateAnzeigeFenster   *****************)
procedure CreateAnzeigeFenster;

 VAR AnzeigenFensterLayOut : WinDesType;
     xpos, ypos, dummy1, dummy2 : WORD;
     xpos0,ypos0           : WORD;


BEGIN
  AnzeigenFensterLayOut:=WinDefDes;

  {Aussehen der Fenster festlegen}
  WinSetOpenMode(AnzeigenFensterLayOut, WinDefType,XmmSave);
  WinSetFillColor(AnzeigenFensterLayOut, LightGray);

  {StartPosition setzen}
  ChangeWin(MainScreen);
  if (ihhs <> 6) then
  TemperatureWin  := TextLineWin(round(realval(GetMaxX)*0.005),
                             round(realval(GetMaxY)*0.857),
                             'Temperature:',6,
                             AnzeigenFensterLayOut,dummy1,dummy2);

  ChangeWin(MainScreen);
  if (ihhs = 1) then
  DACValueWin := TextLineWin(round(realval(GetMaxX)*0.005),
                             round(realval(GetMaxY)*0.905),
                             'DAC-Voltage:',6,
                             AnzeigenFensterLayOut,dummy1,dummy2);
  if (ihhs = 2) or (ihhs = 3) or (ihhs = 4)  or (ihhs = 5) {or (ihhs = 6)} then
  DACValueWin := TextLineWin(round(realval(GetMaxX)*0.005),
                             round(realval(GetMaxY)*0.90),
                             '   Position:',6,
                             AnzeigenFensterLayOut,dummy1,dummy2);

   if (ihhs = 6) then
{  DACValueWin := TextLineWin(round(realval(GetMaxX)*0.005),
                             round(realval(GetMaxY)*0.90),
                             '   Position:',6,
                             AnzeigenFensterLayOut,dummy1,dummy2);}
  DACValueWin := OpenTextWin(round(realval(GetMaxX)*0.005),
                             round(realval(GetMaxY)*0.855),18,4,
                             AnzeigenFensterLayOut);


  ChangeWin(MainScreen);
  if (ihhs = 1)then
  SpeedWin := TextLineWin(round(realval(GetMaxX)*0.005),
                             round(realval(GetMaxY)*0.95),
                             ' Speed (Hz):',6,
                             AnzeigenFensterLayOut,dummy1,dummy2);
  if (ihhs = 2) or (ihhs = 3) or (ihhs = 4)  or (ihhs = 5) then
  SpeedWin := TextLineWin(round(realval(GetMaxX)*0.005),
                             round(realval(GetMaxY)*0.95),
                             ' Sp. (mm/s):',6,
                             AnzeigenFensterLayOut,dummy1,dummy2);

  ChangeWin(MainScreen);
  if ihhs<>6 then
  begin
  CommentWin := TextLineWin(round(realval(GetMaxX)*0.25),
                             round(realval(GetMaxY)*0.857),
                             '   Comment:',14,
                             AnzeigenFensterLayOut,dummy1,dummy2);
  end;
  if ihhs=6 then
  begin
  CommentWin := TextLineWin(round(realval(GetMaxX)*0.25),
                             round(realval(GetMaxY)*0.855),
                             'MagnetName:',14,
                             AnzeigenFensterLayOut,dummy1,dummy2);
  end;
  ShowLineObject('',12,CommentID,ButtonLeftEvent,
              Alt_C,global,NilEvProc);
  if (ihhs = 1) or (ihhs =6) then

  DateTimeWin := OpenTextWin(round(realval(GetMaxX)*0.40),
                             round(realval(GetMaxY)*0.06),19,1,
                             AnzeigenFensterLayOut);


  ChangeWin(MainScreen);
  if (ihhs = 1) or (ihhs = 5) then
  BlocNameWin := TextLineWin(round(realval(GetMaxX)*0.25),
                             round(realval(GetMaxY)*0.905),
                             '   Bloc-Nr:',14,
                             AnzeigenFensterLayOut,dummy1,dummy2);
  if (ihhs = 2) or (ihhs = 3) or (ihhs = 4) then
  BlocNameWin := TextLineWin(round(realval(GetMaxX)*0.25),
                             round(realval(GetMaxY)*0.90),
                             'Section-Nr:',14,
                             AnzeigenFensterLayOut,dummy1,dummy2);
  if (ihhs = 6) then
  BlocNameWin := TextLineWin(round(realval(GetMaxX)*0.25),
                             round(realval(GetMaxY)*0.900),
                             'Section-Nr:',14,
                             AnzeigenFensterLayOut,dummy1,dummy2);
  ShowLineObject('',12,BlocNameID,ButtonLeftEvent,
              Alt_B,global,NilEvProc);

  ChangeWin(MainScreen);
  DataFileNameWin := TextLineWin(round(realval(GetMaxX)*0.25),
                             round(realval(GetMaxY)*0.950),
                             ' File-Name:',14,
                             AnzeigenFensterLayOut,dummy1,dummy2);
  ShowLineObject('',14,DataFilenameID,ButtonLeftEvent,
              Alt_F,global,NilEvProc);
  CalDatafilename:=CalDataFileDefName;
  RawDatafilename:=RawDataFileDefName;
  ResDatafilename:=ResDataFileDefName;
  write(CalDataFileName);

  ChangeWin(MainScreen);
{  warningswin := OpenTextWin(round(realval(GetMaxX)*0.60),
                             round(realval(GetMaxY)*0.85),30,2,
                             AnzeigenFensterLayOut);
}
 warningswin := OpenTextWin(round(realval(GetMaxX)*0.58),
                             round(realval(GetMaxY)*0.857),32,2,
                             AnzeigenFensterLayOut);

  ChangeWin(MainScreen);

  if (ihhs = 1) or (ihhs = 4) or (ihhs = 5) {or (ihhs = 6)} then
  begin
    anzeigenfensterlayout.font := GetPixelFont('F5X6NORM');

    zeroanalysiswin := OpenTextWin(round(realval(GetMaxX)*0.005),
                             round(realval(GetMaxY)*0.505),
                             38,6,AnzeigenFensterLayOut);
    ChangeWin(MainScreen);
    ninetyanalysiswin := OpenTextWin(round(realval(GetMaxX)*0.505),
                             round(realval(GetMaxY)*0.505),
                             38,6,AnzeigenFensterLayOut);

    anzeigenfensterlayout := windefdes;
    ChangeWin(MainScreen);
    resultwin := OpenTextWin(round(realval(GetMaxX)*0.005),
                             round(realval(GetMaxY)*0.615),
                             78,7,AnzeigenFensterLayOut);
  end;

  if ihhs = 3 then
  begin
    anzeigenfensterlayout.font := GetPixelFont('F5X6NORM');
    zeroanalysiswin := OpenTextWin(round(realval(GetMaxX)*0.505),
                             round(realval(GetMaxY)*0.13),
                             37,20,AnzeigenFensterLayOut);
    ChangeWin(MainScreen);
  end;
END;

(************************ CreateMessFenster  *************************)
procedure CreateMessFenster;
 CONST   ChSelectString = 'Ch1' + ZeroChr + 'Ch2' + ZeroChr;
 VAR     BetaWinLayOut  : WinDesType;

BEGIN

  {----------------------- erstes Meáfenster -----------------------}
  ChangeWin(MainScreen);
  BetaWinLayOut := WinDefDes;
  {Kleinere Schrift vereinbaren}
  gfont := GetPixelFont('F5X6NORM');
  SetTextStyle(gfont,HorizDir,1);
  betaWinLayOut.TFont := gfont;
  BetaWinLayOut.Font  := gfont;
  if ihhs = 1 then
  WinSetHeader(betaWinLayOut,LightGray,
               C_black+'magnet in zero degree position ');
  if ihhs = 2 then
  WinSetHeader(betaWinLayOut,LightGray,
               C_black+'magnetic field data');
  if ihhs = 3 then
  WinSetHeader(betaWinLayOut,LightGray,
               C_black+'straightness data');
  if ihhs = 4 then
  WinSetHeader(betaWinLayOut,LightGray,
               C_black+'first field integrals (Tesla mm)');
  if ihhs = 5 then
  WinSetHeader(betaWinLayOut,LightGray,
               C_black+'increasing field');
  if ihhs = 6 then
  WinSetHeader(betaWinLayOut,LightGray,
               C_black+'first field integral By (Tesla mm)');
  {FensterSpeicherModus}
  WinSetOpenMode(betaWinLayOut,WinDefType,XmmSave);
  {FensterHintergrungfarbe}
  WinSetFillColor(betaWinLayOut,White);

  if (ihhs = 1) or (ihhs = 4) or (ihhs = 5) then
  zerowin := openwin(round(realval(GetMaxX)*0.036),
                     round(realval(GetMaxY)*0.114),
                     round(realval(GetMaxX)*0.495),
                     round(realval(GetMaxY)*0.500),
                     betaWinLayOut);
  if ihhs = 2 then
  zerowin := openwin(round(realval(GetMaxX)*0.036),
                     round(realval(GetMaxY)*0.125),
                     round(realval(GetMaxX)*0.99),
                     round(realval(GetMaxY)*0.476),
                     betaWinLayOut);
  if ihhs = 3 then
  zerowin := openwin(round(realval(GetMaxX)*0.036),
                     round(realval(GetMaxY)*0.125),
                     round(realval(GetMaxX)*0.495),
                     round(realval(GetMaxY)*0.476),
                     betaWinLayOut);

  if (ihhs = 6) then
  zerowin := openwin(round(realval(GetMaxX)*0.036),
                     round(realval(GetMaxY)*0.115),
                     round(realval(GetMaxX)*0.495),
                     round(realval(GetMaxY)*0.480),
                     betaWinLayOut);

     {X-Y-Achse}
     WinSetViewPort(20,15,WinGetMaxX,WinGetMaxY-1);
     ScaleColors(lightGray,Black, Black);
     if ihhs = 1 then
     Axis(ScaleData.xminzero,ScaleData.xmaxzero,'number of turns ',
            ScaleData.yminzero,ScaleData.ymaxzero,'  ',Axis0LayOut,
                       AxisXY or AxisFrame or AxisGrid );
     if (ihhs = 2) or (ihhs = 3) then
     Axis(ScaleData.xminzero,ScaleData.xmaxzero,'distance (mm)',
            ScaleData.yminzero,ScaleData.ymaxzero,'  ',Axis0LayOut,
                       AxisXY or AxisFrame or AxisGrid );
     if ihhs = 4 then
     Axis(ScaleData.xminzero,ScaleData.xmaxzero,'z-position (mm)',
            ScaleData.yminzero,ScaleData.ymaxzero,'  ',Axis0LayOut,
                       AxisXY or AxisFrame or AxisGrid );
     if ihhs = 5 then
     Axis(ScaleData.xminzero,ScaleData.xmaxzero,'time',
            ScaleData.yminzero,ScaleData.ymaxzero,'  ',Axis0LayOut,
                       AxisXY or AxisFrame or AxisGrid );
    if ihhs = 6 then
     Axis(ScaleData.xminzero,ScaleData.xmaxzero,'z-position (mm)',
            ScaleData.yminzero,ScaleData.ymaxzero,'  ',Axis0LayOut,
                       AxisXY or AxisFrame or AxisGrid );
  {-------------------------  zweites Meáfenster ------------------------------}
  ChangeWin(MainScreen);
  BetaWinLayOut := WinDefDes;
  {Kleinere Schrift vereinbaren}
  gfont := GetPixelFont('F5X6NORM');
  SetTextStyle(gfont,HorizDir,1);
  betaWinLayOut.TFont := gfont;
  BetaWinLayOut.Font  := gfont;
  if ihhs = 1 then
  WinSetHeader(betaWinLayOut,LightGray,
               C_black+'magnet in ninety degree position ');
  if ihhs = 2 then
  WinSetHeader(betaWinLayOut,LightGray,
               C_black+' hall probe misalignment');
  if ihhs = 3 then
  WinSetHeader(betaWinLayOut,LightGray,
               C_black+'straightness data');
  if ihhs = 4 then
  WinSetHeader(betaWinLayOut,LightGray,
               C_black+'second field integrals (Tesla mm**2)');
  if ihhs = 5 then
  WinSetHeader(betaWinLayOut,LightGray,
               C_black+'decreasing field');
  if ihhs = 6 then
  WinSetHeader(betaWinLayOut,LightGray,
               C_black+'first field integral Bz (Tesla mm)');
  {FensterSpeicherModus}
  WinSetOpenMode(betaWinLayOut,WinDefType,XmmSave);
  {FensterHintergrungfarbe}
  WinSetFillColor(betaWinLayOut,White);

  if (ihhs = 1) or (ihhs = 4) or (ihhs = 5) then
  ninetywin := openwin(round(realval(GetMaxX)*0.536),
                     round(realval(GetMaxY)*0.114),
                     round(realval(GetMaxX)*0.995),
                     round(realval(GetMaxY)*0.500),
                     betaWinLayOut);
  if (ihhs = 2) or (ihhs = 3) then
  ninetywin := openwin(round(realval(GetMaxX)*0.036),
                     round(realval(GetMaxY)*0.483),
                     round(realval(GetMaxX)*0.99),
                     round(realval(GetMaxY)*0.833),
                     betaWinLayOut);
  if (ihhs = 6) then
  ninetywin := openwin(round(realval(GetMaxX)*0.536),
                     round(realval(GetMaxY)*0.115),
                     round(realval(GetMaxX)*0.995),
                     round(realval(GetMaxY)*0.480),
                     betaWinLayOut);
  {Kleinere Schrift vereinbaren}
  gfont := GetPixelFont('F5X6NORM');
  SetTextStyle(gfont,HorizDir,1);
  betaWinLayOut.TFont := gfont;
  BetaWinLayOut.Font  := gfont;

     {X-Y-Achse}
     WinSetViewPort(20,15,WinGetMaxX,WinGetMaxY-1);
     ScaleColors(lightGray,Black, Black);
     if ihhs = 1 then
     Axis(ScaleData.xminninety,ScaleData.xmaxninety,'number of turns',
            ScaleData.yminninety,ScaleData.ymaxninety,'   ',Axis0LayOut,
                       AxisXY or AxisFrame or AxisGrid );
     if (ihhs = 2) or (ihhs = 3) then
     Axis(ScaleData.xminninety,ScaleData.xmaxninety,'distance (mm)',
            ScaleData.yminninety,ScaleData.ymaxninety,'   ',Axis0LayOut,
                       AxisXY or AxisFrame or AxisGrid );
     if ihhs = 4 then
     Axis(ScaleData.xminninety,ScaleData.xmaxninety,'z-position (mm)',
            ScaleData.yminninety,ScaleData.ymaxninety,'   ',Axis0LayOut,
                       AxisXY or AxisFrame or AxisGrid );
     if ihhs = 5 then
     Axis(ScaleData.xminninety,ScaleData.xmaxninety,'time',
            ScaleData.yminninety,ScaleData.ymaxninety,'   ',Axis0LayOut,
                       AxisXY or AxisFrame or AxisGrid );
    if ihhs = 6 then
     Axis(ScaleData.xminninety,ScaleData.xmaxninety,'z-position (mm)',
            ScaleData.yminninety,ScaleData.ymaxninety,'   ',Axis0LayOut,
                       AxisXY or AxisFrame or AxisGrid );

  if ihhs = 6 then
  begin
{-------------------------  drittes Meáfenster ------------------------------}
    ChangeWin(MainScreen);
    BetaWinLayOut := WinDefDes;
    {Kleinere Schrift vereinbaren}
    gfont := GetPixelFont('F5X6NORM');
    SetTextStyle(gfont,HorizDir,1);
    betaWinLayOut.TFont := gfont;
    BetaWinLayOut.Font  := gfont;
    WinSetHeader(betaWinLayOut,LightGray,
               C_black+'difference By (Tesla mm)');
    {FensterSpeicherModus}
    WinSetOpenMode(betaWinLayOut,WinDefType,XmmSave);
    {FensterHintergrungfarbe}
    WinSetFillColor(betaWinLayOut,White);

    win3 := openwin(round(realval(GetMaxX)*0.036),
                     round(realval(GetMaxY)*0.485),
                     round(realval(GetMaxX)*0.495),
                     round(realval(GetMaxY)*0.850),
                     betaWinLayOut);

    {Kleinere Schrift vereinbaren}
    gfont := GetPixelFont('F5X6NORM');
    SetTextStyle(gfont,HorizDir,1);
    betaWinLayOut.TFont := gfont;
    BetaWinLayOut.Font  := gfont;

    {X-Y-Achse}
    WinSetViewPort(20,15,WinGetMaxX,WinGetMaxY-1);
    ScaleColors(lightGray,Black, Black);
    Axis(ScaleData.xminzero,ScaleData.xmaxzero,'z-position (mm)',
            ScaleData.yminzero,ScaleData.ymaxzero,'   ',Axis0LayOut,
                       AxisXY or AxisFrame or AxisGrid );
{-------------------------  viertes Meáfenster ------------------------------}

    ChangeWin(MainScreen);
    BetaWinLayOut := WinDefDes;
    {Kleinere Schrift vereinbaren}
    gfont := GetPixelFont('F5X6NORM');
    SetTextStyle(gfont,HorizDir,1);
    betaWinLayOut.TFont := gfont;
    BetaWinLayOut.Font  := gfont;
    WinSetHeader(betaWinLayOut,LightGray,
               C_black+'difference Bz (Tesla mm)');
    {FensterSpeicherModus}
    WinSetOpenMode(betaWinLayOut,WinDefType,XmmSave);
    {FensterHintergrungfarbe}
    WinSetFillColor(betaWinLayOut,White);

    win4 := openwin(round(realval(GetMaxX)*0.536),
                     round(realval(GetMaxY)*0.485),
                     round(realval(GetMaxX)*0.995),
                     round(realval(GetMaxY)*0.850),
                     betaWinLayOut);

    {Kleinere Schrift vereinbaren}
    gfont := GetPixelFont('F5X6NORM');
    SetTextStyle(gfont,HorizDir,1);
    betaWinLayOut.TFont := gfont;
    BetaWinLayOut.Font  := gfont;

    {X-Y-Achse}
    WinSetViewPort(20,15,WinGetMaxX,WinGetMaxY-1);
    ScaleColors(lightGray,Black, Black);
    Axis(ScaleData.xminninety,ScaleData.xmaxninety,'z-position (mm)',
            ScaleData.yminninety,ScaleData.ymaxninety,'   ',Axis0LayOut,
                       AxisXY or AxisFrame or AxisGrid );
  end; (* ihhs = 6,   3. und 4. Fenster*)

END;

(****************************************************************************)
procedure replotdata(inew : integer);
var
   i,j,iv               : integer;
   OldWin               : WinCtrlPtrType;
   zerochannel,chan3    : integer;
   ninetychannel,chan4  : integer;
   xmin,xmax,ymin,ymax  : real;
   ymin1,ymax1,ymin2,ymax2  : real;
   color                : word;
begin
  OldWin:=WinActCtrl;

  zerochannel   := scaledata.dispchannel0;
  ninetychannel := scaledata.dispchannel90;

  if ((inew = 1) OR (inew = 3)) then
  begin
    Changewin(zerowin);
    clearwin;

    if(ScaleData.autoscale0 = 0) then
    begin
      xmin:=ScaleData.xminzero;
      xmax:=ScaleData.xmaxzero;
      ymin:=ScaleData.yminzero;
      ymax:=ScaleData.ymaxzero;
    end;
    if(ScaleData.autoscale0 = 1) then
    begin
      xmin:=xdata0^[1];
      xmax:=xdata0^[ianzdata0[zerochannel]];
      ymin:=1.e10;
      ymax:=-1.e10;
      for i:=1 to ianzdata0[zerochannel] do
      begin
        if (ihhs = 1) or (ihhs = 2) or (ihhs = 3) then
        begin
          ymin:=minval(minval(ymin,ydata0^[zerochannel,i]),
               scaledata.resfactor0*yresdata0^[zerochannel,i]);
          ymax:=maxval(maxval(ymax,ydata0^[zerochannel,i]),
               scaledata.resfactor0*yresdata0^[zerochannel,i]);
        end;
        if (ihhs = 4)  then
        begin
          ymin:=minval(ymin,ydata0^[zerochannel,i]);
          ymax:=maxval(ymax,ydata0^[zerochannel,i]);
        end;
        if (ihhs = 6) then
        begin
          ymin:=minval(ymin,ydata0^[zerochannel,i]);
          ymax:=maxval(ymax,ydata0^[zerochannel,i]);
        end;
      end;
    end;

    if (ScaleData.autoscale0 = 1) then
    begin
      if (ihhs = 1) or (ihhs = 2) or (ihhs = 3) or (ihhs = 4) then
      begin
        ymax:=1.2*maxval(abs(ymin),abs(ymax));
        ymin:=-ymax;
      end;
      if (ihhs = 6) then
      begin
        ymax:=1.2*maxval(abs(ymin),abs(ymax));
        ymin:=-ymax;
      end;
    end;

    if ihhs = 1 then
      Axis(xmin,xmax,'number of turns ',
            ymin,ymax,'Voltage (V)',Axis0LayOut,
                       AxisXY or AxisFrame or AxisGrid );
    if (ihhs = 2) then
      Axis(xmin,xmax,'distance (mm) ',
            ymin,ymax,'magnetic field (a.u.)',Axis0LayOut,
                       AxisXY or AxisFrame or AxisGrid );
    if (ihhs = 3) then
      Axis(xmin,xmax,'distance (mm) ',
            ymin,ymax,'distance (mm)',Axis0LayOut,
                       AxisXY or AxisFrame or AxisGrid );
    if ihhs = 4 then
      Axis(xmin,xmax,'z-position (mm)',
            ymin,ymax,'Tesla mm',Axis0LayOut,
                       AxisXY or AxisFrame or AxisGrid );
    if ihhs = 6 then
      Axis(xmin,xmax,'z-position (mm)',
            ymin,ymax,'Tesla mm',Axis0LayOut,
                       AxisXY or AxisFrame or AxisGrid );
    InitShowLine(plotdatarec);

    color := red;
    for i:=1 to ianzdata0[zerochannel] do
    begin
      ShowLine(xdata0^[i],ydata0^[zerochannel,i],
                       color,Axis0LayOut,plotdatarec);
    end;
    color:=blue;
    InitShowLine(plotdatarec);
    for i:=1 to ianzdata0[zerochannel] do
    begin
      ShowLine(xdata0^[i],
               yresdata0^[zerochannel,i]*scaledata.resfactor0,
               color,Axis0LayOut,plotdatarec);
    end;

  end; (*if inew = 1 or inew = 3*)

(*****************************************************)

  if ((inew = 2) OR (inew = 3)) then
  begin
    Changewin(ninetywin);
    clearwin;

    if(ScaleData.autoscale90 = 0) then
    begin
      xmin:=ScaleData.xminninety;
      xmax:=ScaleData.xmaxninety;
      ymin:=ScaleData.yminninety;
      ymax:=ScaleData.ymaxninety;
    end;
    if(ScaleData.autoscale90 = 1) then
    begin
      xmin:=xdata90^[1];
      xmax:=xdata90^[ianzdata90[ninetychannel]];
      ymin:=1.e10;
      ymax:=-1.e10;
      for i:=1 to ianzdata90[ninetychannel] do
      begin
        if (ihhs = 1) or (ihhs = 2) or (ihhs = 3) then
        begin
          ymin:=minval(minval(ymin,ydata90^[ninetychannel,i]),
               scaledata.resfactor90*yresdata90^[ninetychannel,i]);
          ymax:=maxval(maxval(ymax,ydata90^[ninetychannel,i]),
               scaledata.resfactor90*yresdata90^[ninetychannel,i]);
        end;
        if (ihhs = 4) then
        begin
          ymin:=minval(ymin,ydata90^[ninetychannel,i]);
          ymax:=maxval(ymax,ydata90^[ninetychannel,i]);
        end;
        if (ihhs = 6) then
        begin
          ymin:=minval(ymin,ydata90^[ninetychannel,i]);
          ymax:=maxval(ymax,ydata90^[ninetychannel,i]);
        end;
      end;
    end;

    if(ScaleData.autoscale90 = 1) then
    begin
      if (ihhs = 1) or (ihhs = 2) or (ihhs = 3) or (ihhs = 4) then
      begin
        ymax:=1.2*maxval(abs(ymin),abs(ymax));
        ymin:=-ymax;
      end;
      if (ihhs = 6) then
      begin
        ymax:=1.2*maxval(abs(ymin),abs(ymax));
        ymin:=-ymax;
      end;
    end;

    if ihhs = 1 then
      Axis(xmin,xmax,'number of turns ',
            ymin,ymax,'Voltage (V)',Axis0LayOut,
                       AxisXY or AxisFrame or AxisGrid );
    if (ihhs = 2) or (ihhs = 3) then
      Axis(xmin,xmax,'distance (mm) ',
            ymin,ymax,'displacement (mue m)',Axis0LayOut,
                       AxisXY or AxisFrame or AxisGrid );
    if ihhs = 4 then
      Axis(xmin,xmax,'time',
            ymin,ymax,'Tesla mm**2',Axis0LayOut,
                       AxisXY or AxisFrame or AxisGrid );
    if ihhs = 6 then
      Axis(xmin,xmax,'z-position (mm)',
            ymin,ymax,'Tesla mm',Axis0LayOut,
                       AxisXY or AxisFrame or AxisGrid );
    InitShowLine(plotdatarec);
    color := red;
    for i:=1 to ianzdata90[ninetychannel] do
    begin
      (*      writeln(i,ydata90[ninetychannel,i]);*)
      ShowLine(xdata0^[i], ydata90^[ninetychannel,i],
                       color,Axis0LayOut,plotdatarec);
    end;
    color:=blue;
    InitShowLine(plotdatarec);
    for i:=1 to ianzdata90[ninetychannel] do
    begin
      ShowLine(xdata0^[i], yresdata90^[ninetychannel,i]
               *scaledata.resfactor90, color,Axis0LayOut,plotdatarec);
    end;
  end; (*if inew=2 or inew=3*)

  changewin(oldwin);
end;
(***********************************************)

procedure replotdataSW2(var iv,iyz,ilz : integer);
(* iv = Anzahl der Positionen; iyz= 1(By), 2(Bz);ilz 1=1.Messung, 2=Wdhlg *)
var
   i,j,l                : integer;
   OldWin               : WinCtrlPtrType;
   zerochannel,chan3    : integer;
   ninetychannel,chan4  : integer;
   xmin,xmax,ymin,ymax  : real;
   ymin1,ymax1,ymin2,ymax2  : real;
   color                : word;
   dff,y1d,y2d          :real;
begin
  OldWin:=WinActCtrl;

  zerochannel   := scaledata.dispchannel0;
  ninetychannel := scaledata.dispchannel90;
  chan3         := scaledata.dispchannel0;
  chan4         := scaledata.dispchannel90;
  (******************************* SW2Bloc**************************)
{  for j:=1 to MesRep do
  begin}
    if iyz=1 then Changewin(zerowin);
    if iyz=2 then ChangeWin(ninetywin);
    if (ilz=1) then ClearWin;

     ymin1:=1.e10;
     ymax1:=-1.e10;
     ymin2:=1.e10;
     ymax2:=-1.e10;
    xmin:=xdata0^[1];
    xmax:=xdata0^[ianzdata0[zerochannel]];
    for i:=1 to GridData.ianzz do
    begin
      ymax1:=maxval(ymax1,yResdata_Byz^[iv,1,i]);
      ymin1:=minval(ymin1,yResdata_Byz^[iv,1,i]);

      ymax2:=maxval(ymax2,yResdata_Byz^[iv,2,i]);
      ymin2:=minval(ymin2,yResdata_Byz^[iv,2,i]);

    end;
    if iyz=1 then
    begin
      ymax1:=1.2*maxval(abs(ymin1),abs(ymax1));
      ymin1:=-ymax1;
      ymax:=ymax1;
      ymin:=ymin1;
    end;
    if iyz=2 then
    begin
      ymax2:=1.2*maxval(abs(ymin2),abs(ymax2));
      ymin2:=-ymax2;
      ymax:=ymax2;
      ymin:=ymin2;
    end;

    Axis(xmin,xmax,'z-position (mm)',
            ymin,ymax,'Tesla mm',Axis0LayOut,
                       AxisXY or AxisFrame or AxisGrid );

    InitShowLine(plotdatarec);
    if ilz=1 then color:=blue;
    if ilz=2 then color:=red;
    for i:=1 to GridData.ianzz do
    begin
      if iyz=1 then ShowLine(xResData^[i],yResdata_Byz^[iv,2*ilz-1,i]*scaledata.resfactor0,
                       color,Axis0LayOut,plotdatarec);
      if iyz=2 then ShowLine(xResData^[i],yResdata_Byz^[iv,2*ilz,i]*scaledata.resfactor0,
                       color,Axis0LayOut,plotdatarec);
    end;
    color:=blue;
{  end;    (* j=1..2 *)}
  if ilz=2 then  (*Differenz anzeigen*)
  begin
    dff:=100;     (* 100/dff = % des Messwertes*)
    y1d:=ymin/dff;
    y2d:=ymax/dff;
    if y2d<5e-3 then
    begin
      y2d:=5e-3;
      y1d:=-5e-3;
    end;
    if iyz=1 then Changewin(win3);
    if iyz=2 then ChangeWin(win4);
    ClearWin;
    Axis(xmin,xmax,'z-position (mm)', y1d,y2d,'Tesla mm',Axis0LayOut,
                       AxisXY or AxisFrame or AxisGrid );
    InitShowLine(plotdatarec);
    for i:=1 to GridData.ianzz do
    begin
      if iyz=1 then ShowLine(xResData^[i],
        (yresdata_Byz^[iv,1,i]-yresdata_Byz^[iv,3,i])*scaledata.resfactor0,
        color,Axis0LayOut,plotdatarec);
      if iyz=2 then ShowLine(xResData^[i],
        (yresdata_Byz^[iv,2,i]-yresdata_Byz^[iv,4,i])*scaledata.resfactor0,
        color,Axis0LayOut,plotdatarec);
    end;
  end;     (* ilz=2 *)
  changewin(oldwin);
END;

(***********************************************)
procedure HHSoutputscreen(var imsr:integer);
var
   i         : integer;
   C_Col:char;
   radgra,DeltaAbs: real;
   PosStr1         : String;
Procedure calculate_Moments(var imsr:integer);
begin
  if (imsr=1) then
  begin
    if (definedata.zero = 1) then
      HHSR[imsr,7]:=speed0
    else HHSR[imsr,7]:=9999;
    if (definedata.ninety = 1) then
      HHSR[imsr,8]:=speed90
    else  HHSR[imsr,8]:=9999;
  end;
  if (imsr=2) then
  begin
    if (definedata.zero = 1) then
      HHSR[imsr,7]:=speed0
    else HHSR[imsr,7]:=9999;
    if (definedata.ninety = 1) then
      HHSR[imsr,8]:=speed90
    else HHSR[imsr,8]:=9999;
  end;

  HHSR[imsr,1]:=0.0;                       {Mx_0, nicht benutzt}
  if (definedata.ninety = 1) then
    HHSR[imsr,2]:=1000*(mx/abs(HHSR[imsr,8]))    {Mx_90}
  else HHSR[imsr,2]:=0.0;

  if (definedata.zero = 1) then
    HHSR[imsr,3]:=1000*(my_0/abs(HHSR[imsr,7]))
  else HHSR[imsr,3]:=0.0;   {My_0}
  if (definedata.ninety = 1) then
    HHSR[imsr,4]:=1000*(my_90/abs(HHSR[imsr,8])) {My_90}
  else HHSR[imsr,4]:=0.0;

  if (definedata.zero = 1) then
    HHSR[imsr,5]:=1000*(mz/abs(HHSR[imsr,7]))
  else HHSR[imsr,5]:=0.0;     {Mz_0}
  if (definedata.ninety = 1) then
    HHSR[imsr,6]:=1000*(mz/abs(HHSR[imsr,8]))
  else HHSR[imsr,6]:=0.0;    {Mz_90, nicht benutzt}
end;

begin
  if (DVM3Data.Range <>0) then CheckDelta:=DVM3Data.Range;
  PosStr1:=copy(BlocName,1,1);
(*    pi:=4.0*atan(1.0);*)
  radgra:=180.0/pi;
  if definedata.zero = 1 then
  begin
    Changewin(zeroanalysiswin);
    writeln(C_blue,' ');
    writeln('Amplitude (mV/Hz): ',1000.0*(amp0[1]/abs(speed0)):8:3);
    writeln('     Angle (Grad): ',radgra*phi0[1]:8:3);
    writeln('  Data_rms (0/00): ',abs(1000.0*(data0_rms/amp0[1])):8:3);
    writeln(' Speed_rms (0/00): ',abs(1000.0*(speed0_rms/abs(speed0))):8:3);
    ChangeWin(MainScreen);
  end;

  if definedata.ninety = 1 then

  begin
    Changewin(ninetyanalysiswin);
    writeln(C_blue,'   ');
    writeln('Amplitude (mV/Hz): ',1000.0*(amp90[1]/abs(speed90)):8:3);
    writeln('     Angle (Grad): ',radgra*phi90[1]:8:3);
    writeln('  Data_rms (0/00): ',abs(1000.0*(data90_rms/amp90[1])):8:3);
    writeln(' Speed_rms (0/00): ',abs(1000.0*(speed90_rms/abs(speed90))):8:3);
    ChangeWin(MainScreen);
  end;
  Calculate_Moments(imsr);
  Changewin(resultwin);
  clearwin;
  writeln(C_blue,'                  RESULTS');
  if (HHSBlocPos=1) then
  begin
    writeln(C_blue,'        M_z = ',1000*(mz/abs(speed0)):8:3);
    if (definedata.ninety = 1) then
      writeln(C_blue,'        M_x = ',1000*(mx/abs(speed90)):8:3)
    else
      writeln(C_blue,'        M_x = 0.0');

      writeln(C_blue,'   M_y (0)  = ',1000*(my_0/abs(speed0)):8:3);
    if (definedata.ninety = 1) then
      writeln(C_blue,'   M_y (90) = ',1000*(my_90/abs(speed90)):8:3)
    else
      writeln(C_blue,'   M_y (90) = 0.0');
  end;

  if ((HHSBlocPos=2) and (imsr=1)) then
  begin
    writeln(C_Blue,'M_x    = ',HHSR[1,2]:8:3);
    writeln(C_BLUE,'M_y(0) = ',HHSR[1,3]:8:3);
    writeln(C_BLUE,'M_y(90)= ',HHSR[1,4]:8:3);
    writeln(C_BLUE,'M_z    = ',HHSR[1,5]:8:3);
  end;

  if ((HHSBlocPos=2) and (imsr=2)) then
  begin
    writeln(C_Blue,'Speed0_1 = ',HHSR[1,7]:8:3,'   Speed0_2 = ',HHSR[2,7]:8:3,
    '   Speed0_1 -Speed0_2 = ',(HHSR[1,7]-HHSR[2,7]):8:3);
    writeln(C_Blue,'Speed90_1= ',HHSR[1,8]:8:3,'   Speed90_2= ',HHSR[2,8]:8:3,
    '   Speed90_1-Speed90_2= ',(HHSR[1,8]-HHSR[2,8]):8:3);

    write(C_Blue,'Mx_1     = ',HHSR[1,2]:8:3,'   Mx_2     = ',HHSR[2,2]:8:3);
    if ((PosStr1='B') or (PosStr1='b')) then
    begin
      C_col:=C_Red;
      DeltaAbs:=Abs(HHSR[1,2]+HHSR[2,2]);
      if (DeltaAbs<2*CheckDelta) then C_col:=C_Yellow;
      if (DeltaAbs<CheckDelta) then C_col:=C_Blue;
      Write(C_Col,'   Mx_1+Mx_2          = ',DeltaAbs:8:3);
    end;
    if ((PosStr1='A') or (PosStr1='a')) then
    begin
      C_col:=C_Red;
      DeltaAbs:=Abs(HHSR[1,2]+HHSR[2,3]);
      if (DeltaAbs<2*CheckDelta) then C_col:=C_Yellow;
      if (DeltaAbs<CheckDelta) then C_col:=C_Blue;
      Write(C_Col,'   Mx_1+My(0)_2       = ',DeltaAbs:8:3);
    end;
    writeln;

    write(C_BLUE,'My(0)_1  = ',HHSR[1,3]:8:3,'   My(0)_2  = ',HHSR[2,3]:8:3);
    if ((PosStr1='B') or (PosStr1='b')) then
    begin
      C_col:=C_Red;
      DeltaAbs:=Abs(HHSR[1,3]+HHSR[2,3]);
      if (DeltaAbs<2*CheckDelta) then C_col:=C_Yellow;
      if (DeltaAbs<CheckDelta) then C_col:=C_Blue;
      Write(C_Col,'   My(0)_1+My(0)_2    = ',DeltaAbs:8:3);
    end;
    if ((PosStr1='A') or (PosStr1='a')) then
    begin
      C_col:=C_Red;
      DeltaAbs:=Abs(HHSR[1,3]+HHSR[2,2]);
      if (DeltaAbs<2*CheckDelta) then C_col:=C_Yellow;
      if (DeltaAbs<CheckDelta) then C_col:=C_Blue;
      Write(C_Col,'   My(0)_1+Mx_2       = ',DeltaAbs:8:3);
    end;
    writeln;

    write(C_BLUE,'My(90)_1 = ',HHSR[1,4]:8:3,'   My(90)_2 = ',HHSR[2,4]:8:3);
    if ((PosStr1='B') or (PosStr1='b')) then
    begin
      C_col:=C_Red;
      DeltaAbs:=Abs(HHSR[1,4]+HHSR[2,4]);
      if (DeltaAbs<2*CheckDelta) then C_col:=C_Yellow;
      if (DeltaAbs<CheckDelta) then C_col:=C_Blue;
      Write(C_Col,'   My(90)_1+My(90)_2  = ',DeltaAbs:8:3);
    end;
    writeln;

    write(C_BLUE,'Mz_1     = ',HHSR[1,5]:8:3,'   Mz_2     = ',HHSR[2,5]:8:3);
    if ((PosStr1='B') or (PosStr1='b')) then
    begin
      C_col:=C_Red;
      DeltaAbs:=Abs(HHSR[1,5]-HHSR[2,5]);
      if (DeltaAbs<2*CheckDelta) then C_col:=C_Yellow;
      if (DeltaAbs<CheckDelta) then C_col:=C_Blue;
      Write(C_Col,'   Mz_1-Mz_2          = ',DeltaAbs:8:3);
    end;
    if ((PosStr1='A') or (PosStr1='a')) then
    begin
      C_col:=C_Red;
      DeltaAbs:=Abs(HHSR[1,5]+HHSR[2,5]);
      if (DeltaAbs<2*CheckDelta) then C_col:=C_Yellow;
      if (DeltaAbs<CheckDelta) then C_col:=C_Blue;
      Write(C_Col,'   Mz_1+Mz_2          = ',DeltaAbs:8:3);
    end;

  end;
  ChangeWin(MainScreen);

  replotdata(3);
END;

END.