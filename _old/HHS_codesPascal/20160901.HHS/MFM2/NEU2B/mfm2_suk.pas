{$N+}
unit mfm2_suk;
{03.02.2015 HHS+SW2}
{19.09.2003  SetUp Prozeduren}
{18.10.2006 Wdhlg in DefineGrid eingebaut}
{21.06.2007 ParkPos in DefineGrid eingebaut}
{29.06.2007 ToleranzMax in DefineGrid eingebaut}
INTERFACE

USES DOS, CRT, Graph, SCALELIB, MBBASE, BESLIB, SDLIB,
          STDLIB, MOUSELIB, VESALIB, WINLIB,
          TPDECL, AUXIO1,
          MFM2_TCK,MFM2_HWK,MFM2_UFk,MFM2_ANK,MFM2_SCk;

procedure DataBackSlash;
procedure CFGFileNameProc;
procedure SetUpStoreLoadProc;
procedure SetUpStoreProc;
procedure SetUpLoadProc(setupini : integer);
procedure SetUpAnalysisProc;
procedure adjustreadings;
procedure SetUpMBInc1Proc;
procedure SetUpMBInc1bProc;
procedure SetUpMBInc2Proc;
procedure SetUpDACProc;
procedure SetUpServoProc;
procedure SetUpDefineHHSProc;
procedure SetUpDefine5mProc;
procedure SetUpSMSProc(iSMS: integer);
procedure SetUpIntegratorProc;
procedure SetUpDVMProc(iDVM: integer);
procedure SetUpScaleProc;
procedure SetUpProc;
procedure ServoParModify(Controller_loc : LongInt;Axis_loc : LongInt);
procedure STWIParModify(Controller_loc : LongInt; Axis_loc : LongInt);
procedure Choose_Cont_Axis;
procedure DefineGrid;
procedure DefineRefOffsets;

IMPLEMENTATION

(************************************************************)
procedure DataBackSlash;
var iwo:byte;
    strend:String[1];
begin
  iwo:=length(pathdata);
  strend:=copy(pathdata,iwo,1);
  if (strend<>'\') then pathdata:=pathdata+'\';
end;

procedure CFGFileNameProc;
var
     OldWin             : WinCtrlPtrType;

begin

  OldWin:=WinActCtrl;

repeat
begin
SetCursor(0,0);
CFGFileName := editstr(CFGFileName,0,14);
if (CFGFilename = 'STWI1.CFG') or (CFGFilename = 'STWI2.CFG') or
   (CFGFilename = 'stwi1.cfg') or (CFGFilename = 'stwi2.cfg') then
   begin
   changewin(warningswin);
   clearwin;
   writeln(' use other file name ');
   write(' (press return) ');
   readkey;
   end;
clearwin;
changewin(OldWin);

end;
until (CFGFilename <> 'STWI1.CFG')  and  (CFGFilename <> 'STWI2.CFG')
  and (CFGFilename <> 'stwi1.cfg')  and  (CFGFilename <> 'stwi2.cfg');
CFGFileName0 := CFGFileName;

changewin(mainscreen);

end;

(*************************************************************)
procedure SetUpStoreLoadProc;

 VAR SetUpStoreWinLayOut                    : WinDesType;
     OldWin,SetUpStoreWin                   : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent                          : WORD;
     RampStr                            : String[8];

 BEGIN

  OldWin:=WinActCtrl;

  SetUpStoreWinLayOut := WinDefDes;
  WinSetHeader(SetUpStoreWinLayOut,LightGray,
               C_LightBlue+'Configuration File');
  WinSetFillColor(SetUpStoreWinLayOut, yellow);
  WinSetOpenMode(SetUpStoreWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,2,18,width,height);
  xpos:=(3*width) div 4; ypos:=145;
  WinHeight:= 2*height;
  SetUpStorewin:=OpenWin(xpos,ypos, xpos+width,
                                  ypos+WinHeight,SetUpStoreWinLayOut);

  ChangeWin(SetUpStoreWin);

  CFGFileNameProc;

  CloseWin(SetUpStorewin);

  ChangeWin(OldWin);
           end;

(******************************************************)
procedure SetUpStoreProc;
var
     AskWinLayOut       : WinDesType;
     OldWin, AskWin     : WinCtrlPtrType;
     dummy              : string[1];
     width,height,xpos,ypos,WinHeight  : WORD;

          begin

          OldWin:=WinActCtrl;

          SetUpStoreLoadProc;       (* get configuration file name *)

          AskWinLayOut := WinDefDes;
          WinSetHeader(AskWinLayOut,LightGray,
               C_LightBlue+'Hello');
          WinSetFillColor(AskWinLayOut, magenta);
          WinSetOpenMode(AskWinLayOut, WinHeadText, XmmSave);

          GetButtonDim(FONT8x16,2,18,width,height);
          xpos:=(3*width) div 4; ypos:=145;
          WinHeight:= 3*height;
          Askwin:=OpenWin(xpos,ypos, xpos+width,
                                  ypos+WinHeight,AskWinLayOut);

          ChangeWin(AskWin);

          writeln(' Do you really want to write');
          writeln(' configuration data to File');
          writeln(' ');
          writeln('  ',CFGFileName,' ?           (y/n)');

          dummy:=readkey;
          CloseWin(AskWin);
          if (dummy = 'y') or (dummy = 'Y') then   (* store data *)
          begin

          filename:=CFGFileName;
{          filename0:=CFGFileName0; }

{          writeln(cfgfilename);  }

          openoutfile(0,1);
(*
          writeln(outfile,DACData.StartVoltage);
          writeln(outfile,DACData.EndVoltage);
          writeln(outfile,DACData.Time);
          writeln(outfile,DACData.Steps);
          writeln(outfile,SMSData.TimeBase);
          writeln(outfile,SMSData.NumberOfSteps);
          writeln(outfile,SMSData.Frequency);
          writeln(outfile,SMSData.PulseLength);
          writeln(outfile,DVMData.Aperture);
          writeln(outfile,DVMData.Readings);
*)
          CFGData.DefineData   :=DEFINEData;
          CFGData.GridData     :=GridData;
          CFGData.AnalysisData :=AnalysisData;
          CFGData.DACData      :=DACData;
          CFGData.MBInc1Data   :=MBInc1Data;
          CFGData.MBInc2Data   :=MBInc2Data;
          CFGData.SMS1Data     :=SMS1Data;
          CFGData.SMS2Data     :=SMS2Data;
          CFGData.DVM1Data     :=DVM1Data;
          CFGData.DVM2Data     :=DVM2Data;
          CFGData.DVM3Data     :=DVM3Data;
          CFGData.PAR32DATA    :=PAR32Data;
          CFGData.ScaleData    :=ScaleData;
          CFGData.ServoData    :=ServoData;
          CFGData.ServoParData :=ServoParData;

          write(CFGFile,CFGData);

          closeoutfile(0,1);

          if (ihhs = 6) then
          begin
(************* spaeter in Hauptfile integrieren *)
          STWICFGData.STWIData :=STWIData;
          STWIfilename:=STWICFGfilename;
          openoutfile(2,1);
          write(STWICFGFile,STWICFGData);
          closeoutfile(2,1);
(************************************************)
          end;

          end;              (* ask *)

          changewin(oldwin);

          end;

(******************************************************)
procedure SetUpLoadProc(setupini : integer);
var
  OldWin                   : WinCtrlPtrType;

begin
  OldWin:=WinActCtrl;

  if setupini = 0 then
  begin
    SetUpStoreLoadProc;     (* get configuration file name *)
  end;
  setupini := 0;

  filename0:=CFGFileName0;
{          filename:=CFGFileName;   }

  openoutfile(1,1);

  read(CFGFile0,CFGData0);

  DefineData   :=CFGData0.DefineData;
  GridData     :=CFGData0.GridData;
  AnalysisData :=CFGData0.AnalysisData;
  DACData      :=CFGData0.DACData;
  MBInc1Data   :=CFGData0.MBInc1Data;
  MBInc2Data   :=CFGData0.MBInc2Data;
  SMS1Data     :=CFGData0.SMS1Data;
  SMS2Data     :=CFGData0.SMS2Data;
  DVM1Data     :=CFGData0.DVM1Data;
  DVM2Data     :=CFGData0.DVM2Data;
  DVM3Data     :=CFGData0.DVM3Data;
  PAR32Data    :=CFGData0.PAR32Data;
  Scaledata    :=CFGData0.ScaleData;
  Servodata    :=CFGData0.ServoData;
  ServoPardata :=CFGData0.ServoParData;
(*
  readln(outfile,DACData.StartVoltage);
  readln(outfile,DACData.EndVoltage);
  readln(outfile,DACData.Time);
  readln(outfile,DACData.Steps);
          etc.....
*)
  closeoutfile(1,1);

  if (ihhs = 6) then
  begin
(************* spaeter in Hauptfile integrieren  *)
    STWIfilename0:=STWICFGfilename0;
    openoutfile(3,1);
    read(STWICFGFile0,STWICFGData0);
    closeoutfile(3,1);
    STWIData:=STWICFGData0.STWIData;
(************************************************)
  end;

  changewin(oldwin);

END;

(***********************************************************)
PROCEDURE SetUpAnalysisProc;
  CONST SpeedCorrID   = 1000;
        PtsPerRevID   = 1001;
        NrOfRevID     = 1002;
        OrdOfFAID     = 1003;
        Hall_Pr_RespID= 1004;
        firstID  = SpeedCorrID;
        lastID   = Hall_Pr_RespID;

  VAR SetUpAnalysisWinLayOut       : WinDesType;
     OldWin,SetUpAnalysisWin        : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent                     : WORD;
     old_speedcorr                : integer;
BEGIN

  OldWin:=WinActCtrl;
  old_speedcorr:=AnalysisData.SpeedCorr;

  SetUpAnalysisWinLayOut := WinDefDes;
  WinSetHeader(SetUpAnalysisWinLayOut,LightGray,
               C_LightBlue+'Define Analysis');
  WinSetFillColor(SetUpAnalysisWinLayOut, yellow);
  WinSetOpenMode(SetUpAnalysisWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=(3*width) div 4; ypos:=50;
  WinHeight:= 6*height;
  SetUpAnalysiswin:=OpenWin(xpos,ypos, xpos+7*width div 4,
                                  ypos+WinHeight,SetUpAnalysisWinLayOut);

  ChangeWin(SetUpAnalysisWin);
  SetMargin(5,LeftMargin);
  SetCursor(0,0);
  ShowLineObject('  Speed Correction = ',32, SpeedCorrID, buttonleftevent,
                    NoKey, Local,NilEvProc);
  WRITELN(AnalysisData.SpeedCorr:6);
  ShowLineObject('Pts per Revolution = ',32, PtsPerRevID, buttonleftevent,
                      NoKey, Local,NilEvProc);
  WRITELN(AnalysisData.PtsPerRev:10:2);
  ShowLineObject('Nr. of Revolutions = ',32, NrOfRevID, buttonleftevent,
                      NoKey, Local,NilEvProc);
  WRITELN(AnalysisData.NrOfRev:6);
  ShowLineObject('Fourier Exp. Ord.  = ',32, OrdOfFAID, buttonleftevent,
                      NoKey, Local,NilEvProc);
  WRITELN(AnalysisData.OrdOfFA:6);
  ShowLineObject('Hall Pr. Resp. (ms)= ',32, Hall_Pr_RespID, buttonleftevent,
                       NoKey, Local,NilEvProc);
  WRITELN(AnalysisData.Hall_Pr_Resp:8:3);

  repeat
    locEvent:=WaitOfEvent(firstID,lastID);
    case locEvent of
      SpeedCorrID   : BEGIN
            SetCursor(21,0);
            AnalysisData.SpeedCorr := Editint(AnalysisData.SpeedCorr,0,1,6);
            SetCursor(21,0);
            WRITELN(AnalysisData.SpeedCorr:6);
                  END;
      PtsPerRevID   : BEGIN
            SetCursor(21,1);
            AnalysisData.PtsPerRev :=
                       Editreal(AnalysisData.PtsPerRev,0,100000,10,5);
            SetCursor(21,1);
            WRITELN(AnalysisData.PtsPerRev:10:2);
                  END;
      NrOfRevID   : BEGIN
            SetCursor(21,2);
            AnalysisData.NrOfRev := Editint(AnalysisData.NrOfRev,0,100,6);
            SetCursor(21,2);
            WRITELN(AnalysisData.NrOfRev:6);
                  END;
      OrdOfFaID   : BEGIN
            SetCursor(21,3);
            AnalysisData.OrdOfFA := Editint(AnalysisData.OrdOfFA,0,10,6);
            SetCursor(21,3);
            WRITELN(AnalysisData.OrdOfFA:6);
                  END;
      Hall_Pr_RespID   : BEGIN
            SetCursor(21,4);
            AnalysisData.Hall_Pr_Resp:=
                       Editreal(AnalysisData.Hall_Pr_Resp,0,1000,8,3);
            SetCursor(21,4);
            WRITELN(AnalysisData.Hall_Pr_Resp:8:3);
                  END;
    END;   (*case locEvent*)
  until locEvent=0;;
  ChangeWin(OldWin);
  CloseWin(SetUpAnalysiswin);

  if ifirst = 0 then
  begin
    if (AnalysisData.SpeedCorr > old_speedcorr) and (ihhs = 1) then
    begin
      SpeedCorrection;
      fa;
    end;

    if (AnalysisData.SpeedCorr < old_speedcorr) and (ihhs = 1) then
    begin
      RestoreRawData;
      fa;
    end;
  end;

END;


(************************************************************)
PROCEDURE adjustreadings;
var
     rloc,floc,sloc:real;
     oldreadings   : integer;
BEGIN
  oldreadings:=DVM1Data.Readings;
  rloc:=DVM1Data.Readings;
  floc:=MBINC1Data.Delay_4;
  sloc:=rloc/floc;
  DVM1Data.Readings:=round(sloc)*MBINC1Data.Delay_4;
  if DVM1Data.Readings > oldreadings then
    DVM1Data.Readings := DVM1Data.Readings-MBInc1Data.Delay_4;
END;

(************************************************************)
PROCEDURE SetUpMBInc1Proc;
  CONST n1ID     = 1001;
        n2ID     = 1002;
        n3ID     = 1003;
        n4ID     = 1004;
        n5ID     = 1005;
        n6ID     = 1006;
        firstID  = n1ID;
        lastID   = n6ID;

 VAR SetUpMBIncWinLayOut                    : WinDesType;
     OldWin,SetUpMBIncWin                   : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent                          : WORD;
     RampStr                            : String[8];

BEGIN
  OldWin:=WinActCtrl;

  SetUpMBIncWinLayOut := WinDefDes;
  WinSetHeader(SetUpMBIncWinLayOut,LightGray,
               C_LightBlue+'MBInc Set UP');
  WinSetFillColor(SetUpMBIncWinLayOut, yellow);
  WinSetOpenMode(SetUpMBIncWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=(3*width) div 4; ypos:=50;
  WinHeight:= 6*height;
  SetUpMBIncwin:=OpenWin(xpos,ypos, xpos+8*width div 4,
                                  ypos+WinHeight,SetUpMBIncWinLayOut);

  ChangeWin(SetUpMBIncWin);
  SetMargin(5,LeftMargin);
  SetCursor(0,0);
  ShowLineObject('     Revolutions before Start = ',38, n1ID, buttonleftevent,
                       NoKey, Local,NilEvProc);
  WRITELN(MBInc1Data.delay_1:5);

  ShowLineObject('          Offset before Start = ',38, n2ID, buttonleftevent, NoKey, Local,
                  NilEvProc);
  WRITELN(MBInc1Data.delay_2:5);

  ShowLineObject('           Factor for Trigger = ',38, n3ID, buttonleftevent, NoKey, Local,
                  NilEvProc);
  WRITELN(MBInc1Data.delay_3:5);

  ShowLineObject(' Factor for Speed Measurement = ',38, n4ID, buttonleftevent, NoKey, Local,
                  NilEvProc);
  WRITELN(MBInc1Data.delay_4:5);
  ShowLineObject('             Time Base (1..6) = ',38, n5ID, buttonleftevent, NoKey, Local,
                  NilEvProc);
  WRITELN(MBInc1Data.time_base:5);
  ShowLineObject('           Offset before Stop = ',38, n6ID, buttonleftevent, NoKey, Local,
                  NilEvProc);
  WRITELN(MBInc1Data.delay_5:5);

  repeat
    locEvent:=WaitOfEvent(firstID,lastID);
    case locEvent of
      n1ID   : BEGIN
            SetCursor(32,0);
            MBInc1Data.Delay_1 := Editint(MBInc1Data.Delay_1,0,1000,5);
            SetCursor(32,0);
            WRITELN(MBInc1Data.Delay_1:5);
                  END;
      n2ID   : BEGIN
            SetCursor(32,1);
            MBInc1Data.Delay_2 := Editint(MBInc1Data.Delay_2,0,20000,5);
            SetCursor(32,1);
            WRITELN(MBInc1Data.Delay_2:5);
                  END;
      n3ID   : BEGIN
            SetCursor(32,2);
            MBInc1Data.Delay_3 := Editint(MBInc1Data.Delay_3,0,10240,5);
            SetCursor(32,2);
            if odd(MBInc1Data.Delay_3) = TRUE then
               MBInc1Data.Delay_3 := MBInc1Data.Delay_3-1;
            WRITELN(MBInc1Data.Delay_3:5);
                  END;
      n4ID   : BEGIN
            SetCursor(32,3);
            MBInc1Data.Delay_4 := Editint(MBInc1Data.Delay_4,0,10000,5);
            SetCursor(32,3);
            WRITELN(MBInc1Data.Delay_4:5);
            adjustreadings;
                  END;
      n5ID   : BEGIN
            SetCursor(32,4);
            MBInc1Data.Time_Base := Editint(MBInc1Data.Time_Base,1,6,5);
            SetCursor(32,4);
            WRITELN(MBInc1Data.Time_Base:5);
                  END;
      n6ID   : BEGIN
            SetCursor(32,5);
            MBInc1Data.Delay_5 := Editint(MBInc1Data.Delay_5,0,10000,5);
            SetCursor(32,5);
            WRITELN(MBInc1Data.Delay_5:5);
                  END;
    END;
  until locEvent=0;;
  ChangeWin(OldWin);
  CloseWin(SetUpMBIncwin);
END;

(************************************************************)
PROCEDURE SetUpMBInc1bProc;    (*SW2 - Betonblock*)
  CONST n1ID     = 1001;
        n2ID     = 1002;
        n3ID     = 1003;
        n4ID     = 1004;
        n5ID     = 1005;
        timebase1ID = 1006;
        firstID  = n1ID;
        lastID   = timebase1ID;

VAR SetUpMBIncWinLayOut                    : WinDesType;
    OldWin,SetUpMBIncWin                   : WinCtrlPtrType;
    width,height,xpos,ypos,WinHeight,
    locEvent                               : WORD;
    RampStr                                : String[8];

BEGIN

  OldWin:=WinActCtrl;

  SetUpMBIncWinLayOut := WinDefDes;
  WinSetHeader(SetUpMBIncWinLayOut,LightGray,
               C_LightBlue+'MBInc Set UP');
  WinSetFillColor(SetUpMBIncWinLayOut, yellow);
  WinSetOpenMode(SetUpMBIncWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=(3*width) div 4; ypos:=50;
  WinHeight:= 6*height;
  SetUpMBIncwin:=OpenWin(xpos,ypos, xpos+8*width div 4,
                                  ypos+WinHeight,SetUpMBIncWinLayOut);

  ChangeWin(SetUpMBIncWin);
  SetMargin(5,LeftMargin);
  SetCursor(0,0);
  ShowLineObject('                      Delay_1 = ',38, n1ID, buttonleftevent,
                                        NoKey, Local,NilEvProc);
  WRITELN(MBInc1Data.Delay_1:5);
  ShowLineObject('                      Delay_2 = ',38, n2ID, buttonleftevent,
                                         NoKey, Local,NilEvProc);
  WRITELN(MBInc1Data.Delay_2:5);
  ShowLineObject('                      Delay_3 = ',38, n3ID, buttonleftevent,
                                        NoKey, Local,NilEvProc);
  WRITELN(MBInc1Data.Delay_3:5);
  ShowLineObject('                      Delay_4 = ',38, n4ID, buttonleftevent,
                                        NoKey, Local,NilEvProc);
  WRITELN(MBInc1Data.Delay_4:5);
  ShowLineObject('                      Delay_5 = ',38, n5ID, buttonleftevent,
                                        NoKey, Local,NilEvProc);
  WRITELN(MBInc1Data.Delay_5:5);
  ShowLineObject('    Timebase_1:               = ',38, TimeBase1ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(MBInc1Data.time_base:5);

  repeat
    locEvent:=WaitOfEvent(firstID,lastID);
    case locEvent of
      n1ID   : BEGIN
            SetCursor(32,0);
            MBInc1Data.Delay_1 := Editint(MBInc1Data.Delay_1,0,1000,5);
            SetCursor(32,0);
            WRITELN(MBInc1Data.Delay_1:5);
                  END;
      n2ID   : BEGIN
            SetCursor(32,1);
            MBInc1Data.Delay_2 := Editint(MBInc1Data.Delay_2,0,20000,5);
            SetCursor(32,1);
            WRITELN(MBInc1Data.Delay_2:5);
                  END;
      n3ID   : BEGIN
            SetCursor(32,2);
            MBInc1Data.Delay_3 := Editint(MBInc1Data.Delay_3,0,1000,5);
            SetCursor(32,2);
            WRITELN(MBInc1Data.Delay_3:5);
                  END;
      n4ID   : BEGIN
            SetCursor(32,3);
            MBInc1Data.Delay_4 := Editint(MBInc1Data.Delay_4,0,10000,5);
            SetCursor(32,3);
            WRITELN(MBInc1Data.Delay_4:5);
                  END;
      n5ID   : BEGIN
            SetCursor(32,4);
            MBInc2Data.Delay_5 := Editint(MBInc2Data.Delay_5,1,10000,5);
            SetCursor(32,4);
            WRITELN(MBInc2Data.Delay_5:5);
                  END;
      TimeBase1ID   : BEGIN
            SetCursor(32,5);
            MBInc1Data.Time_Base := Editint(MBInc1Data.Time_Base,1,5,5);
            SetCursor(32,5);
            WRITELN(MBInc1Data.Time_Base:5);
                  END;
    END;
  until locEvent=0;;
  ChangeWin(OldWin);
  CloseWin(SetUpMBIncwin);
END;


(************************************************************)
PROCEDURE SetUpMBInc2Proc;
  CONST n1ID     = 1001;
        n2ID     = 1002;
        n3ID     = 1003;
        n4ID     = 1004;
        n5ID     = 1005;
        timebase1ID = 1006;
        timebase2ID = 1007;
        firstID  = n1ID;
        lastID   = timebase2ID;

 VAR SetUpMBIncWinLayOut                    : WinDesType;
     OldWin,SetUpMBIncWin                   : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent                          : WORD;
     RampStr                            : String[8];

BEGIN

  OldWin:=WinActCtrl;

  SetUpMBIncWinLayOut := WinDefDes;
  WinSetHeader(SetUpMBIncWinLayOut,LightGray,
               C_LightBlue+'MBInc Set UP');
  WinSetFillColor(SetUpMBIncWinLayOut, yellow);
  WinSetOpenMode(SetUpMBIncWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=(3*width) div 4; ypos:=50;
  WinHeight:= 6*height;
  SetUpMBIncwin:=OpenWin(xpos,ypos, xpos+8*width div 4,
                                  ypos+WinHeight,SetUpMBIncWinLayOut);

  ChangeWin(SetUpMBIncWin);
  SetMargin(5,LeftMargin);
  SetCursor(0,0);
  ShowLineObject('                      Delay_1 = ',38, n1ID, buttonleftevent,
                                        NoKey, Local,NilEvProc);
  WRITELN(MBInc2Data.Delay_1:5);
  ShowLineObject('                      Delay_2 = ',38, n2ID, buttonleftevent,
                                         NoKey, Local,NilEvProc);
  WRITELN(MBInc2Data.Delay_2:5);
  ShowLineObject('                      Delay_3 = ',38, n3ID, buttonleftevent,
                                        NoKey, Local,NilEvProc);
  WRITELN(MBInc2Data.Delay_3:5);
  ShowLineObject('                      Delay_4 = ',38, n4ID, buttonleftevent,
                                        NoKey, Local,NilEvProc);
  WRITELN(MBInc2Data.Delay_4:5);
  ShowLineObject('                      Delay_5 = ',38, n5ID, buttonleftevent,
                                        NoKey, Local,NilEvProc);
  WRITELN(MBInc2Data.Delay_5:5);
  ShowLineObject('    Timebase_1: delayed pulse = ',38, TimeBase1ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(MBInc2Data.time_base_1:5);
  ShowLineObject('    Timebase_2:  master clock = ',38, TimeBase2ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(MBInc2Data.time_base_2:5);

  repeat
    locEvent:=WaitOfEvent(firstID,lastID);
    case locEvent of
      n1ID   : BEGIN
            SetCursor(32,0);
            MBInc2Data.Delay_1 := Editint(MBInc2Data.Delay_1,0,1000,5);
            SetCursor(32,0);
            WRITELN(MBInc2Data.Delay_1:5);
                  END;
      n2ID   : BEGIN
            SetCursor(32,1);
            MBInc2Data.Delay_2 := Editint(MBInc2Data.Delay_2,0,20000,5);
            SetCursor(32,1);
            WRITELN(MBInc2Data.Delay_2:5);
                  END;
      n3ID   : BEGIN
            SetCursor(32,2);
            MBInc2Data.Delay_3 := Editint(MBInc2Data.Delay_3,0,1000,5);
            SetCursor(32,2);
            WRITELN(MBInc2Data.Delay_3:5);
                  END;
      n4ID   : BEGIN
            SetCursor(32,3);
            MBInc2Data.Delay_4 := Editint(MBInc2Data.Delay_4,0,10000,5);
            SetCursor(32,3);
            WRITELN(MBInc2Data.Delay_4:5);
                  END;
      n5ID   : BEGIN
            SetCursor(32,4);
            MBInc2Data.Delay_5 := Editint(MBInc2Data.Delay_5,1,10000,5);
            SetCursor(32,4);
            WRITELN(MBInc2Data.Delay_5:5);
                  END;
      TimeBase1ID   : BEGIN
            SetCursor(32,5);
            MBInc2Data.Time_Base_1 := Editint(MBInc2Data.Time_Base_1,1,5,5);
            SetCursor(32,5);
            WRITELN(MBInc2Data.Time_Base_1:5);
                  END;
      TimeBase2ID   : BEGIN
            SetCursor(32,6);
            MBInc2Data.Time_Base_2 := Editint(MBInc2Data.Time_Base_2,1,5,5);
            SetCursor(32,6);
            WRITELN(MBInc2Data.Time_Base_2:5);
                  END;
    END;
  until locEvent=0;;
  ChangeWin(OldWin);
  CloseWin(SetUpMBIncwin);
END;

(************************************************************)
PROCEDURE SetUpDACProc;
  CONST StartVoltageID     = 1001;
        EndVoltageID       = 1002;
        RampID             = 1003;
        StepsID            = 1004;
        MaxDiffID          = 1005;
        MinDTID            = 1006;
        Speed_ConstID      = 1007;
        firstID            = StartVoltageID;
        lastID             = Speed_ConstID;

 VAR SetUpDACWinLayOut                    : WinDesType;
     OldWin,SetUpDACWin                   : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent                          : WORD;
     RampStr                            : String[8];

BEGIN

  OldWin:=WinActCtrl;

  SetUpDACWinLayOut := WinDefDes;
  WinSetHeader(SetUpDACWinLayOut,LightGray,
               C_LightBlue+'DAC Set UP');
  WinSetFillColor(SetUpDACWinLayOut, yellow);
  WinSetOpenMode(SetUpDACWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=(3*width) div 4; ypos:=50;
  WinHeight:= 6*height;
  SetUpDACwin:=OpenWin(xpos,ypos, xpos+7*width div 4,
                                  ypos+WinHeight,SetUpDACWinLayOut);

  ChangeWin(SetUpDACWin);
  SetMargin(5,LeftMargin);
  SetCursor(0,0);
  ShowLineObject('   Start Voltage (V) = ',29, StartVoltageID,
                     buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(DACData.StartVoltage:6:2);

  ShowLineObject('      EndVoltage (V) = ',29, EndVoltageID,
                        buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(DACData.EndVoltage:6:2);

  ShowLineObject('    Ramping Time (s) = ',29, RampID, buttonleftevent,
                      NoKey, Local,NilEvProc);
  WRITELN(DACData.Time:6:2);
  ShowLineObject('     Number of Steps = ',29, StepsID, buttonleftevent,
                       NoKey, Local,NilEvProc);
  WRITELN(DACData.Steps:6);

  ShowLineObject('  Max. Step Size (V) = ',29, MaxDiffID, buttonleftevent,
                    NoKey, Local,NilEvProc);
  WRITELN(DACData.MaxDiff:6:3);

  ShowLineObject('Min. Dwell Time (ms) = ',29, MinDTID, buttonleftevent,
                       NoKey, Local,NilEvProc);
  WRITELN(DACData.MinDT:6:3);
  ShowLineObject('   Speed / DAC-value = ',29, Speed_ConstID, buttonleftevent,
                     NoKey, Local,NilEvProc);
  WRITELN(DACData.Speed_Const:6:3);

  repeat
    locEvent:=WaitOfEvent(firstID,lastID);
    case locEvent of
      StartVoltageID   : BEGIN
            SetCursor(23,0);
            DACData.StartVoltage := Editreal(DACData.StartVoltage,-2,2,6,2);
            SetCursor(23,0);
            WRITELN(DACData.StartVoltage:6:2);
                  END;
      EndVoltageID    : BEGIN
                   SetCursor(23,1);
            DACData.EndVoltage := Editreal(DACData.EndVoltage,-2,2.0,6,2);
            SetCursor(23,1);
            WRITELN(DACData.EndVoltage:6:2);
                  END;
      RampID : BEGIN
                   SetCursor(23,2);
            DACData.Time := Editreal(DACData.Time,0,100,6,2);
            SetCursor(23,2);
            WRITELN(DACData.Time:6:2);
                  END;
      StepsID  : BEGIN
                   SetCursor(23,3);
            DACData.Steps := Editint(DACData.Steps,10,100,6);
            SetCursor(23,3);
            WRITELN(DACData.Steps:6);
                  END;
      MaxDiffID  : BEGIN
                   SetCursor(23,4);
            DACData.MaxDiff := Editreal(DACData.MaxDiff,0,100,6,3);
            SetCursor(23,4);
            WRITELN(DACData.MaxDiff:6:3);
                  END;
      MinDTID  : BEGIN
                   SetCursor(23,5);
            DACData.MinDT := Editreal(DACData.MinDT,0,100,6,3);
            SetCursor(23,5);
            WRITELN(DACData.MinDT:6:3);
                  END;
      Speed_ConstID  : BEGIN
                   SetCursor(23,6);
            DACData.Speed_Const := Editreal(DACData.Speed_Const,0,100,6,3);
            SetCursor(23,6);
            WRITELN(DACData.Speed_Const:6:3);
                  END;
    END;
  until locEvent=0;;
  ChangeWin(OldWin);
  CloseWin(SetUpDACwin);
END;

(************************************************************)
PROCEDURE ServoParModify(Controller_loc : LongInt; Axis_loc : LongInt);
  CONST
       SIL_ID             = 1002;
       SVA_ID             = 1003;
       SXYEA_ID              = 1004;
       SXYER_ID              = 1005;
       SXYGA_ID              = 1006;
       SXYGF_ID              = 1007;
       SXYLV_ID              = 1008;
       SXYLA_ID              = 1009;
       SXYZE_ID              = 1010;
       SXYKZ_ID              = 1011;
       SXYPO_ID              = 1012;
       firstID               = SIL_ID;
       lastID                = SXYPO_ID;

 VAR SetUpServoParWinLayOut           : WinDesType;
     OldWin,SetUpServoParWin          : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent                          : WORD;
     RampStr                           : String[8];
     SIL_loc                           : ARRAY[1..3,1..2] of longint;
     SVA_loc                           : ARRAY[1..3,1..2] of longint;
     SEA_loc                           : ARRAY[1..3,1..2] of longint;
     SER_loc                           : ARRAY[1..3,1..2] of longint;
     SGA_loc                           : ARRAY[1..3,1..2] of longint;
     SGF_loc                           : ARRAY[1..3,1..2] of longint;
     SLV_loc                           : ARRAY[1..3,1..2] of longint;
     SLA_loc                           : ARRAY[1..3,1..2] of longint;
     SZE_loc                           : ARRAY[1..3,1..2] of longint;
     SKZ_loc                           : ARRAY[1..3,1..2] of longint;
     SPO_loc                           : ARRAY[1..3,1..2] of longint;

BEGIN
(** get actual parameters **)
  SIL_loc[1,1]:=ServoParData.c0.SIL;
  SVA_loc[1,1]:=ServoParData.c0.SVA;
  SEA_loc[1,1]:=ServoParData.c0.SXEA;
  SER_loc[1,1]:=ServoParData.c0.SXER;
  SGA_loc[1,1]:=ServoParData.c0.SXGA;
  SGF_loc[1,1]:=ServoParData.c0.SXGF;
  SLV_loc[1,1]:=ServoParData.c0.SXLV;
  SLA_loc[1,1]:=ServoParData.c0.SXLA;
  SZE_loc[1,1]:=ServoParData.c0.SXZE;
  SKZ_loc[1,1]:=ServoParData.c0.SXKZ;
  SPO_loc[1,1]:=ServoParData.c0.SXPO;
  SEA_loc[1,2]:=ServoParData.c0.SYEA;
  SER_loc[1,2]:=ServoParData.c0.SYER;
  SGA_loc[1,2]:=ServoParData.c0.SYGA;
  SGF_loc[1,2]:=ServoParData.c0.SYGF;
  SLV_loc[1,2]:=ServoParData.c0.SYLV;
  SLA_loc[1,2]:=ServoParData.c0.SYLA;
  SZE_loc[1,2]:=ServoParData.c0.SYZE;
  SKZ_loc[1,2]:=ServoParData.c0.SYKZ;
  SPO_loc[1,2]:=ServoParData.c0.SYPO;

  SIL_loc[2,1]:=ServoParData.c1.SIL;
  SVA_loc[2,1]:=ServoParData.c1.SVA;
  SEA_loc[2,1]:=ServoParData.c1.SXEA;
  SER_loc[2,1]:=ServoParData.c1.SXER;
  SGA_loc[2,1]:=ServoParData.c1.SXGA;
  SGF_loc[2,1]:=ServoParData.c1.SXGF;
  SLV_loc[2,1]:=ServoParData.c1.SXLV;
  SLA_loc[2,1]:=ServoParData.c1.SXLA;
  SZE_loc[2,1]:=ServoParData.c1.SXZE;
  SKZ_loc[2,1]:=ServoParData.c1.SXKZ;
  SPO_loc[2,1]:=ServoParData.c1.SXPO;
  SEA_loc[2,2]:=ServoParData.c1.SYEA;
  SER_loc[2,2]:=ServoParData.c1.SYER;
  SGA_loc[2,2]:=ServoParData.c1.SYGA;
  SGF_loc[2,2]:=ServoParData.c1.SYGF;
  SLV_loc[2,2]:=ServoParData.c1.SYLV;
  SLA_loc[2,2]:=ServoParData.c1.SYLA;
  SZE_loc[2,2]:=ServoParData.c1.SYZE;
  SKZ_loc[2,2]:=ServoParData.c1.SYKZ;
  SPO_loc[2,2]:=ServoParData.c1.SYPO;

  SIL_loc[3,1]:=ServoParData.c2.SIL;
  SVA_loc[3,1]:=ServoParData.c2.SVA;
  SEA_loc[3,1]:=ServoParData.c2.SXEA;
  SER_loc[3,1]:=ServoParData.c2.SXER;
  SGA_loc[3,1]:=ServoParData.c2.SXGA;
  SGF_loc[3,1]:=ServoParData.c2.SXGF;
  SLV_loc[3,1]:=ServoParData.c2.SXLV;
  SLA_loc[3,1]:=ServoParData.c2.SXLA;
  SZE_loc[3,1]:=ServoParData.c2.SXZE;
  SKZ_loc[3,1]:=ServoParData.c2.SXKZ;
  SPO_loc[3,1]:=ServoParData.c2.SXPO;
  SEA_loc[3,2]:=ServoParData.c2.SYEA;
  SER_loc[3,2]:=ServoParData.c2.SYER;
  SGA_loc[3,2]:=ServoParData.c2.SYGA;
  SGF_loc[3,2]:=ServoParData.c2.SYGF;
  SLV_loc[3,2]:=ServoParData.c2.SYLV;
  SLA_loc[3,2]:=ServoParData.c2.SYLA;
  SZE_loc[3,2]:=ServoParData.c2.SYZE;
  SKZ_loc[3,2]:=ServoParData.c2.SYKZ;
  SPO_loc[3,2]:=ServoParData.c2.SYPO;

  OldWin:=WinActCtrl;

  SetUpServoParWinLayOut := WinDefDes;
  WinSetHeader(SetUpServoParWinLayOut,LightGray,
               C_LightBlue+'Servo Parameters');
  WinSetFillColor(SetUpServoParWinLayOut, lightgreen);
  WinSetOpenMode(SetUpServoParWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=(6*width) div 4; ypos:=90;
  WinHeight:= 10*height;
  SetUpServoParwin:=OpenWin(xpos,ypos, xpos+2*width,
                                  ypos+WinHeight,SetUpServoParWinLayOut);

  ChangeWin(SetUpServoParWin);
  SetMargin(5,LeftMargin);

  SetCursor(0,0);
  WRITELN(' Controller No : ',Controller_loc:3);
  WRITELN(' Axis No       : ',Axis_loc:3);
  ShowLineObject('                SIL ',36, SIL_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(SIL_loc[Controller_loc,1]:8);
  ShowLineObject('                SVA ',36, SVA_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(SVA_loc[Controller_loc,1]:8);
  ShowLineObject('                SEA ',36, SXYEA_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(SEA_loc[Controller_loc,Axis_loc]:8);
  ShowLineObject('                SER ',36, SXYER_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(SER_loc[Controller_loc,Axis_loc]:8);
  ShowLineObject('                SGA ',36, SXYGA_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(SGA_loc[Controller_loc,Axis_loc]:8);
  ShowLineObject('                SGF ',36, SXYGF_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(SGF_loc[Controller_loc,Axis_loc]:8);
  ShowLineObject('                SLV ',36, SXYLV_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(SLV_loc[Controller_loc,Axis_loc]:8);
  ShowLineObject('                SLA ',36, SXYLA_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(SLA_loc[Controller_loc,Axis_loc]:8);
  ShowLineObject('                SZE ',36, SXYZE_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(SZE_loc[Controller_loc,Axis_loc]:8);
  ShowLineObject('                SKZ ',36, SXYKZ_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(SKZ_loc[Controller_loc,Axis_loc]:8);
  ShowLineObject('                SPO ',36, SXYPO_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(SPO_loc[Controller_loc,Axis_loc]:8);

  repeat
    locEvent:=WaitOfEvent(firstID,lastID);
    case locEvent of
      SIL_ID          : BEGIN
                        SetCursor(20,2);
                        SIL_loc[Controller_loc,1]:=
                        Editint(SIL_loc[Controller_loc,1],
                                 0,100,8);
                        SetCursor(20,2);
                        WRITELN(SIL_loc[Controller_loc,1]:8);
                        END;
      SVA_ID          : BEGIN
                        SetCursor(20,3);
                        SVA_loc[Controller_loc,1]:=
                        Editint(SVA_loc[Controller_loc,1],
                                 0,100000,8);
                        SetCursor(20,3);
                        WRITELN(SVA_loc[Controller_loc,1]:8);
                        END;
      SXYEA_ID          : BEGIN
                        SetCursor(20,4);
                        SEA_loc[Controller_loc,Axis_loc]:=
                        Editint(SEA_loc[Controller_loc,Axis_loc],
                                 0,100000,8);
                        SetCursor(20,4);
                        WRITELN(SEA_loc[Controller_loc,Axis_loc]:8);
                        END;
      SXYER_ID          : BEGIN
                        SetCursor(20,5);
                        SER_loc[Controller_loc,Axis_loc]:=
                        Editint(SER_loc[Controller_loc,Axis_loc],
                                 0,100000,8);
                        SetCursor(20,5);
                        WRITELN(SER_loc[Controller_loc,Axis_loc]:8);
                        END;
      SXYGA_ID          : BEGIN
                        SetCursor(20,6);
                        SGA_loc[Controller_loc,Axis_loc]:=
                        Editint(SGA_loc[Controller_loc,Axis_loc],
                                 0,255,8);
                        SetCursor(20,6);
                        WRITELN(SGA_loc[Controller_loc,Axis_loc]:8);
                        END;
      SXYGF_ID          : BEGIN
                        SetCursor(20,7);
                        SGF_loc[Controller_loc,Axis_loc]:=
                        Editint(SGF_loc[Controller_loc,Axis_loc],
                                 -8,4,8);
                        SetCursor(20,7);
                        WRITELN(SGF_loc[Controller_loc,Axis_loc]:8);
                        END;
      SXYLV_ID          : BEGIN
                        SetCursor(20,8);
                        SLV_loc[Controller_loc,Axis_loc]:=
                        Editint(SLV_loc[Controller_loc,Axis_loc],
                                 0,1500000,8);
                        SetCursor(20,8);
                        WRITELN(SLV_loc[Controller_loc,Axis_loc]:8);
                        END;
      SXYLA_ID          : BEGIN
                        SetCursor(20,9);
                        SLA_loc[Controller_loc,Axis_loc]:=
                        Editint(SLA_loc[Controller_loc,Axis_loc],
                                 0,100000,8);
                        SetCursor(20,9);
                        WRITELN(SLA_loc[Controller_loc,Axis_loc]:8);
                        END;
      SXYZE_ID          : BEGIN
                        SetCursor(20,10);
                        SZE_loc[Controller_loc,Axis_loc]:=
                        Editint(SZE_loc[Controller_loc,Axis_loc],
                                 0,255,8);
                        SetCursor(20,10);
                        WRITELN(SZE_loc[Controller_loc,Axis_loc]:8);
                        END;
      SXYKZ_ID          : BEGIN
                        SetCursor(20,11);
                        SKZ_loc[Controller_loc,Axis_loc]:=
                        Editint(SKZ_loc[Controller_loc,Axis_loc],
                                 0,99,8);
                        SetCursor(20,11);
                        WRITELN(SKZ_loc[Controller_loc,Axis_loc]:8);
                        END;
      SXYPO_ID          : BEGIN
                        SetCursor(20,12);
                        SPO_loc[Controller_loc,Axis_loc]:=
                        Editint(SPO_loc[Controller_loc,Axis_loc],
                                 -255,255,8);
                        SetCursor(20,12);
                        WRITELN(SPO_loc[Controller_loc,Axis_loc]:8);
                        END;
    END;
  until locEvent=0;;
  ChangeWin(OldWin);
  CloseWin(SetUpServoParwin);

  (** save actual parameters **)
  ServoParData.c0.SIL:=SIL_loc[1,1];
  ServoParData.c0.SVA:=SVA_loc[1,1];
  ServoParData.c0.SXEA:=SEA_loc[1,1];
  ServoParData.c0.SXER:=SER_loc[1,1];
  ServoParData.c0.SXGA:=SGA_loc[1,1];
  ServoParData.c0.SXGF:=SGF_loc[1,1];
  ServoParData.c0.SXLV:=SLV_loc[1,1];
  ServoParData.c0.SXLA:=SLA_loc[1,1];
  ServoParData.c0.SXZE:=SZE_loc[1,1];
  ServoParData.c0.SXKZ:=SKZ_loc[1,1];
  ServoParData.c0.SXPO:=SPO_loc[1,1];
  ServoParData.c0.SYEA:=SEA_loc[1,2];
  ServoParData.c0.SYER:=SER_loc[1,2];
  ServoParData.c0.SYGA:=SGA_loc[1,2];
  ServoParData.c0.SYGF:=SGF_loc[1,2];
  ServoParData.c0.SYLV:=SLV_loc[1,2];
  ServoParData.c0.SYLA:=SLA_loc[1,2];
  ServoParData.c0.SYZE:=SZE_loc[1,2];
  ServoParData.c0.SYKZ:=SKZ_loc[1,2];
  ServoParData.c0.SYPO:=SPO_loc[1,2];

  ServoParData.c1.SIL:=SIL_loc[2,1];
  ServoParData.c1.SVA:=SVA_loc[2,1];
  ServoParData.c1.SXEA:=SEA_loc[2,1];
  ServoParData.c1.SXER:=SER_loc[2,1];
  ServoParData.c1.SXGA:=SGA_loc[2,1];
  ServoParData.c1.SXGF:=SGF_loc[2,1];
  ServoParData.c1.SXLV:=SLV_loc[2,1];
  ServoParData.c1.SXLA:=SLA_loc[2,1];
  ServoParData.c1.SXZE:=SZE_loc[2,1];
  ServoParData.c1.SXKZ:=SKZ_loc[2,1];
  ServoParData.c1.SXPO:=SPO_loc[2,1];
  ServoParData.c1.SYEA:=SEA_loc[2,2];
  ServoParData.c1.SYER:=SER_loc[2,2];
  ServoParData.c1.SYGA:=SGA_loc[2,2];
  ServoParData.c1.SYGF:=SGF_loc[2,2];
  ServoParData.c1.SYLV:=SLV_loc[2,2];
  ServoParData.c1.SYLA:=SLA_loc[2,2];
  ServoParData.c1.SYZE:=SZE_loc[2,2];
  ServoParData.c1.SYKZ:=SKZ_loc[2,2];
  ServoParData.c1.SYPO:=SPO_loc[2,2];

  ServoParData.c2.SIL:=SIL_loc[3,1];
  ServoParData.c2.SVA:=SVA_loc[3,1];
  ServoParData.c2.SXEA:=SEA_loc[3,1];
  ServoParData.c2.SXER:=SER_loc[3,1];
  ServoParData.c2.SXGA:=SGA_loc[3,1];
  ServoParData.c2.SXGF:=SGF_loc[3,1];
  ServoParData.c2.SXLV:=SLV_loc[3,1];
  ServoParData.c2.SXLA:=SLA_loc[3,1];
  ServoParData.c2.SXZE:=SZE_loc[3,1];
  ServoParData.c2.SXKZ:=SKZ_loc[3,1];
  ServoParData.c2.SXPO:=SPO_loc[3,1];
  ServoParData.c2.SYEA:=SEA_loc[3,2];
  ServoParData.c2.SYER:=SER_loc[3,2];
  ServoParData.c2.SYGA:=SGA_loc[3,2];
  ServoParData.c2.SYGF:=SGF_loc[3,2];
  ServoParData.c2.SYLV:=SLV_loc[3,2];
  ServoParData.c2.SYLA:=SLA_loc[3,2];
  ServoParData.c2.SYZE:=SZE_loc[3,2];
  ServoParData.c2.SYKZ:=SKZ_loc[3,2];
  ServoParData.c2.SYPO:=SPO_loc[3,2];

END;

(************************************************************)
PROCEDURE STWIParModify(Controller_loc : LongInt; Axis_loc : LongInt);

  CONST
       Modus_ID = 1000;
       Ist_ID   = 1001;
       Soll_ID  = 1002;
       AC_ID    = 1003;
       VL_ID    = 1004;
       VH_ID    = 1005;
       Time_ID  = 1006;
       KD_ID    = 1007;
       KP_ID    = 1008;
       KI_ID    = 1009;
       IL_ID    = 1010;
       F_VL_ID  = 1011;
       F_VH_ID  = 1012;
       L_VL_ID  = 1013;
       L_VH_ID  = 1014;
       firstID  = Modus_ID;
       lastID   = L_VH_ID;

 VAR SetUpServoParWinLayOut           : WinDesType;
     OldWin,SetUpServoParWin          : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent                         : WORD;
     Modus_loc                        : longint;
     Ist_loc                          : longint;
     Soll_loc                         : longint;
     AC_loc                           : longint;
     VL_loc                           : longint;
     VH_loc                           : longint;
     Time_loc                         : longint;
     KD_loc                           : longint;
     KP_loc                           : longint;
     KI_loc                           : longint;
     IL_loc                           : longint;
     F_VL_loc                         : longint;
     F_VH_loc                         : longint;
     L_VL_loc                         : longint;
     L_VH_loc                         : longint;

     i,j                              : integer;
BEGIN
  (** get actual parameters **)

  i:=Controller_loc;
  j:=axis_loc;
  Modus_loc:=STWIData.Par.Modus[i,j];
  Ist_loc  :=STWIData.Par.Ist[i,j];
  Soll_loc :=STWIData.Par.Soll[i,j];
  AC_loc   :=STWIData.Par.AC[i,j];
  VL_loc   :=STWIData.Par.VL[i,j];
  VH_loc   :=STWIData.Par.VH[i,j];
  Time_loc :=STWIData.Par.Time[i,j];
  KD_loc   :=STWIData.Par.KD[i,j];
  KP_loc   :=STWIData.Par.KP[i,j];
  KI_loc   :=STWIData.Par.KI[i,j];
  IL_loc   :=STWIData.Par.IL[i,j];
  F_VL_loc :=STWIData.Par.F_VL[i,j];
  F_VH_loc :=STWIData.Par.F_VH[i,j];
  L_VL_loc :=STWIData.Par.L_VL[i,j];
  L_VH_loc :=STWIData.Par.L_VH[i,j];

  OldWin:=WinActCtrl;

  SetUpServoParWinLayOut := WinDefDes;
  WinSetHeader(SetUpServoParWinLayOut,LightGray,
               C_LightBlue+'STWI Servo Parameters');
  WinSetFillColor(SetUpServoParWinLayOut, lightgreen);
  WinSetOpenMode(SetUpServoParWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=(6*width) div 4; ypos:=90;
  WinHeight:= 13*height;
  SetUpServoParwin:=OpenWin(xpos,ypos, xpos+2*width,
                                  ypos+WinHeight,SetUpServoParWinLayOut);

  ChangeWin(SetUpServoParWin);
  SetMargin(5,LeftMargin);

  SetCursor(0,0);
  WRITELN(' Controller No : ',Controller_loc:3);
  WRITELN(' Axis No       : ',Axis_loc:3);

  ShowLineObject('              Modus ',36, Modus_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(Modus_loc:8);
  ShowLineObject('                Ist ',36, Ist_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(Ist_loc:8);
  ShowLineObject('               Soll ',36, Soll_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(Soll_loc:8);
  ShowLineObject('                 AC ',36, AC_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(AC_loc:8);
  ShowLineObject('                 VL ',36, VL_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(VL_loc:8);
  ShowLineObject('                 VH ',36, VH_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(VH_loc:8);
  ShowLineObject('               Time ',36, Time_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(Time_loc:8);
  ShowLineObject('                 KD ',36, KD_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(KD_loc:8);
  ShowLineObject('                 KP ',36, KP_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(KP_loc:8);
  ShowLineObject('                 KI ',36, KI_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(KI_loc:8);
  ShowLineObject('                 IL ',36, IL_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(IL_loc:8);
  ShowLineObject('               F_VL ',36, F_VL_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(F_VL_loc:8);
  ShowLineObject('               F_VH ',36, F_VH_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(F_VH_loc:8);
  ShowLineObject('               L_VL ',36, L_VL_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(L_VL_loc:8);
  ShowLineObject('               L_VH ',36, L_VH_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(L_VH_loc:8);

  repeat
    locEvent:=WaitOfEvent(firstID,lastID);
    case locEvent of
      Modus_ID          : BEGIN
                        SetCursor(20,2);
                        Modus_loc:=Editint(Modus_loc,0,100,8);
                        SetCursor(20,2);
                        WRITELN(Modus_loc:8);
                        END;
      Ist_ID            : BEGIN
                        SetCursor(20,3);
                        Ist_loc:=Editint(Ist_loc,-90000000,90000000,8);
                        SetCursor(20,3);
                        WRITELN(Ist_loc:8);
                        END;
      Soll_ID           : BEGIN
                        SetCursor(20,4);
                        Soll_loc:=Editint(Soll_loc,-90000000,90000000,8);
                        SetCursor(20,4);
                        WRITELN(Soll_loc:8);
                        END;
      AC_ID             : BEGIN
                        SetCursor(20,5);
                        AC_loc:=Editint(AC_loc,0,10000,8);
                        SetCursor(20,5);
                        WRITELN(AC_loc:8);
                        END;
      VL_ID           : BEGIN
                        SetCursor(20,6);
                        VL_loc:=Editint(VL_loc,0,200,8);
                        SetCursor(20,6);
                        WRITELN(VL_loc:8);
                        END;
      VH_ID           : BEGIN
                        SetCursor(20,7);
                        VH_loc:=Editint(VH_loc,0,200,8);
                        SetCursor(20,7);
                        WRITELN(VH_loc:8);
                        END;
      Time_ID         : BEGIN
                        SetCursor(20,8);
                        Time_loc:=Editint(Time_loc,0,15,8);
                        SetCursor(20,8);
                        WRITELN(Time_loc:8);
                        END;
      KD_ID           : BEGIN
                        SetCursor(20,9);
                        KD_loc:=Editint(KD_loc,0,150,8);
                        SetCursor(20,9);
                        WRITELN(KD_loc:8);
                        END;
      KP_ID           : BEGIN
                        SetCursor(20,10);
                        KP_loc:=Editint(KP_loc,0,100,8);
                        SetCursor(20,10);
                        WRITELN(KP_loc:8);
                        END;
      KI_ID           : BEGIN
                        SetCursor(20,11);
                        KI_loc:=Editint(KI_loc,0,100,8);
                        SetCursor(20,11);
                        WRITELN(KI_loc:8);
                        END;
      IL_ID           : BEGIN
                        SetCursor(20,12);
                        IL_loc:=Editint(IL_loc,0,256,8);
                        SetCursor(20,12);
                        WRITELN(IL_loc:8);
                        END;
      F_VL_ID         : BEGIN
                        SetCursor(20,13);
                        F_VL_loc:=Editint(F_VL_loc,0,100,8);
                        SetCursor(20,13);
                        WRITELN(F_VL_loc:8);
                        END;
      F_VH_ID         : BEGIN
                        SetCursor(20,14);
                        F_VH_loc:=Editint(F_VH_loc,0,100,8);
                        SetCursor(20,14);
                        WRITELN(F_VH_loc:8);
                        END;
      L_VL_ID         : BEGIN
                        SetCursor(20,15);
                        L_VL_loc:=Editint(L_VL_loc,0,100,8);
                        SetCursor(20,15);
                        WRITELN(L_VL_loc:8);
                        END;
      L_VH_ID         : BEGIN
                        SetCursor(20,16);
                        L_VH_loc:=Editint(L_VH_loc,0,100,8);
                        SetCursor(20,16);
                        WRITELN(L_VH_loc:8);
                        END;
    END;
  until locEvent=0;;
  ChangeWin(OldWin);
  CloseWin(SetUpServoParwin);

  (** save actual parameters **)

  STWIData.Par.Modus[i,j]:=Modus_loc;
  STWIData.Par.Ist[i,j]  :=Ist_loc;
  STWIData.Par.Soll[i,j] :=Soll_loc;
  STWIData.Par.AC[i,j]   :=AC_loc;
  STWIData.Par.VL[i,j]   :=VL_loc;
  STWIData.Par.VH[i,j]   :=VH_loc;
  STWIData.Par.Time[i,j] :=Time_loc;
  STWIData.Par.KD[i,j]   :=KD_loc;
  STWIData.Par.KP[i,j]   :=KP_loc;
  STWIData.Par.KI[i,j]   :=KI_loc;
  STWIData.Par.IL[i,j]   :=IL_loc;
  STWIData.Par.F_VL[i,j] :=F_VL_loc;
  STWIData.Par.F_VH[i,j] :=F_VH_loc;
  STWIData.Par.L_VL[i,j] :=L_VL_loc;
  STWIData.Par.L_VH[i,j] :=L_VH_loc;

END;


(************************************************************)
PROCEDURE Choose_Cont_Axis;
  CONST
       Controller_ID      = 1000;
       Axis_ID            = 1001;
       Modify_ID          = 1002;
       firstID            = Controller_ID;
       lastID             = Modify_ID;

 VAR   SetUpChooseWinLayOut           : WinDesType;
       OldWin,SetUpChooseWin          : WinCtrlPtrType;
       width,height,xpos,ypos,WinHeight,
       locEvent                         : WORD;
       Controller_loc,Axis_loc          : LongInt;

Begin
  OldWin:=WinActCtrl;

  SetUpChooseWinLayOut := WinDefDes;
  WinSetHeader(SetUpChooseWinLayOut,LightGray,
    C_LightBlue+'Choose Cont. + Axis');
  WinSetFillColor(SetUpChooseWinLayOut,lightgreen);
  WinSetOpenMode(SetUpChooseWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=(6*width) div 4; ypos:=71;
  WinHeight:= 4*height;
  SetUpChoosewin:=OpenWin(xpos,ypos, xpos+2*width,
                                  ypos+WinHeight,SetUpChooseWinLayOut);

  ChangeWin(SetUpChooseWin);
  SetMargin(5,LeftMargin);


  Controller_loc:=1;
  Axis_loc:=1;

  SetCursor(0,0);


  if (ihhs = 6) then
  begin
    ShowLineObject('   Controller (1,2) ',36, Controller_ID,
                       buttonleftevent, NoKey, Local,NilEvProc);
    WRITELN(Controller_loc:3);
    ShowLineObject('     Axis (1,2,3,4) ',36, Axis_ID,
                       buttonleftevent, NoKey, Local,NilEvProc);
    WRITELN(Axis_loc:3);
  end;

  ShowLineObject('             Modify ',36, Modify_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN('  ');

  repeat
    locEvent:=WaitOfEvent(firstID,lastID);
    case locEvent of
      Controller_ID   : BEGIN
                          SetCursor(20,0);
                          if (ihhs = 2) or (ihhs = 3) or (ihhs = 5) then
                            Controller_loc:=Editint(Controller_loc,1,3,3);
                          if (ihhs = 4) or (ihhs =6) then
                            Controller_loc:=Editint(Controller_loc,1,2,3);
                          SetCursor(20,0);
                          writeln(Controller_loc:3);
                        END;
      Axis_ID         : BEGIN
                          SetCursor(20,1);
                          if (ihhs = 2) or (ihhs = 3) or (ihhs = 5) then
                            Axis_loc:=Editint(Axis_loc,1,2,3);
                          if (ihhs = 4) or (ihhs =6) then
                            Axis_loc:=Editint(Axis_loc,1,4,3);
                          SetCursor(20,1);
                          writeln(Axis_loc:3);
                        END;
      Modify_ID       : BEGIN
                          if (ihhs = 2) or (ihhs = 3) or (ihhs = 5) then
                            ServoParModify(Controller_loc, Axis_loc);
                          if (ihhs = 4)  or (ihhs = 6) then
                            STWIParModify(Controller_loc, Axis_loc);
                        END;
    end;
  until locEvent=0;

  ChangeWin(OldWin);
  CloseWin(SetUpChoosewin);
END;

(************************************************************)
PROCEDURE SetUpServoProc;
  CONST
       Parameters_ID      = 1000;
       Anorad_c_x_ID      = 1001;
       Anorad_c_xa_ID     = 1002;
       Anorad_c_y_ID      = 1003;
       Anorad_c_z_ID      = 1004;
       Anorad_c_theta_ID  = 1005;
       JS_c_x_ID          = 1006;
       JS_c_y_ID          = 1007;
       JS_c_z_ID          = 1008;
       JS_c_theta_ID      = 1009;
       firstID            = Parameters_ID;
       lastID             = JS_c_theta_ID;

 VAR SetUpServoWinLayOut              : WinDesType;
     OldWin,SetUpServoWin             : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent                          : WORD;
     RampStr                           : String[8];

BEGIN

  OldWin:=WinActCtrl;

  SetUpServoWinLayOut := WinDefDes;
  WinSetHeader(SetUpServoWinLayOut,LightGray,
               C_LightBlue+'Servo Motor Set UP');
  WinSetFillColor(SetUpServoWinLayOut, yellow);
  WinSetOpenMode(SetUpServoWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=(3*width) div 4; ypos:=50;
  WinHeight:= 10*height;
  SetUpServowin:=OpenWin(xpos,ypos, xpos+2*width,
                                  ypos+WinHeight,SetUpServoWinLayOut);

  ChangeWin(SetUpServoWin);
  SetMargin(5,LeftMargin);
  SetCursor(0,0);

  ShowLineObject(' Set Motor Parameters  ',36, Parameters_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  writeln('  ');
  ShowLineObject('   Scaling constant x = ',36, Anorad_c_x_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(ServoData.Anorad_c.x:8:2);
  ShowLineObject('  Scaling constant xa = ',36, Anorad_c_xa_ID,
                     buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(ServoData.Anorad_c.xa:8:4);
  ShowLineObject('   Scaling constant y = ',36, Anorad_c_y_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(ServoData.Anorad_c.y:8:2);
  ShowLineObject('   Scaling constant z = ',36, Anorad_c_z_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(ServoData.Anorad_c.z:8:2);
  ShowLineObject(' Scaling const. theta = ',36, Anorad_c_theta_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(ServoData.Anorad_c.theta:8:2);

  writeln(' ');

  ShowLineObject('    joy stick constant x = ',36, JS_c_x_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(ServoData.JS_c.x:8:2);
  ShowLineObject('    joy stick constant y = ',36, JS_c_y_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(ServoData.JS_c.y:8:2);
  ShowLineObject('    joy stick constant z = ',36, JS_c_z_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(ServoData.JS_c.z:8:2);
  ShowLineObject('joy stick constant theta = ',36, JS_c_theta_ID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(ServoData.JS_c.theta:8:2);

  repeat
    locEvent:=WaitOfEvent(firstID,lastID);
    case locEvent of
      Parameters_ID   : BEGIN
                        Choose_Cont_Axis;
                        END;
      Anorad_c_x_ID   : BEGIN
            SetCursor(24,1);
            ServoData.Anorad_c.x :=
                     Editreal(ServoData.Anorad_c.x,-10000,10000,8,2);
            SetCursor(24,1);
            WRITELN(ServoData.Anorad_c.x:8:2);
                  END;
      Anorad_c_xa_ID   : BEGIN
            SetCursor(24,2);
            ServoData.Anorad_c.xa :=
                     Editreal(ServoData.Anorad_c.xa,-10000,10000,8,4);
            SetCursor(24,2);
            WRITELN(ServoData.Anorad_c.xa:8:4);
                  END;
      Anorad_c_y_ID   : BEGIN
            SetCursor(24,3);
            ServoData.Anorad_c.y :=
                     Editreal(ServoData.Anorad_c.y,-10000,10000,8,2);
            SetCursor(24,3);
            WRITELN(ServoData.Anorad_c.y:8:2);
                  END;
      Anorad_c_z_ID   : BEGIN
            SetCursor(24,4);
            ServoData.Anorad_c.z :=
                     Editreal(ServoData.Anorad_c.z,-10000,10000,8,2);
            SetCursor(24,4);
            WRITELN(ServoData.Anorad_c.z:8:2);
                  END;
      Anorad_c_theta_ID   : BEGIN
            SetCursor(24,5);
            ServoData.Anorad_c.theta :=
                     Editreal(ServoData.Anorad_c.theta,-10000,10000,8,2);
            SetCursor(24,5);
            WRITELN(ServoData.Anorad_c.theta:8:2);
                  END;
      JS_c_x_ID   : BEGIN
            SetCursor(27,7);
            ServoData.JS_c.x := Editreal(ServoData.JS_c.x,-50000,50000,8,2);
            SetCursor(27,7);
            WRITELN(ServoData.JS_c.x:8:2);
                  END;
      JS_c_y_ID   : BEGIN
            SetCursor(27,8);
            ServoData.JS_c.y := Editreal(ServoData.JS_c.y,-50000,50000,8,2);
            SetCursor(27,8);
            WRITELN(ServoData.JS_c.y:8:2);
                  END;
      JS_c_z_ID   : BEGIN
            SetCursor(27,9);
            ServoData.JS_c.z := Editreal(ServoData.JS_c.z,-50000,50000,8,2);
            SetCursor(27,9);
            WRITELN(ServoData.JS_c.z:8:2);
                  END;
      JS_c_theta_ID   : BEGIN
            SetCursor(27,10);
            ServoData.JS_c.theta :=
                      Editreal(ServoData.JS_c.theta,-50000,50000,8,2);
            SetCursor(27,10);
            WRITELN(ServoData.JS_c.theta:8:2);
                  END;
    END;
  until locEvent=0;

  ChangeWin(OldWin);
  CloseWin(SetUpServowin);
END;

(************************************************************)
PROCEDURE SetUpDefineHHSProc;
  CONST speedctrlID = 1000;
        HHSBlocPosID= 1001;
        zeroID      = 1002;
        ninetyID    = 1003;
        d1ID        = 1004;
        d2ID        = 1005;
        d3ID        = 1006;
        savelogfileID = 1007;
        PathDataID    = 1008;
        firstID     = speedctrlID;
        lastID      = PathDataID;

 VAR SetUpDefineWinLayOut         : WinDesType;
     OldWin,SetUpDefineWin        : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent                     : WORD;

BEGIN

  OldWin:=WinActCtrl;

  SetUpDefineWinLayOut := WinDefDes;
  WinSetHeader(SetUpDefineWinLayOut,LightGray,
               C_LightBlue+'Define Measurement');
  WinSetFillColor(SetUpDefineWinLayOut, yellow);
  WinSetOpenMode(SetUpDefineWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=(3*width) div 4; ypos:=50;
  WinHeight:= 7*height;
  SetUpDefinewin:=OpenWin(xpos,ypos, xpos+7*width div 4,
                                  ypos+WinHeight,SetUpDefineWinLayOut);

  ChangeWin(SetUpDefineWin);
  SetMargin(5,LeftMargin);
  SetCursor(0,0);
  ShowLineObject('    Speed Control (1/0) = ',32, speedctrlID,
                      buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(DefineData.speedctrl:6);

  ShowLineObject('   Bloc Positions (1/2) = ',32, HHSBlocPosID, buttonleftevent,
                         NoKey, Local,NilEvProc);
  WRITELN(HHSBlocPos:6);

  ShowLineObject('       0 rotation (1/0) = ',32, zeroID, buttonleftevent,
                         NoKey, Local,NilEvProc);
  WRITELN(DefineData.zero:6);
  ShowLineObject('      90 rotation (1/0) = ',32, ninetyID, buttonleftevent,
                        NoKey, Local,NilEvProc);
  WRITELN(DefineData.ninety:6);
  ShowLineObject('       start delay (ms) = ',32, d1ID, buttonleftevent,
                         NoKey, Local,NilEvProc);
  WRITELN(DefineData.delay_1:6);
  ShowLineObject(' dvm readout delay (ms) = ',32, d2ID, buttonleftevent,
                         NoKey, Local,NilEvProc);
  WRITELN(DefineData.delay_2:6);
  ShowLineObject(' magnet flip delay (ms) = ',32, d3ID, buttonleftevent,
                         NoKey, Local,NilEvProc);
  WRITELN(DefineData.delay_3:6);
  ShowLineObject('    save LOG-file (1/0) = ',32, SaveLogFileID, buttonleftevent,
                         NoKey, Local,NilEvProc);
  WRITELN(DefineData.SaveLogFile:6);
   FileCounter(-1);
  ShowLineObject('    path = ',32, PathDataID,
                   buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(PathData:20);
  repeat
    locEvent:=WaitOfEvent(firstID,lastID);
    case locEvent of
      speedctrlID : BEGIN
            SetCursor(26,0);
            DefineData.speedctrl := Editint(DefineData.speedctrl,0,1,6);
            SetCursor(26,0);
            WRITELN(DefineData.speedctrl:6);
                  END;
      HHSBlocPosID   : BEGIN
            SetCursor(26,1);
            HHSBlocPos := Editint(HHSBlocPos,1,2,6);
            SetCursor(26,1);
            WRITELN(HHSBlocPos:1);
                  END;

      zeroID   : BEGIN
            SetCursor(26,2);
            DefineData.zero := Editint(DefineData.zero,0,1,6);
            SetCursor(26,2);
            WRITELN(DefineData.zero:1);
                  END;
      ninetyID   : BEGIN
            SetCursor(26,3);
            DefineData.ninety := Editint(DefineData.ninety,0,1,6);
            SetCursor(26,3);
            WRITELN(DefineData.ninety:6);
                  END;
      d1ID   : BEGIN
            SetCursor(26,4);
            DefineData.delay_1 := Editint(DefineData.delay_1,0,100000,6);
            SetCursor(26,4);
            WRITELN(DefineData.delay_1:6);
                  END;
      d2ID   : BEGIN
            SetCursor(26,5);
            DefineData.delay_2 := Editint(DefineData.delay_2,0,100000,6);
            SetCursor(26,5);
            WRITELN(DefineData.delay_2:6);
                  END;
      d3ID   : BEGIN
            SetCursor(26,6);
            DefineData.delay_3 := Editint(DefineData.delay_3,0,100000,6);
            SetCursor(26,6);
            WRITELN(DefineData.delay_3:6);
                  END;
      SaveLogFileID   : BEGIN
            SetCursor(26,7);
            DefineData.SaveLogFile := Editint(DefineData.SaveLogFile,0,100,6);
            SetCursor(26,7);
            WRITELN(DefineData.SaveLogFile:6);
                  END;
 PathDataID    : BEGIN
            SetCursor(11,8);
            writeln('                    ');
            SetCursor(11,8);
            PathData := Editstr(PathData,1,20);
            SetCursor(11,8);
            WRITELN(PathData);
            FileCounter(3);
                      END;
    END;
  until locEvent=0;;
  ChangeWin(OldWin);
  CloseWin(SetUpDefinewin);
END;


(**************************************************************)
PROCEDURE DefineGrid;
  CONST xminID         = 1000;
        xmaxID         = 1001;
        ianzxID        = 1002;
        yminID         = 1003;
        ymaxID         = 1004;
        ianzyID        = 1005;
        zminID         = 1006;
        zmaxID         = 1007;
        ianzzID        = 1008;
        xoff1ID        = 1009;
        xoff2ID        = 1010;
        intBy12ID      = 1011;
        intBz12ID      = 1012;
        firstID        = xminID;
        lastID         = intBz12ID;

 VAR DefineGridWinLayOut         : WinDesType;
     OldWin,DefineGridWin        : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent                     : WORD;
     Ycursor:integer;
BEGIN

  OldWin:=WinActCtrl;

  DefineGridWinLayOut := WinDefDes;
  WinSetHeader(DefineGridWinLayOut,LightGray,
               C_LightBlue+'Define Grid');
  WinSetFillColor(DefineGridWinLayOut, lightgreen);
  WinSetOpenMode(DefineGridWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=3*width div 2; ypos:=100;
  WinHeight:= 11*height;
  DefineGridWin:=OpenWin(xpos,ypos, xpos+6*width div 4,
                                  ypos+WinHeight,DefineGridWinLayOut);

  ChangeWin(DefineGridWin);
  SetMargin(5,LeftMargin);
  SetCursor(0,0);
  SetCursor(0,3);
  ShowLineObject('     ymin (mm) = ',32, yminID, buttonleftevent, NoKey,
                       Local,NilEvProc);
  WRITELN(GridData.ymin:9:3);
  ShowLineObject('     ymax (mm) = ',32, ymaxID, buttonleftevent, NoKey,
                       Local,NilEvProc);
  WRITELN(GridData.ymax:9:3);
  ShowLineObject(' y data points = ',32, ianzyID, buttonleftevent, NoKey,
                   Local,NilEvProc);
  WRITELN(GridData.ianzy:9);
  ShowLineObject('     zmin (mm) = ',32, zminID, buttonleftevent, NoKey,
                       Local,NilEvProc);
  WRITELN(GridData.zmin:9:3);
  ShowLineObject('     zmax (mm) = ',32, zmaxID, buttonleftevent, NoKey,
                       Local,NilEvProc);
  WRITELN(GridData.zmax:9:3);
  ShowLineObject(' z data points = ',32, ianzzID, buttonleftevent, NoKey,
                       Local,NilEvProc);
  WRITELN(GridData.ianzz:9);
  if  (ihhs = 6)  then
  begin
    ShowLineObject('          Dz2I = ',32, xoff1ID, buttonleftevent, NoKey,
                       Local,NilEvProc);
    WRITELN(STWIData.Meas.Dz2I:9:3);
    ShowLineObject('          Dy2I = ',32, xoff2ID, buttonleftevent, NoKey,
                       Local,NilEvProc);
    WRITELN(STWIData.Meas.Dy2I:9:3);
    ShowLineObject('By int (0,1,2) = ',32, intBy12ID, buttonleftevent, NoKey,
                       Local,NilEvProc);
    WRITELN(STWIData.Meas.IntBy12:9);
    ShowLineObject('Bz int (0,1,2) = ',32, intBz12ID, buttonleftevent, NoKey,
                       Local,NilEvProc);
    WRITELN(STWIData.Meas.IntBz12:9);
    ShowLineObject('Mess-Wdhlg.    = ',32, ianzxID, buttonleftevent, NoKey,
                   Local,NilEvProc);
    WRITELN(GridData.ianzx:9);
    ShowLineObject('Park-Pos. SMS1 = ',32, xminID, buttonleftevent, NoKey,
                   Local,NilEvProc);
    WRITELN(GridData.xmin:9:3);
    ShowLineObject('ToleranzMax/Tmm= ',32, xmaxID, buttonleftevent, NoKey,
                   Local,NilEvProc);
    WRITELN(GridData.xmax:9:3);
  end;

  repeat
    locEvent:=WaitOfEvent(firstID,lastID);
    case locEvent of
      xminID : BEGIN;
            Ycursor:=0;
            if (ihhs=6) then Ycursor:=14;
            SetCursor(17,Ycursor);
            if (ihhs=6) then GridData.xmin := Editreal(GridData.xmin,-100,100,9,3)
            else
            GridData.xmin := Editreal(GridData.xmin,-10000,10000,9,3);
            SetCursor(17,Ycursor);
            WRITELN(GridData.xmin:9:3);
                  END;
      xmaxID : BEGIN;
            Ycursor:=1;
            if (ihhs=6) then Ycursor:=15;
            SetCursor(17,Ycursor);
            if (ihhs=6) then GridData.xmax := Editreal(GridData.xmax,0,1,9,3)
            else
            GridData.xmax := Editreal(GridData.xmax,-10000,10000,9,3);
            SetCursor(17,1);
            WRITELN(GridData.xmax:9:3);
                  END;

      ianzxID : BEGIN;
            Ycursor:=2;
            if (ihhs=6) or (ihhs=4) then Ycursor:=13;
            SetCursor(17,Ycursor);
            GridData.ianzx := Editint(GridData.ianzx,1,1024,9);
            SetCursor(17,Ycursor);
            WRITELN(GridData.ianzx:9);
                  END;
      yminID : BEGIN;
            SetCursor(17,3);
            GridData.ymin := Editreal(GridData.ymin,-10000,10000,9,3);
            SetCursor(17,3);
            WRITELN(GridData.ymin:9:3);
                  END;
      ymaxID : BEGIN;
            SetCursor(17,4);
            GridData.ymax := Editreal(GridData.ymax,-10000,10000,9,3);
            SetCursor(17,4);
            WRITELN(GridData.ymax:9:3);
                  END;
      ianzyID : BEGIN;
            SetCursor(17,5);
            GridData.ianzy := Editint(GridData.ianzy,1,1024,9);
            SetCursor(17,5);
            WRITELN(GridData.ianzy:9);
                  END;
      zminID : BEGIN;
            SetCursor(17,6);
            GridData.zmin := Editreal(GridData.zmin,-10000,10000,9,3);
            SetCursor(17,6);
            WRITELN(GridData.zmin:9:3);
                  END;
      zmaxID : BEGIN;
            SetCursor(17,7);
            GridData.zmax := Editreal(GridData.zmax,-10000,10000,9,3);
            SetCursor(17,7);
            WRITELN(GridData.zmax:9:3);
                  END;
      ianzzID : BEGIN;
            SetCursor(17,8);
            GridData.ianzz := Editint(GridData.ianzz,1,1024,9);
            SetCursor(17,8);
            WRITELN(GridData.ianzz:9);
                  END;
      xoff1ID : BEGIN;
            SetCursor(17,9);
            if (ihhs = 6)  then
            begin
            STWIData.Meas.Dz2I := Editreal(STWIData.Meas.Dz2I,-50000,50000,9,3);
            SetCursor(17,9);
            WRITELN(STWIData.Meas.Dz2I:9:3);
            end;
                  END;
      xoff2ID : BEGIN;
            SetCursor(17,10);
            if (ihhs = 6)  then
            begin
            STWIData.Meas.Dy2I := Editreal(STWIData.Meas.Dy2I,-50000,50000,9,3);
            SetCursor(17,10);
            WRITELN(STWIData.Meas.Dy2I:9:3);
            end;
                  END;
      intBy12ID : BEGIN;
            SetCursor(17,11);
            STWIData.Meas.intBy12 := Editint(STWIData.Meas.IntBy12,0,2,9);
            SetCursor(17,11);
            WRITELN(STWIData.Meas.IntBy12:9);
                  END;
      intBz12ID : BEGIN;
            SetCursor(17,12);
            STWIData.Meas.intBz12 := Editint(STWIData.Meas.IntBz12,0,2,9);
            SetCursor(17,12);
            WRITELN(STWIData.Meas.IntBz12:9);
                  END;
    END;
  until locEvent=0;;
  ChangeWin(OldWin);
  CloseWin(DefineGridWin);
END;

(**************************************************************)
PROCEDURE DefineRefOffsets;
  CONST x1ID        = 1000;
        x2ID        = 1001;
        y1ID        = 1002;
        y2ID        = 1003;
        z1ID        = 1004;
        z2ID        = 1005;
        theta1ID    = 1006;
        theta2ID    = 1007;
        firstID     = x1ID;
        lastID      = theta2ID;

 VAR DefineRefOffsetWinLayOut    : WinDesType;
     OldWin,DefineRefOffsetWin   : WinCtrlPtrType;
     width,height,xpos,ypos,
     WinHeight,locEvent          : WORD;

BEGIN

  OldWin:=WinActCtrl;

  DefineRefOffsetWinLayOut := WinDefDes;
  WinSetHeader(DefineRefOffsetWinLayOut,LightGray,
               C_LightBlue+'Define Ref Offsets');
  WinSetFillColor(DefineRefOffsetWinLayOut, lightgreen);
  WinSetOpenMode(DefineRefOffsetWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=3*width div 2; ypos:=100;
  WinHeight:= 11*height;
  DefineRefOffsetWin:=OpenWin(xpos,ypos, xpos+6*width div 4,
                 ypos+WinHeight,DefineRefOffsetWinLayOut);

  ChangeWin(DefineRefOffsetWin);
  SetMargin(5,LeftMargin);
  SetCursor(0,0);
  ShowLineObject('     x_1 (mm) = ',32, x1ID, buttonleftevent, NoKey,
                       Local,NilEvProc);
  WRITELN(STWIData.Ref_offset.x1:9:2);
  ShowLineObject('     x_2 (mm) = ',32, x2ID, buttonleftevent, NoKey,
                       Local,NilEvProc);
  WRITELN(STWIData.Ref_offset.x2:9:2);
  ShowLineObject('     y_1 (mm) = ',32, y1ID, buttonleftevent, NoKey,
                       Local,NilEvProc);
  WRITELN(STWIData.Ref_offset.y1:9:2);
  ShowLineObject('     y_2 (mm) = ',32, y2ID, buttonleftevent, NoKey,
                       Local,NilEvProc);
  WRITELN(STWIData.Ref_offset.y2:9:2);
  ShowLineObject('     z_1 (mm) = ',32, z1ID, buttonleftevent, NoKey,
                       Local,NilEvProc);
  WRITELN(STWIData.Ref_offset.z1:9:2);
  ShowLineObject('     z_2 (mm) = ',32, z2ID, buttonleftevent, NoKey,
                       Local,NilEvProc);
  WRITELN(STWIData.Ref_offset.z2:9:2);
  ShowLineObject('theta_1 (deg) = ',32, theta1ID, buttonleftevent, NoKey,
                       Local,NilEvProc);
  WRITELN(STWIData.Ref_offset.theta1:9:2);
  ShowLineObject('theta_2 (deg) = ',32, theta2ID, buttonleftevent, NoKey,
                       Local,NilEvProc);
  WRITELN(STWIData.Ref_offset.theta2:9:2);

  repeat
    locEvent:=WaitOfEvent(firstID,lastID);
    case locEvent of
      x1ID : BEGIN;
            SetCursor(16,0);
            STWIData.Ref_Offset.x1 :=
                 Editreal(STWIData.Ref_Offset.x1,-200,200,9,2);
            SetCursor(16,0);
            WRITELN(STWIData.Ref_Offset.x1:9:2);
                  END;
      x2ID : BEGIN;
            SetCursor(16,1);
            STWIData.Ref_Offset.x2 :=
                 Editreal(STWIData.Ref_Offset.x2,-200,200,9,2);
            SetCursor(16,1);
            WRITELN(STWIData.Ref_Offset.x2:9:2);
                  END;
      y1ID : BEGIN;
            SetCursor(16,2);
            STWIData.Ref_Offset.y1 :=
                 Editreal(STWIData.Ref_Offset.y1,-200,200,9,2);
            SetCursor(16,2);
            WRITELN(STWIData.Ref_Offset.y1:9:2);
                  END;
      y2ID : BEGIN;
            SetCursor(16,3);
            STWIData.Ref_Offset.y2 :=
                 Editreal(STWIData.Ref_Offset.y2,-200,200,9,2);
            SetCursor(16,3);
            WRITELN(STWIData.Ref_Offset.y2:9:2);
                  END;
      z1ID : BEGIN;
            SetCursor(16,4);
            STWIData.Ref_Offset.z1 :=
                 Editreal(STWIData.Ref_Offset.z1,-200,200,9,2);
            SetCursor(16,4);
            WRITELN(STWIData.Ref_Offset.z1:9:2);
                  END;
      z2ID : BEGIN;
            SetCursor(16,5);
            STWIData.Ref_Offset.z2 :=
                 Editreal(STWIData.Ref_Offset.z2,-200,200,9,2);
            SetCursor(16,5);
            WRITELN(STWIData.Ref_Offset.z2:9:2);
                  END;
      theta1ID : BEGIN;
            SetCursor(16,6);
            STWIData.Ref_Offset.theta1 :=
                 Editreal(STWIData.Ref_Offset.theta1,-360,360,9,2);
            SetCursor(16,6);
            WRITELN(STWIData.Ref_Offset.theta1:9:2);
                  END;
      theta2ID : BEGIN;
            SetCursor(16,7);
            STWIData.Ref_Offset.theta2 :=
                 Editreal(STWIData.Ref_Offset.theta2,-360,360,9,2);
            SetCursor(16,7);
            WRITELN(STWIData.Ref_Offset.theta2:9:2);
                  END;
    END;
  until locEvent=0;;
  ChangeWin(OldWin);
  CloseWin(DefineRefOffsetWin);
END;

(************************************************************)
PROCEDURE SetUpDefine5mProc;
  CONST gridID         = 1000;
        speed_xID      = 1001;
        speed_yID      = 1002;
        speed_zID      = 1003;
        speedctrlID    = 1004;
        auto_reverseID = 1005;
        d1ID           = 1006;
        d2ID           = 1007;
        d3ID           = 1008;
        d4ID           = 1009;
        SaveStackID    = 1010;
        SaveLogFileID  = 1011;
        Red_factorID   = 1012;
        FileNrID       = 1013;
        PathDataID     = 1014;
        firstID        = gridID;
        lastID         = PathDataID;

 VAR SetUpDefineWinLayOut         : WinDesType;
     OldWin,SetUpDefineWin        : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent                     : WORD;
     pathnr,i,ii,iii              : integer;
     dummy                        : string[1];

BEGIN

  OldWin:=WinActCtrl;

  SetUpDefineWinLayOut := WinDefDes;
  WinSetHeader(SetUpDefineWinLayOut,LightGray,
               C_LightBlue+'Define Measurement');
  WinSetFillColor(SetUpDefineWinLayOut, yellow);
  WinSetOpenMode(SetUpDefineWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=(3*width) div 4; ypos:=50;
  WinHeight:= 10*height;
  SetUpDefinewin:=OpenWin(xpos,ypos, xpos+7*width div 4,
                                  ypos+WinHeight,SetUpDefineWinLayOut);

  ChangeWin(SetUpDefineWin);
  SetMargin(5,LeftMargin);

  SetCursor(0,0);
  ShowLineObject('    define grid ',32, gridID, buttonleftevent, NoKey,
                      Local,NilEvProc);
  Writeln;

  if  (ihhs = 6)  then
  begin
    ShowLineObject('set ref offsets ',32, speedctrlID,
                      buttonleftevent, NoKey, Local,NilEvProc);
    WRITELN;
  end;

  ShowLineObject('     Auto Reverse (1/0) = ',32, auto_reverseID,
                       buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(DefineData.autoreverse:6);

  if  (ihhs = 6) then
  begin
    ShowLineObject('    Scan Speed in theta = ',32, Speed_xID,
                       buttonleftevent, NoKey, Local,NilEvProc);
  end;
  WRITELN(DefineData.speed_x:6:2);

  ShowLineObject('        Scan Speed in y = ',32, Speed_yID,
                       buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(DefineData.speed_y:6:2);

  ShowLineObject('        Scan Speed in z = ',32, Speed_zID,
                       buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(DefineData.speed_z:6:2);

  if (ihhs=1) or (ihhs = 6) then
  begin
     ShowLineObject('       start delay (ms) = ',32, d1ID, buttonleftevent,
                      NoKey, Local,NilEvProc);
  end;

  WRITELN(DefineData.delay_1:6);


  if (ihhs = 6) then
  begin
     ShowLineObject('           delay_2 (ms) = ',32, d2ID, buttonleftevent,
                   NoKey, Local,NilEvProc);
  end;
  WRITELN(DefineData.delay_2:6);

  if(ihhs=1) or (ihhs = 6)  then
  begin
    ShowLineObject('           delay_3 (ms) = ',32, d3ID, buttonleftevent,
                             NoKey, Local,NilEvProc);
  end;
  WRITELN(DefineData.delay_3:6);

  if(ihhs=1) or (ihhs = 6) then
  begin
    ShowLineObject('         end delay (ms) = ',32, d4ID, buttonleftevent,
                             NoKey, Local,NilEvProc);
  end;
  WRITELN(DefineData.delay_4:6);

  ShowLineObject('       save stack (1/0) = ',32, SaveStackID, buttonleftevent,
                         NoKey, Local,NilEvProc);
  WRITELN(DefineData.SaveStack:6);
  ShowLineObject('    save LOG-file (1/0) = ',32, SaveLogFileID, buttonleftevent,
                         NoKey, Local,NilEvProc);
  WRITELN(DefineData.SaveLogFile:6);
  ShowLineObject('      reduction factor  = ',32, Red_factorID,
                   buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(DefineData.Red_factor:6);
  FileCounter(-1);
  ShowLineObject('               File-Nr  = ',32, FileNrID,
                   buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(FileNr:6);
  ShowLineObject('    path = ',32, PathDataID,
                   buttonleftevent, NoKey, Local,NilEvProc);
  WRITELN(PathData:20);

  repeat
    locEvent:=WaitOfEvent(firstID,lastID);
    case locEvent of
      gridID      : BEGIN
                      DefineGrid;
                    END;
      speedctrlID : BEGIN
                      if (ihhs = 6) then
                        DefineRefOffsets;
                      END;
      auto_reverseID: BEGIN
                        SetCursor(26,2);
                        DefineData.autoreverse :=
                          Editint(DefineData.autoreverse,0,1,6);
                        SetCursor(26,2);
                        WRITELN(DefineData.autoreverse:6);
                      END;
      speed_xID     : BEGIN
                        SetCursor(26,3);
                        DefineData.Speed_x :=
                          Editreal(DefineData.Speed_x,0,100,6,2);
                        SetCursor(26,3);
                        WRITELN(DefineData.Speed_x:6:2);
                      END;
      speed_yID     : BEGIN
                        SetCursor(26,4);
                        DefineData.Speed_y :=
                          Editreal(DefineData.Speed_y,0,20,6,2);
                        SetCursor(26,4);
                        WRITELN(DefineData.Speed_y:6:2);
                      END;
      speed_zID     : BEGIN;
                        SetCursor(26,5);
                        DefineData.Speed_z :=
                          Editreal(DefineData.Speed_z,0,20,6,2);
                        SetCursor(26,5);
                        WRITELN(DefineData.Speed_z:6:2);
                      END;
      d1ID          : BEGIN
                        SetCursor(26,6);
                        DefineData.delay_1 :=
                          Editint(DefineData.delay_1,0,100000,6);
                        SetCursor(26,6);
                        WRITELN(DefineData.delay_1:6);
                      END;
      d2ID          : BEGIN
                        SetCursor(26,7);
                        DefineData.delay_2 :=
                          Editint(DefineData.delay_2,0,100000,6);
                        SetCursor(26,7);
                        WRITELN(DefineData.delay_2:6);
                      END;
      d3ID          : BEGIN
                        SetCursor(26,8);
                        DefineData.delay_3 :=
                          Editint(DefineData.delay_3,0,100000,6);
                        SetCursor(26,8);
                        WRITELN(DefineData.delay_3:6);
                      END;
      d4ID          : BEGIN
                        SetCursor(26,9);
                        DefineData.delay_4 :=
                          Editint(DefineData.delay_4,0,100000,6);
                        SetCursor(26,9);
                        WRITELN(DefineData.delay_4:6);
                      END;
      SaveStackID   : BEGIN
                        SetCursor(26,10);
                        DefineData.SaveStack :=
                          Editint(DefineData.SaveStack,0,100,6);
                        SetCursor(26,10);
                        WRITELN(DefineData.SaveStack:6);
                      END;
      SaveLogFileID : BEGIN
                        SetCursor(26,11);
                        DefineData.SaveLogFile :=
                          Editint(DefineData.SaveLogFile,0,100,6);
                        SetCursor(26,11);
                        WRITELN(DefineData.SaveLogFile:6);
                      END;
      Red_factorID  : BEGIN
                        SetCursor(26,12);
                        DefineData.Red_factor :=
                          Editint(DefineData.Red_factor,0,100000,6);
                        SetCursor(26,12);
                        WRITELN(DefineData.Red_factor:6);
                      END;
      FileNrID      : BEGIN
                        SetCursor(26,13);
                        FileNr := Editint(FileNr,1,100000,6);
                        SetCursor(26,13);
                        WRITELN(FileNr:6);
                        FileCounter(2);
                      END;
      PathDataID    : BEGIN
                        SetCursor(11,14);
                        writeln('                    ');
                        SetCursor(11,14);
                        PathData := Editstr(PathData,1,20);
                        DataBackSlash;
                        SetCursor(11,14);
                        WRITELN(PathData);
                        FileCounter(3);
                      END;
    END;
  until locEvent=0;;
  ChangeWin(OldWin);
  CloseWin(SetUpDefinewin);

END;

(******************************************************************)
PROCEDURE SetUpSMSProc(iSMS: integer);
  CONST StepsID       = 1001;
        TimeBaseID    = 1002;
        FrqDivID      = 1003;
        PulseLenID    = 1004;
        RampStepsID   = 1005;
        RampDstpsID    = 1006;
        ReferenzID     = 1007;
        RampsHightID   = 1008;
        firstID       = StepsID;
        lastID        = RampsHightID;

 VAR SetUpSMSWinLayOut                    : WinDesType;
     OldWin,SetUpSMSWin                   : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent                             : WORD;
     steps_local        : longint;
     timebase_local     : byte;
     frqdiv_local       : word;
     pulselen_local     : word;
     rampsteps_local    : longint;
     rampdstps_local    : real;
     referenz_local     : real;
     rampsHight_local   : real;
BEGIN

  OldWin:=WinActCtrl;

  SetUpSMSWinLayOut := WinDefDes;
  if iSMS = 1 then
       WinSetHeader(SetUpSMSWinLayOut,LightGray,
       C_LightBlue+'SMS 1 Set UP');
  if iSMS = 2 then
       WinSetHeader(SetUpSMSWinLayOut,LightGray,
       C_LightBlue+'SMS 2 Set UP');

  WinSetFillColor(SetUpSMSWinLayOut, yellow);
  WinSetOpenMode(SetUpSMSWinLayOut, WinHeadText, XmmSave);
  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=(3*width) div 4; ypos:=50;
  WinHeight:= 6*height;
  SetUpSMSwin:=OpenWin(xpos,ypos, xpos+7*width div 4,
                                  ypos+WinHeight,SetUpSMSWinLayOut);

  if iSMS = 1 then
  begin
    steps_local:=SMS1Data.Steps;
    timebase_local:=SMS1Data.TimeBase;
    frqdiv_local:=SMS1Data.FrqDiv;
    pulselen_local:=SMS1Data.PulseLen;
    rampsteps_local:=SMS1Data.RampSteps;
    rampDstps_local:=SMS1Data.RampDstps;
    if ihhs=6 then
    begin
      Referenz_local:=SMS1_Referenz;
      RampsHight_local:=SMS_Ramp_Hight;
    end;
  end;
  if iSMS = 2 then
  begin
    steps_local:=SMS2Data.Steps;
    timebase_local:=SMS2Data.TimeBase;
    frqdiv_local:=SMS2Data.FrqDiv;
    pulselen_local:=SMS2Data.PulseLen;
    rampsteps_local:=SMS2Data.RampSteps;
    rampDstps_local:=SMS2Data.RampDstps;
    if ihhs=6 then
    begin
      Referenz_local:=SMS2_Referenz;
      RampsHight_local:=SMS_Ramp_Hight;
    end;
  end;

  ChangeWin(SetUpSMSWin);
  SetMargin(5,LeftMargin);
  SetCursor(0,0);

  ShowLineObject('   Number of Steps = ',29, StepsID, ButtonLeft,
                     NoKey, Local,NilEvProc);
  WRITELN(steps_local:6);

  ShowLineObject('         Time Base = ',29, TimeBaseID, ButtonLeft,
                           NoKey, Local,NilEvProc);
  WRITELN(timebase_local:6);

  ShowLineObject('            FrqDiv = ',29, FrqDivID, ButtonLeft,
                              NoKey, Local,NilEvProc);
  WRITELN(frqdiv_local:6);

  ShowLineObject('          PulseLen = ',29, PulseLenID, ButtonLeft,
                            NoKey, Local,NilEvProc);
  WRITELN(pulselen_local:6);

  ShowLineObject('   Steps of a Ramp = ',29, RampStepsID, ButtonLeft,
                       NoKey, Local,NilEvProc);
  WRITELN(rampsteps_local:6);

  ShowLineObject('  Ramp: DeltaStep  = ',29, RampDstpsID, ButtonLeft,
                    NoKey, Local,NilEvProc);
  WRITELN(rampDstps_local:6:0);
  if ihhs=6 then
  begin
    ShowLineObject('  RampsHight     = ',29, RampsHightID, ButtonLeft,
                    NoKey, Local,NilEvProc);
    WRITELN(RampsHight_local:6:3);

    ShowLineObject('  Reference Value = ',29, ReferenzID, ButtonLeft,
                    NoKey, Local,NilEvProc);
    WRITELN(Referenz_local:9:3);
  end;

  repeat
    locEvent:=WaitOfEvent(firstID,lastID);
    case locEvent of
      StepsID   : BEGIN
            SetCursor(21,0);
            steps_local := Editint(steps_local,-40000,40000,6);
            SetCursor(21,0);
            WRITELN(steps_local:6);
                  END;
      TimeBaseID: BEGIN
            SetCursor(21,1);
            timebase_local := Editint(timebase_local,3,15,6);
            SetCursor(21,1);
            WRITELN(timebase_local:6);
                  END;
      FrqDivID  : BEGIN
            SetCursor(21,2);
            frqdiv_local := Editint(frqdiv_local,1,65535,6);
            SetCursor(21,2);
            WRITELN(frqdiv_local:6);
                  END;
      PulseLenID: BEGIN
            SetCursor(21,3);
            pulselen_local := Editint(pulselen_local,1,65535,6);
            SetCursor(21,3);
            WRITELN(pulselen_local:6);
                  END;
      RampStepsID:BEGIN
            SetCursor(21,4);
            rampsteps_local := Editint(rampsteps_local,0,100,6);
            SetCursor(21,4);
            WRITELN(rampsteps_local:6);
                  END;
      RampDstpsID:BEGIN
            SetCursor(21,5);
            rampDstps_local := EditReal(RampDstps_local,0,100,6,0);
            SetCursor(21,5);
            WRITELN(rampDstps_local:6,0);
                  END;
      RampsHightID: BEGIN
                    if ihhs=6 then
                    begin
            SetCursor(20,6);
            RampsHight_local := EditReal(RampsHight_local,0,10,6,3);
            SetCursor(20,6);
            WRITELN(RampsHight_local:6,3);
                    end;
                  END;

      ReferenzID: BEGIN
                    if ihhs=6 then
                    begin
            SetCursor(20,7);
            Referenz_local := EditReal(Referenz_local,0,100,9,3);
            SetCursor(20,7);
            WRITELN(Referenz_local:9,3);
                    end;
                  END;

    END;
  until locEvent=0;;
  ChangeWin(OldWin);
  CloseWin(SetUpSMSwin);

  if iSMS = 1 then
  begin
    SMS1Data.Steps:=steps_local;
    SMS1Data.TimeBase:=timebase_local;
    SMS1Data.FrqDiv:=frqdiv_local;
    SMS1Data.PulseLen:=pulselen_local;
    SMS1Data.RampSteps:=rampsteps_local;
    SMS1Data.RampDstps:=rampDstps_local;
    if ihhs=6 then
    begin
      SMS1_Referenz:=Referenz_local;
      SMS_Ramp_Hight:=RampsHight_local;
    end;
  end;
  if iSMS = 2 then
  begin
    SMS2Data.Steps:=steps_local;
    SMS2Data.TimeBase:=timebase_local;
    SMS2Data.FrqDiv:=frqdiv_local;
    SMS2Data.PulseLen:=pulselen_local;
    SMS2Data.RampSteps:=rampsteps_local;
    SMS2Data.RampDstps:=rampDstps_local;
    if ihhs=6 then
    begin
      SMS2_Referenz:=Referenz_local;
      SMS_Ramp_Hight:=RampsHight_local;
    end;
  end;
END;

(************************************************************)
PROCEDURE SetUpIntegratorProc;
begin
END;


(************************************************************)
PROCEDURE SetUpDVMProc(iDVM: integer);
  CONST ApertureID   = 2001;
        ReadingsID   = 2002;
        RangeID      = 2003;
        AZEROID      = 2004;
        ScanID       = 2005;
        MeasNomID    = 2006;
        SmsPauseID   = 2007;
        SMSRefID     = 2008;
        firstID  = ApertureID;
        lastID   = SMSRefID;

 VAR SetUpDVMWinLayOut                    : WinDesType;
     OldWin,SetUpDVMWin                   : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent                          : WORD;
     aperture_local, range_local  : real;
     readings_local, samp_local   : integer;
     Scan_local, MeasNom_local,SMSPause_local,SMSRef_local   : integer;

PROCEDURE IDVM_vorher;
begin
  if iDVM = 1 then
  begin
    aperture_local:=DVM1Data.Aperture;
    range_local:=DVM1Data.Range;
    readings_local:=DVM1Data.Readings;
  end;
  if iDVM = 2 then
  begin
    aperture_local:=DVM2Data.Aperture;
    range_local:=DVM2Data.Range;
    readings_local:=DVM2Data.Readings;
  end;
  if iDVM = 3 then
  begin
    aperture_local:=DVM3Data.Aperture;     {STWI-Amplifier}
    range_local:=DVM3Data.Range;           {Keithley-Filter (0, 1, 2, 3}
    readings_local:=DVM3Data.Readings;     {hp=1, Keithley=2}
    if (ihhs = 6) then
    begin
      Samp_local:=AnalysisData.SpeedCorr; {vier;, Zahl der Blockpositionen}
      Scan_local:=number_of_scans;
      MeasNom_local:=AnalysisData.NrOfRev; {MesRep; Zahl der Messungen/Posit.}
      SMSPause_local:=ROUND(AnalysisData.PtsPerRev);{SMS_Pause}
      SMSRef_Local:=AnalysisData.OrdOfFA;  {Ref-Fahrt in der Messroutine}
    end;
  end;
end;

PROCEDURE IDVM_nachher;
begin
  if iDVM = 1 then
  begin
    DVM1Data.Aperture:=aperture_local;
    DVM1Data.Range:=range_local;
    DVM1Data.Readings:=readings_local;
  end;
  if iDVM = 2 then
  begin
    DVM2Data.Aperture:=aperture_local;
    DVM2Data.Range:=range_local;
    DVM2Data.Readings:=readings_local;
  end;
  if (iDVM = 3) and (ihhs <> 6) then
  begin
    DVM3Data.Aperture:=aperture_local;
    DVM3Data.Range:=range_local;
    DVM3Data.Readings:=readings_local;
  end;
  if (iDVM = 3) and (ihhs = 6) then
  begin
    DVM3Data.Aperture:=aperture_local; {STWI-Amplifier}
    STWI_AMPLIFIER:=aperture_local;
    DVM3Data.Range:=round(range_local);{Filter Keithley, 4=ohne Preamp}
    DVM3Data.Readings:=readings_local; {DVM Select}
    if (round(readings_local)=1) then DVM_ident:='h';
    if (round(readings_local)=2) then DVM_ident:='k';
    if (samp_local<5) and (samp_local>0) then vier:=samp_local;
    if MeasNom_local=1 then MesRep:=MeasNom_local;
    if MeasNom_local=2 then MesRep:=MeasNom_local;
    if scan_local>0 then number_of_scans:=Scan_local;
    SMS_Pause:=SMSPause_local;
    SMSREF:=SMSRef_Local;
    AnalysisData.SpeedCorr:=Samp_local; {vier;, Zahl der Blockpositionen}
    AnalysisData.NrOfRev:=MesRep; {MesRep; Zahl der Messungen/Posit.}
    AnalysisData.PtsPerRev:=SMS_Pause;{SMS_Pause}
    AnalysisData.OrdOfFA:=SMSRef;  {Ref-Fahrt in der Messroutine}

  end;
end;

BEGIN

  OldWin:=WinActCtrl;

  SetUpDVMWinLayOut := WinDefDes;
  if iDVM = 1 then
     WinSetHeader(SetUpDVMWinLayOut,LightGray,
     C_LightBlue+'DVM 1 Set UP');
  if iDVM = 2 then
     WinSetHeader(SetUpDVMWinLayOut,LightGray,
     C_LightBlue+'DVM 2 Set UP');
  if (iDVM = 3) and (ihhs <> 6) then
     WinSetHeader(SetUpDVMWinLayOut,LightGray,
     C_LightBlue+'DVM 3 Set UP');
  if (iDVM = 3) and (ihhs = 6) then
     WinSetHeader(SetUpDVMWinLayOut,LightGray,
     C_LightBlue+'SW2 special');
  WinSetFillColor(SetUpDVMWinLayOut, yellow);
  WinSetOpenMode(SetUpDVMWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=(3*width) div 4; ypos:=50;
  WinHeight:= 6*height;
  SetUpDVMWin:=OpenWin(xpos,ypos, xpos+3*width div 2,
                                  ypos+WinHeight,SetUpDVMWinLayOut);

  IDVM_vorher;

  ChangeWin(SetUpDVMWin);
  SetMargin(5,LeftMargin);
  SetCursor(0,0);
  if (not ((iDVM = 3) and (ihhs = 6))) then
  begin
    ShowLineObject('  Aperture (ms) = ',29, ApertureID, buttonleftevent,
                    NoKey, Local,NilEvProc);
    WRITELN(' ',aperture_local:7:2);

    ShowLineObject(' Nr of Readings = ',29, ReadingsID, buttonleftevent,
                   NoKey, Local,NilEvProc);
    WRITELN('  ',readings_local:6);
    ShowLineObject('          Range = ',29, RangeID, buttonleftevent,
                            NoKey, Local,NilEvProc);
    WRITELN('  ',range_local:8:3);
    ShowLineObject('AZERO ON/OFF 1/0= ',29, AZEROID, buttonleftevent,
                            NoKey, Local,NilEvProc);
    WRITELN('  ',AZERO:6);
  end;
  if (idvm = 3) and (ihhs = 6) then
  begin
    ShowLineObject(' STWI-Amplifier = ',29, ApertureID, buttonleftevent,
                    NoKey, Local,NilEvProc);
    WRITELN(aperture_local:7:2);
    ShowLineObject('HP(1),Keithley(2)=',29, ReadingsID, buttonleftevent,
                   NoKey, Local,NilEvProc);
    WRITELN(readings_local:6);
    ShowLineObject('Filt o=0 f=1 m=2 s=3 =',29, RangeID, buttonleftevent,
                            NoKey, Local,NilEvProc);
    WRITELN(range_local:2:0);
    ShowLineObject('Block Positionen = ',29, AZEROID, buttonleftevent,
                            NoKey, Local,NilEvProc);
    WRITELN(Samp_local:6);

    ShowLineObject('S�tze / Position = ',29, MeasNomID, buttonleftevent,
                            NoKey, Local,NilEvProc);
    WRITELN(MeasNom_local:6);
    ShowLineObject('Scans            = ',29, ScanID, buttonleftevent,
                            NoKey, Local,NilEvProc);
    WRITELN(Scan_local:6);
    ShowLineObject('SMS_Pause        = ',29, SMSPauseID, buttonleftevent,
                            NoKey, Local,NilEvProc);
    WRITELN(SMSPause_local:6);
    ShowLineObject('Ref (0,1,2,3)    = ',29, SMSRefID, buttonleftevent,
                            NoKey, Local,NilEvProc);
    WRITELN(SMSRef_local:6);

  end;
  repeat
    locEvent:=WaitOfEvent(firstID,lastID);
    case locEvent of
      ApertureID   : BEGIN
            SetCursor(19,0);
            aperture_local:= Editreal(aperture_local,0,3000,7,2);
            SetCursor(19,0);
            WRITELN(aperture_local:7:2);
                     END;
      ReadingsID   : BEGIN
            SetCursor(20,1);
            readings_local := Editint(readings_local,0,6000,6);
            SetCursor(20,1);
            adjustreadings;
            WRITELN(readings_local:6);
                     END;
      RangeID      : BEGIN
            if (idvm = 3) and (ihhs = 6) then
            begin
              SetCursor(22,2);
              range_local := Editreal(range_local,0.000,20000.0,2,0);
              SetCursor(22,2);
              WRITELN(range_local:2:0);
            end
            else
            begin
              SetCursor(20,2);
              range_local := Editreal(range_local,0.000,20000.0,8,3);
              SetCursor(20,2);
              WRITELN(range_local:8:3);
            end;
                     END;
      AZEROID      : BEGIN
                       SetCursor(20,3);
                       if (idvm=3) and (ihhs=6) then
                       begin
                         Samp_local := Editint(Samp_local,1,4,6);
                         SetCursor(20,3);
                         WRITELN(Samp_local:6);
                       end
                       else
                       begin
                         AZERO := Editint(AZERO,0,1,6);
                         SetCursor(20,3);
                         WRITELN(AZERO:6);
                       end;
                     END;
      MeasNomID    : Begin
                       SetCursor(20,4);
                       MeasNom_local := Editint(MeasNom_local,1,2,6);
                       SetCursor(20,4);
                       WRITELN(MeasNom_local:6);
                     End;
      ScanID       : Begin
                       SetCursor(20,5);
                       Scan_local := Editint(Scan_local,1,99,6);
                       SetCursor(20,5);
                       WRITELN(Scan_local:6);
                     End;
      SMSPauseID   : Begin
                       SetCursor(20,6);
                       SMSPause_local := Editint(SMSPause_local,0,10000,6);
                       SetCursor(20,6);
                       WRITELN(SMSPause_local:6);
                     End;
      SMSRefID   : Begin
                       SetCursor(20,7);
                       SMSRef_local := Editint(SMSRef_local,0,3,6);
                       SetCursor(20,7);
                       WRITELN(SMSRef_local:6);
                     End;
    END;
  until locEvent=0;;
  ChangeWin(OldWin);
  CloseWin(SetUpDVMwin);

  IDVM_nachher;
END;


(************************************************************)
PROCEDURE SetUpScaleProc;
  CONST xmin0ID   = 6001;
        xmax0ID   = 6002;
        ymin0ID   = 6003;
        ymax0ID   = 6004;
        xmin90ID   = 6005;
        xmax90ID   = 6006;
        ymin90ID   = 6007;
        ymax90ID   = 6008;
        disp0ID    = 6009;
        disp90ID   = 6010;
        auto0ID    = 6011;
        auto90ID   = 6012;
        fact0ID    = 6013;
        fact90ID   = 6014;
        replotID   = 6015;

        firstID  = xmin0ID;
        lastID   = replotID;

 VAR SetUpScaleWinLayOut                    : WinDesType;
     OldWin,SetUpScaleWin                   : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent                          : WORD;

BEGIN
  ireplot:=0;
  OldWin:=WinActCtrl;

  SetUpScaleWinLayOut := WinDefDes;
  WinSetHeader(SetUpScaleWinLayOut,LightGray,
               C_LightBlue+'Change Scaling');
  WinSetFillColor(SetUpScaleWinLayOut, yellow);
  WinSetOpenMode(SetUpScaleWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=(3*width) div 4; ypos:=50;
  WinHeight:= 10*height;
  SetUpScaleWin:=OpenWin(xpos,ypos, xpos+2*width,
                                  ypos+WinHeight,SetUpScaleWinLayOut);

  ChangeWin(SetUpScaleWin);
  SetMargin(5,LeftMargin);
  SetCursor(0,0);
  ShowLineObject('   xmin_zero = ',29, xmin0ID, buttonleftevent, NoKey,
                     Local,NilEvProc);
  WRITELN('  ',ScaleData.xminzero:7:3);

  ShowLineObject('   xmax_zero = ',29, xmax0ID, buttonleftevent, NoKey,
                     Local,NilEvProc);
  WRITELN('  ',ScaleData.xmaxzero:7:3);

  ShowLineObject('   ymin_zero = ',29, ymin0ID, buttonleftevent, NoKey,
                     Local,NilEvProc);
  WRITELN('  ',ScaleData.yminzero:7:3);

  ShowLineObject('   ymax_zero = ',29, ymax0ID, buttonleftevent, NoKey,
                     Local,NilEvProc);
  WRITELN('  ',ScaleData.ymaxzero:7:3);

  ShowLineObject(' xmin_ninety = ',29, xmin90ID, buttonleftevent, NoKey,
                   Local,NilEvProc);
  WRITELN('  ',ScaleData.xminninety:7:3);

  ShowLineObject(' xmax_ninety = ',29, xmax90ID, buttonleftevent, NoKey,
                   Local,NilEvProc);
  WRITELN('  ',ScaleData.xmaxninety:7:3);

  ShowLineObject(' ymin_ninety = ',29, ymin90ID, buttonleftevent, NoKey,
                   Local,NilEvProc);
  WRITELN('  ',ScaleData.yminninety:7:3);

  ShowLineObject(' ymax_ninety = ',29, ymax90ID, buttonleftevent, NoKey,
                   Local,NilEvProc);
  WRITELN('  ',ScaleData.ymaxninety:7:3);
  ShowLineObject('   0 display channel = ',31, disp0ID, buttonleftevent,
                     NoKey, Local,NilEvProc);
  WRITELN('  ',ScaleData.dispchannel0:3);
  ShowLineObject('  90 display channel = ',31, disp90ID, buttonleftevent,
                    NoKey, Local,NilEvProc);
  WRITELN('  ',ScaleData.dispchannel90:3);
  ShowLineObject('   0 autoscale (1/0) = ',31, auto0ID, buttonleftevent,
                     NoKey, Local,NilEvProc);
  WRITELN('  ',ScaleData.autoscale0:3);
  ShowLineObject('  90 autoscale (1/0) = ',31, auto90ID, buttonleftevent,
                    NoKey, Local,NilEvProc);
  WRITELN('  ',ScaleData.autoscale90:3);
  ShowLineObject('  0 residuals factor = ',33, fact0ID, buttonleftevent,
                    NoKey, Local,NilEvProc);
  WRITELN('  ',ScaleData.resfactor0:8:2);
  ShowLineObject(' 90 residuals factor = ',33, fact90ID, buttonleftevent,
                   NoKey, Local,NilEvProc);
  WRITELN('  ',ScaleData.resfactor90:8:2);

  ShowLineObject('    ',29, ReplotID, buttonleftevent, NoKey, Local,
                  NilEvProc);
  setcolor(red);
  writeln('     REPLOT');
  setcolor(black);

  repeat
    locEvent:=WaitOfEvent(firstID,lastID);
    case locEvent of
      xmin0ID   : BEGIN
            SetCursor(17,0);
            ScaleData.xminzero:= Editreal(ScaleData.xminzero,
                                          -1000,1000,7,3);
            SetCursor(17,0);
            WRITELN(ScaleData.xminzero:7:3);
                  END;

      xmax0ID   : BEGIN
            SetCursor(17,1);
            ScaleData.xmaxzero:= Editreal(ScaleData.xmaxzero,
                                          -1000,1000,7,3);
            SetCursor(17,1);
            WRITELN(ScaleData.xmaxzero:7:3);
                  END;

      ymin0ID   : BEGIN
            SetCursor(17,2);
            ScaleData.yminzero:=
                         Editreal(ScaleData.yminzero,-10000,10000,7,3);
            SetCursor(17,2);
            WRITELN(ScaleData.yminzero:7:3);
                  END;

      ymax0ID   : BEGIN
            SetCursor(17,3);
            ScaleData.ymaxzero:=
                         Editreal(ScaleData.ymaxzero,-10000,10000,7,3);
            SetCursor(17,3);
            WRITELN(ScaleData.ymaxzero:7:3);
                  END;

      xmin90ID   : BEGIN
            SetCursor(17,4);
            ScaleData.xminninety:= Editreal(ScaleData.xminninety,
                                             -1000,1000,7,3);
            SetCursor(17,4);
            WRITELN(ScaleData.xminninety:7:3);
                  END;

      xmax90ID   : BEGIN
            SetCursor(17,5);
            ScaleData.xmaxninety:= Editreal(ScaleData.xmaxninety,
                                             -1000,1000,7,3);
            SetCursor(17,5);
            WRITELN(ScaleData.xmaxninety:7:3);
                  END;

      ymin90ID   : BEGIN
            SetCursor(17,6);
            ScaleData.yminninety:=
                     Editreal(ScaleData.yminninety,-10000,10000,7,3);
            SetCursor(17,6);
            WRITELN(ScaleData.yminninety:7:3);
                  END;

      ymax90ID   : BEGIN
            SetCursor(17,7);
            ScaleData.ymaxninety:=
                     Editreal(ScaleData.ymaxninety,-10000,10000,7,3);
            SetCursor(17,7);
            WRITELN(ScaleData.ymaxninety:7:3);
                  END;
      disp0ID   : BEGIN
            SetCursor(25,8);
            ScaleData.dispchannel0:= Editint(ScaleData.dispchannel0,1,8,3);
            SetCursor(25,8);
            WRITELN(ScaleData.dispchannel0:3);
                  END;
      disp90ID   : BEGIN
            SetCursor(25,9);
            ScaleData.dispchannel90:= Editint(ScaleData.dispchannel90,1,8,3);
            SetCursor(25,9);
            WRITELN(ScaleData.dispchannel90:3);
                  END;
      auto0ID   : BEGIN
            SetCursor(25,10);
            ScaleData.autoscale0:= Editint(ScaleData.autoscale0,0,1,3);
            SetCursor(25,10);
            WRITELN(ScaleData.autoscale0:3);
                  END;
      auto90ID   : BEGIN
            SetCursor(25,11);
            ScaleData.autoscale90:= Editint(ScaleData.autoscale90,0,1,3);
            SetCursor(25,11);
            WRITELN(ScaleData.autoscale90:3);
                  END;
      fact0ID   : BEGIN
            SetCursor(25,12);
            ScaleData.resfactor0:=
                      Editreal(ScaleData.resfactor0,0.0,1000.0,8,2);
            SetCursor(25,12);
            WRITELN(ScaleData.resfactor0:8:2);
                  END;
      fact90ID   : BEGIN
            SetCursor(25,13);
            ScaleData.resfactor90:=
                      Editreal(ScaleData.resfactor90,0.0,1000.0,8,2);
            SetCursor(25,13);
            WRITELN(ScaleData.resfactor90:8:2);
                  END;
      replotID   : BEGIN
                  ireplot:=1;
                  locevent:=0;
                  END;

    END;
  until locEvent=0;
  ChangeWin(OldWin);
  CloseWin(SetUpScalewin);
END;


(*****************************************************)
PROCEDURE SetUpProc;

 CONST DefineID      = 4000;
       AnalysisID    = 4001;
       DACID         = 4002;
       ServoID       = 4003;
       SMS1ID        = 4004;
       SMS2ID        = 4005;
       MBInc1ID      = 4006;
       MBInc2ID      = 4007;
       IntegratorID  = 4008;
       DVM1ID        = 4009;
       DVM2ID        = 4010;
       DVM3ID        = 4011;
       PAR32ID       = 4012;
       ADCID         = 4013;
       ScaleID       = 4014;
       StoreSetUpID  = 4015;
       LoadSetUpID   = 4016;
       firstID       = DefineID;
       lastID        = LoadSetUPID;

 VAR SetUpWinLayOut                    : WinDesType;
     SetUpWin,OldWin                   : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent, infoEvent               : WORD;
     dirStr                            : String[8];

begin

  OldWin:=WinActCtrl;

  SetUpWinLayOut := WinDefDes;
  WinSetHeader(SetUpWinLayOut,LightGray,
               C_LightBlue+'SET-UP MENUE');
  WinSetFillColor(SetUpWinLayOut, lightgreen);
  WinSetOpenMode(SetUpWinLayOut, WinHeadText, XmmSave);
  xpos:=round(0.003*realval(GetMaxX));
  ypos:=round(realval(GetMaxY)*0.055);
  GetButtonDim(FONT8x16,1,18,width,height);
  WinHeight:= 11*height;
  SetUpWin:=OpenWin(xpos,ypos, xpos+width, ypos+WinHeight,SetUpWinLayOut);

  ChangeWin(SetUpWin);
  SetMargin(5,LeftMargin);
  SetCursor(0,0);
  ShowLineObject('Measurement',18, DefineID, buttonleftevent, NoKey, Local,
                  NilEvProc);
  writeln(' ');
  if ihhs<>6 then
  begin
    ShowLineObject('Data Analysis',18, AnalysisID, buttonleftevent, NoKey,
                       Local,NilEvProc);
    writeln(' ');
  end;
  ShowLineObject('Servo Motors',18, ServoID, buttonleftevent, NoKey, Local,
                  NilEvProc);
  writeln(' ');
  ShowLineObject('DAC  ',18, DACID, buttonleftevent, NoKey, Local,
                  NilEvProc);
  writeln(' ');
  ShowLineObject('SMS 1',18,SMS1ID, buttonleftevent, NoKey, Local,
                  NilEvProc);
  writeln(' ');
    if (ihhs <> 1) then ShowLineObject('SMS 2',18,SMS2ID, buttonleftevent, NoKey, Local,
                  NilEvProc);
  writeln(' ');
  ShowLineObject('MBInc 1',18, MBInc1ID, buttonleftevent, NoKey, Local,
                  NilEvProc);
  writeln(' ');
  ShowLineObject('MBInc 2',18, MBInc2ID, buttonleftevent, NoKey, Local,
                  NilEvProc);
  writeln(' ');
  ShowLineObject('DVM 1',18,DVM1ID, buttonleftevent, NoKey, Local,
                  NilEvProc);
  writeln(' ');
  if (ihhs <> 6) and (ihhs <> 1) then
ShowLineObject('DVM 2',18,DVM2ID, buttonleftevent, NoKey, Local,
                  NilEvProc);
  writeln(' ');
  if (ihhs <> 6)  and (ihhs <> 1) then ShowLineObject('DVM 3',18,DVM3ID, buttonleftevent,
                 NoKey, Local,NilEvProc);
  if (ihhs = 6) then ShowLineObject('SW2 special',18,DVM3ID, buttonleftevent,
                 NoKey, Local,NilEvProc);


  writeln(' ');
    if (ihhs <>6) and (ihhs <> 1) then ShowLineObject('Integrator',18, IntegratorID, buttonleftevent, NoKey,
                  Local,NilEvProc);
  writeln(' ');
  ShowLineObject('Change Scaling',18,ScaleID, buttonleftevent, NoKey, Local,
                  NilEvProc);
  writeln(' ');
  ShowLineObject('',18,StoreSetUpID, buttonleftevent, NoKey, Local,
                  NilEvProc);
  setcolor(red);
  writeln('Store Set Up');
  setcolor(black);
  ShowLineObject('',18,LoadSetUpID, buttonleftevent, NoKey, Local,
                  NilEvProc);
  setcolor(red);
  write('Load Set Up');
  setcolor(black);

  repeat
    locEvent:=WaitOfEvent(firstID,lastID);
    case locEvent of
      DefineID     : begin
                       if ihhs = 1 then SetUpDefineHHSProc;
                       if ihhs = 6 then SetUpDefine5mProc;
                     end;
      AnalysisID   : if ihhs <> 6 then SetUpAnalysisProc;
      DACID        : SetUpDACProc;
      ServoID      : SetUpServoProc;
      SMS1ID       : SetUpSMSProc(1);
      SMS2ID       : SetUpSMSProc(2);
      MBInc1ID     : begin
                       if (ihhs = 6) then SetUpMBInc1bProc;
                       if (ihhs <> 6) then SetUpMBInc1Proc;
                     end;
      MBInc2ID     : SetUpMBInc2Proc;
      IntegratorID : SetUpIntegratorProc;
      DVM1ID       : SetUpDVMProc(1);
      DVM2ID       : SetUpDVMProc(2);
      DVM3ID       : SetUpDVMProc(3);


      ScaleID      : SetUpScaleProc;
      StoreSetUpID : SetUpStoreProc;
      LoadSetUpID  : SetUpLoadProc(0);
    END;
  until (locEvent=0) or (ireplot=1);
  ChangeWin(OldWin);
  CloseWin(SetUpWin);
  if ireplot = 1 then
  begin
    replotdata(3);
    ireplot:=0;
  end;
END;

END.