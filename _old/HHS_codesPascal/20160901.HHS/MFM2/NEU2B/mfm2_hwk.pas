(*** Hello: replace  ibwrt with IEEE_write (if you have time) **)
(*** Talking with the OWIS Controller is time critical! bull shit! **)
{$N+}
unit mfm2_hwk;
(*03.02.2015 HHS+SW2*)
(*30.03.2001*   keithley_copy eingebaut "keithley.dat"*)
(*26.06.2007 SMS1Ref,SMS2Ref Fahrgeschwindigkeit SMSRampSteps erh�ht*)
INTERFACE

USES DOS, CRT, Graph, SCALELIB, MBBASE, BESLIB, SDLIB,
          STDLIB, MOUSELIB, VESALIB, WINLIB,MBADTLIB,
          TPDECL, AUXIO1,
          MFM2_TCK,MFM2_UFK;

TYPE
  STRING1 = STRING[1];
  STRING2 = STRING[2];
  STRING4 = STRING[4];
  const rdn_size=10000;
Type trdn = Array[1..rdn_size] of char;
var  chbreak:char;

PROCEDURE RouteStop;
PROCEDURE CHARFIND(var rdf:trdn;var charpos:integer;chf:char);
procedure OpenOutFile(mode1 : integer; mode2 : integer);
procedure CloseOutFile(mode1 : integer; mode2 : integer);
procedure TransparentMode(mode : integer);
procedure SetTerHos;
procedure Wake_Up;

procedure gpiberr(msg:string);
Procedure IEEE_write(device: integer; wr_text : string);
Procedure IEEE_read(device: integer; n: integer);
procedure IEEE_write_slow(var SW12: integer; wr_text: string);

Procedure RS232_Open(port : word);
Procedure RS232_Close(port : word);
Procedure RS232_write(port: word; wr_text : string;
                      checksum_flag: integer; i10 : integer);
Procedure RS232_read(port: word; checksum_flag: integer);

Procedure HHSIni;
Procedure GBIni;
Procedure STWIIni;
Procedure HHSClose;

procedure SWini(iSTWI: integer);

Procedure DMMIni;
procedure ServoCheck(var axisnr: integer; var i_ServoCheck : integer);
procedure ServoDrive(axisnr: integer; relabs: integer;
          i_check_xyztheta:integer; distance:real; velocity: real);
procedure ServoDriveSTWI(axisnr: integer; relabs: integer;
          iOneTwo: integer; distance:real; velocity: real);
procedure get_dist_vel(var axisnr:integer; var distance: real;
          var velocity: real; var s_dist: string; var s_velo: string);
procedure ServoRef(reftyp: integer; refaxn: integer);
procedure NumString(iterhos: integer; linvel: longint; var s44: string);
procedure JoyStick(onoff: integer);
procedure ServoIni;
procedure SetServoParameters;
procedure SetServoParameters_new;
procedure servo_read_par;
procedure read_x_axis_par;
procedure SetSTWIParameters(conax : integer);
procedure ReadSTWIParameters;

procedure MBINC1ini;
procedure MBINC2ini;
procedure MBINC1aini;
procedure MBINC1bini;
procedure MBINC2aini;
procedure AdjustProc;
procedure MBInc1Start;
procedure MBInc2Start;
procedure MBInc1Stop;
procedure MBInc2Stop;

procedure get_aktdacvoltage;
procedure DACdrive(startvolt:real;endvolt:real;mode:integer);
procedure DACini;

procedure SMS1Ini;
procedure SMS1Drive;
procedure SMS2Ini;
procedure SMS2Drive;
procedure stringsmanipulate;

PROCEDURE SMSRampIni;
PROCEDURE SMSRampeFill(var SMSR:SMSRampe;SMSData:SMSDataType);
PROCEDURE SMSRampeDo(MotNr:byte;var RelStep:longint;var SMSR:SMSRampe;StepSel:integer);
PROCEDURE SMSRampClose;
PROCEDURE WarningLeer;
PROCEDURE SMS1Ref;
PROCEDURE SMS2Ref;
PROCEDURE SMSReference;

procedure dvmini(iDVM: integer);
procedure dvmerr(msg:string; rdchar:char);
procedure deletezeroes(sendstr2:string);
procedure DVMStart(iDVM: integer);
procedure DVMReadOut(iDVM: integer; ipos:integer);
procedure DVMReadOut_new(iDVM: integer; ipos:integer);
procedure DVMreset(iDVM: integer);

procedure LaserIni;

function DVM_get_data(id : integer) : real;
function STWI_ask_motion : integer;
function MBInc1_timebase_factor : real;
Function Byte_Hex(chb : byte) : string;
Function check_sum(wr_text : string) : byte;
FUNCTION ASCII_HEX_TO_BIN (c : STRING2) : BYTE;
FUNCTION HEX_DEZ (c : STRING2) : INTEGER;
FUNCTION CH_DEZ (c : char) : INTEGER;
Function Longint_S4(value : longint) : string4;
Procedure StopProc;


IMPLEMENTATION

uses MFM2_ANK;

PROCEDURE RouteStop;
begin
  chbreak:=readkey;
end;

Procedure StopProc;
var
  startvolt, endvolt      : real;
  tmpSteps:longint;
begin
  if (ihhs=1) then
  begin
    EndVolt:=0;
    if ifirstdac = 1 then
    begin
      mbopen(slave1);
      DACini;
      ifirstdac:=0;
    end;
    Startvolt:=aktdacvoltage;
    DACDrive(StartVolt,EndVolt,1);
    TmpSteps:= SMS1data.Steps;
    SMS1data.Steps:=0;
    SMS1Drive;
    SMS1Data.Steps:=TmpSteps;
  end;
end;

PROCEDURE CHARFIND(var rdf:trdn;var charpos:integer;chf:char);
var
  i1                        : Integer;
begin
  for i1:=1 to rdn_size do
  begin
    if (rdf[i1]=chf) or (rdf[i1]=#10) then     {#10 Ende der keithleyMessung}
    begin
      charpos:=i1;
      exit;
    end;
  end;
end;

PROCEDURE KEYTHLEY_COPY(var rdf:trdn;chf:char);
var f11:text;
ix,iz:integer;
rdfc :char;
begin
  iz:=1;
  assign(f11,'KEITHLEY.DAT');
  rewrite(f11);

  write(f11,iz:4,'  ');
  for ix:=1 to rdn_size do
  begin
    rdfc:=rdf[ix];
    if (rdfc<>chf) then write(f11,rdfc);
    if (rdfc=chf) then
    begin
      writeln(f11);
      inc(iz);
      write(f11,iz:4,'  ');
    end;
    if (rdfc=#10) then    {Ende der keithley-Messung}
    begin
      writeln(f11);
      close(f11);
      exit;
    end;
  end;
  close(f11);
end;


function DVM_get_data(id : integer) : real;
var
  ystring, s1 : string;
  i           : integer;
  xxx         : real;
begin
  if ( id = 1) then
    ibrd(HP1,rd,18);
  if ( id = 2) then
    ibrd(HP2,rd,18);
  if ( id = 3) then
    ibrd(HP3,rd,18);
  ystring:='';
  for i:=1 to 16 do
  begin
     s1:=rd[i];
   ystring:=ystring+s1;
   end;
   val(ystring,xxx,code);
   DVM_get_data:=xxx;
end;

PROCEDURE DMM_Value(dmm:integer;dmmwo:char);
var dmmval:longint;
   OldWin  : WinCtrlPtrType;
begin
  OldWin:=WinActCtrl;
  if ifirstDMM = 1 then dmmini;
  dmmstartconvertion(dmm1);

  while  not dmmeoc(dmm1) do;
  dmmval:=dmmgetdata(dmm1);
  if (dmmwo='T') then
  begin
    changewin(temperaturewin);
    clearwin;
    temperature:=1000.0*DMMConvVal(dmm1,dmmval);
    write(temperature:6:2);
  end;
  if (dmmwo='W') then
  begin
{    changewin(warningswin);
    clearwin; }
    temperature:=DMMConvVal(dmm1,dmmval);
    write(temperature:10:5);
  end;
  changewin(OldWin);
end;

procedure DMMIni;
var
   i    :       integer;
begin
DMMopen(dmm1);

DMMSetRange(dmm1,DMM_400mV,DMM_VOLT or DMM_DC);
for i:=1 to 3 do
      DMMRead(dmm1);
DMMZero(dmm1);
for i:=1 to 3 do
      DMMRead(dmm1);
ifirstDMM:=0;
end;

procedure HHSini;
var
   OldWin  : WinCtrlPtrType;
   number  : integer;

begin
    OldWin:=WinActCtrl;
    changewin(warningswin);

     mbopen(slave1);
delay(200);
     DACini;
writeln('DAC ini');
delay(200);
     SMS1Ini;
writeln('SMS ini');
delay(200);
     if ihhs = 1 then
     begin
         DVMini(1);
writeln('DVMini(1)');
delay(2000);
       DVMreset(1);
writeln('DVMreset(1)');
delay(1000);
       DVMini(1);
writeln('DVMini(1)');
delay(2000);
     end;
     changewin(oldwin);
delay(200);

    if(ihhs = 1) then
    begin
    DefineData.Red_factor:=1;
    DefineData.SaveStack:=0;
    filename:='mfm_hhs.num';
    openoutfile(1,0);
    readln(outfile,number);
    readln(outfile,path);
    closeoutfile(1,0);
    end;
end;

procedure GBini;
var
   OldWin  : WinCtrlPtrType;
begin
     OldWin:=WinActCtrl;
     changewin(warningswin);
     MB_READCONFIG;
     mbopen(slave1);
     mbopen(slave2);
     DACini;
     DACOpen(DAC1);
     changewin(oldwin);
end;

procedure STWIIni;
var
   OldWin  : WinCtrlPtrType;
begin
     OldWin:=WinActCtrl;
     changewin(warningswin);
     delay(200);
     SWini(1);  (* initialize IEEE *)
     delay(200);
     SWini(2);  (* initialize IEEE *)
     delay(200);
     DVMini(1);
     MB_READCONFIG;
     mbopen(slave1);
     mbopen(slave2);
     DACini;
     DACOpen(DAC1);

     SetSTWIParameters(1);
     ReadSTWIParameters;
     ifirstSTWI:=0;
     changewin(oldwin);

end;

procedure HHSclose;
     begin
     GenClose(Gen1);
(*     mbclose(slave1);                *)
     end;

procedure OpenOutFile(mode1 : integer; mode2 : integer);
(* mode1 = 0 : write on new file  *)
(* mode1 = 1 : read from old file *)
(* mode2 = 0 : open text file *)
(* mode2 = 1 : open CFG file  *)

var
     OldWin    : WinCtrlPtrType;
     ch        : char;

begin

OldWin:=WinActCtrl;

fehler:=0;
ch:=' ';

if mode2 = 0 then    (* text file *)
ASSIGN  (OutFile,FileName);

if mode2 = 1 then    (* cfg file *)
begin
(* if new configuration file has different format than
old configuration file then:
   read old file with old format (CFGFile0)
   write new file with new format (CFGFile) *)
if mode1 = 0 then
ASSIGN  (CFGFile,FileName);
if mode1 = 1 then
ASSIGN  (CFGFile0,FileName0);
if mode1 = 2 then
ASSIGN  (STWICFGFile,STWIFileName);
if mode1 = 3 then
ASSIGN  (STWICFGFile0,STWIFileName0);
(*ASSIGN  (CFGFile,FileName);*)
end;

{$I-}
if (mode1 = 0) or (mode1 = 2) then
begin
if mode2 = 0 then
   REWRITE (OutFile);
if mode2 = 1 then
begin
if (mode1 = 0) then
   REWRITE (CFGFile);
if (mode1 = 2) then
   REWRITE (STWICFGFile);
end;
end;

if (mode1 = 1) or (mode1 = 3) then
begin
if mode2 = 0 then
   RESET (OutFile);
if mode2 = 1 then
begin
if (mode1 = 1) then
   RESET (CFGFile0);
if (mode1 = 3) then
   RESET (STWICFGFile0);
end;
end;
fehler:=ioresult;

if fehler <> 0 then
begin;
    repeat
         CASE fehler OF
            152 : begin
                changewin(warningswin);
                writeln(' drive not ready');
                write(' insert disk and hit return');
                end;
             2: begin
                changewin(warningswin);
                writeln(' file not found, press q');
                write(' and enter new file name');
                end;
         END;
         Ch:=readkey;
         Ch := UpCase (Ch);
         if ch = 'Q' then
               fehler:= 0
           else
           begin

               if (mode1 = 0) or (mode1 = 2) then
               begin
               if mode2 = 0 then
               rewrite(outfile);
               if mode2 = 1 then
               begin
               if(mode1 = 0) then
                  rewrite(CFGFile);
               if(mode1 = 2) then
                  rewrite(STWICFGFile);
               end;
               end;
               if (mode1 = 1) or (mode1 = 3) then
               begin
               if mode2 = 0 then
               reset(outfile);
               if mode2 = 1 then
               begin
               if(mode1 = 1)then
                  reset(CFGFile0);
               if(mode1 = 3)then
                  reset(STWICFGFile0);
               end;
               end;

               fehler:=ioresult;
           end;
    until fehler = 0;
end;
if ch = 'Q' then fehler:= 1;
{$I+}

changewin(warningswin);
clearwin;
changewin(oldwin);

end;

(******************************************************)
procedure CloseOutFile(mode1 : integer; mode2 : integer);
(* mode1 = 0 : write on new file  *)
(* mode1 = 1 : read from old file *)
(* mode2 = 0 : close text file *)
(* mode2 = 1 : close CFG file  *)
var
     OldWin     : WinCtrlPtrType;
     ch         : char;

begin

OldWin:=WinActCtrl;

if fehler = 0 then
begin
{$I-}
if mode2 = 0 then
CLOSE (OutFile);
if mode2 = 1 then
begin
if mode1 = 0 then
CLOSE (CFGFile);
if mode1 = 1 then
CLOSE (CFGFile0);
if mode1 = 2 then
CLOSE (STWICFGFile);
if mode1 = 3 then
CLOSE (STWICFGFile0);
end;

fehler:=ioresult;

if fehler <> 0 then
begin;
         CASE fehler OF
            101 : begin
                changewin(warningswin);
                writeln(' insert new disk');
                write(' and hit return');
                end;
         END;
         ch:=readkey;
end;
{$I+}

changewin(warningswin);
clearwin;
changewin(oldwin);

end;

END;

(******************************************************)
procedure SetTerHos;
var       wr_string  : string;
begin
     if iterhos = 0 then
        wr_string:='SHT0';
     if iterhos = 1 then
        wr_string:='SHT1';
     RS232_write(com1,wr_string,iterhos,0);
end;

(******************************************************)
procedure TransparentMode(mode : integer);
  CONST SendID    = 1000;
        ReceiveID = 1001;
        firstID   = sendID;
        lastID    = receiveID;

 VAR ManualTMWinLayOut                 : WinDesType;
     OldWin,ManualTMWin                : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent                          : WORD;
     ch                                : char;
     i,j,n                             : integer;
     ystring,sendstr                   : string;
     s1                                : string[1];
     nlines,ncolumns                   : integer;
     isend                             : integer;
 BEGIN

  nlines:=8;
  ncolumns:=37;

  OldWin:=WinActCtrl;

  ManualTMWinLayOut := WinDefDes;
  WinSetHeader(ManualTMWinLayOut,LightGray,
               C_LightBlue+'Transparent Mode');
  WinSetFillColor(ManualTMWinLayOut, yellow);
  WinSetOpenMode(ManualTMWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=3*width div 2 - 30; ypos:=90;
  WinHeight:= 10*height;
  ManualTMwin:=OpenWin(xpos,ypos, xpos+2*width,
                                  ypos+WinHeight,ManualTMWinLayOut);

  ChangeWin(ManualTMWin);
  SetMargin(5,LeftMargin);

  xpos:=35;
  ypos:=230;
  MoveTo(xpos,ypos);
  ButtonDefDes.Frame^.ActivBack := LightBlue;
  ShowButtonObject('Send',10, SendID, ButtonLeft, ALT_S, local, NilEvProc);

  xpos:=140;
  ypos:=230;
  MoveTo(xpos,ypos);
  ButtonDefDes.Frame^.ActivBack := LightBlue;
  ShowButtonObject('Receive',10, receiveID, ButtonLeft, ALT_S,
                                 local, NilEvProc);

  isend:=0;

  repeat
   locEvent:=WaitOfEvent(firstID,lastID);
   case locEvent of
(********************************************************)
      sendID : begin

          clear_screen(nlines,ncolumns);
          setcursor(1,1);
          readln(wr_text);

          if (mode=1)then
            begin
            if ifirstrs232 = 1 then
               begin
               rs232_open(com1);
               ifirstrs232:=0;
               end;
               RS232_write(com1,wr_text,iterhos,0);
            end;

          if(mode=2)then
            begin
            if ifirstlaser = 1 then
               begin
               LaserIni;
               ifirstlaser:=0;
               end;
               IEEE_write(LI,wr_text);
               isend:=1;
            end;

          if(mode=3) or (mode=4) or (mode=5) then
            begin
            if ifirstDVM = 1 then
               begin
               DVMINI(1);
               DVMINI(2);
        (*       DVMINI(3); *)
               ifirstDVM:=0;
               end;
               if (mode = 3) then
                        IEEE_write(HP1,wr_text);
               if (mode = 4) then
                        IEEE_write(HP2,wr_text);
         (*      if (mode = 5) then
                        IEEE_write(HP3,wr_text);  *)
            end;
           delay(200);

          if (mode=6) or (mode=7) then
            begin
            if ifirstSTWI = 1 then
               begin
               SWini(1);
               SWini(2);
               ifirstSTWI:=0;
               end;
          end;
          if (mode=6) then
               IEEE_write(SW1,wr_text);
          if (mode=7) then
               IEEE_write(SW2,wr_text);
           delay(200);

      end;
(*************************************************)
      receiveID : begin
          if (mode=1)then
            begin
            if ifirstrs232 = 1 then
               begin
               rs232_open(com1);
               ifirstrs232:=0;
               end;
               RS232_read(com1,0);
               clear_screen(nlines,ncolumns);
               setcursor(0,0);
               writeln(rd_text);
            end;

          if (mode=2)then
            begin
              if(isend=1)then
                begin
                isend:=0;
                ystring:='';
                ibrd(LI,rd,20);
                for i:= 1 to 20 do
                begin
                s1:=rd[i];
                ystring:=ystring+s1;
                end;
                clear_screen(nlines,ncolumns);
                setcursor(0,3);
                writeln(ystring);
             end;
           end;      (** mode = 2 ***)

          if (mode=3) or (mode=4) or (mode=5) then
            begin
              ystring:='';
              if (mode = 3) then
         (*            ibrd(HP1,rd,20);  *)
                       ibrd(HP1,rd,18);
              if (mode = 4) then
                       ibrd(HP2,rd,18);
         (*     if (mode = 5) then
                       ibrd(HP3,rd,18);  *)
              for i:= 1 to 20 do
              begin
              s1:=rd[i];
              ystring:=ystring+s1;
              end;
              clear_screen(nlines,ncolumns);
              setcursor(0,3);
              writeln(ystring);
           end;      (** mode = 3, 4, 5 ***)
           delay(200);

          if (mode=6) or (mode=7) then
            begin

            if ifirstSTWI = 1 then
               begin
               SWini(1);
               SWini(2);
               ifirstSTWI:=0;
               end;

          if (mode=6)then
              ibrd(SW1,rd,255);
          if (mode=7)then
              ibrd(SW2,rd,255);
              ystring:='';
              for i:= 1 to 255 do
              begin
              s1:=rd[i];
              ystring:=ystring+s1;
              end;
              clear_screen(nlines,ncolumns);
              setcursor(0,3);
              writeln(ystring);

           end;      (** mode = 6, 7 ***)
           delay(200);

          end;       (** receiveID **)
(*************************************************)
  end;   (** case of **)

  until locEvent=0;

  ChangeWin(OldWin);
  CloseWin(ManualTMwin);
end;

(******* IEEE procedures *************************************)
procedure gpiberr(msg:string);
var
     OldWin                   : WinCtrlPtrType;

begin

    OldWin:=WinActCtrl;
    changewin(warningswin);
    writeln (msg);

    write('ibsta = ', ibsta,' <');
    if ibsta and ERR  <> 0 then write (' ERR');
    if ibsta and TIMO <> 0 then write (' TIMO');
    if ibsta and EEND <> 0 then write (' END');
    if ibsta and SRQI <> 0 then write (' SRQI');
    if ibsta and RQS  <> 0 then write (' RQS');
    if ibsta and CMPL <> 0 then write (' CMPL');
    if ibsta and LOK  <> 0 then write (' LOK');
    if ibsta and REM  <> 0 then write (' REM');
    if ibsta and CIC  <> 0 then write (' CIC');
    if ibsta and ATN  <> 0 then write (' ATN');
    if ibsta and TACS <> 0 then write (' TACS');
    if ibsta and LACS <> 0 then write (' LACS');
    if ibsta and DTAS <> 0 then write (' DTAS');
    if ibsta and DCAS <> 0 then write (' DCAS');
    writeln(' >');

    write('iberr = ', iberr);
    if iberr = EDVR then writeln (' EDVR <DOS Error>');
    if iberr = ECIC then writeln (' ECIC <Not CIC>');
    if iberr = ENOL then writeln (' ENOL <No Listener>');
    if iberr = EADR then writeln (' EADR <Address error>');
    if iberr = EARG then writeln (' EARG <Invalid argument>');
    if iberr = ESAC then writeln (' ESAC <Not Sys Ctrlr>');
    if iberr = EABO then writeln (' EABO <Op. aborted>');
    if iberr = ENEB then writeln (' ENEB <No GPIB board>');
    if iberr = EOIP then writeln (' EOIP <Async I/O in prg>');
    if iberr = ECAP then writeln (' ECAP <No capability>');
    if iberr = EFSO then writeln (' EFSO <File sys. error>');
    if iberr = EBUS then writeln (' EBUS <Command error>');
    if iberr = ESTB then writeln (' ESTB <Status byte lost>');
    if iberr = ESRQ then writeln (' ESRQ <SRQ stuck on>');
    if iberr = ETAB then writeln (' ETAB <Table Overflow>');

    writeln('ibcnt = ', ibcnt);

(*  Call the IBONL function to disable the hardware and software.           *)

    ibonl (bd,0);

    halt;
    changewin(oldwin);

end;

(*************************************************************)
Procedure IEEE_write(device: integer; wr_text : string);
var
   n,i  : integer;
begin
    n:=length(wr_text);
    for i := 1 to n do
    wrt[i] := wr_text[i];
(*    ibwrt (LI,wrt,n);  *)
(**** changed 30.9.1997 **)
    ibwrt (device,wrt,n);
    if (ibsta and ERR) <> 0 then gpiberr('Ibwrt Error');
end;

(**********************************************************)
Procedure IEEE_read(device: integer; n: integer);
var
   i     : integer;
   s1    : string;
   rdtxt : string;
begin
    rdtxt:='';
    rd_text:='';
(*    ibrd(LI,rd,20);  *)
(*** changed 30.9.1997 *)
    ibrd(device,rd,20);

    i:=0;
    repeat
          begin
          i:=i+1;
          s1:=rd[i];
          rd_text:=rd_text+s1;
          end;
    until s1 = #13;
end;

(*********** RS232 procedures ***************************)
Function Byte_Hex(chb : byte) : string;
var
    i         : integer;
    chb1,chb2 : byte;
    c1,c2     : char;
    x         : array[0..15] of char;
begin
    x[0] := '0';
    x[1] := '1';
    x[2] := '2';
    x[3] := '3';
    x[4] := '4';
    x[5] := '5';
    x[6] := '6';
    x[7] := '7';
    x[8] := '8';
    x[9] := '9';
    x[10] := 'A';
    x[11] := 'B';
    x[12] := 'C';
    x[13] := 'D';
    x[14] := 'E';
    x[15] := 'F';

    chb2:= chb mod 16;
    chb1:= (chb-chb2 ) div 16;

    c1:=x[round(chb1)];
    c2:=x[round(chb2)];

    Byte_Hex:=c1+c2;

    end;

(**********************************************)
Function longint_s4(value : longint) : string4;
var
   value1,value2     : word;
   c1,c2,c3,c4       : byte;
   cc1,cc2,cc3,cc4   : char;
   OldWin            : WinCtrlPtrType;

begin
OldWin:=WinActCtrl;
changewin(warningswin);

(*   writeln(value);
   readkey; *)

   value2:=value;
   c3:=hi(value2);
   c4:=lo(value2);
   value:= value shr 16;
   value1:=value;
   c1:=hi(value1);
   c2:=lo(value1);

   cc1:=char(c1);
   cc2:=char(c2);
   cc3:=char(c3);
   cc4:=char(c4);

(*   writeln('c1 = ',c1);
   writeln('c2 = ',c2);
   writeln('c3 = ',c3);
   writeln('c4 = ',c4); *)

    Longint_S4:=cc1+cc2+cc3+cc4;

(*    writeln(c1,' ',c2,' ',c3,' ',c4);
    readkey; *)

    changewin(oldwin);

    end;

(******************************************************)
FUNCTION CH_DEZ (c : char) : INTEGER;
var
   value        : integer;

begin
if c='0' then value:=0;
if c='1' then value:=1;
if c='2' then value:=2;
if c='3' then value:=3;
if c='4' then value:=4;
if c='5' then value:=5;
if c='6' then value:=6;
if c='7' then value:=7;
if c='8' then value:=8;
if c='9' then value:=9;
if c='A' then value:=10;
if c='B' then value:=11;
if c='C' then value:=12;
if c='D' then value:=13;
if c='E' then value:=14;
if c='F' then value:=15;
ch_dez:=value;
end;

(******************************************************)
FUNCTION HEX_DEZ (c : STRING2) : INTEGER;

Var
  value : integer;

BEGIN
  value:=ch_dez(c[1])*16;
(*  write(' ',value); *)
  value:=value+ch_dez(c[2]);
(*  write(' ',value,' '); *)
  HEX_DEZ:=value;
END;

(******************************************************)
Function check_sum(wr_text : string) : byte;
var
    i   : integer;
    chb : byte;
begin
    chb:=0;
    for i:=1 to length(wr_text) do
        begin
{$IFOPT R+}
  {$R-}
  {$DEFINE R_PLUS}
{$ENDIF}
        chb:=chb+ord(wr_text[i]);
{$IFDEF R_PLUS}
  {$UNDEF R_PLUS}
  {$R+}
{$ENDIF}
        end;
    check_sum:=chb;
end;

(******************************************************)
Procedure RS232_Open(port : word);
begin
AUXInit(port, 9600, 8, n, 1);
AUXOpen(port);
end;

(*************************************************)
Procedure RS232_Close(port : word);
begin
AUXClose(port);
end;

(*************************************************)
Procedure RS232_write(port: word; wr_text : string;
                      checksum_flag: integer; i10 : integer);
var
   ch      : char;
   chb,tt  : byte;
   chbh    : string;
   i,ii    : integer;
   value   : integer;
   deltime : integer;
   OldWin               : WinCtrlPtrType;
begin

(* checksum_flag = 0 : send checksum_byte        *)
(* checksum_flag = 1 : do not send checksum_byte *)


   delay(10);

   OldWin:=WinActCtrl;
   changewin(warningswin);
   writeln('          ');

if checksum_flag = 0 then
begin
chb:=check_sum(wr_text);
if i10 > 0 then
   begin
   chb:=chb+256;
   for i:=1 to i10 do
       chb:=chb-16;
   end;

if ascii_hex_to_bin('10') = chb then
   chb:=chb+128;
if ascii_hex_to_bin('0D') = chb then
   chb:=chb+128;

chbh:=Byte_Hex(chb);
writeln('check sum (binary) : ',chb);
writeln('check sum (hex) : ',chbh);
end;
ii:=0;
deltime:=1;
for i:=1 to length(wr_text) do
    begin
    ii:=ii+1;
    ch:=wr_text[i];
    delay(deltime);
    if ii < 20 then
         write(ch)
         else
         begin
         if (ord(ch) = 13) or (ord(ch) = 16) then
            begin
            write('kaese');
            readkey;
            end;
         writeln(ch);
         ii:=0;
         end;
    AUXWrite(port,ch);
    delay(deltime);
    end;

if checksum_flag = 0 then
    begin
         ch:=char(chb);
         writeln(ch);
         AUXWrite(port,ch);
    end;

AUXWrite(port,#13);

changewin(oldwin);

if (i_log = 1) then
begin
assign(outfile,'rs232.log');
append(outfile);
writeln(outfile,wr_text);
close(outfile);
end;

end;

(*************************************************)
Procedure RS232_read(port: word; checksum_flag: integer);
(*************************************************)
var
   ch      : char;
   chb     : byte;
   chbh    : string;
   i       : integer;
   status  : Boolean;
begin

status:=TRUE;

rd_text:='';
repeat
  delay(10);
  Status:=AUXReadBuf(port,ch);
  rd_text:=rd_text+ch;
(*write(ch);*)
until status = False;

if (i_log = 1) then
begin
assign(outfile,'rs232.log');
append(outfile);
writeln(outfile,rd_text);
close(outfile);
end;

end;

(******************************************************)
procedure Wake_Up;
var       v : single;
          i : integer;
          dummy : string;
begin
     if (ifirst = 1)then
     begin
     GBini;
     ifirst:=0;
     end;
     i:=0;
     dummy:='0';
if (i_wake_up = 1) then
begin
repeat
  begin
     i:=i+1;
     v:=10;
     DACSetV(DAC1,DACA,v);
     delay(3000);
     v:=0;
     DACSetV(DAC1,DACA,v);
     delay(1000);
     if keypressed then
        dummy:=readkey;
  end;
until dummy = 'q';
end;
     DacClose(DAC1);
end;

(******************************************************)
procedure ServoCheck(var axisnr : integer; var i_ServoCheck : integer);
var i,i1,iterhos_save           : integer;
    OldWin                      : WinCtrlPtrType;
    dummy,wr_string,s1,s2,s12   : string;
begin
(* i_log:=1; *)
OldWin:=WinActCtrl;
iterhos_save:=iterhos;
iterhos:=1;
SetTerHos;
i_ServoCheck:=0;

if (imeasure = 0) then
begin
SetCursor(15,14);
write('                    ');
end;

(**** check begin ****)
(**** SIC3 funktioniert offenbar nicht so, wie es im Manual steht **)
(*dummy:='0';
repeat
begin
      Delay(1000);
      RS232_read(com1,0);
      writeln(rd_text);
      if keypressed then
         dummy:=readkey;
end;
until (rd_text[2] = 'B') or (dummy = 's') or (dummy = 'S');
if(rd_text[4] > '0') or (rd_text[4] < '0') or
  (rd_text[5] > '1') or (rd_text[5] < '1') then
  begin
     i_ServoCheck:=1;
     writeln(rd_text);
     ChangeWin(WarningsWin);
     writeln('');
     write(' Confirm Error ');
     readkey;
     ChangeWin(OldWin);
  end;  *)

(**** check begin ****)
     rd_text:='';
     wr_string:='T1';
     RS232_write(com1,wr_string,iterhos,0);
     delay(100);
     RS232_read(com1,0);
     i:=0;
     i1:=0;
     repeat
       begin
          i:=i+1;
          if (rd_text[i] = 'X') and (rd_text[i+2] = '-')then
             s1:=rd_text[i+4]+rd_text[i+5];
          if (rd_text[i] = 'Y') and (rd_text[i+2] = '-')then
             begin
             s2:=rd_text[i+4]+rd_text[i+5];
             i1:=1;
             end;
       end;
     until i1=1;

     if (axisnr = 2)  then
        s12:=s1;
     if (axisnr = 3) or (axisnr = 4) then
        s12:=s2;

        if (s12[1] > ' ') or (s12[1] < ' ')  or
           (s12[2] > '1') or (s12[2] < '1')  then
           begin
           i_ServoCheck:=1;
           SetCursor(15,14);
           write('Error Code = ',s12);
           ChangeWin(WarningsWin);
           writeln('');
           write(' Confirm Error (q)');
           Wake_up;
           ChangeWin(OldWin);
           end;

(**** check end ****)
if (i_ServoCheck = 0) then
begin
dummy:='0';
repeat
begin
      Delay(500);
      RS232_read(com1,0);
      if keypressed then
         dummy:=readkey;
end;
until (rd_text[2] = 'E') or (dummy = 's') or (dummy = 'S');
i_ServoCheck:=0;
if(rd_text[4] > '0') or (rd_text[4] < '0') or
  (rd_text[5] > '1') or (rd_text[5] < '1') then
  begin
     i_ServoCheck:=1;
     SetCursor(15,14);
     write('Error Code = ');
     for i:=1 to 5 do
         write(rd_text[i]);
     ChangeWin(WarningsWin);
     writeln('');
     write(' Confirm Error (q)');
     Wake_Up;
     ChangeWin(OldWin);
  end;
end;

iterhos:=iterhos_save;
SetTerHos;
i_log:=0;
end;

procedure ServoRef(reftyp: integer; refaxn: integer);
(* offsets from reference points not yet implemented *)
var
   s1a, s1b, s1c  : string;
   i,n            : integer;
   AskWinLayOut   : WinDesType;
   OldWin, AskWin : WinCtrlPtrType;
   dummy          : string[1];
   width,height,xpos,ypos,WinHeight  : WORD;

begin
     OldWin:=WinActCtrl;
     AskWinLayOut := WinDefDes;
     WinSetHeader(AskWinLayOut,LightGray,
     C_LightBlue+'Hello');
     WinSetFillColor(AskWinLayOut, magenta);
     WinSetOpenMode(AskWinLayOut, WinHeadText, XmmSave);
     GetButtonDim(FONT8x16,2,18,width,height);
     xpos:=(3*width) div 4; ypos:=145;
     WinHeight:= 3*height;
     Askwin:=OpenWin(xpos,ypos, xpos+width,
                                  ypos+WinHeight,AskWinLayOut);
     ChangeWin(AskWin);
     if(refaxn = 1) then
               s1a:='x';
     if(refaxn = 2) then
               s1a:='y';
     if(refaxn = 3) then
               s1a:='z';
     if(refaxn = 4) then
               s1a:='theta';
     writeln(' Did you move the ',s1a,'-axes');
     writeln(' to start position,');
     writeln(' distance <=10mm to ref. point?');
     writeln(' ');
     writeln(' (y/n) ');
     s1a:=readkey;
     CloseWin(AskWin);
     if (s1a = 'y') or (s1a = 'Y') then
     begin (* start reference procedure *)

    str(reftyp,s1a);
    if (refaxn = 1) or (refaxn = 4) then   (* x- or theta-axis *)
       begin
       s1b:='3';
       s1c:='4';
       end;
    if (refaxn = 2) or (refaxn = 3) then   (* y- or z-axis *)
       begin
       s1b:='1';
       s1c:='2';
       end;

    wr_text:= 'ENDS='+s1a+s1b;

    if (refaxn = 1) or (refaxn = 3) then
        IEEE_write(SW1,wr_text);
    if (refaxn = 2) or (refaxn = 4) then
        IEEE_write(SW2,wr_text);

(*    repeat
    delay(100);
    until STWI_ask_motion=0; *)

    delay(8000);

    wr_text:= 'ENDS='+s1a+s1c;
    if (refaxn = 1) or (refaxn = 3) then
        IEEE_write(SW1,wr_text);
    if (refaxn = 2) or (refaxn = 4) then
        IEEE_write(SW2,wr_text);

    delay(8000);
    (**** now drive offsets **)
    ServoDriveSTWI(refaxn, 3, 3, 0.0, 0.0);

    delay(100);
  end;

end;

procedure get_dist_vel(var axisnr:integer; var distance: real;
          var velocity: real; var s_dist: string; var s_velo: string);
var
   dist_inc, velo_inc : longint;
begin

          if(axisnr = 1) then
                    begin
                    dist_inc:=round(distance*ServoData.anorad_c.x);
                    end;
          if(axisnr = 2) then
                    begin
                    dist_inc:=round(distance*ServoData.anorad_c.y);
                    end;
          if(axisnr = 3) then
                    begin
                    dist_inc:=round(distance*ServoData.anorad_c.z);
                    end;
          if(axisnr = 4) then
                    begin
                    dist_inc:=round(distance*ServoData.anorad_c.theta);
                    end;

          if(axisnr = 1) or (axisnr = 2) or (axisnr = 3) then
             velo_inc:=round(velocity/STWI_Scal_Vel_xyz);
          if(axisnr = 4) then
             velo_inc:=round(velocity/STWI_Scal_Vel_theta);

   str(dist_inc,s_dist);
   str(velo_inc,s_velo);
end;

(************* servo motor procedures *****************)
procedure ServoDriveSTWI(axisnr:integer; relabs:integer; iOneTwo: integer;
          distance:real; velocity:real);
var
   i,n,sw12       : integer;
   s_dist, s1_dist, s2_dist, s_velo, s1_velo, s2_velo : string;
   SETA, SETB     : string;
   STARTA, STARTB : string;
   VELA, VELB     : string;
   dist, velo     : real;
begin

(* falls imeasure = 1:
   dir Controller werden nur initialisiert mit
   Distanz und Geschwindigkeit;
   die Motoren werden aber nicht gestartet. *)

(*   software names:
   axisnr = 1 : x-axis
   axisnr = 2 : y-axis
   axisnr = 3 : z-axis
   axisnr = 4 : theta-axis

   hardware (jumpers on axis boards):
   Crate 1 (SW1) : axes 1,2 = z-axis
   Crate 1 (SW1) : axes 3,4 = x-axis
   Crate 2 (SW2) : axes 1,2 = y-axis
   Crate 2 (SW2) : axes 3,4 = theta-axis

   relabs = 1 : relative movememnt
   relabs = 2 : absolute movememnt
   relabs = 3 : relative movememnt by reference offsets

   ionetwo = 1 : Achse 1
   ionetwo = 2 : Achse 2
   ionetwo = 3 : Achse 1 + 2
   ionetwo = 4 : Achse 1 - 2

   IEEE addresses SW1, SW2 *)

   if (ifirstSTWI = 1) then
   begin
   STWIini;
   ifirstSTWI:=0;
   end;

   if (axisnr = 1) then (* x-axis *)
      begin
      SW12:=SW1;
      SETA:='SET3';
      SETB:='SET4';
      STARTA:='START3';
      STARTB:='START4';
      VELA:='/3//////';
      VELB:='/4//////';

      end;

   if (axisnr = 2) then (* y-axis *)
      begin
      SW12:=SW2;
      SETA:='SET1';
      SETB:='SET2';
      STARTA:='START1';
      STARTB:='START2';
      VELA:='/1//////';
      VELB:='/2//////';
      end;

   if (axisnr = 3) then (* z-axis *)
      begin
      SW12:=SW1;
      SETA:='SET1';
      SETB:='SET2';
      STARTA:='START1';
      STARTB:='START2';
      VELA:='/1//////';
      VELB:='/2//////';
      end;

   if (axisnr = 4) then (* theta-axis *)
      begin
      SW12:=SW2;
      SETA:='SET3';
      SETB:='SET4';
      STARTA:='START3';
      STARTB:='START4';
      VELA:='/3//////';
      VELB:='/4//////';
      end;

   velo:=velocity;
   (** first motor **)
   dist:=0.0;
   if (relabs = 3) then
     begin
     if (axisnr = 1) then
        dist:=STWIData.ref_offset.x1;
     if (axisnr = 2) then
        dist:=STWIData.ref_offset.y1;
     if (axisnr = 3) then
        dist:=STWIData.ref_offset.z1;
     if (axisnr = 4) then
        dist:=STWIData.ref_offset.theta1;
     end;

   if (relabs < 3) then
   begin
     if(ionetwo = 1) or (ionetwo = 3) or (ionetwo = 4)  then
     begin
     (*** first motor ***)
       if(axisnr =  1) or (axisnr = 3) or (axisnr =  4) then
         dist:=distance;
       if(axisnr = 2)  then
         dist:=-distance;
     end;
   end;

   get_dist_vel(axisnr, dist, velo, s1_dist, s1_velo);

   dist:=0.0;
   if (relabs = 3) then
     begin
     if (axisnr = 1) then
        dist:=STWIData.ref_offset.x2;
     if (axisnr = 2) then
        dist:=STWIData.ref_offset.y2;
     if (axisnr = 3) then
        dist:=STWIData.ref_offset.z2;
     if (axisnr = 4) then
        dist:=STWIData.ref_offset.theta2;
     end;

   if (relabs < 3) then
   begin
     if(ionetwo = 2) or (ionetwo = 3) then
     begin
     (*** second motor ***)
       if(axisnr = 3) then
         dist:=distance;
       if(axisnr = 1) or (axisnr = 2) or (axisnr = 4) then
         dist:=-distance;
     end;
     if(ionetwo = 4) then
     begin
     (*** second motor ***)
       if(axisnr = 3) then
         dist:=-distance;
       if(axisnr = 1) or (axisnr = 2) or (axisnr = 4) then
         dist:=distance;
     end;
   end;

   get_dist_vel(axisnr, dist, velo, s2_dist, s2_velo);

   (* write dist and velocity axis A *)

   if (ionetwo = 1) or (ionetwo = 3) or (ionetwo = 4) then
       wr_text:= SETA+'='+s1_dist;
   if (ionetwo = 2)then
       wr_text:= SETA+'=0';
    delay(200);
    IEEE_write(SW12,wr_text);
    delay(200);
    wr_text:=VELA+s1_velo+'/;';
    IEEE_write(SW12,wr_text);
    delay(500);

   (* write dist and velocity axis B *)

    if (ionetwo = 2) or (ionetwo = 3) or (ionetwo = 4) then
       wr_text:= SETB+'='+s2_dist;
    if (ionetwo = 1) then
       wr_text:= SETB+'=0';
    IEEE_write(SW12,wr_text);
    delay(200);
    wr_text:=VELB+s2_velo+'/;';
    IEEE_write(SW12,wr_text);
    delay(500);

    if(imeasure = 0) then
    begin
(* start axes A and B one after the other *)
    wr_text:= STARTA;
    IEEE_write(SW12,wr_text);

    delay(200);

    wr_text:= STARTB;
    IEEE_write(SW12,wr_text);

(* start axes A and B at the same time *)
(*    wr_text:= 'START';
    IEEE_write(SW12,wr_text);  *)
    end;

end;

(************* servo motor procedures *****************)
procedure ServoDrive(axisnr:integer; relabs:integer;
          i_check_xyztheta:integer; distance:real; velocity: real);
var
   relpos,linvel,relpos_a,linvel_a : longint;
   s1,s1_a,s44,s44_a,wr_string     : string;
   s4,s4_a              : string4;
   s2                   : string2;
   ii,iii               : integer;
   s0                   : char;
   i10,i10_a            : integer;
   reab                 : string2;
   velo,dista           : real;
   velo_a,dista_a       : real;
   wr_iterhos0,wr_iterhos1 : string;
   ikabel               : integer;
   i_ServoCheck         : integer;
begin

  if(ServoData.JS_c.x = 50000)then
     ikabel:=1;
  if(ServoData.JS_c.x = 2)then
     ikabel:=2;
  if(ServoData.JS_c.x = 3)then
     ikabel:=3;

  if (relabs = 1) then
     reab:='RP';
  if (relabs = 2) then
     reab:='AP';    {check}

  i10:=0;
  i10_a:=0;

  if iterhos = 0 then
  begin

  {initialize controller}
  if axisnr = 1 then       {x-axis}
     begin
     wr_string:='@W0';
     RS232_write(com1,wr_string,iterhos,0);
     dista:=distance*servodata.anorad_c.x;
     velo:=velocity*servodata.anorad_c.x;
     dista_a:=distance*servodata.anorad_c.x*servodata.anorad_c.xa;
     velo_a:=velocity*servodata.anorad_c.x*servodata.anorad_c.xa;

     if(ikabel = 1) or (ikabel = 2) then
     begin
     wr_string:='SXMO';
     s1:='';
     s1:=s1+char(0);
     s1:=s1+char(0);
     s1:=s1+char(0);
     s1:=s1+char(1);
     wr_string:=wr_string+s1;
     RS232_write(com1,wr_string,iterhos,0); (* Motor Enable      *)
     end;

     if(ikabel = 1) or (ikabel = 3) then
     begin
     wr_string:='SYMO';
     s1:='';
     s1:=s1+char(0);
     s1:=s1+char(0);
     s1:=s1+char(0);
     s1:=s1+char(1);
     wr_string:=wr_string+s1;
     RS232_write(com1,wr_string,iterhos,0); (* Motor Enable       *)
     end;

     if(ikabel = 1) or (ikabel = 2) then
     begin
     wr_string:='SXMM';
     s1:='';
     s1:=s1+char(0);
     s1:=s1+char(0);
     s1:=s1+char(0);
     s1:=s1+char(0);
     wr_string:=wr_string+s1;
     RS232_write(com1,wr_string,iterhos,0); (* Linear Point to POint Mode *)
     end;

     if(ikabel = 1) or (ikabel = 3) then
     begin
     wr_string:='SYMM';
     s1:='';
     s1:=s1+char(0);
     s1:=s1+char(0);
     s1:=s1+char(0);
     s1:=s1+char(0);
     wr_string:=wr_string+s1;
     RS232_write(com1,wr_string,iterhos,0); (* Linear Point to POint Mode *)
     end;

     end;

  if axisnr = 2 then       {y-axis}
     begin
     wr_string:='@W1';
     RS232_write(com1,wr_string,iterhos,0);
     dista:=distance*servodata.anorad_c.y;
     velo:=velocity*servodata.anorad_c.y;
     wr_string:='SXMO';
     s1:='';
     s1:=s1+char(0);
     s1:=s1+char(0);
     s1:=s1+char(0);
     s1:=s1+char(1);
     wr_string:=wr_string+s1;
     RS232_write(com1,wr_string,iterhos,0); (* Motor Enable               *)
     wr_string:='SXMM';
     s1:='';
     s1:=s1+char(0);
     s1:=s1+char(0);
     s1:=s1+char(0);
     s1:=s1+char(0);
     wr_string:=wr_string+s1;
     RS232_write(com1,wr_string,iterhos,0); (* Linear Point to POint Mode *)
     end;
  if axisnr = 3 then       {z-axis}
     begin
     wr_string:='@W1';
     RS232_write(com1,wr_string,iterhos,0);
     dista:=distance*servodata.anorad_c.z;
     velo:=velocity*servodata.anorad_c.z;
     wr_string:='SYMO';
     s1:='';
     s1:=s1+char(0);
     s1:=s1+char(0);
     s1:=s1+char(0);
     s1:=s1+char(1);
     wr_string:=wr_string+s1;
     RS232_write(com1,wr_string,iterhos,0); (* Motor Enable               *)
     wr_string:='SYMM';
     s1:='';
     s1:=s1+char(0);
     s1:=s1+char(0);
     s1:=s1+char(0);
     s1:=s1+char(0);
     wr_string:=wr_string+s1;
     RS232_write(com1,wr_string,iterhos,0); (* Linear Point to POint Mode *)
     end;
  if axisnr = 4 then       {theta}
     begin
     wr_string:='@W2';
     RS232_write(com1,wr_string,iterhos,0);
     dista:=distance*servodata.anorad_c.theta;
     velo:=velocity*servodata.anorad_c.theta;
     wr_string:='SYMO';
     s1:='';
     s1:=s1+char(0);
     s1:=s1+char(0);
     s1:=s1+char(0);
     s1:=s1+char(1);
     wr_string:=wr_string+s1;
     RS232_write(com1,wr_string,iterhos,0); (* Motor Enable               *)
     wr_string:='SYMM';
     s1:='';
     s1:=s1+char(0);
     s1:=s1+char(0);
     s1:=s1+char(0);
     s1:=s1+char(0);
     wr_string:=wr_string+s1;
     RS232_write(com1,wr_string,iterhos,0); (* Linear Point to POint Mode *)
     end;

  end;

  if iterhos = 1 then
  begin
  {initialize controller}
  if axisnr = 1 then       {x-axis}
     begin
     wr_string:='@W0';
     RS232_write(com1,wr_string,iterhos,0);
     dista:=distance*servodata.anorad_c.x;
     velo:=velocity*servodata.anorad_c.x;
     dista_a:=distance*servodata.anorad_c.x*servodata.anorad_c.xa;
     velo_a:=velocity*servodata.anorad_c.x*servodata.anorad_c.xa;
     if(ikabel = 1) or (ikabel = 2) then
     begin
     wr_string:='SXMO1';
     RS232_write(com1,wr_string,iterhos,0); (* Motor Enable               *)
     end;
     if(ikabel = 1) or (ikabel = 3) then
     begin
     wr_string:='SYMO1';
     RS232_write(com1,wr_string,iterhos,0); (* Motor Enable               *)
     end;
     if(ikabel = 1) or (ikabel = 2) then
     begin
     wr_string:='SXMM0';
     RS232_write(com1,wr_string,iterhos,0); (* Linear Point to POint Mode *)
     end;
     if(ikabel = 1) or (ikabel = 3) then
     begin
     wr_string:='SYMM0';
     RS232_write(com1,wr_string,iterhos,0); (* Linear Point to POint Mode *)
     end;

     end;

  if axisnr = 2 then       {y-axis}
     begin
     wr_string:='@W1';
     RS232_write(com1,wr_string,iterhos,0);
     dista:=distance*servodata.anorad_c.y;
     velo:=velocity*servodata.anorad_c.y;
     wr_string:='SXMO1';
     RS232_write(com1,wr_string,iterhos,0); (* Motor Enable               *)
     wr_string:='SXMM0';
     RS232_write(com1,wr_string,iterhos,0); (* Linear Point to POint Mode *)
     end;
  if axisnr = 3 then       {z-axis}
     begin
     wr_string:='@W1';
     RS232_write(com1,wr_string,iterhos,0);
     dista:=distance*servodata.anorad_c.z;
     velo:=velocity*servodata.anorad_c.z;
     wr_string:='SYMO1';
     RS232_write(com1,wr_string,iterhos,0); (* Motor Enable               *)
     wr_string:='SYMM0';
     RS232_write(com1,wr_string,iterhos,0); (* Linear Point to POint Mode *)
     end;
  if axisnr = 4 then       {theta}
     begin
     wr_string:='@W2';
     RS232_write(com1,wr_string,iterhos,0);
     dista:=distance*servodata.anorad_c.theta;
     velo:=velocity*servodata.anorad_c.theta;
     wr_string:='SYMO1';
     RS232_write(com1,wr_string,iterhos,0); (* Motor Enable               *)
     wr_string:='SYMM0';
     RS232_write(com1,wr_string,iterhos,0); (* Linear Point to POint Mode *)
     end;

  end;

  if velo < 0 then velo:=-velo;
  if velo_a < 0 then velo_a:=-velo_a;

  relpos:=round(dista);
  if axisnr = 1 then
      relpos_a:=round(dista_a);

  if iterhos = 0 then
     begin
     s4:=longint_s4(relpos);
     s4_a:=longint_s4(relpos_a);
(*     s2:=s4[1]+s4[2];
     writeln(ASCII_HEX_TO_BIN (s2));
     s2:=s4[3]+s4[4];
     writeln(ASCII_HEX_TO_BIN (s2));  *)

     s44:='';
     for ii:=1 to length(s4) do
     begin
     if byte_hex(ord(s4[ii])) = '10' then
        begin
        i10:=i10+1;
        s44:=s44+char(ASCII_HEX_TO_BIN('10'));
        end;
     if byte_hex(ord(s4[ii])) = '0D' then
        begin
        i10:=i10+1;
        s44:=s44+char(ASCII_HEX_TO_BIN('10'));
        end;
     s44:=s44+s4[ii];
     end;

     s44_a:='';
     for ii:=1 to length(s4_a) do
     begin
     if byte_hex(ord(s4_a[ii])) = '10' then
        begin
        i10_a:=i10_a+1;
        s44_a:=s44_a+char(ASCII_HEX_TO_BIN('10'));
        end;
     if byte_hex(ord(s4_a[ii])) = '0D' then
        begin
        i10_a:=i10_a+1;
        s44_a:=s44_a+char(ASCII_HEX_TO_BIN('10'));
        end;
     s44_a:=s44_a+s4_a[ii];
     end;

  if (axisnr = 1) then
     begin
     wr_string:='@W0';
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='SX'+reab+s44;
     RS232_write(com1,wr_string,iterhos,i10); (* Set Relative Position      *)
     wr_string:='SY'+reab+s44_a;
     RS232_write(com1,wr_string,iterhos,i10_a); (* Set Relative Position      *)
     end;
  if (axisnr = 2) then
     begin
     wr_string:='@W1';
     RS232_write(com1,wr_string,iterhos,i10);
     wr_string:='SX'+reab+s44;
     RS232_write(com1,wr_string,iterhos,i10); (* Set Relative Position      *)
     end;
  if (axisnr = 3) then
     begin
     wr_string:='@W1';
     RS232_write(com1,wr_string,iterhos,i10);
     wr_string:='SY'+reab+s44;
     RS232_write(com1,wr_string,iterhos,i10); (* Set Relative Position      *)
     end;
  if (axisnr = 4) then
     begin
     wr_string:='@W2';
     RS232_write(com1,wr_string,iterhos,i10);
     wr_string:='SY'+reab+s44;
     RS232_write(com1,wr_string,iterhos,i10); (* Set Relative Position      *)
     end;

{     iii:=length(s44);
     for ii:=1 to iii do
     begin
     write(byte_hex(ord(s44[ii])));
     end;
     readkey;}

     i10:=0;
     i10_a:=0;

     end;

  if iterhos = 1 then
     begin
     str(relpos,s1);
     str(relpos_a,s1_a);

  if (axisnr = 1) then
     begin
     wr_string:='@W0';
     RS232_write(com1,wr_string,iterhos,0); (* Set Relative Position      *)
     wr_string:='SX'+reab+s1;
     RS232_write(com1,wr_string,iterhos,0); (* Set Relative Position      *)
     wr_string:='SY'+reab+s1_a;
     RS232_write(com1,wr_string,iterhos,0); (* Set Relative Position      *)
     end;
  if (axisnr = 2) then
     begin
     wr_string:='@W1';
     RS232_write(com1,wr_string,iterhos,0); (* Set Relative Position      *)
     wr_string:='SX'+reab+s1;
     RS232_write(com1,wr_string,iterhos,0); (* Set Relative Position      *)
     end;
  if (axisnr = 3) then
     begin
     wr_string:='@W1';
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='SY'+reab+s1;
     RS232_write(com1,wr_string,iterhos,0); (* Set Relative Position      *)
     end;
  if (axisnr = 4) then
     begin
     wr_string:='@W2';
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='SY'+reab+s1;
     RS232_write(com1,wr_string,iterhos,0); (* Set Relative Position      *)
     end;

  end;

  linvel:=round(velo);
  if axisnr = 1 then
      linvel_a:=round(velo_a);

  if iterhos = 0 then
     begin
     s4:=longint_s4(linvel);
     s4_a:=longint_s4(linvel_a);
  (*   s2:=s4[1]+s4[2];
     writeln(ASCII_HEX_TO_BIN (s2));
     s2:=s4[3]+s4[4];
     writeln(ASCII_HEX_TO_BIN (s2));  *)
     s44:='';
     for ii:=1 to length(s4) do
     begin
     if byte_hex(ord(s4[ii])) = '10' then
        begin
        i10:=i10+1;
        s44:=s44+char(ASCII_HEX_TO_BIN('10'));
        end;
     if byte_hex(ord(s4[ii])) = '0D' then
        begin
        i10:=i10+1;
        s44:=s44+char(ASCII_HEX_TO_BIN('10'));
        end;
     s44:=s44+s4[ii];
     end;

     s44_a:='';
     for ii:=1 to length(s4_a) do
     begin
     if byte_hex(ord(s4_a[ii])) = '10' then
        begin
        i10_a:=i10_a+1;
        s44_a:=s44_a+char(ASCII_HEX_TO_BIN('10'));
        end;
     if byte_hex(ord(s4_a[ii])) = '0D' then
        begin
        i10_a:=i10_a+1;
        s44_a:=s44_a+char(ASCII_HEX_TO_BIN('10'));
        end;
     s44_a:=s44_a+s4_a[ii];
     end;

  if (axisnr = 1) then
     begin
     wr_string:='@W0';
     RS232_write(com1,wr_string,iterhos,0); (* Set Linear Velocity     *)
     if(ikabel = 1) or (ikabel = 2) then
     begin
     wr_string:='SXLV'+s44;
     RS232_write(com1,wr_string,iterhos,i10); (* Set Linear Velocity     *)
     end;
     if(ikabel = 1) or (ikabel = 3) then
     begin
     wr_string:='SYLV'+s44_a;
     RS232_write(com1,wr_string,iterhos,i10_a); (* Set Linear Velocity     *)
     end;
     wr_iterhos0:='SVV'+s44;
     end;
  if (axisnr = 2) then
     begin
     wr_string:='@W1';
     RS232_write(com1,wr_string,iterhos,i10); (* Set Linear Velocity     *)
     wr_string:='SXLV'+s44;
     RS232_write(com1,wr_string,iterhos,i10); (* Set Linear Velocity     *)
     end;
  if (axisnr = 3) then
     begin
     wr_string:='@W1';
     RS232_write(com1,wr_string,iterhos,i10); (* Set Linear Velocity     *)
     wr_string:='SYLV'+s44;
     RS232_write(com1,wr_string,iterhos,i10); (* Set Linear Velocity     *)
     end;
  if (axisnr = 4) then
     begin
     wr_string:='@W2';
     RS232_write(com1,wr_string,iterhos,i10); (* Set Linear Velocity     *)
     wr_string:='SYLV'+s44;
     RS232_write(com1,wr_string,iterhos,i10); (* Set Linear Velocity     *)
     end;

     end;

  if iterhos = 1 then
     begin

  if axisnr = 1 then
     begin
     wr_string:='@W0';
     RS232_write(com1,wr_string,iterhos,0); (* Set Linear Velocity     *)
     str(linvel,s1);
     str(linvel_a,s1_a);
     if(ikabel = 1) or (ikabel = 2) then
     begin
     wr_string:='SXLV'+s1;
     RS232_write(com1,wr_string,iterhos,0); (* Set Linear Velocity     *)
     end;
     if(ikabel = 1) or (ikabel = 3) then
     begin
     wr_string:='SYLV'+s1_a;
     RS232_write(com1,wr_string,iterhos,0); (* Set Linear Velocity     *)
     end;
     wr_iterhos1:='SVV'+s1;
     end;
  if (axisnr = 2) then
     begin
     wr_string:='@W1';
     RS232_write(com1,wr_string,iterhos,0); (* Set Linear Velocity     *)
     str(linvel,s1);
     wr_string:='SXLV'+s1;
     RS232_write(com1,wr_string,iterhos,0); (* Set Linear Velocity     *)
     end;
  if (axisnr = 3) then
     begin
     wr_string:='@W1';
     RS232_write(com1,wr_string,iterhos,0); (* Set Linear Velocity     *)
     str(linvel,s1);
     wr_string:='SYLV'+s1;
     RS232_write(com1,wr_string,iterhos,0); (* Set Linear Velocity     *)
     end;
  if (axisnr = 4) then
     begin
     wr_string:='@W2';
     RS232_write(com1,wr_string,iterhos,0); (* Set Linear Velocity     *)
     str(linvel,s1);
     wr_string:='SYLV'+s1;
     RS232_write(com1,wr_string,iterhos,0); (* Set Linear Velocity     *)
     end;

     end;

  if (axisnr = 1) then
     begin
     wr_string:='@W0';
     RS232_write(com1,wr_string,iterhos,0); (* Set Linear Velocity     *)
     if(ikabel = 1) then
     begin
     if iterhos = 0 then
        wr_string:=wr_iterhos0;
     if iterhos = 1 then
        wr_string:=wr_iterhos1;
     RS232_write(com1,wr_string,iterhos,0); (* Set Vector Velocity     *)
     wr_string:='BC'; {Parallelfahrt mit Kabelschlepp}
     end;
     if ikabel = 2 then
          wr_string:='BX'; {Fahrt ohne Kabelschlepp}
     if ikabel = 3 then
          wr_string:='BY'; {Fahrt ohne Kabelschlepp}
     RS232_write(com1,wr_string,iterhos,0); (* Start Motion *)
     end;
  if (axisnr = 2) then
     begin
     wr_string:='@W1';
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='BX';
     RS232_write(com1,wr_string,iterhos,0); (* Start Motion *)
     end;
  if (axisnr = 3) then
     begin
     wr_string:='@W1';
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='BY';
     RS232_write(com1,wr_string,iterhos,0); (* Start Motion *)
     end;
  if (axisnr = 4) then
     begin
     wr_string:='@W2';
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='BY';
     RS232_write(com1,wr_string,iterhos,0); (* Start Motion *)
     end;

     if (axisnr = 1) then
        ServoData.Act_Position.x:=ServoData.Act_Position.x+distance;
     if (axisnr = 2) then
        ServoData.Act_Position.y:=ServoData.Act_Position.y+distance;
     if (axisnr = 3) then
        ServoData.Act_Position.z:=ServoData.Act_Position.z+distance;
     if (axisnr = 4) then
        ServoData.Act_Position.theta:=ServoData.Act_Position.theta+distance;

RS232_READ(com1,0);

if (axisnr > 1) and (i_check_xyztheta=1) then
   if(abs(distance) > 0.5) then  (* sonst Timing Probleme;
                                 das kann man noch mal
                                 sauber programmieren, oder
                                 die Schwelle ins Menu aufnehmen *)
       begin
       delay(100);
       ServoCheck(axisnr,i_ServoCheck);
       end;

end;

(********************************************************)
  procedure JoyStick( onoff : integer);
  var
     linvel                                : longint;
     linvel1, linvel2, linvel3, linvel4, linvel1a    : longint;
     s44, wr_string                        : string;
     enable_x,enable_y                     : string;
  begin
  if onoff = 1 then
     begin
     ServoData.i_JS:=1;

     wr_string:='@W0';
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='SXMM21'; {JoyStick ein}
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='SXMO1'; {JoyStick ein}
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='SYMM21'; {JoyStick ein}
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='SYMO1'; {JoyStick ein}
     RS232_write(com1,wr_string,iterhos,0);

     wr_string:='@W1';
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='SXMO1'; {JoyStick ein}
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='SXMM21'; {JoyStick ein}
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='SYMO1'; {JoyStick ein}
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='SYMM21'; {JoyStick ein}
     RS232_write(com1,wr_string,iterhos,0);

     wr_string:='@W2';
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='SXMO1'; {JoyStick ein}
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='SXMM21'; {JoyStick ein}
     RS232_write(com1,wr_string,iterhos,0);

     linvel1:=round(ServoData.JS_velocity.x*ServoData.JS_c.x);
     linvel1a:=round(ServoData.JS_velocity.x*
               ServoData.JS_c.x*ServoData.Anorad_c.xa);
     linvel2:=round(ServoData.JS_velocity.y*ServoData.JS_c.y);
     linvel3:=round(ServoData.JS_velocity.z*ServoData.JS_c.z);
     linvel4:=round(ServoData.JS_velocity.theta*ServoData.JS_c.theta);

     wr_string:='@W0';
     RS232_write(com1,wr_string,iterhos,0);
     numstring(iterhos,linvel1,s44);
     wr_string:='SXLV'+s44;
     RS232_write(com1,wr_string,iterhos,0);
     numstring(iterhos,linvel1a,s44);
     wr_string:='SYLV'+s44;
     RS232_write(com1,wr_string,iterhos,0);

     wr_string:='@W1';
     RS232_write(com1,wr_string,iterhos,0);
     numstring(iterhos,linvel2,s44);
     wr_string:='SXLV'+s44;
     RS232_write(com1,wr_string,iterhos,0);
     numstring(iterhos,linvel3,s44);
     wr_string:='SYLV'+s44;
     RS232_write(com1,wr_string,iterhos,0);

     wr_string:='@W2';
     RS232_write(com1,wr_string,iterhos,0);
     numstring(iterhos,linvel4,s44);
     wr_string:='SXLV'+s44;
     RS232_write(com1,wr_string,iterhos,0);

     wr_string:='@W0';
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='BX';
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='BY';
     RS232_write(com1,wr_string,iterhos,0);

     wr_string:='@W1';
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='BX';
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='BY';
     RS232_write(com1,wr_string,iterhos,0);

     wr_string:='@W2';
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='BX';
     RS232_write(com1,wr_string,iterhos,0);

     end;

  if onoff = 0 then
     begin
     ServoData.i_JS:=0;
     wr_string:='@W0';
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='EX';
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='EY';
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='@W1';
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='EX';
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='EY';
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='@W2';
     RS232_write(com1,wr_string,iterhos,0);
     wr_string:='EX';
     RS232_write(com1,wr_string,iterhos,0);
     end;

     wr_string:='@W0';
     RS232_write(com1,wr_string,iterhos,0);
     RS232_read(com1,0);

  end;

(**********************************************************************)
  procedure numstring(iterhos : integer; linvel : longint; var s44: string);

  var
     s4         : string4;
     ii,i10     : integer;

  begin

     if iterhos = 0 then
     begin
     s4:=longint_s4(linvel);
     s44:='';
     for ii:=1 to length(s4) do
     begin
     if byte_hex(ord(s4[ii])) = '10' then
        begin
        i10:=1;
        s44:=s44+char(ASCII_HEX_TO_BIN('10'));
        end;
     if byte_hex(ord(s4[ii])) = '0D' then
        begin
        i10:=1;
        s44:=s44+char(ASCII_HEX_TO_BIN('10'));
        end;
     s44:=s44+s4[ii];
     end;

  end;

  if iterhos = 1 then
     begin
     str(linvel,s44);
     end;

end;

(********************************************************)
procedure ServoIni;
begin

   (* controller has been switched on and no commands
      have been sent so far; then, it should work urgh! *)

   if ifirstrs232 = 1 then
      begin
      RS232_open(com1);
      ifirstrs232:=0;
      delay(500);
      end;
   if iterhos = 0 then    (* host mode *)
      begin
(*      RS232_write(com1,'SIC7',0,0); *)(* Initiate Communication *)
(*      delay(500);  *)
      RS232_write(com1,'SHT0',1,0);   (* Communication via Host *)
      RS232_write(com1,'SHT1',1,0);
      RS232_write(com1,'SHT0',1,0);
      end;

   if iterhos = 1 then    (* terminal mode *)
      begin
(*      RS232_write(com1,'SIC7',1,0); *)(* Initiate Communication *)
(*      delay(500); *)
      RS232_write(com1,'SHT1',1,0);   (* Communication via Terminal *)
      end;

   SetServoParameters_new;
   ifirstservo:=0;

   RS232_read(com1, 0);

end;

(****** set parameters for Servo Controller *************)
procedure SetServoParameters;
var

   wr_string    : string;
begin
(* Controller 1, x-axis *)
      wr_string:='@W0';
      RS232_write(com1,wr_string,iterhos,0);

      wr_string:='SIC3';
      RS232_write(com1,wr_string,iterhos,0);

      wr_string:='SIL28';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SXEA32000';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SXER32000';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SXGA20';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SXGF-3';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SXLV1500000'; (*max 1500000 *)
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SXLA100000'; (*max 1500000 *)
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SXZE225';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SXKZ27';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SXPO0';
      RS232_write(com1,wr_string,iterhos,0);

(* Controller 1, y-axis *)
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYEA32000';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYER32000';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYGA10';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYGF0';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYLV15000';  (* max 15000 *)
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYLA15000';  (* max 15000 *)
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYZE237';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYKZ1';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYPO0';
      RS232_write(com1,wr_string,iterhos,0);

(* Controller 1, x-, y- Parallelfahrt *)
      wr_string:='SVA100000';
      RS232_write(com1,wr_string,iterhos,0);

(* Controller 2, x-axis *)
      wr_string:='@W1';
      RS232_write(com1,wr_string,iterhos,0);

      wr_string:='SIC3';
      RS232_write(com1,wr_string,iterhos,0);

      wr_string:='SIL31';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SXEA32000';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SXER32000';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SXGA80';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SXGF-1';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SXLV1000';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SXLA1000';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SXZE234';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SXKZ27';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SXPO0';
      RS232_write(com1,wr_string,iterhos,0);

(* Controller 2, y-axis *)
      wr_string:='@W1';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYEA32000';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYER32000';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYGA80';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYGF-2';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYLV1000';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYLA1000';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYZE249';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYKZ27';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYPO0';
      RS232_write(com1,wr_string,iterhos,0);

(* Controller 3, y-axis *)
      wr_string:='@W2';
      RS232_write(com1,wr_string,iterhos,0);

      wr_string:='SIC3';
      RS232_write(com1,wr_string,iterhos,0);

      wr_string:='SIL28';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYEA32000';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYER32000';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYGA10';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYGF-2';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYLV1000';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYLA1000';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYZE246';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYKZ27';
      RS232_write(com1,wr_string,iterhos,0);
      wr_string:='SYPO0';
      RS232_write(com1,wr_string,iterhos,0);

      wr_string:='@W0';
      RS232_write(com1,wr_string,iterhos,0);

end;

(****** set parameters for Servo Controller *************)
procedure SetServoParameters_new;
var
   wr_string    : string;
   s1           : string;
begin
(* Controller 1, x-axis *)
      wr_string:='@W0';
      RS232_write(com1,wr_string,iterhos,0);

      wr_string:='SIC3';
      RS232_write(com1,wr_string,iterhos,0);

      str(ServoParData.c0.SIL,s1);
      wr_string:='SIL'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c0.SXEA,s1);
      wr_string:='SXEA'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c0.SXER,s1);
      wr_string:='SXER'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c0.SXGA,s1);
      wr_string:='SXGA'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c0.SXGF,s1);
      wr_string:='SXGF'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c0.SXLV,s1);
      wr_string:='SXLV'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c0.SXLA,s1);
      wr_string:='SXLA'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c0.SXZE,s1);
      wr_string:='SXZE'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c0.SXKZ,s1);
      wr_string:='SXKZ'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c0.SXPO,s1);
      wr_string:='SXPO'+s1;
      RS232_write(com1,wr_string,iterhos,0);

(* Controller 1, y-axis *)
      str(ServoParData.c0.SYEA,s1);
      wr_string:='SYEA'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c0.SYER,s1);
      wr_string:='SYER'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c0.SYGA,s1);
      wr_string:='SYGA'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c0.SYGF,s1);
      wr_string:='SYGF'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c0.SYLV,s1);
      wr_string:='SYLV'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c0.SYLA,s1);
      wr_string:='SYLA'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c0.SYZE,s1);
      wr_string:='SYZE'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c0.SYKZ,s1);
      wr_string:='SYKZ'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c0.SYPO,s1);
      wr_string:='SYPO'+s1;
      RS232_write(com1,wr_string,iterhos,0);

(* Controller 1, x-, y- Parallelfahrt *)
      str(ServoParData.c0.SVA,s1);
      wr_string:='SVA'+s1;
      RS232_write(com1,wr_string,iterhos,0);

(* Controller 2, x-axis *)
      wr_string:='@W1';
      RS232_write(com1,wr_string,iterhos,0);

      wr_string:='SIC3';
      RS232_write(com1,wr_string,iterhos,0);

      str(ServoParData.c1.SIL,s1);
      wr_string:='SIL'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c1.SXEA,s1);
      wr_string:='SXEA'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c1.SXER,s1);
      wr_string:='SXER'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c1.SXGA,s1);
      wr_string:='SXGA'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c1.SXGF,s1);
      wr_string:='SXGF'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c1.SXLV,s1);
      wr_string:='SXLV'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c1.SXLA,s1);
      wr_string:='SXLA'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c1.SXZE,s1);
      wr_string:='SXZE'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c1.SXKZ,s1);
      wr_string:='SXKZ'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c1.SXPO,s1);
      wr_string:='SXPO'+s1;
      RS232_write(com1,wr_string,iterhos,0);

(* Controller 2, y-axis *)
      str(ServoParData.c1.SYEA,s1);
      wr_string:='SYEA'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c1.SYER,s1);
      wr_string:='SYER'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c1.SYGA,s1);
      wr_string:='SYGA'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c1.SYGF,s1);
      wr_string:='SYGF'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c1.SYLV,s1);
      wr_string:='SYLV'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c1.SYLA,s1);
      wr_string:='SYLA'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c1.SYZE,s1);
      wr_string:='SYZE'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c1.SYKZ,s1);
      wr_string:='SYKZ'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c1.SYPO,s1);
      wr_string:='SYPO'+s1;
      RS232_write(com1,wr_string,iterhos,0);

(* Controller 3, x-axis *)
      wr_string:='@W2';
      RS232_write(com1,wr_string,iterhos,0);

      wr_string:='SIC3';
      RS232_write(com1,wr_string,iterhos,0);

      str(ServoParData.c2.SIL,s1);
      wr_string:='SIL'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c2.SXEA,s1);
      wr_string:='SXEA'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c2.SXER,s1);
      wr_string:='SXER'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c2.SXGA,s1);
      wr_string:='SXGA'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c2.SXGF,s1);
      wr_string:='SXGF'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c2.SXLV,s1);
      wr_string:='SXLV'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c2.SXLA,s1);
      wr_string:='SXLA'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c2.SXZE,s1);
      wr_string:='SXZE'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c2.SXKZ,s1);
      wr_string:='SXKZ'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c2.SXPO,s1);
      wr_string:='SXPO'+s1;
      RS232_write(com1,wr_string,iterhos,0);

(* Controller 3, y-axis *)
      str(ServoParData.c2.SYEA,s1);
      wr_string:='SYEA'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c2.SYER,s1);
      wr_string:='SYER'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c2.SYGA,s1);
      wr_string:='SYGA'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c2.SYGF,s1);
      wr_string:='SYGF'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c2.SYLV,s1);
      wr_string:='SYLV'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c2.SYLA,s1);
      wr_string:='SYLA'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c2.SYZE,s1);
      wr_string:='SYZE'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c2.SYKZ,s1);
      wr_string:='SYKZ'+s1;
      RS232_write(com1,wr_string,iterhos,0);
      str(ServoParData.c2.SYPO,s1);
      wr_string:='SYPO'+s1;
      RS232_write(com1,wr_string,iterhos,0);

      wr_string:='@W0';
      RS232_write(com1,wr_string,iterhos,0);

end;


(********************************************************)
procedure servo_read_par;
begin
(****** read parameters of Servo Controller *************)
  RS232_write(com1,'RXGA',1,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  delay(2000);
  RS232_write(com1,'RXPO',1,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXZE',1,0);
  delay(100);
  RS232_read(com1,1);
  write(rd_text);
  RS232_write(com1,'RXGF',1,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXSF',1,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXTR',1,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXER',1,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXEA',1,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXTO',1,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXTL',1,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXFN',1,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXVF',1,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXAF',1,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXKZ',1,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXLV',1,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXLA',1,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXKD',1,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RVV',1,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RVA',1,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RVS',1,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RIL',1,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
end;

(********************************************************)
procedure read_x_axis_par;
begin
(****** read parameters of Servo Controller *************)
  RS232_write(com1,'RXGA',0,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  delay(2000);
  RS232_write(com1,'RXPO',0,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXZE',0,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXGF',0,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXSF',0,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXTR',0,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXER',0,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXEA',0,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXTO',0,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXTL',0,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXFN',0,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXVF',0,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXAF',0,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXKZ',0,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXLV',0,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXLA',0,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RXKD',0,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RVV',0,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RVA',0,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RVS',0,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
  RS232_write(com1,'RIL',0,0);
  delay(100);
  RS232_read(com1,0);
  write(rd_text);
end;

(************* Parameters for STWI Controller *********)
function STWI_ask_motion : integer;
var
   k,i  : integer;
begin
   delay(200);
   wr_text:='CLBUF';
   IEEE_write(SW1,wr_text);
   delay(200);
   IEEE_write(SW2,wr_text);
   delay(200);
   wr_text:='?M';
   IEEE_write(SW1,wr_text);
   delay(200);
   IEEE_write(SW2,wr_text);
   delay(200);

   k:=0;
   ibrd(SW1,rd,255);
   delay(200);
   for i:=1 to 4 do
     if (rd[i]<>'0') then
        k:=1;

   ibrd(SW2,rd,255);
   delay(200);
   for i:=1 to 4 do
     if (rd[i]<>'0') then
        k:=1;

   STWI_ask_motion:=k;
end;


procedure SetSTWIParameters(conax : integer);
var
   i,j,k,n,k1,k2 : integer;
   st1       : string;
   SW12      : integer;
   icont,iax : integer;
begin
     delay(200);
     if (conax = 0) then  (* test *)
        begin
        icont:=1;
        iax:=4;
        end;
     if (conax = 1) then  (* all controllers and axes *)
        begin
        icont:=2;
        iax:=4;
        end;

     for i:=1 to icont do     (* loop over controllers *)
     begin
     if (i=1) then
       SW12:=SW1;
     if (i=2) then
       SW12:=SW2;
     for j:=1 to iax do     (* loop over axes *)
       begin
       str(j,st1);
       wr_text:='/'+st1+'/';
       str(STWIData.Par.Modus[i,j],st1);
       wr_text:=wr_text+st1+'/';
       str(STWIData.Par.Ist[i,j],st1);
       wr_text:=wr_text+st1+'/';
       str(STWIData.Par.Soll[i,j],st1);
       wr_text:=wr_text+st1+'/';
       str(STWIData.Par.AC[i,j],st1);
       wr_text:=wr_text+st1+'/';
       str(STWIData.Par.VL[i,j],st1);
       wr_text:=wr_text+st1+'/';
       str(STWIData.Par.VH[i,j],st1);
       wr_text:=wr_text+st1+'/';
       str(STWIData.Par.Time[i,j],st1);
       wr_text:=wr_text+st1+'/';
       str(STWIData.Par.KD[i,j],st1);
       wr_text:=wr_text+st1+'/';
       str(STWIData.Par.KP[i,j],st1);
       wr_text:=wr_text+st1+'/';
       str(STWIData.Par.KI[i,j],st1);
       wr_text:=wr_text+st1+'/';
       str(STWIData.Par.IL[i,j],st1);
       wr_text:=wr_text+st1+'/';
       str(STWIData.Par.F_VL[i,j],st1);
       wr_text:=wr_text+st1+'/';
       str(STWIData.Par.F_VH[i,j],st1);
       wr_text:=wr_text+st1+'/';
       str(STWIData.Par.L_VL[i,j],st1);
       wr_text:=wr_text+st1+'/';
       str(STWIData.Par.L_VH[i,j],st1);
       wr_text:=wr_text+st1+'/;';
(*       for k:=1 to 20 do
          write(sendstr[k]);
       readkey;
       writeln;
       for k:=21 to 40 do
          write(sendstr[k]);
       readkey;
       writeln;
       for k:=41 to 60 do
          write(sendstr[k]);
       write('!');
       readkey;
       writeln;
       IEEE_write_slow(SW12,wr_text)  *)
       IEEE_write(SW12,wr_text);
       delay(500);
   end;

   wr_text:='AXIS=4';
       IEEE_write(SW12,wr_text);
       delay(200);
   wr_text:='RESET';
       IEEE_write(SW12,wr_text);
       delay(200);
   end;
end;

procedure IEEE_write_slow(var SW12: integer; wr_text: string);
var
   k, k1, k2, kk, n, imax : integer;
begin
       imax:=2;
       n:=length(wr_text);
       k1:=1;
       if (n>imax)  then
         k2:=imax
       else
         k2:=n;
       repeat
       for k := k1 to k2 do
          wrt[k-k1+1] := wr_text[k];
          kk:=k2-k1+1;
       ibwrt(SW12,wrt,kk);
       n:=n-imax;
(*       for k:=1 to kk do
         write(wrt[k]);
       writeln(' ',k1,' ',k2,' ',kk,' ',n);
       readkey;     *)
       k1:=k1+imax;
       if (n>imax)  then
         k2:=k2+imax
       else
         k2:=k2+n;
       delay(50);
       until (n<0) or (n=0);
end;

procedure ReadSTWIParameters;
var
   i,j,k,ii,ii1 : integer;
   val_loc      : longint;
   st_loc,st1   : string;
   par_loc      : array[1..16] of longint;
   SW12         : integer;
begin
     delay(200);
     for i:=1 to 2 do     (* loop over controllers *)
     begin
     if (i=1) then
       SW12:=SW1;
     if (i=2) then
          SW12:=SW2;
          wr_text:='CLBUF';
          IEEE_write(SW12,wr_text);
          delay(200);
          for j:=1 to 4 do    (* loop over axes *)
          begin
          str(j,st1);
          wr_text:='?P'+st1;
          IEEE_write(SW12,wr_text);
          delay(200);
          ibrd(SW12,rd,255);
          delay(200);

(*          write('  ');
          for k:=1 to 20 do
            write(rd[k]);
          readkey;
          writeln;
          for k:=21 to 40 do
            write(rd[k]);
          readkey;
          writeln;
          for k:=41 to 60 do
            write(rd[k]);
          write('*');
          readkey;
          writeln;     *)

          (* aufdr�seln: *)

          ii:=1;
          for k:=1 to 16 do
          begin
          st_loc:='';
          ii1:=0;
          repeat
          ii:=ii+1;
          if (rd[ii] <> '/') then
             begin
             ii1:=ii1+1;
             st_loc:=st_loc+rd[ii];
             end;
          until rd[ii] = '/';
          val(st_loc,val_loc,ii1);
          par_loc[k]:=val_loc;
          end;       (* k *)

          STWIData.Par.Modus[i,j]:=par_loc[2];
          STWIData.Par.Ist[i,j]  :=par_loc[3];
          STWIData.Par.Soll[i,j] :=par_loc[4];
          STWIData.Par.AC[i,j]   :=par_loc[5];
          STWIData.Par.VL[i,j]   :=par_loc[6];
          STWIData.Par.VH[i,j]   :=par_loc[7];
          STWIData.Par.Time[i,j] :=par_loc[8];
          STWIData.Par.KD[i,j]   :=par_loc[9];
          STWIData.Par.KP[i,j]   :=par_loc[10];
          STWIData.Par.KI[i,j]   :=par_loc[11];
          STWIData.Par.IL[i,j]   :=par_loc[12];
          STWIData.Par.F_VL[i,j] :=par_loc[13];
          STWIData.Par.F_VH[i,j] :=par_loc[14];
          STWIData.Par.L_VL[i,j] :=par_loc[15];
          STWIData.Par.L_VH[i,j] :=par_loc[16];

          end;                  (* j *)
     end;                       (* i *)
     filename:='STWI.PAR';
     openoutfile(0,0);
     for i:=1 to 2 do
     begin
     for j:=1 to 4 do
     begin
     writeln(outfile,'  ');
     writeln(outfile,' Controller ',i,' Axis ',j);
     writeln(outfile,STWIData.Par.Modus[i,j],'  ',STWIData.Par.Ist[i,j],'  ',
              STWIData.Par.Soll[i,j]);
     writeln(outfile,STWIData.Par.AC[i,j],'  ',STWIData.Par.VL[i,j],'  ',
              STWIData.Par.VH[i,j]);
     writeln(outfile,STWIData.Par.Time[i,j],'  ',STWIData.Par.KD[i,j],'  ',
              STWIData.Par.KP[i,j],'  ',STWIData.Par.KI[i,j]);
     writeln(outfile,STWIData.Par.IL[i,j],'  ',STWIData.Par.F_VL[i,j],'  ',
              STWIData.Par.F_VH[i,j],'  ',STWIData.Par.L_VL[i,j],'  ',
              STWIData.Par.L_VH[i,j]);
     end;
     end;
     closeoutfile(0,0);
end;

(************* MBinc procedures ***********************)
function MBInc1_timebase_factor : real;
var
  factor  : real;
  begin
  factor := 1.0;
  if MBInc1Data.time_base > 1 then
     factor:=factor*10;
  if MBInc1Data.time_base > 2 then
     factor:=factor*10;
  if MBInc1Data.time_base > 3 then
     factor:=factor*10;
  if MBInc1Data.time_base > 4 then
     factor:=factor*10;
  mbinc1_timebase_factor:=(factor/5.0e6);
  end;

(***************************************************)
procedure MBINC1ini;

(********* Ablaufsteuerung fuer Helmholtzspule **************************)
var
   CountMode    :       CountModeType;
   delay_1	:	word;
   delay_2	:	word;
   delay_3	:	word;
   delay_4	:	word;
   delay_5	:	word;
   delay_4_local:       word;
   delay_5_local:       word;
   timebase     :       integer;
   stc_timebase :       word;
   value	:	word;
   out5 	:	Boolean;
   overrun	:	Boolean;
   OldWin       :       WinCtrlPtrType;

Begin
    OldWin:=WinActCtrl;
    changewin(warningswin);

     MBopen(slave1);
     STC_Reset(INC1);

     delay_1:=MBINC1data.Delay_1;   (*number of revolution before start*)
     delay_2:=MBINC1data.Delay_2;   (*offset before start*)
     delay_3:=MBINC1data.Delay_3;   (*division factor for trigger generation*)
     delay_4:=MBINC1data.Delay_4;   (*division factor for speed measurement*)
     delay_5:=MBINC1data.delay_5;
     timebase :=MBINC1data.Time_Base;(* 1 : 5 MHz
                                       2 : 500 KHz
                                       3 : 50 KHz
                                       4 : 5 KHz
                                       5 : 500 Hz *)
     ianzdata0[1] :=DVM1Data.Readings;
     ianzdata0[2] :=round(realval(DVM1Data.Readings)/
                          realval(MBInc1Data.Delay_4));
     ianzdata90[1] :=DVM1Data.Readings;
     ianzdata90[2] :=round(realval(DVM1Data.Readings)/
                          realval(MBInc1Data.Delay_4));

(********* Counter 1 **********************************)

     With CountMode
     Do Begin
        GateCtrl        := STC_gateoff;
        Source          := STC_counter1;    {ZERO Pulse}
        SourceEdge      := STC_CountRising; {egal}
        Mode            := STC_Mode0;       {Mode A}
        CountDir        := STC_CountDown;
        OutCtrl         := STC_Toggle;
        end;

        STC_Mode(INC1,STC_Counter1,delay_1,0,CountMode);
        STC_SetTC(INC1,STC_Counter1,FALSE);     {Output is low}

(********* Counter 2 **********************************)

     With CountMode
     Do Begin
        if delay_1 > 0 then
            GateCtrl := STC_GateTC;	    {connected to Output of Counter 1}
        if delay_1 = 0 then
            GateCtrl := STC_GateOff;
        Source          := STC_counter2;    {Signal Pulse}
        SourceEdge      := STC_CountRising; {egal}
        Mode            := STC_Mode0;       {Mode A, B}
        CountDir        := STC_CountDown;
        OutCtrl         := STC_Toggle;
        end;

        STC_Mode(INC1,STC_Counter2,delay_2,0,CountMode);
        STC_SetTC(INC1,STC_Counter2,FALSE);     {Output is low}

(********* Counter 3 **********************************)

     With CountMode
     Do Begin
        if delay_2 > 0 then
            GateCtrl    := STC_LevelHigh;   {connected to Output of Counter 2}
        if delay_2 = 0 then
            GateCtrl := STC_GateOff;
        Source          := STC_counter2;    {Signal Pulse}
        SourceEdge      := STC_CountRising; {egal}
        Mode            := STC_Mode1;       {Mode E}
        CountDir        := STC_CountDown;
        OutCtrl         := STC_PulsHigh;
        end;
     STC_Mode(INC1,STC_Counter3,delay_3,0,CountMode);
     STC_SetTC(INC1,STC_Counter3,FALSE);     {Output is low}

(********* Counter 4 **********************************)

     if timebase = 6 then
     begin
        delay_4_local:=1;
        stc_timebase := STC_counter1;        {measurement of pulses
                                              per revolution}
     end
     else
     begin
        delay_4_local:=round(
         (realval(MBInc1Data.Delay_3)/2.0)*realval(MBInc1Data.Delay_4));
                     {MBInc1Data.Delay_4 = 1
                     -> Nr of Speed Measuremenmts = Nr of Trigger Pulses}
        stc_timebase := STC_counter2       {normal case}
     end;

     With CountMode
     Do Begin
        if delay_2 > 0 then
           GateCtrl     := STC_LevelHigh;   {connected to OUTPUT of Counter 2}
        if delay_2 = 0 then
           GateCtrl := STC_GateOff;
        Source          := STC_timebase;    {Signal or Zero Pulse}
        SourceEdge      := STC_CountRising; {egal}
        Mode            := STC_Mode1;       {Mode D}
        CountDir        := STC_CountDown;
        OutCtrl         := STC_Toggle;      {PulsHigh;}
        end;

     STC_Mode(INC1,STC_Counter4,delay_4_local,0,CountMode);
     STC_SetTC(INC1,STC_Counter4,FALSE);    {Output is low}

(********* Counter 5 **********************************)

     delay_5_local:=0;      (* egal *)

     if timebase = 1 then
        stc_timebase := STC_F1;
     if timebase = 2 then
        stc_timebase := STC_F2;
     if timebase = 3 then
        stc_timebase := STC_F3;
     if timebase = 4 then
        stc_timebase := STC_F4;
     if timebase = 5 then
        stc_timebase := STC_F5;
     if timebase = 6 then
        begin
        stc_timebase := STC_counter2;
        delay_5_local:=0;  (* egal *)
        end;
     With CountMode
     Do Begin
        GateCtrl        := STC_EdgeHigh;    {connected to output counter 4}
        Source          := STC_timebase;    {<= 5 Mhz Clock or Signal}
        SourceEdge      := STC_CountRising; {egal}
        Mode            := STC_Mode4;       {Mode O}
        CountDir        := STC_CountUp;
        OutCtrl         := STC_Toggle;
        end;
     STC_Mode(INC1,STC_Counter5,delay_5_local,0,CountMode);
     STC_SetTC(INC1,STC_Counter5,FALSE);     {Output is low}

(*********** Start Counters 1 - 5 later *********************)

        changewin(oldwin);
end;

(************************************************************)
procedure MBINC2ini;
var
   CountMode    :       CountModeType;
   delay_1	:	word;
   delay_2	:	word;
   delay_3	:	word;
   delay_4	:	word;
   delay_5	:	word;
   stc_timebase :       word;
   timebase_1   :       integer;
   timebase_2   :       integer;
   OldWin       :       WinCtrlPtrType;

Begin
    OldWin:=WinActCtrl;
    changewin(warningswin);

     MBopen(slave1);
     STC_Reset(INC2);

     delay_1:=MBINC2data.delay_1;
     delay_2:=MBINC2data.delay_2;
     delay_3:=MBINC2data.delay_3;
     delay_4:=MBINC2data.delay_4;
     delay_5:=MBINC2data.delay_5;
     timebase_1:=MBINC2data.Time_Base_1;
     timebase_2:=MBINC2data.Time_Base_2;

(********* Counter 2 **********************************)
     With CountMode
     Do Begin
        GateCtrl        := STC_GateOff;
        Source          := STC_counter2;   {Trigger Pulse of C3, MBINC1}
        SourceEdge      := STC_CountRising;{egal}
        Mode            := STC_Mode1;      {Mode D}
        CountDir        := STC_CountDown;
        OutCtrl         := STC_PulsHigh;
        end;
     STC_Mode(INC2,STC_Counter2,delay_2,0,CountMode);
     STC_SetTC(INC2,STC_Counter2,FALSE);     {Output is low}

(********* Counter 3 **********************************)
(********* produces delayed trigger pulses for PAR32 **)
(********* Delay Time = (1/F)*Delay_3 *****************)
     if timebase_1 = 1 then
        stc_timebase := STC_F1;
     if timebase_1 = 2 then
        stc_timebase := STC_F2;
     if timebase_1 = 3 then
        stc_timebase := STC_F3;
     if timebase_1 = 4 then
        stc_timebase := STC_F4;
     if timebase_1 = 5 then
        stc_timebase := STC_F5;

     With CountMode
     Do Begin
        GateCtrl        := STC_EdgeLow;    {connected to output counter 1}
        Source          := stc_timebase;   {internal clock}
        SourceEdge      := STC_CountRising;{egal}
        Mode            := STC_Mode1;      {Mode F}
        CountDir        := STC_CountDown;
        OutCtrl         := STC_PulsLow;
        end;
     STC_Mode(INC2,STC_Counter3,delay_3,0,CountMode);
     STC_SetTC(INC2,STC_Counter3,TRUE);     {Output is high}

(********* Counter 5 **********************************)
(********* generates clock for INC1 *******************)
(********* for Laser Interferometer Measurements ******)
     if timebase_2 = 1 then
        stc_timebase := STC_F1;
     if timebase_2 = 2 then
        stc_timebase := STC_F2;
     if timebase_2 = 3 then
        stc_timebase := STC_F3;
     if timebase_2 = 4 then
        stc_timebase := STC_F4;
     if timebase_2 = 5 then
        stc_timebase := STC_F5;

     With CountMode
     Do Begin
        GateCtrl        := STC_GateOff;
        Source          := stc_timebase;    {internal clock}
        SourceEdge      := STC_CountRising; {egal}
        Mode            := STC_Mode1;       {Mode D}
        CountDir        := STC_CountDown;
        OutCtrl         := STC_Toggle;
        end;
     STC_Mode(INC2,STC_Counter5,delay_5,0,CountMode);
     STC_SetTC(INC2,STC_Counter5,TRUE);     {Output is high}

(*********** Start Counters 1 - 5 later *********************)

        changewin(oldwin);
end;
(*********************************************************************)
procedure MBINC1aini;
begin
end;
(*********************************************************************)

procedure MBINC1bini;
var
   CountMode    :       CountModeType;
   delay_1	:	word;
   delay_2	:	word;
   delay_3	:	word;
   delay_4	:	word;
   delay_5	:	word;
   delay_4_local:       word;
   stc_timebase :       word;
   timebase     :       integer;
   OldWin       :       WinCtrlPtrType;

Begin
    OldWin:=WinActCtrl;
    changewin(warningswin);

    MBopen(slave1);
    STC_Reset(INC1);

    delay_1:=MBINC1data.Delay_1;   (* *)
    delay_2:=MBINC1data.Delay_2;   (* *)
    delay_3:=MBINC1data.Delay_3;   (* *)
    delay_4:=MBINC1data.Delay_4;   (* *)
    delay_5:=MBINC1data.delay_5;   (**)
    timebase :=MBINC1data.Time_Base;(* 1 : 5 MHz
                                       2 : 500 KHz
                                       3 : 50 KHz
                                       4 : 5 KHz
                                       5 : 500 Hz *)

(********* Counter 2 **********************************)

     With CountMode
     Do Begin
        GateCtrl        := STC_gateoff;
        Source          := STC_counter2;    {ZERO Pulse}
        SourceEdge      := STC_CountRising; {egal}
        Mode            := STC_Mode0;       {Mode A}
        CountDir        := STC_CountDown;
        OutCtrl         := STC_Toggle;
        end;

        STC_Mode(INC1,STC_Counter2,delay_2,0,CountMode);
        STC_SetTC(INC1,STC_Counter2,FALSE);     {Output is low}


(********* Counter 3 **********************************)

(********* Delay Time = (1/F)*Delay_3 *****************)
     if timebase = 1 then
        stc_timebase := STC_F1;
     if timebase = 2 then
        stc_timebase := STC_F2;
     if timebase = 3 then
        stc_timebase := STC_F3;
     if timebase = 4 then
        stc_timebase := STC_F4;
     if timebase = 5 then
        stc_timebase := STC_F5;
     With CountMode Do
     Begin
     if delay_2 > 0 then
            GateCtrl := STC_GateTC;	  {connected to Output of Counter 2}
     if delay_2 = 0 then
            GateCtrl := STC_GateOff;
       GateCtrl        := STC_GateOff;
       Source          := STC_TimeBase;    {Signal Pulse}
       SourceEdge      := STC_CountRising; {egal}
       Mode            := STC_Mode0;       {Mode A}
       CountDir        := STC_CountDown;
       OutCtrl         := STC_PulsHigh;
     end;
     STC_Mode(INC1,STC_Counter3,delay_3,0,CountMode);
     STC_SetTC(INC1,STC_Counter3,False);     {Output is high}

        changewin(oldwin);
end;


(*********************************************************************)
procedure MBINC2aini;
var
   CountMode    :       CountModeType;
   delay_1	:	word;
   delay_2	:	word;
   delay_3	:	word;
   delay_4	:	word;
   delay_5	:	word;
   stc_timebase :       word;
   timebase_1   :       integer;
   timebase_2   :       integer;
   OldWin       :       WinCtrlPtrType;

Begin
    OldWin:=WinActCtrl;
    changewin(warningswin);

     MBopen(slave1);
     STC_Reset(INC2);

     delay_1:=MBINC2data.delay_1;
     delay_2:=MBINC2data.delay_2;
     delay_3:=MBINC2data.delay_3;
     delay_4:=MBINC2data.delay_4;
     delay_5:=MBINC2data.delay_5;
     timebase_1:=MBINC2data.Time_Base_1;
     timebase_2:=MBINC2data.Time_Base_2;

(********* Counter 2 **********************************)
     With CountMode
     Do Begin
        GateCtrl        := STC_GateOff;
        Source          := STC_counter2;   {Trigger Pulse of C3, MBINC1}
        SourceEdge      := STC_CountRising;{egal}
        Mode            := STC_Mode0;      {Mode D}
        CountDir        := STC_CountDown;
        OutCtrl         := STC_PulsHigh;
        end;
     STC_Mode(INC2,STC_Counter2,delay_2,0,CountMode);
     STC_SetTC(INC2,STC_Counter2,FALSE);     {Output is low}

(********* Counter 3 **********************************)
(********* produces delayed trigger pulses for PAR32 **)
(********* Delay Time = (1/F)*Delay_3 *****************)
     if timebase_1 = 1 then
        stc_timebase := STC_F1;
     if timebase_1 = 2 then
        stc_timebase := STC_F2;
     if timebase_1 = 3 then
        stc_timebase := STC_F3;
     if timebase_1 = 4 then
        stc_timebase := STC_F4;
     if timebase_1 = 5 then
        stc_timebase := STC_F5;

     With CountMode
     Do Begin
        GateCtrl        := STC_EdgeLow;    {connected to output counter 1}
        Source          := stc_timebase;   {internal clock}
        SourceEdge      := STC_CountRising;{egal}
        Mode            := STC_Mode1;      {Mode F}
        CountDir        := STC_CountDown;
        OutCtrl         := STC_PulsLow;
        end;
     STC_Mode(INC2,STC_Counter3,delay_3,0,CountMode);
     STC_SetTC(INC2,STC_Counter3,TRUE);     {Output is high}

(********* Counter 5 **********************************)
(********* generates clock for INC1 *******************)
(********* for Laser Interferometer Measurements ******)
     if timebase_2 = 1 then
        stc_timebase := STC_F1;
     if timebase_2 = 2 then
        stc_timebase := STC_F2;
     if timebase_2 = 3 then
        stc_timebase := STC_F3;
     if timebase_2 = 4 then
        stc_timebase := STC_F4;
     if timebase_2 = 5 then
        stc_timebase := STC_F5;

     With CountMode
     Do Begin
        GateCtrl        := STC_GateOff;
        Source          := stc_timebase;    {internal clock}
        SourceEdge      := STC_CountRising; {egal}
        Mode            := STC_Mode1;       {Mode D}
        CountDir        := STC_CountDown;
        OutCtrl         := STC_Toggle;
        end;
     STC_Mode(INC2,STC_Counter5,delay_5,0,CountMode);
     STC_SetTC(INC2,STC_Counter5,TRUE);     {Output is high}

(*********** Start Counters 1 - 5 later *********************)

        changewin(oldwin);
end;

(*********************************************************************)
procedure AdjustProc;
begin
end;

(*********************************************************************)
procedure MBInc1Start;
begin
     STC_Command(INC1,STC_Loadarm,STC_Counter1,STC_Counter2,
                   STC_Counter3,STC_Counter4,STC_Counter5);
end;

(*********************************************************************)
procedure MBInc2Start;
begin
     STC_Command(INC2,STC_Loadarm,STC_Counter1,STC_Counter2,
                   STC_Counter3,STC_Counter4,STC_Counter5);
end;

(*********************************************************************)
procedure MBInc1Stop;
begin
     STC_Command(INC1,STC_Disarm,STC_Counter1,STC_Counter2,
                   STC_Counter3,STC_Counter4,STC_Counter5);
end;

(*********************************************************************)
procedure MBInc2Stop;
begin
     STC_Command(INC2,STC_Disarm,STC_Counter1,STC_Counter2,
                   STC_Counter3,STC_Counter4,STC_Counter5);
end;


(************ DAC procedures ***************************)
procedure get_aktdacvoltage;
begin

end;


(*********************************************************************)
procedure DACdrive(startvolt:real;endvolt:real;mode:integer);
   var

   vstart     : real;
   vend       : real;
   voltage    : real;
   dv,dt      : real;
   value      : Longint;
   maxvalue   : Longint;
   i,j,ierr   : integer;
   OldWin     : WinCtrlPtrType;

   begin
     OldWin:=WinActCtrl;
     changewin(DACvalueWin);

     maxvalue:=32768;       (* 16 Bit DAC *)

     if mode = 0 then
        begin
        if abs(aktdacvoltage-startvolt) < DACData.MaxDiff then
        begin
        DACSet(DAC1,DACA,0);
        DACSet(DAC1,DACB,0);
        aktdacvoltage:=0.0;
        writeln;write(aktdacvoltage:5:2);
        end
        else
        begin
        changewin(WarningsWin);
        writeln(' DAC Speed too high');
        writeln(' change start voltage');
        end;
     end;

     if mode = 1 then
     begin

     ierr   :=0;
     vstart := (maxvalue*Startvolt)/10.;
     vend   := (maxvalue*Endvolt)/10.;
     dv     := (vend-vstart)/(DACdata.Steps-1);
     dt     := (DACdata.Time)/(DACdata.Steps-1);

     if abs(aktdacvoltage-startvolt) > DACData.MaxDiff then  (*1*)
       begin
       changewin(WarningsWin);
       writeln(aktdacvoltage-startvolt);
(*       writeln(' DAC Speed too high');
       writeln(' change Start Voltage');
*)       ierr:=1;
       end;

     if abs((10.0/maxvalue)*dv) > DACData.MaxDiff then   (*2*)
       begin
       changewin(WarningsWin);
       writeln((10.0/8191.0)*dv);
(*       writeln(' DAC Speed too high');
*)       writeln(' change step size');
       ierr:=1;
       end;

     if 1000*abs(dt) < DACData.MinDT then                 (*3*)
       begin
       changewin(WarningsWin);
       writeln(' DAC Speed too high');
       writeln(' change Step size');
       ierr:=1;
       end;

     if ierr = 0 then
       begin
       changewin(DACValueWin);
       for i:=1 to DACdata.Steps do
         begin
         value:=round(vstart+dv*(i-1));
         voltage:=value*(10.0/maxvalue);
         writeln; write(voltage:5:2);
         if value > maxvalue then
            value:= maxvalue;
         if value < -maxvalue then
            value:= -maxvalue;
         DACSet(DAC1,DACA,value);
         delay(round(dt*1000));
         end;
       aktdacvoltage:=Endvolt;
       end;

   end;   (* mode = 1 *)
   changewin(oldwin);
   end;

(*********************************************************************)
procedure DACini;
     begin;

     mbopen(slave1);
     DACOpen(DAC1);
     dacautorange:=false;
     dacrange(DAC1,DACA,3);
     dacrange(DAC1,DACB,3);

     get_aktdacvoltage;

     dacdrive(0,0,0);

     end;


(************* SMS procedures *************************)
procedure SMS1Ini;
var
   FrqDiv       :       word;
   PulseLen     :       word;
   v            :       real;

     begin
     mbopen(slave1);

     GenOpen(Gen1,GenModeStep,SMS1Data.TimeBase);
     GenSetFrq(Gen1,GenA,SMS1Data.FrqDiv,SMS1Data.PulseLen);

     end;

(**********************************************************)
procedure SMS2Ini;
var
   FrqDiv       :       word;
   PulseLen     :       word;
   v            :       real;

     begin
     mbopen(slave1);

     GenOpen(Gen1,GenModeStep,SMS2Data.TimeBase);
     GenSetFrq(Gen1,GenB,SMS2Data.FrqDiv,SMS2Data.PulseLen);

     end;

(***********************************************************)

procedure SMS1Drive;

   (* Jetzt werden die Richtungen f�r dir Motoren bestimmt, Sie werden
    * durch das Vorzeichen der Steps-Variablen bestimmt. *)

var
   steps        :       longint;
   dir1         :       byte;
   dir2         :       byte;
   dir          :       byte;

begin


     Dir1 := GenMotorLeft;
     Dir2 := GenMotorRight;
     Steps:= SMS1data.Steps;

     dir := dir2;
     if steps < 0 then
        begin
        dir := dir1;
        steps:=-steps;
        end;
     GenPutPuls (Gen1, GenA, Steps, Dir);

end;

(**************************************************************)
procedure SMS2Drive;

   (* Jetzt werden die Richtungen f�r dir Motoren bestimmt, Sie werden
    * durch das Vorzeichen der Steps-Variablen bestimmt. *)

var
   steps        :       longint;
   dir1         :       byte;
   dir2         :       byte;
   dir          :       byte;

begin

     Dir1 := GenMotorLeft;
     Dir2 := GenMotorRight;
     Steps:= SMS2data.Steps;

     dir := dir2;
     if steps < 0 then
        begin
        dir := dir1;
        steps:=-steps;
        end;
     GenPutPuls (Gen1, GenB, Steps, Dir);

end;

(*************************************************************)
procedure stringsmanipulate;

var
   a:  real;
   s,s1,s2:  string[80];
   n:  integer;
begin
     write('a=');
     readln(a);
     writeln(a);
     str(a,s);
     writeln(s);
     s1:='zahl'+s;
     writeln(s1);
     n:=length(s1);
     writeln(n);
     delete(s1,5,1);
     writeln(s1);
     s2:='blabla';
     insert(s2,s1,10);
     writeln(s1);
     readkey;
end;

PROCEDURE SMSRampIni;
begin
  MbOpen(slave1);
  GenOpen(Gen1,GenModeStep,SMS1Data.TimeBase);
end;

PROCEDURE SMSRampClose;
begin
  GenClose(Gen1);

end;
PROCEDURE SMSRampeFill(var SMSR:SMSRampe;SMSData:SMSDataType);
var K0,Kd:real;
    i,ir:word;
begin

  SMSR.fd0 := SMSData.FrqDiv;
  SMSR.RampHight := SMS_Ramp_Hight;
  SMSR.RampSteps := SMSData.RampSteps;
  SMSR.DeltaSteps := round(SMSData.RampDstps);
  if  SMS_RAMP_TYP='L' then
  begin
    Kd:=SMSR.RampHight/SMSR.fd0;
    K0:= 1/SMSR.fd0-Kd;
    if SMSR.RampSteps>100 then SMSR.RampSteps:=100; {max. 100 Stufen}
    for i:=1 to SMSR.RampSteps do
    begin
      SMSR.fd[i]:=round(1/(K0+i*Kd));
    end;
  end;
  if SMS_RAMP_TYP='S' then
  begin
    Kd:=SMSR.RampHight/SMSR.fd0;
    K0:=1/SMSR.FD0;
    if SMSR.RampSteps>100 then SMSR.RampSteps:=100;
    ir:=SMSR.RampSteps-1;
    for i:=1 to SMSR.RampSteps do
    begin
      SMSR.fd[i]:=round(1/(K0+Kd*(1+sin((i-1)*3.14159/ir-1.570795))));
    end;
  end;
end;

PROCEDURE SMSRampeDo(MotNr:byte;var RelStep:longint;var SMSR:SMSRampe;Stepsel:integer);
var
    i                : word;
    n1,fd,nmax,dstp,SGet  : word;
    S1,steps         : longint;
    GenMot           : byte;
    dir1,dir2,dir    : byte;

begin
  if chbreak=#3 then EXIT;
  Dir1 := GenMotorLeft;
  Dir2 := GenMotorRight;
  Steps:= RelStep;

  nmax:=SMSR.RampSteps;                        {scharfe Messung stepsel:=1}
  if stepsel=0 then nmax:=SMSR.RampSteps div 2;{allgemeine Fahrt Stepsel:=0, Halbe Stufenzahl}
  if nmax<1 then nmax:=1;

  dir := dir2;
  if steps < 0 then
  begin
    dir := dir1;
    steps:=-steps;
  end;

  GenMot:=GenA;
  if MotNr=2 then GenMot:=GenB;

  dstp:=SMSR.DeltaSteps;

  if SMSR.RampSteps*2*dstp>steps then
  begin

    GenSetFrq(Gen1,GenMot,SMSR.fd[1],SMSR.fd[1]);
    GenPutPuls(Gen1,GenMot,steps,DIR);
    repeat until ((GenGetStatus(Gen1,MotNr) and 4) = 0);
    exit;
 {   nmax:=steps div (2*dstp);
    if nmax<1 then nmax:=1;}
  end;

    i:=1;
  GenSetFrq(Gen1,GenMot,SMSR.fd[1],SMSR.fd[1]);
  GenPutPuls(Gen1,GenMot,steps,DIR);
  repeat
    Sget:=GenGetPuls(Gen1,GenMot);
    if Sget>Steps-nmax*dstp then i:=nmax+1 - (Sget-Steps+nmax*dstp) div DStp;
    if Sget<nmax*dstp then   i:=Sget div DStp;
    if i>nmax then i:=nmax;
    if i<1 then i:=1;
    GenSetFrq(Gen1,GenMot,SMSR.fd[i],SMSR.fd[i]);
  until ((GenGetStatus(Gen1,GenMot) and 4) = 0);

   if keypressed then chbreak:=readkey;
   if chbreak=#3 then EXIT;
end;

PROCEDURE REF_TEXT;
var OldWin    : WinCtrlPtrType;

begin
  OldWin:=WinActCtrl;
  changeWin(WarningsWin);
  writeln;
{ writeln('Reference-Measurement !');}
  writeln('Referenz-Messung l�uft !');
  changeWin(oldwin);
end;
PROCEDURE WarningLeer;
var OldWin    : WinCtrlPtrType;
begin
  OldWin:=WinActCtrl;
  changeWin(WarningsWin);
  writeln;
  writeln;
  changeWin(OldWin);
end;

PROCEDURE SMS1Ref;
var SMS_RampSteps_old: word;
    ManSteps         : longint;
begin
  Ref_Text;
  SMSRampeFill(SMSR1^,SMS1Data);
  SMS_RampSteps_old:=SMSR1^.RampSteps;
{  SMSR1^.Rampsteps:=SMSR1^.RampSteps div 2;}
  SMSR1^.Rampsteps:=8;   {5}
  SMSRampINI;
  ManSteps:=-round(50.0*SMS_Factor);
  repeat
    SMSRampeDo(1,ManSteps,SMSR1^,0);
  until (GenGetStatus(Gen1,GenA) =1); {fahren, bis der Endlagenschalter aus}
  delay(1000);
  SMSR1^.Rampsteps:=1;
  ManSteps:=round(0.5*SMS_Factor);
  SMSRampeDo(1,ManSteps,SMSR1^,0);
  delay(1000);
  ManSteps:=-round(0.6*SMS_Factor);
  repeat
    SMSRampeDo(1,ManSteps,SMSR1^,0);
  until (GenGetStatus(Gen1,GenA) =1); {fahren, bis der Endlagenschalter aus}
  delay(1000);
  SMSR1^.Rampsteps:=20;  {4}
  delay(1000);
  ManSteps:=round(SMS1_Referenz*SMS_Factor);
  SMSRampeDo(1,ManSteps,SMSR1^,0);
  SMSR1^.RampSteps:=SMS_RampSteps_old;
  delay(500);
  WarningLeer;
end;

PROCEDURE SMS2Ref;
var SMS_RampSteps_old: word;
    ManSteps         : longint;
begin
  Ref_Text;
  SMSRampeFill(SMSR2^,SMS2Data);
  SMS_RampSteps_old:=SMSR2^.RampSteps;
{  SMSR2^.Rampsteps:=SMSR2^.RampSteps div 2;}
  SMSR2^.Rampsteps:=8;   {5}
  SMSRampINI;
  ManSteps:=-round(50.0*SMS_Factor);
  repeat
    SMSRampeDo(2,ManSteps,SMSR2^,0);
  until (GenGetStatus(Gen1,GenB) = 1); {fahren, bis der Endlagenschalter aus}

  delay(1000);
  SMSR2^.Rampsteps:=1;
  ManSteps:=round(0.5*SMS_Factor);
  SMSRampeDo(2,ManSteps,SMSR2^,0);
  delay(1000);
  ManSteps:=-round(0.6*SMS_Factor);
  repeat
    SMSRampeDo(2,ManSteps,SMSR2^,0);
  until (GenGetStatus(Gen1,GenB) = 1); {fahren, bis der Endlagenschalter aus}

  delay(1000);
  SMSR2^.Rampsteps:=20; {4}
  ManSteps:=round(SMS2_Referenz*SMS_Factor);
  SMSRampeDo(2,ManSteps,SMSR2^,0);
  delay(500);
  SMSR2^.RampSteps:=SMS_RampSteps_old;
  delay(500);
  WarningLeer;
end;

PROCEDURE SMSREFERENCE;
begin
  SMS1Ref;
  delay(500);
  SMS2Ref;
  Delay(500);
end;

(************* Stretched Wire procedures ************************************)
procedure SWini(iSTWI: integer);
var
   OldWin  : WinCtrlPtrType;
begin

(*    bdname := 'GPIB0  ';
    bd := ibfind (bdname);

    changewin(warningswin);

    writeln(' Gpib0 = ',bd);
    if (bd < 0) then gpiberr('Ibfind Error');
    ibsic (bd);
    if (ibsta and ERR) <> 0 then gpiberr('Ibsic Error');
*)
    if iSTWI = 1 then
       SWname :='STWI1  ';
    if ISTWI = 2 then
       SWname :='STWI2  ';
    hp := ibfind (SWname);

(*    writeln(' HP = ',hp);   *)
    if (hp < 0) then
    begin
    changewin(warningswin);
    gpiberr('Ibfind Error');
    changewin(oldwin);
    end;
    ibclr (hp);
    if (ibsta and ERR) <> 0 then
    begin
    changewin(warningswin);
    gpiberr('Ibclr Error');
    changewin(oldwin);
    end;

    if iSTWI = 1 then
       SW1:=hp;
    if iSTWI = 2 then
       SW2:=hp;

(*  Send the Interface Clear (IFC) message.  This action initializes the    *)
(*  GPIB interface board and makes the interface board Controller-In-Charge.*)
(*  If the error bit ERR is set in IBSTA, call GPIBERR with an error        *)
(*  message.                                                                *)

     end;

(************* DVM procedures ************************************)
procedure dvmini(iDVM: integer);
var
   OldWin  : WinCtrlPtrType;
begin

(*    bdname := 'GPIB0  ';
    bd := ibfind (bdname);

    changewin(warningswin);

    writeln(' Gpib0 = ',bd);
    if (bd < 0) then gpiberr('Ibfind Error');
    ibsic (bd);
    if (ibsta and ERR) <> 0 then gpiberr('Ibsic Error');
*)
    if iDVM = 1 then
       hpname := 'HP3458A';
    if iDVM = 2 then
       hpname := 'HP3458B';
    if iDVM = 3 then
       hpname := 'HP3458C';
    hp := ibfind (hpname);

(*    writeln(' HP = ',hp);   *)
    if (hp < 0) then
    begin
    changewin(warningswin);
    gpiberr('Ibfind Error');
    changewin(oldwin);
    end;
    ibclr (hp);
    if (ibsta and ERR) <> 0 then
    begin
    changewin(warningswin);
    gpiberr('Ibclr Error');
    changewin(oldwin);
    end;

    if iDVM = 1 then
       hp1:=hp;
    if iDVM = 2 then
       hp2:=hp;
(*    if iDVM = 3 then
       hp3:=hp;  *)

(*  Send the Interface Clear (IFC) message.  This action initializes the    *)
(*  GPIB interface board and makes the interface board Controller-In-Charge.*)
(*  If the error bit ERR is set in IBSTA, call GPIBERR with an error        *)
(*  message.                                                                *)

     end;
(*****************************************************)
procedure dvmerr(msg:string; rdchar:char);
(*========================================================================
 *                      Proced. DVMERR
 *  This proced. will notify you that the Fluke 45 returned an invalid
 *  serial poll response byte.  The error message will be printed along with
 *  the serial poll response byte.
 *
 *  The NI-488 function IBONL is called to disable the hardware and software.
 *
 *  The HALT proced. will terminate this program.
 * =========================================================================*)

var
     OldWin                   : WinCtrlPtrType;
begin
    OldWin:=WinActCtrl;
    changewin(warningswin);
    writeln (msg);
    writeln('Status Byte = ', ord(rdchar));

(*  Call the IBONL function to disable the hardware and software.           *)

    ibonl (bd,0);

    halt;

    changewin(oldwin);

end;

(********************************************************)
procedure deletezeroes(sendstr2:string);
var
   i,j  : integer;
begin
    i:=1;
    j:=0;
    while (i <= 40) and (j = 0) do
          begin
          if sendstr2[i] = ' ' then
          i:=i+1
          else j:=1;
          end;
    sendstr2:=copy(sendstr2,i,41-i);
end;

PROCEDURE REVERSE_DATA(var ipos,iired:integer);
var j:integer;
begin
  for j:=1 to iired do
  begin
    if ipos=0 then ybuf[j]:=ydata0^[1,iired+1-j];
    if ipos=90 then ybuf[j]:=ydata90^[1,iired+1-j];
  end;
  for j:=1 to iired do
  begin
    if ipos=0 then ydata0^[1,j]:=ybuf[j];
    if ipos=90 then ydata90^[1,j]:=ybuf[j];
  end;
end;


(****************KEITHLEY 2001 **anfang***************************)

procedure DVMStart(iDVM: integer);

var
   i,j,m,n,ianz,FILT        : integer;
   OldWin          : WinCtrlPtrType;

BEGIN
  if iDVM = 1 then hp:=hp1;
  if iDVM = 2 then hp:=hp2;
  if iDVM = 3 then hp:=hp3;

  OldWin:=WinActCtrl;
  changewin(warningswin);

  if(ihhs = 1) or (ihhs = 2) or (ihhs = 3) or (ihhs = 4) or (ihhs = 5)
  or (ihhs = 6) then
  begin
    (************** FAST READING ************************************)
    if (idvm=1) and (DVM_ident='k') then
    begin
      FILT:=round(DVM3Data.Range);

      IEEE_write(hp,'*RST');
      IEEE_write(hp,':SYST:AZERo:STAT OFF');
      if FILT=0 then IEEE_write(hp,':INPUT:PREAMP:STATE OFF');  (*Pramp deaktiviert*)

      if FILT>0 then
      begin
        IEEE_write(hp,':INPUT:PREAMP:STATE on');  (*Pramp aktiviert*)
        IEEE_write(hp,':INPUT:PREAMP:FILT FAST');

        if FILT=2 then IEEE_write(hp,':INPUT:PREAMP:FILT MED');
        if FILT=3 then IEEE_write(hp,':INPUT:PREAMP:FILT SLOW');
      end;
      sendstr1 := ':volt:dc:range ';            (*Messbereich in Millivolt*)
      str(DVM1Data.Range:6:3,sendstr2);
      deletezeroes(sendstr2);
      sendstr3 := 'e-3';
      sendstr:=sendstr1+sendstr2+sendstr3;
      IEEE_write(hp,sendstr);               (*Umrechnen in Volt und Senden*)

      sendstr1 := ':volt:dc:APER ';             (*Messfenster*)
      str(DVM1Data.aperture:7:2,sendstr2);
      deletezeroes(sendstr2);
      sendstr3 := 'e-3';
      sendstr:=sendstr1+sendstr2+sendstr3;
      IEEE_write(hp,sendstr);

      IEEE_write(hp,':VOLT:DC:DIG 7.5');

      IEEE_write(hp,':INIT:cont 1');             (*Trigger aktivieren*)
      IEEE_write(hp, ':TRIG:DELAY 0');
      IEEE_write(hp,':TRACE:CLEAR');             (*Buffer l�schen*)

      sendstr1 := ':TRACE:POIN ';
      str(DVM1Data.Readings,sendstr2);
      sendstr:=sendstr1+sendstr2;
      IEEE_write(hp,sendstr);
      IEEE_write(hp,':TRACE:FEED SENSE');   (*Rohdaten in Buffer schreiben*)
      IEEE_write(hp,':TRACE:FEED:CONT NEXT');   (*BufferControl*)
      IEEE_write(hp,':TRIG:SOUR EXT');          (*Triggerquelle angeben*)
      delay(2000);
      changewin(oldwin);
      exit;
    end;
    (* weitere DVM's : HP3458 *)

    if DVM_ident='h' then
    begin
     sendstr := 'PRESET FAST';
      n:=length(sendstr);
      (* TARM SYNC, TRIG AUTO *)
      for i := 1 to 11 do
        wrt[i] := sendstr[i];
      ibwrt (hp,wrt,11);
      if (ibsta and ERR) <> 0 then gpiberr('Ibwrt Error');

      if (AZERO = 1) then
      IEEE_write(hp,'AZERO ON');

      sendstr1 := 'APER ';
      str(DVM1Data.aperture:7:2,sendstr2);
      deletezeroes(sendstr2);
      sendstr3 := 'e-3';
      sendstr:=sendstr1+sendstr2+sendstr3;
      n:=length(sendstr);
      for i:= 1 to n do
      wrt[i] := sendstr[i];
      ibwrt (hp,wrt,n);
      if (ibsta and ERR) <> 0 then gpiberr('Ibwrt Error');

      sendstr := 'MFORMAT DINT';
      for i:= 1 to 35 do
      wrt[i] := sendstr[i];
      ibwrt (hp,wrt,12);
      if (ibsta and ERR) <> 0 then gpiberr('Ibwrt Error');

      sendstr := 'MEM FIFO';
      for i:= 1 to 35 do
        wrt[i] := sendstr[i];
      ibwrt (hp,wrt,8);
      if (ibsta and ERR) <> 0 then gpiberr('Ibwrt Error');

      sendstr1 := 'RANGE ';
      str(DVM1Data.Range:6:3,sendstr2);
      deletezeroes(sendstr2);
      sendstr:=sendstr1+sendstr2;
      n:=length(sendstr);
      for i:= 1 to n do
        wrt[i] := sendstr[i];
      ibwrt (hp,wrt,n);
      if (ibsta and ERR) <> 0 then gpiberr('Ibwrt Error');

      sendstr1 := 'NRDGS ';
      str(DVM1Data.Readings,sendstr2);
    (*    deletezeroes(sendstr2); *)
      sendstr3:=',EXT';
      sendstr:=sendstr1+sendstr2+sendstr3;
      n:=length(sendstr);
      for i:= 1 to n do
        wrt[i] := sendstr[i];
      ibwrt (hp,wrt,n);
      if (ibsta and ERR) <> 0 then gpiberr('Ibwrt Error');

      sendstr := 'TARM SGL,1';
      for i:= 1 to 35 do
        wrt[i] := sendstr[i];
      ibwrt (hp,wrt,10);
      if (ibsta and ERR) <> 0 then gpiberr('Ibwrterror');
    end;
  end;
  changewin(oldwin);
end;

procedure DVMReadOut(iDVM: integer; ipos:integer);
var
    rdn                 : Trdn;
   i,j,m,n,ianz        : integer;
   code                : integer;
   ystring             : string[17];
   OldWin              : WinCtrlPtrType;
   s1                  : string[1];
   xxx                 : real;
   ired,iired,i0,cpos  : integer;
   reduction_factor    : integer;


begin
  if iDVM = 1 then hp:=hp1;
  if iDVM = 2 then hp:=hp2;
  if iDVM = 3 then hp:=hp3;
  reduction_factor:=DefineData.Red_Factor;
  OldWin:=WinActCtrl;


  if ipos = 0 then ianz:=ianzdata0[1];
  if ipos = 90 then ianz:=ianzdata90[1];
  ianz:=DVM1Data.Readings;
  if(ihhs = 1)then changewin(resultwin);
  if (iDVM=1) and (DVM_ident='k') then
  begin
    if (DefineData.SaveStack = 1) and (ihhs <> 4) and (ihhs <> 6) then
    begin
        (***** store data to file **)
      filename := path+DVMDataFileName;
      openoutfile(0,0);
    end;
ChangeWin(WarningsWin);
writeln(hp,'dvm_ident=',dvm_ident,ianz);
    IEEE_WRITE(hp,':FORM:DATA ASC');
    delay(100);
writeln(hp,':FORM:DATA ASC');
    IEEE_write(hp,':FORM:ELEM READ');
    delay(100);
writeln(hp,':FORM:ELEM READ');
    ired:=0;
    iired:=0;
    IEEE_write(hp,':TRACE:DATA?');
    delay(100);
writeln(hp,'DATA?',memavail,' ',maxavail);
    ibrd(hp,rdn,rdn_size);
writeln(rdn_size);
    KEYTHLEY_COPY(rdn,',');
    CHARFIND(rdn,cpos,',');
    i0:=0;
writeln(iired);
    for j:=1 to  ianz do
    begin
      ystring:='';
      for i:= 1 to cpos-1 do
      begin
        if i<cpos then
        begin
          s1:=rdn[i+i0];
          ystring:=ystring+s1;
        end;
      end;
      val(ystring,xxx,code);
      if (DefineData.SaveStack = 1) and (ihhs <> 4) and (ihhs <> 6) then
            writeln(outfile,realval(j),xxx);
      ired:=ired+1;
      if (ired = reduction_factor ) then
      begin
        if(iired < 255)then
        begin
	  iired:=iired+1;
writeln('*',iired);
          if ipos = 0 then ydata0^[1,iired]:=xxx;
          if ipos = 90 then ydata90^[1,iired]:=xxx;
	end;
	ired:=0;
      end;
      i0:=i0+cpos;
    end;
  end      (* KEITHLEY, nur als DVM 1 genutzt *)
  else
  begin
{    writeln('dvm_ident=',dvm_ident,' HP3458');}
    sendstr := 'OFORMAT ASCII';
    for i:= 1 to 35 do
      wrt[i] := sendstr[i];
    ibwrt (hp,wrt,13);
    if (ibsta and ERR) <> 0 then gpiberr('Ibwrt Error');

    sendstr1 := 'RMEM 1,';
    str(DVM1Data.Readings,sendstr2);
    (*    deletezeroes(sendstr2); *)
    sendstr3:=',1';
    sendstr:=sendstr1+sendstr2+sendstr3;
    n:=length(sendstr);

    for i:= 1 to n do
      wrt[i] := sendstr[i];
    ibwrt (hp,wrt,n);
    if (ibsta and ERR) <> 0 then gpiberr('Ibwrt Error');

    (* jeder Wert ist 15 Character lang gefolgt von einem Komma und einem
    Leerzeichen, bzw einem CR und LF nach dem letzten Zeichen einer
    Gruppe, insgesamt also 17 Character pro Wert *)

    if (DefineData.SaveStack = 1) and (ihhs <> 4) and (ihhs <> 6) then
    begin
      (***** store data to file **)
      filename := path+DVMDataFileName;
      openoutfile(0,0);
    end;

    ired:=0;
    iired:=0;

    ianz:=DVM1Data.Readings;

    for j:=1 to  ianz do
    begin
      ystring:='';
      ibrd(hp,rd,17);
      for i:= 1 to 17 do
      begin
        if i<=16 then
        begin
          s1:=rd[i];
          ystring:=ystring+s1;
        end;
      end;
      val(ystring,xxx,code);
      if (DefineData.SaveStack = 1) and (ihhs <> 4) and (ihhs <> 6) then
        writeln(outfile,realval(j),xxx);
      ired:=ired+1;
      if (ired = reduction_factor )then
      begin
        if(iired < 255)then
        begin
          iired:=iired+1;
          if ipos = 0 then ydata0^[1,iired]:=xxx;
          if ipos = 90 then ydata90^[1,iired]:=xxx;
        end;
        ired:=0;
      end;
    end;
    REVERSE_DATA(ipos,iired);
  end; (*if (idvm=1) and (DVM_ident='k' else DVM_ident='h') *)

  if (DefineData.SaveStack = 1) and (ihhs <> 4) and (ihhs <> 6) then
    closeoutfile(0,0);

  if (ihhs = 1) then changewin(oldwin);
end;

(*********** KEITHLEY 2001 ende *******************************)

(***********************************************************)
procedure DVMReadOut_new(iDVM: integer; ipos:integer);
(* jeder Wert ist 15 Character lang gefolgt von einem Komma und einem
   Leerzeichen, bzw einem CR und LF nach dem letzten Zeichen einer
   Gruppe, insgesamt also 17 Character pro Wert *)
var
   i,j,m,n,ianz        : integer;
   code                : integer;
   ystring             : string[17];
   OldWin              : WinCtrlPtrType;
   s1                  : string[1];
   s2                  : string;
   xxx,www             : real;
   ired,iired          : integer;
   AktPoint_DVM        : PointPtrType_DVM;
   LastPoint_DVM       : PointPtrType_DVM;
   reduction_factor    : integer;

   begin

    reduction_factor:=DefineData.Red_Factor;

    OldWin:=WinActCtrl;

    sendstr := 'OFORMAT ASCII';
    for i:= 1 to 35 do
       wrt[i] := sendstr[i];
    ibwrt (hp1,wrt,13);
    ibwrt (hp2,wrt,13);
    if (ibsta and ERR) <> 0 then gpiberr('Ibwrt Error');

    sendstr1 := 'RMEM 1,';
    str(DVM1Data.Readings,sendstr2);
(*    deletezeroes(sendstr2); *)
    sendstr3:=',1';
    sendstr:=sendstr1+sendstr2+sendstr3;
    n:=length(sendstr);

    for i:= 1 to n do
       wrt[i] := sendstr[i];
    ibwrt (hp1,wrt,n);
    ibwrt (hp2,wrt,n);
    if (ibsta and ERR) <> 0 then gpiberr('Ibwrt Error');

    if ipos = 0 then
    begin
    ianz:=ianzdata0[1];
    end;
    if ipos = 90 then
    begin
    ianz:=ianzdata90[1];
    end;


   if DefineData.SaveStack = 1 then
      begin
      (***** initialize stack for DVM data **)
      CreatePoint_DVM(TOP_DVM);
      AktPoint_DVM:=TOP_DVM;
      LastPoint_DVM:=NIL;
      end;

   ired:=(-DVM1Data.Readings+1) mod reduction_factor + reduction_factor-1;
   iired:=0;

   ianz:=DVM1Data.Readings;

    for j:=1 to  ianz do
    begin
      ibrd(hp1,rd1,17);
      ibrd(hp2,rd2,17);

      ystring:='';
      for i:= 1 to 17 do
         begin
         if i<=16 then
            begin
            s1:=rd1[i];
            ystring:=ystring+s1;
            end;
         end;
         val(ystring,xxx,code);

      ystring:='';
      for i:= 1 to 17 do
         begin
         if i<=16 then
            begin
            s1:=rd2[i];
            ystring:=ystring+s1;
            end;
         end;
         val(ystring,www,code);
         if DefineData.SaveStack = 1 then
           (** write data to stack **)
           with AktPoint_DVM^ DO
                begin
                Last:=LastPoint_DVM;
                DVM.CH1:=xxx;
                DVM.CH2:=www;
                DVM.CH3:=0.0;
                if j < ianz then
                   begin
                   CreatePoint_DVM(AktPoint_DVM^.Next);
                   LastPoint_DVM:=AktPoint_DVM;
                   AktPoint_DVM:=AktPoint_DVM^.Next;
                   End_DVM:=AktPoint_DVM;
                   end;
                end;

	 ired:=ired+1;
	 if (ired = reduction_factor )then
		begin
                if(iired < 255)then
                         begin
		         iired:=iired+1;
                         if ipos = 0 then
                           ydata0^[1,iired]:=xxx;
                         if ipos = 90 then
                           ydata90^[1,iired]:=xxx;
			 end;
		ired:=0;
		end;
    end;

 (*   reverse data *)
    for j:=1 to iired do
        begin
        if ipos=0 then
           ybuf[j]:=ydata0^[1,iired+1-j];
        if ipos=90 then
           ybuf[j]:=ydata90^[1,iired+1-j];
        end;
    for j:=1 to iired do
        begin
        if ipos=0 then
           ydata0^[1,j]:=ybuf[j];
        if ipos=90 then
           ydata0^[1,j]:=ybuf[j];
        end;

    if DefineData.SaveStack = 1 then
    (** save data from stack to file and reverse data **)
    begin
    (* DataFileNameProc(0) wurde bereits in plpath aufgerufen
       und File Namen sind alle erzeugt *)
    s2:=DVMDataFileName;
    savestack_DVM(s2);
    end;

end;

(************* RESET DISPLAY etc **************************)
procedure DVMreset(iDVM : integer);
var
   i    : integer;
   OldWin  : WinCtrlPtrType;

begin
     if iDVM = 1 then
        hp:=hp1;
     if iDVM = 2 then
        hp:=hp2;
(*     if iDVM = 3 then
        hp:=hp3;            *)

    OldWin:=WinActCtrl;
    changewin(warningswin);

    if (DVM_ident='k') and (idvm=1) then
    begin
      delay(500);
      IEEE_write(hp,':syst:pres');
      changewin(oldwin);
      exit;
    end;

    delay(200);
   { IEEE_write(hp,'APER 500.0e-3');}
    IEEE_WRITE(hp,'TIMER 0.5');
    delay(200);
    IEEE_write(hp,'AZERO ON');
    delay(200);
    IEEE_write(hp,'TARM AUTO');
    delay(200);
    IEEE_write(hp,'TRIG AUTO');
    delay(200);
    IEEE_write(hp,'DISP ON');
    delay(200);
    IEEE_write(hp,'RESET');
    changewin(oldwin);
end;

(*************** Laser Interferometer Procedures **********)
procedure LaserIni;
var
   OldWin  : WinCtrlPtrType;
   n,i    : integer;
begin

(*    bdname := 'GPIB0  ';
    bd := ibfind (bdname);

    writeln(' Gpib0 = ',bd);

    if (bd < 0) then gpiberr('Ibfind Error');
    ibsic (bd);
    if (ibsta and ERR) <> 0 then gpiberr('Ibsic Error');
*)
    liname := 'LI     ';
    li := ibfind (liname);

    if (li < 0) then
    begin
    changewin(warningswin);
    gpiberr('Ibfind Error');
    changewin(oldwin);
    end;
    ibclr (li);
    if (ibsta and ERR) <> 0 then
    begin
    changewin(warningswin);
    gpiberr('Ibclr Error');
    changewin(oldwin);
    end;

(*  Send the Interface Clear (IFC) message.  This action initializes the    *)
(*  GPIB interface board and makes the interface board Controller-In-Charge.*)
(*  If the error bit ERR is set in IBSTA, call GPIBERR with an error        *)
(*  message.                                                                *)

    wr_text := 'ERST';       (**** Reset ***)
    IEEE_write(LI,wr_text);

    wr_text := 'XMET';       (**** numbers in mm ***)
    IEEE_write(LI,wr_text);

    wr_text := 'YMET';       (**** numbers in mm ***)
    IEEE_write(LI,wr_text);

    wr_text := 'ZMET';       (**** numbers in mm ***)
    IEEE_write(LI,wr_text);

    wr_text := 'WMET';       (**** numbers in mm ***)
    IEEE_write(LI,wr_text);

    wr_text := 'XDR1';       (**** Drive Enable ***)
    IEEE_write(LI,wr_text);

    wr_text := 'YDR1';       (**** Drive Enable ***)
    IEEE_write(LI,wr_text);

    wr_text := 'ZDR1';       (**** Drive Enable ***)
    IEEE_write(LI,wr_text);

    wr_text := 'WDR1';       (**** Drive Enable ***)
    IEEE_write(LI,wr_text);

    wr_text := 'XZRO';       (**** Set Counter to Zero ***)
    IEEE_write(LI,wr_text);

    wr_text := 'YZRO';       (**** Set Counter to Zero ***)
    IEEE_write(LI,wr_text);

    wr_text := 'ZZRO';       (**** Set Counter to Zero ***)
    IEEE_write(LI,wr_text);

    wr_text := 'WZRO';       (**** Set Counter to Zero ***)
    IEEE_write(LI,wr_text);

    wr_text := 'XOP0';       (**** Optic lambda/64 ***)
    IEEE_write(LI,wr_text);  (**** influences only the result of
                              XPOS Command, not the 32 bit IO !!! ***)

    wr_text := 'YOP0';       (**** Optic lambda/64 ***)
    IEEE_write(LI,wr_text);

    wr_text := 'ZOP0';       (**** Optic lambda/64 ***)
    IEEE_write(LI,wr_text);

    wr_text := 'WOP0';       (**** Optic lambda/64 ***)
    IEEE_write(LI,wr_text);
end;

FUNCTION ASCII_HEX_TO_BIN (c : STRING2) : BYTE;
VAR
 len : BYTE ABSOLUTE c;
 i   : WORD;
 val : WORD;
 mat : WORD;
 sum : WORD;

BEGIN
  sum := 0;
  mat := 0;
  {ewt. Leerzeichen l�schen}
  FOR i := len DOWNTO 1 DO
  BEGIN
    IF (c[i] > '9') THEN val := ORD(c[i]) - $37
                    ELSE val := ORD(c[i]) - $30;
    sum := sum OR (val SHL (4 * mat));
    INC(mat);
  END;
  ASCII_HEX_TO_BIN := sum;
END;

Function length_STWIPAR (rd_text : string) : integer;
var
   ii, islash   : integer;
begin
ii:=0;
islash:=0;
repeat
ii:=ii+1;
if (rd_text[ii] = '/') then
   islash:=islash+1;
until islash = 16;
length_STWIPAR:=ii;
end;

END.
