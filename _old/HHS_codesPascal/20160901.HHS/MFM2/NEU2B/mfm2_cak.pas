{$N+}
unit mfm2_cak;
{03.02.2015 HHS+SW2}
{20.12.1999}
INTERFACE

USES DOS, CRT, Graph, SCALELIB, MBBASE, BESLIB, SDLIB,
          STDLIB, MOUSELIB, VESALIB, WINLIB,
          TPDECL, AUXIO1,
          MFM2_TCK,MFM2_HWK,MFM2_UFK,MFM2_ANK,MFM2_SCK;

procedure encoder_zero;
procedure integrator_drift;
procedure CalProc;




IMPLEMENTATION

(*********** Calibration procedures ******************)
procedure encoder_zero;
Begin
end;

(*****************************************************)
procedure integrator_drift;
begin
end;

(*****************************************************)
procedure CalProc;
  CONST zeroID   = 3001;
        driftID   = 3002;
        firstID  = zeroID;
        lastID   = driftID;

 VAR CalWinLayOut                         : WinDesType;
     CalWin,OldWin                     : WinCtrlPtrType;
     width,height,xpos,ypos,WinHeight,
     locEvent                          : WORD;
     RampStr                            : String[8];

 BEGIN
(*  OldWin:=WinActCtrl;

  CalWinLayOut := WinDefDes;
  WinSetHeader(CalWinLayOut,LightGray,
               C_LightBlue+'Calibration MENU');
  WinSetFillColor(CalWinLayOut, Yellow);
  WinSetOpenMode(CalWinLayOut, WinHeadText, XmmSave);

  GetButtonDim(FONT8x16,1,18,width,height);
  xpos:=width+4; ypos:=25;
  WinHeight:= 8*height;
  CalWin:=OpenWin(xpos,ypos, xpos+width, ypos+WinHeight,CalWinLayOut);

  ChangeWin(CalWin);
  SetMargin(5,LeftMargin);
  SetCursor(0,0);
  ShowLineObject('    encoder zero',18, zeroID, ButtonLeft, NoKey, Local,
                  NilEvProc);
  writeln(' ');
  ShowLineObject('integrator drift',18, driftID, ButtonLeft, NoKey, Local,
                  NilEvProc);
  writeln(' ');

  repeat
   locEvent:=WaitOfEvent(firstID,lastID);
   case locEvent of
      zeroID   : encoder_zero;
      driftID  : integrator_drift;
   END;
  until locEvent=0;;
  ChangeWin(OldWin);
  CloseWin(CalWin);      *)
  END;

END.