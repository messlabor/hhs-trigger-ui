      character*80 name1,name2,ans
      integer getnamq
      dimension x1(1024),y1(1024)
      write(6,*)' maximum number of data points : 1024'
      if(.not.(getnamq(' old file ',ans) .ne. 0))goto 23000
      stop
23000 continue
      name1 = ans
      if(.not.(getnamq(' new file ',ans) .ne. 0))goto 23002
      stop
23002 continue
      name2 = ans
      write(6,*) 'shift = '
      read(5,*)shift
      i=0.
      open(unit=10,file=name1)
23004 continue
      i=i+1
      if(.not.(i.gt.1024))goto 23007
      write(6,*)' data set eventually too large'
      write(6,*)' data set truncated'
      goto 20
23007 continue
      read(10,*,end=20)x1(i),y1(i)
23005 goto 23004
20    npts1=i-1
      close(10)
      open(unit=10,file=name2)
      do 23009 i=1,npts1
      write(10,*)x1(i)+shift,y1(i)
23009 continue
      close(10)
      stop
      end
