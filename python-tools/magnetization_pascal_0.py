import numpy as np
import pandas as pd
import math
import matplotlib.pyplot as plt
from matplotlib import gridspec

import h5py
from scipy.optimize import curve_fit
from scipy.fft import fft, fftfreq
from scipy.signal import find_peaks

from hhs_tools import *


df_raw = readOldHHS("../_old/N444/N444_A_1.RAW", degree=0, columns=["turns","voltage","Hz"])
df_dat = readOldHHS("../_old/N444/N444_A_1.DAT", degree=0, columns=["turns","voltage","residual"])

DVM1Data_Aperture = 0.06841561833961954 # s
# DVM1Data_Aperture = 0.02 # s
radgra = 180 / math.pi

yrawdata = np.array(df_raw["voltage"])
ycorrdata = np.array(df_dat["voltage"])

yresdata = np.array(df_dat["residual"])
N = len(ycorrdata)
freq = df_raw["Hz"].mean()
T = (df_dat["turns"][1] - df_dat["turns"][0]) / freq
print(f"Frequency: {freq}")
print(f"Number of points: {N}")
Delta_phi = 0.5 * (DVM1Data_Aperture) * 2 * math.pi * freq # rad
print(f"Delta_phi: {Delta_phi}")

print('-----------------------------------------------------------------------')
print(' TEST Loop')
# Fourier Analysis Mode 2
ampc = 0.0
amps = 0.0
xiperiod = N / np.max(df_dat["turns"]) # number of pts per revolution
xk = 2.0 * math.pi / xiperiod
for i in range(0,N):
    ampc = ampc + math.cos(xk * i) * ycorrdata[i]
    amps = amps + math.sin(xk * i) * ycorrdata[i]
ampc = 2.0 * ampc / N
amps = 2.0 * amps / N
phi0_orig = math.atan(-amps/ampc)
amp0 = ampc / math.cos(phi0_orig)
amp0_mod = math.sqrt(ampc**2 + amps**2)

print(f"amps: {amps}")
print(f"ampc: {ampc}")
print(f"phi0 original: {phi0_orig}")
print(f"amp0: {amp0}")
print(f"amp0_mod: {amp0_mod}")
phi0 = phi0_orig - Delta_phi
print(f"phi0 corre: {phi0}")

# Amplitude and phase shall be positive
if amp0 < 0:
    amp0 = -amp0
    phi0 += math.pi
if phi0 > 2*math.pi:
    phi0 -= 2*math.pi
print(f"amp0 positive: {amp0}") # mV/Hz
print(f"phi0 positive(rad): {phi0}") # rad

# Calculate magnetization components for 0 degrees
mz = amp0 * math.cos(phi0)
my_0 = amp0 * math.sin(phi0)

print('-----------------------------------')
print('')
print(f' M_z      = {1000 * mz / freq: .14E}')
print(f' M_y (0)  = {1000 * my_0 / freq: .14E}')
print(f'averaged speed (Hz): {freq: .14E}')
print(f'1 amplitude (0) (mV/Hz): {1000 * amp0 / freq: .14E}')
print(f'1 phase (0) (rad): {phi0 * radgra : .14E}')
print('')