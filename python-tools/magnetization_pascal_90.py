import numpy as np
import pandas as pd
import math
import matplotlib.pyplot as plt
from matplotlib import gridspec

import h5py
from scipy.optimize import curve_fit
from scipy.fft import fft, fftfreq
from scipy.signal import find_peaks

from hhs_tools import *


df_raw = readOldHHS("../_old/N444/N444_A_1.RAW", degree=90, columns=["turns","voltage","Hz"])
df_dat = readOldHHS("../_old/N444/N444_A_1.DAT", degree=90, columns=["turns","voltage","residual"])

DVM1Data_Aperture = 0.01 # value for NPLC=1

radgra = 180 / math.pi

yrawdata = np.array(df_raw["voltage"])
ycorrdata = np.array(df_dat["voltage"])

yresdata = np.array(df_dat["residual"])
N = len(ycorrdata)
freq = df_raw["Hz"].mean()
T = (df_dat["turns"][1] - df_dat["turns"][0]) / freq

# Fourier Analysis
fy = fft(ycorrdata)
xf = fftfreq(N, T)[:N//2]

print(fy.shape)

amp0 = 2.0/N * np.abs(fy[0:N//2])
idx = np.argmax(np.abs(fy))
ampc = float(2 * np.real(fy[idx]) / N)
amps = 2 * np.imag(fy[idx]) / N
phi = math.atan2(-amps, ampc)
print(phi)
print(ampc)
print(amps)
amp = ampc / math.cos(phi)

# Phase corrections for 0 degrees
Delta_phi = 0.5 * DVM1Data_Aperture * 2.0 * math.pi * freq
phi += Delta_phi

if amp < 0.0:
    amp = -amp
    phi += math.pi
if phi > 2.0 * math.pi:
    phi -= 2.0 * math.pi

# Calculate magnetization components for 0 degrees

mx = -amp * math.cos(phi/radgra)
my_90 = amp * math.sin(phi/radgra)

print('-----------------------------------')
print(f' M_x      = {1000 * mx / abs(freq): .14E}')
print(f' M_y (90)  = {1000 * my_90 / abs(freq): .14E}')
print('')
print('--------- 0 Degrees----------------')
print(f' averaged speed (Hz): {freq: .14E}')
print(f' 1 amplitude (0) (mV/Hz): {1000*amp/freq: .14E}')
print(f' 1 phase (0) (rad): {phi*radgra: .14E}')
print('')