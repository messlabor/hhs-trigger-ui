Installation
================

Here we aim to provide a simple and easy way to set up the environment used for this project. 

These instructions will guide the user through cloning the repository, and executing the installation bash script. The steps performed by the bash script are described below.

We will need to build epics, the required modules, the iocs, and the python environment.

Each one of the IOC projects, `DVM IOC: <https://codebase.helmholtz.cloud/messlabor/hp3458a-gpib>`_ and `Trigger IOC: <https://codebase.helmholtz.cloud/messlabor/ioc-pmac-trigger>`_ has a installation section dedicated to explain how to install the required software. However, when working with the both projects, it is easier to use the provided scripts to build the environment.

.. note::

    The bash commands might need to be adjusted depending on the system or the privileges of the user. 

.. note::

    The version and the order to which the modules are installed is important.


Before checking the code below, some useful information:

1. It would be wise, when using a new PC, to take parts of the code, one at time and running in the terminal block by block. So if any errors occur, you can easily debug.
2. The code starts by setting up important environmental variables for EPICS installation, such as EPICS_HOST_ARCH, EPICS_BASE, SUPPORT and IOC. The epics base is added to the PATH of the system, and some other variables are set, such as LC_ALL and TZ to avoid some warnings during installation.
3. After that we install all the utilities using apt-get install. Note that if you are using another linux distribution, the commands might be different. In here we use ubuntu, you can check yours by going to the terminal and using "uname -a". 
4. Some python packages are also installed, such as the h5py library to support our data procedure, ophyd for the communication via python to the epics records, and sphynx which is used for documentation.
5. Different folders are created, and this step is one which we are interested in paying attention to ownerships and access rights to the folders. You can get errors trying to create the folder with a user without enough privileges, as well as having problems later trying to run epics. So, one option is grant an user the ownership of the whole /opt folder and then create the folder and clone the repositories.
6. After that we git clone epics base and run make. this step can take from 5 - 30 minutes depending on your machine. It is a large amount of files.
7. Without too many details, each one of the important modules are cloned and built in the repository. The script already contain the necessary steps to make sure it is built correctly, for example, correcting paths and changing configuration files. 
8. The important point to pay attention is the Keysight Libraries installation, which you can run without the --mode unattended if you wish to have better control of the installation. However, note that if you change the installation paths, the VISA EPICS driver which is cloned and built later will need adjustments on the path as well. This is because the VISA-EPICS requires that we set the correct location to where the visa libraries are installed. This took me some time to do it. So I wish you do not have to.
9. The last steps are downloading Phoebus. Note that the HHS.sh file in the hhs-trigger-ui folder points to the path of the phoebus installation, so make sure you know where this is in case you change the installation procedure.
10. Additionally, some variables are added inside the ~/.bashrc. These ones are related to each measurement and they are dynamically changed by the phoebus interface. This was done so we can access local variables from phoebus inside python or any other language.

Installation Step-By-Step
---------------------------

First of all, check if you have the correct linux kernel for the Keysight IOLS (5.15 is recommended). You can do that by running

.. code-block:: bash

    uname -r

You should have **5.15.0-128-generic** or something similar. If not we need to install it. For the ubuntu 22.04 that we are using we can do this with

.. code-block:: bash

    sudo apt update 
    sudo apt install linux-image-generic

You must now restart your PC and at the startup screen choose "Advanced Options for Ubuntu" and then choose the **5.15.0-128-generic** image. Do not choose the one with (recovery mode).

Double check if your kernel is correct by using the command **uname -r** again on the terminal, and if you have the 5.15 kernel, proceed with the steps below.

.. note::

    The correct linux kernel is important for communication with the Multimeter using the Keysight IOLS.



The next step is to clone this repository. Open a terminal on your system and navigate to /opt/epics. Create this directory if it does not already exist.  Then clone this repository using the following command:

.. code-block:: bash

    # create folder
    mkdir /opt/epics
    # install git
    sudo apt-get update
    sudo apt-get install git
    # clone repository
    cd /opt/epics
    git clone https://codebase.helmholtz.cloud/messlabor/helmholtzcoil/hhs-trigger-ui.git

After the repository has been successfully cloned navigate to the /opt/epics/hhs-trigger-ui directory and execute the installation bash script with the following command:

.. code-block:: bash

    cd /opt/epics/hhs-trigger-ui
    ./installation.sh

Installation.sh code
---------------------

Below you can see the bash file responsible for installing everything we need. some important notes were given above on this documentation, so make sure you read them.

Additionally, a command which might pose problems is the installation of the keysight IOLS and kernels. The commands responsible for this installation need sudo ("root") privileges and even though installation can show to be successfull, if you did it with the wrong linux kernel (as mentioned above), you will not be able to communicate with the device.

If you installed the Keysight IOLS with the wrong kernels, reinstall it with the correct ones. After installation of the Keysight IOLS, reset the PC, as recommended by the keysight manuals.

.. literalinclude:: ../../installation.sh
    :language: bash


.. note::

    Remember to reset the PC and start again with the 5.15 kernels.