# HHS Trigger ui

Documentation: 

https://messlabor.pages.hzdr.de/helmholtzcoil/hhs-trigger-ui/


### Fodler structure

- **_old** --> Contains the old Helmholtz coil code and data. This can be used as a reference when developing further the data analysis of this project.

- **data** --> The folder which the new measurement system saves the data.

- **docs** --> Folder with the sphinx documentation.

- **Helmholtzspule_090724_1742** --> Contain the control techniques file to comission the motor using the unidrive. This requires commercial software from Control Techniques. It has a lot of parameter inside, I would recommend changing it only if really necessary. This will also affect the PowerPMAC system later. 

- **python-tools** --> Has a collection of different python files and functions used for reading, writing, and analysing the data.

- **references** --> Some pdfs for references that might be useful.
