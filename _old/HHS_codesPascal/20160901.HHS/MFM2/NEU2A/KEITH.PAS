(* Test des Keythley-Auslesens, weitere Routinen *);
PROCEDURE CHARFIND1(var rdf:trdn;var charstart,charpos:integer;chf:char);
var
  i1                        : Integer;
begin
  for i1:=charstart to rdn_size do
  begin
    if (rdf[i1]=chf) then
    begin
      charpos:=charstart-1+i1;
      exit;
    end;
  end;
end;

PROCEDURE KEYTHLEY_COPY;
var f1:text;
ix,iz:integer;
begin
  iz:=1;
  assign(f1,'KEITHLEY.DAT');
  rewrite(f1);
  write(f1,iz:4,'  ::');
  for ix:=1 to rdn_size do
  begin
    write(f1,rdn[ix]);
    if (rdn[ix]=',') then
    begin
      writeln(f1);
      inc(iz);
    end;
  end;
  close(f1);
end;

procedure DVMReadOut1(iDVM: integer; ipos:integer);
var
    rdn                 : Trdn;
   i,j,m,n,ianz        : integer;
   code                : integer;
   ystring             : string[17];
   OldWin              : WinCtrlPtrType;
   s1                  : string[1];
   xxx                 : real;
   ired,iired,i0,cpos,cstart  : integer;
   reduction_factor    : integer;


begin
  if iDVM = 1 then hp:=hp1;
  if iDVM = 2 then hp:=hp2;
  if iDVM = 3 then hp:=hp3;
  reduction_factor:=DefineData.Red_Factor;
  OldWin:=WinActCtrl;


  if ipos = 0 then ianz:=ianzdata0[1];
  if ipos = 90 then ianz:=ianzdata90[1];
  ianz:=DVM1Data.Readings;
  if(ihhs = 1)then changewin(resultwin);

  if (iDVM=1) and (DVM_ident='k') then
  begin
    if (DefineData.SaveStack = 1) and (ihhs <> 4) and (ihhs <> 6) then
    begin
        (***** store data to file **)
      filename := path+DVMDataFileName;
      openoutfile(0,0);
    end;
{       writeln('dvm_ident=',dvm_ident,' Keithley 2001');}
    IEEE_WRITE(hp,':FORM:DATA ASC');
    IEEE_write(hp,':FORM:ELEM READ');
    ired:=0;
    iired:=0;
    IEEE_write(hp,':TRACE:DATA?');
    ibrd(hp,rdn,rdn_size);

    KEYTHLEY_COPY;        {Testausgabe des Keythleyinhalts;}

    cpos:=1;
    i0:=0;
    for j:=1 to  ianz do
    begin
      ystring:='';
      CHARFIND1(rdn,cstart,cpos,',');
      cstart:=cpos+1;
      for i:= 1 to cpos-1 do
      begin
        if i<cpos then
        begin
          s1:=rdn[i+i0];
          ystring:=ystring+s1;
        end;
      end;
      val(ystring,xxx,code);
      if (DefineData.SaveStack = 1) and (ihhs <> 4) and (ihhs <> 6) then
            writeln(outfile,realval(j),xxx);
      ired:=ired+1;
      if (ired = reduction_factor ) then
      begin
        if(iired < 255)then
        begin
	  iired:=iired+1;
          if ipos = 0 then ydata0^[1,iired]:=xxx;
          if ipos = 90 then ydata90^[1,iired]:=xxx;
	end;
	ired:=0;
      end;
      i0:=i0+cpos;
    end;
  end      (* KEITHLEY, nur als DVM 1 genutzt *)
  else
  begin
{    writeln('dvm_ident=',dvm_ident,' HP3458');}
    sendstr := 'OFORMAT ASCII';
    for i:= 1 to 35 do
      wrt[i] := sendstr[i];
    ibwrt (hp,wrt,13);
    if (ibsta and ERR) <> 0 then gpiberr('Ibwrt Error');

    sendstr1 := 'RMEM 1,';
    str(DVM1Data.Readings,sendstr2);
    (*    deletezeroes(sendstr2); *)
    sendstr3:=',1';
    sendstr:=sendstr1+sendstr2+sendstr3;
    n:=length(sendstr);

    for i:= 1 to n do
      wrt[i] := sendstr[i];
    ibwrt (hp,wrt,n);
    if (ibsta and ERR) <> 0 then gpiberr('Ibwrt Error');

    (* jeder Wert ist 15 Character lang gefolgt von einem Komma und einem
    Leerzeichen, bzw einem CR und LF nach dem letzten Zeichen einer
    Gruppe, insgesamt also 17 Character pro Wert *)

    if (DefineData.SaveStack = 1) and (ihhs <> 4) and (ihhs <> 6) then
    begin
      (***** store data to file **)
      filename := path+DVMDataFileName;
      openoutfile(0,0);
    end;

    ired:=0;
    iired:=0;

    ianz:=DVM1Data.Readings;

    for j:=1 to  ianz do
    begin
      ystring:='';
      ibrd(hp,rd,17);
      for i:= 1 to 17 do
      begin
        if i<=16 then
        begin
          s1:=rd[i];
          ystring:=ystring+s1;
        end;
      end;
      val(ystring,xxx,code);
      if (DefineData.SaveStack = 1) and (ihhs <> 4) and (ihhs <> 6) then
        writeln(outfile,realval(j),xxx);
      ired:=ired+1;
      if (ired = reduction_factor )then
      begin
        if(iired < 255)then
        begin
          iired:=iired+1;
          if ipos = 0 then ydata0^[1,iired]:=xxx;
          if ipos = 90 then ydata90^[1,iired]:=xxx;
        end;
        ired:=0;
      end;
    end;
    REVERSE_DATA(ipos,iired);
  end; (*if (idvm=1) and (DVM_ident='k' else DVM_ident='h') *)



  if (DefineData.SaveStack = 1) and (ihhs <> 4) and (ihhs <> 6) then
    closeoutfile(0,0);

  if (ihhs = 1) then changewin(oldwin);
end;

(*********** KEITHLEY 2001 ende *******************************)
