import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import hhs_tools as hhs
import webbrowser

from scipy.signal import find_peaks, savgol_filter


'''
The code below was some analysis for the data of the new helmholtz coil.
We plot a series of plots to show different results. An example of output is the Figure_1.svg file. What we do is:

- We plot the raw data of the Voltage [V] x Position [ct] on the top left.
- Next to it we can see the FFT analysis with the peak frequency highlighted. The amplitude of this frequency is the one used in the other method of FFT analysis for computing the Magnetization. The FFT analysis is useful for us to see how good is our sampling, and if other frequencies appear on the chart, it means noise is coming from somewhere.
- Inside the hhs_tools.py I added a specific value of counts which is a pointer to where the magnet is parallel to the surface, in order to have the start point of a rotation.  The plot with title single turns is a overlay of all the single turns of the measurement.
- The plot next to it is an average of this single turns by defining a small range around the center of where each point is taken. This is of course adding error to our measurements because the system is not always taking values at the same point of the curve. But still, the magnetizations at the end should not differ so much from the reference values.
- Tha other plot with tile Flux is the integral of the average single turns. The flux is then fitted into a curve of sines and cosines contribution, where the coefficients give (if using the correct units) the magnetization. 

Regardless of the units of the results, as mentioned in the documentation, the problem here is probably the phase of the flux. I believe there is a phase shift necessary to the Flux. This is to be investigated, but maybe the code below can help.
'''

fig = plt.figure(figsize=(15, 10)) 
gs = fig.add_gridspec(4, 5)

path0 = "../data/20240905_152009_N444_NPLC1__0deg_results.hdf5"
path90 = "../data/20240905_152309_N444_NPLC1__90deg_results.hdf5"

DVM_Aperture = 0.02 # [s] for NPLC 1

################### 0 Degrees ###################
data0, df0 = hhs.read_data(path0)

# plt.plot(df0["Turn Time"])

# phase correction
speed0_mean = df0.loc[:, 'Speed_Hz'].mean()
# speed0_std = df0.loc[:, 'Speed_Hz'].std()
phase_correction = 360 * speed0_mean * (DVM_Aperture / 2)
df0["Degrees"] = df0["Degrees"] - phase_correction

# Plot Raw Data#
ax0 = fig.add_subplot(gs[0,:3])
ax0.plot(df0["Position"], df0["Voltage"], label="Voltage")
ax0.set_xlabel("Position [ct]")
ax0.set_ylabel("Voltage [V]")
ax0.set_title("Raw Data --> " + path0)
ax0.legend()

# Plot FFT
xf0, yf0 = hhs.fft_analysis(data0, df0)
N = len(df0["Voltage"])
amp0 = 2.0/N * np.abs(yf0[0:N//2])
ax1 = fig.add_subplot(gs[0,3])
ax1.plot(xf0, amp0)
ax1.set_ylim(0, amp0.max() * 1.2)
ax1.set_title("FFT Analysis")
ax1.set_xlabel("Frequency [Hz]")
ax1.set_ylabel("Amplitude")
peaks0, _ = find_peaks(amp0, height= amp0.max()*0.1, threshold=amp0.max()*0.1)
ax1.plot(xf0[peaks0], amp0[peaks0], "x")
for i in peaks0:
    ax1.text(xf0[i], amp0[i]*1.04, f"{xf0[i]:.2f} Hz", fontsize=8)

# Plot single turns
ax2 = fig.add_subplot(gs[1,0])
ax2.set_title("Single Turns")
ax2.set_xlabel("Degrees [°]")
ax2.set_ylabel("Voltage [V]")
for i in df0["Turn"].unique():
    temp = df0[df0["Turn"] == i]
    ax2.plot(temp["Degrees"], temp["Voltage"], alpha=0.2)

# Plot average single turns
ax3 = fig.add_subplot(gs[1,1:3])
ax3.set_title("Average Single Turns")
ax3.set_xlabel("Degrees [°]")
avg0 = hhs.compute_binned_avg_stats(df0, bin_width=1)
print(avg0.head())
ax3.errorbar(avg0["time_mean"], avg0["voltage_mean"], 
                xerr=avg0["time_std"], yerr=avg0["voltage_std"], 
                fmt='o', color="black", alpha=0.5, label="Average ± Std Dev")

# Plot flux
df_flux0, Mv0, Mh0, Mv0_err, Mh0_err = hhs.compute_moments(avg0)
print(df_flux0.head())
ax4 = fig.add_subplot(gs[1,3])
ax4.plot(df_flux0["time_mean"], df_flux0["flux"], "o")
ax4.plot(df_flux0["time_mean"], df_flux0["fitted_flux"], label="Fit")
ax4.legend()
ax4.set_title("Flux")
ax4.set_xlabel("Time [s]")
ax4.set_ylabel("Flux [Vs]")

# Add Parameters Table
tab0 = fig.add_subplot(gs[0:2,4])
tab0.axis("off")
# Mv and Mh are inverted compared to paper because of rotation direction
data0["Mz"] = f"{-Mv0} ± {Mv0_err}" # inverted becausee of rotation direction
data0["My"] = f"{Mh0} ± {Mh0_err}"
for idx, (key, value) in enumerate(data0.items()):
    tab0.text(0, 1-idx*0.05, f"{key}: {value}", fontsize=10)

coord0 = hhs.plot_0degrees_axis(fig, gs[1,4])

################### 90 Degrees ###################
data90, df90 = hhs.read_data(path90)

# phase correction
speed90_mean = df90.loc[:, 'Speed_Hz'].mean()
# speed0_std = df0.loc[:, 'Speed_Hz'].std()
phase_correction = 360 * speed90_mean * (DVM_Aperture / 2)
df90["Degrees"] = df90["Degrees"] - phase_correction

# Plot Raw Data#
ax5 = fig.add_subplot(gs[2,:3])
ax5.plot(df90["Position"], df90["Voltage"], label="Voltage")
ax5.set_xlabel("Position [ct]")
ax5.set_ylabel("Voltage [V]")
ax5.set_title("Raw Data --> " + path90)
ax5.legend()

# Plot FFT
xf90, yf90 = hhs.fft_analysis(data90, df90)
N = len(df90["Voltage"])
amp90 = 2.0/N * np.abs(yf90[0:N//2])
ax6 = fig.add_subplot(gs[2,3])
ax6.plot(xf0, amp90)
ax6.set_ylim(0, amp90.max() * 1.2)
ax6.set_title("FFT Analysis")
ax6.set_xlabel("Frequency [Hz]")
ax6.set_ylabel("Amplitude")
peaks90, _ = find_peaks(amp90, height= amp90.max()*0.1, threshold=amp90.max()*0.1)
ax6.plot(xf90[peaks90], amp90[peaks90], "x")
for i in peaks90:
    ax6.text(xf90[i], amp90[i]*1.04, f"{xf90[i]:.2f} Hz", fontsize=8)

# Plot single turns
ax7 = fig.add_subplot(gs[3,0])
ax7.set_title("Single Turns")
ax7.set_xlabel("Degrees [°]")
ax7.set_ylabel("Voltage [V]")
for i in df90["Turn"].unique():
    temp = df90[df90["Turn"] == i]
    ax7.plot(temp["Degrees"], temp["Voltage"], alpha=0.2)

# Plot average single turns
ax8 = fig.add_subplot(gs[3,1:3])
ax8.set_title("Average Single Turns")
ax8.set_xlabel("Degrees [°]")
avg90 = hhs.compute_binned_avg_stats(df90, bin_width=1)
ax8.errorbar(avg90["degree_mean"], avg90["voltage_mean"], 
                xerr=avg90["degree_std"], yerr=avg90["voltage_std"], 
                fmt='o', color="black", alpha=0.5, label="Average ± Std Dev")

# Plot flux
df_flux90, Mv90, Mh90, Mv90_err, Mh90_err = hhs.compute_moments(avg90)
ax9 = fig.add_subplot(gs[3,3])
ax9.plot(df_flux90["time_mean"], df_flux90["flux"], "o")
ax9.plot(df_flux90["time_mean"], df_flux90["fitted_flux"], label="Fit")
ax9.legend()
ax9.set_title("Flux")
ax9.set_xlabel("Time [s]")
ax9.set_ylabel("Flux [Vs]")

# Add Parameters Table
tab90 = fig.add_subplot(gs[2:,4])
tab90.axis("off")
data90["Mz"] = f"{Mv90} ± {Mv90_err}"
data90["Mx"] = f"{Mh90} ± {Mh90_err}"
for idx, (key, value) in enumerate(data90.items()):
    tab90.text(0, 1-idx*0.05, f"{key}: {value}", fontsize=10)

# Add coordinate system
coord90 = hhs.plot_90degrees_axis(fig, gs[3,4])

# Save and Show
plt.tight_layout()
plt.show()
# save and open
# fig.savefig("example.svg", bbox_inches='tight')
# webbrowser.open("example.svg")

