import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import hhs_tools as hhs
import webbrowser

from scipy.signal import find_peaks, savgol_filter


'''
Read the comments of the file data_analysis.py

'''


fig = plt.figure(figsize=(15, 10)) 
gs = fig.add_gridspec(4, 5)

path0 = "./data/20240905_152009_N444_NPLC1__0deg_results.hdf5"
path90 = "./data/20240905_152309_N444_NPLC1__90deg_results.hdf5"

################### 0 Degrees ###################
data0, df0 = hhs.read_data(path0)

# phase correction
DVM_Aperture = 0.02 # [s] for NPLC 1
speed0_mean = df0.loc[:, 'Speed_Hz'].mean()
speed0_std = df0.loc[:, 'Speed_Hz'].std()
phase_correction = 360 * speed0_mean * (DVM_Aperture / 2)
df0["Degrees_corrected"] = df0["Degrees"] - phase_correction

# Plot Raw Data#
ax0 = fig.add_subplot(gs[0,:3])
ax0.plot(df0["Position"], df0["Voltage"], label="Voltage")
df0["smoothed_voltage"] = savgol_filter(df0["Voltage"], 10, 3)  # window size , polynomial order
# ax0.plot(df0["Position"], df0["smoothed_voltage"], label="Smoothed")
ax0.set_xlabel("Position [ct]")
ax0.set_ylabel("Voltage [V]")
ax0.set_title("Raw Data --> " + path0)
ax0.legend()

# Plot FFT
xf0, yf0 = hhs.fft_analysis(data0, df0)
N = len(df0["Voltage"])
amp0 = 2.0/N * np.abs(yf0[0:N//2])
ax1 = fig.add_subplot(gs[0,3])
ax1.plot(xf0, amp0)
ax1.set_ylim(0, amp0.max() * 1.2)
ax1.set_title("FFT Analysis")
ax1.set_xlabel("Frequency [Hz]")
ax1.set_ylabel("Amplitude")
peaks0, _ = find_peaks(amp0, height= amp0.max()*0.1, threshold=amp0.max()*0.1)
ax1.plot(xf0[peaks0], amp0[peaks0], "x")
for i in peaks0:
    ax1.text(xf0[i], amp0[i]*1.04, f"{xf0[i]:.2f} Hz", fontsize=8)

# Plot flux
df_flux0, Mv0, Mh0, Mv0_err, Mh0_err = hhs.compute_moments_v2(df0)
print(df_flux0.head())
ax4 = fig.add_subplot(gs[1,:4])
ax4.plot(df_flux0["Time"], df_flux0["flux"], "o")
ax4.plot(df_flux0["Time"], df_flux0["fitted_flux"], label="Fit")
ax4.legend()
ax4.set_title("Flux")
ax4.set_xlabel("Radians")
ax4.set_ylabel("Flux [Vs]")

# Add Parameters Table
tab0 = fig.add_subplot(gs[0:2,4])
tab0.axis("off")
data0["My"] = f"{Mv0} ± {Mv0_err}"
data0["Mz"] = f"{Mh0} ± {Mh0_err}"
for idx, (key, value) in enumerate(data0.items()):
    tab0.text(0, 1-idx*0.05, f"{key}: {value}", fontsize=10)

coord0 = hhs.plot_0degrees_axis(fig, gs[1,4])


################### 0 Degrees ###################
data0, df0 = hhs.read_data(path0)
print(df0.head())
# Plot Raw Data#
ax0 = fig.add_subplot(gs[0,:3])
ax0.plot(df0["Position"], df0["Voltage"], label="Voltage")
df0["smoothed_voltage"] = savgol_filter(df0["Voltage"], 10, 3)  # window size , polynomial order
# ax0.plot(df0["Position"], df0["smoothed_voltage"], label="Smoothed")
ax0.set_xlabel("Position [ct]")
ax0.set_ylabel("Voltage [V]")
ax0.set_title("Raw Data --> " + path0)
ax0.legend()

# Plot FFT
xf0, yf0 = hhs.fft_analysis(data0, df0)
N = len(df0["Voltage"])
amp0 = 2.0/N * np.abs(yf0[0:N//2])
ax1 = fig.add_subplot(gs[0,3])
ax1.plot(xf0, amp0)
ax1.set_ylim(0, amp0.max() * 1.2)
ax1.set_title("FFT Analysis")
ax1.set_xlabel("Frequency [Hz]")
ax1.set_ylabel("Amplitude")
peaks0, _ = find_peaks(amp0, height= amp0.max()*0.1, threshold=amp0.max()*0.1)
ax1.plot(xf0[peaks0], amp0[peaks0], "x")
for i in peaks0:
    ax1.text(xf0[i], amp0[i]*1.04, f"{xf0[i]:.2f} Hz", fontsize=8)

# Plot flux
df_flux0, Mv0, Mh0, Mv0_err, Mh0_err = hhs.compute_moments_v2(df0)
print(df_flux0.head())
ax2 = fig.add_subplot(gs[1,:4])
ax2.plot(df_flux0["Time"], df_flux0["flux"], "o")
ax2.plot(df_flux0["Time"], df_flux0["fitted_flux"], label="Fit")
ax2.legend()
ax2.set_title("Flux")
ax2.set_xlabel("Radians")
ax2.set_ylabel("Flux [Vs]")

# Add Parameters Table
tab0 = fig.add_subplot(gs[0:2,4])
tab0.axis("off")
data0["My"] = f"{Mv0} ± {Mv0_err}"
data0["Mz"] = f"{Mh0} ± {Mh0_err}"
for idx, (key, value) in enumerate(data0.items()):
    tab0.text(0, 1-idx*0.05, f"{key}: {value}", fontsize=10)

coord0 = hhs.plot_0degrees_axis(fig, gs[1,4])

################### 90 Degrees ###################
data90, df90 = hhs.read_data(path90)

# Plot Raw Data#
ax5 = fig.add_subplot(gs[2,:3])
ax5.plot(df90["Position"], df90["Voltage"], label="Voltage")
df90["smoothed_voltage"] = savgol_filter(df90["Voltage"], 20, 3)  # window size 51, polynomial order 3
# ax5.plot(df90["Position"], df90["smoothed_voltage"], label="Smoothed")
ax5.set_xlabel("Position [ct]")
ax5.set_ylabel("Voltage [V]")
ax5.set_title("Raw Data --> " + path90)
ax5.legend()

# Plot FFT
xf90, yf90 = hhs.fft_analysis(data90, df90)
N = len(df90["Voltage"])
amp90 = 2.0/N * np.abs(yf90[0:N//2])
ax6 = fig.add_subplot(gs[2,3])
ax6.plot(xf0, amp90)
ax6.set_ylim(0, amp90.max() * 1.2)
ax6.set_title("FFT Analysis")
ax6.set_xlabel("Frequency [Hz]")
ax6.set_ylabel("Amplitude")
peaks90, _ = find_peaks(amp90, height= amp90.max()*0.1, threshold=amp90.max()*0.1)
ax6.plot(xf90[peaks90], amp90[peaks90], "x")
for i in peaks90:
    ax6.text(xf90[i], amp90[i]*1.04, f"{xf90[i]:.2f} Hz", fontsize=8)

# Plot flux
df_flux90, Mv90, Mh90, Mv90_err, Mh90_err = hhs.compute_moments_v2(df90)
ax9 = fig.add_subplot(gs[3,:4])
ax9.plot(df_flux90["Time"], df_flux90["flux"], "o")
ax9.plot(df_flux90["Time"], df_flux90["fitted_flux"], label="Fit")
ax9.legend()
ax9.set_title("Flux")
ax9.set_xlabel("Time")
ax9.set_ylabel("Flux [Vs]")

# Add Parameters Table
tab90 = fig.add_subplot(gs[2:,4])
tab90.axis("off")
data90["Mx"] = f"{Mv90} ± {Mv90_err}"
data90["Mz"] = f"{Mh90} ± {Mh90_err}"
for idx, (key, value) in enumerate(data90.items()):
    tab90.text(0, 1-idx*0.05, f"{key}: {value}", fontsize=10)

# Add coordinate system
coord90 = hhs.plot_90degrees_axis(fig, gs[3,4])

# Save and Show
plt.tight_layout()
plt.show()
# save and open
# fig.savefig("example.svg", bbox_inches='tight')
# webbrowser.open("example.svg")

