import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.integrate as spi
import hhs_tools as hhs
import math

# Assuming `hhs.read_data` is defined in your module
path0 = "../data/20240905_152009_N444_NPLC1__0deg_results.hdf5"
DVM_Aperture = 0.02  # [s] for NPLC 1

################### 0 Degrees ###################
# Read data
data, df = hhs.read_data(path0)
freq = df.loc[:, 'Speed_Hz'].mean()
speed_deg = df.loc[:, 'Speed_deg'].mean()

ydata = df["Voltage"].values
xdata = df["Position"].values
N = len(ydata)

# Calculate period
P = 1 / freq  # Period in seconds

# Define Fourier coefficient calculation
def compute_real_fourier_coeffs(func, P, N_terms=1):
    coeffs = []
    for n in range(N_terms + 1):
        an = (2.0 / P) * spi.quad(lambda t: func(t) * np.cos(2 * np.pi * n * t / P), 0, P)[0]
        bn = (2.0 / P) * spi.quad(lambda t: func(t) * np.sin(2 * np.pi * n * t / P), 0, P)[0]
        coeffs.append((an, bn))
    return np.array(coeffs)

# Interpolate voltage function
interp_func = lambda t: np.interp(t, xdata, ydata)

# Compute Fourier coefficients for n=1
coeffs = compute_real_fourier_coeffs(interp_func, P, N_terms=1)
a0, b0 = coeffs[0]
a1, b1 = coeffs[1]

# Calculate amplitude and phase
amp = np.sqrt(a1**2 + b1**2)
phi = math.atan2(-b1, a1)  # atan2 handles phase quadrant correctly

# Phase correction
Delta_phi = 0.5 * DVM_Aperture * 2.0 * math.pi * freq
phi += Delta_phi

if amp < 0.0:
    amp = -amp
    phi += math.pi
if phi > 2.0 * math.pi:
    phi -= 2.0 * math.pi

# Magnetization components
radgra = 180 / math.pi
mz = amp * math.cos(phi / radgra)
my_0 = amp * math.sin(phi / radgra)

# Generate Fourier series fit
def first_order_series(t, a0, a1, b1, P):
    return a0 / 2 + a1 * np.cos(2 * np.pi * t / P) + b1 * np.sin(2 * np.pi * t / P)

t_fit = np.linspace(xdata.min(), xdata.max(), 1000)  # Smooth time range
y_fit = first_order_series(t_fit, a0, a1, b1, P)

# Plot
plt.figure(figsize=(10, 6))
plt.plot(xdata, ydata, label='Real Data', linestyle='-', marker='o', markersize=3, alpha=0.7)
plt.plot(t_fit, y_fit, label='First Order Fourier Series', linestyle='--', color='red', linewidth=2)
plt.xlabel('Position (deg)')
plt.ylabel('Voltage')
plt.title('Real Data vs First-Order Fourier Series Approximation')
plt.legend()
plt.grid()
plt.show()

# Output results
print('-------------------------')
print(f' M_z      = {1000 * mz / abs(freq): .14E}')
print(f' M_y (0)  = {1000 * my_0 / abs(freq): .14E}')
print('-------------------------')
print(f' averaged speed (Hz): {freq: .14E}')
print('')
print(f' 1 amplitude (0) (mV/Hz): {1000 * amp / freq: .14E}')
print(f' 1 phase (0) (rad): {phi: .14E}')
