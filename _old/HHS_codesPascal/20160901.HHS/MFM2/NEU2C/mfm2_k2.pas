{$N+}
{04.04.2008}
{22.10.2007}
{22.10.2003 hhs}
{28.01.2005 }

program MfM2_K2;

(********** MBCLOSE not Found ?????????????************)

USES DOS, CRT, Graph, SCALELIB, MBBASE, BESLIB, SDLIB,
          STDLIB, MOUSELIB, VESALIB, WINLIB,MBADTLIB,
          TPDECL, AUXIO1,
          MFM2_TCK,MFM2_HWK,MFM2_UFK,MFM2_MAK,MFM2_ANK,MFM2_SCK,MFM2_SUK,
          MFM2_CAK,MFM2_STK;
Const ErstellungsDatum ='04.04.2008';

label ENDE;
(******************************************************)
PROCEDURE START_HILFE;
var chc:char;
begin
  writeln;
  writeln('*****************************************************************');
  writeln('MFM2_K2 for HHS measurements:');
  writeln(' Use only the HP-DVM !  (i.e. MFM2_K2.EXE h)');
  writeln('      Connect the IEEE-cable between the PC and the HP-DVM');
  writeln('      Connect the Trigger-cable to the HP-DVM(Ext.Trigg)');
  writeln('      Switch On the big Power Supply, release the NOT-STOP');
  writeln('      Switch On "Freigabe: Ein" at the Pietsch-Bus');
  writeln('-----------------------------------------------------------------');
  writeln(' MFM2_K2 for Bloc - Measurements');
  writeln('        mfm2_k2     DVM 1,2 = HP3458A');
  writeln('        mfm2_k2 h   DVM 1,2 = HP3458A');
  writeln('        mfm2_k2 k   DVM 1   = KEITHLEY 2001 with PreAmp 1801');
{  write('press > space < to continue');
  repeat chc:=readkey;
  until (chc=#32);
  delline;
  writeln;}
{  writeln('        mfm2_k2          DVM 1,2 = HP3458A,   v=1,  r=1,  p=1');
  writeln('        mfm2_k2 h v r p  DVM 1,2 = HP3458A');
  writeln('        mfm2_k2 k v r p  DVM 1   = KEITHLEY 2001 with PreAmp 1801');
  writeln;
  writeln(' Parameter >v< and >r<  are only used for SW2 measurements! ');
  writeln(' Parameter >v< : 1<= v <=4, =Number of Sample Positions');
  writeln(' Parameter >r< : 1<= r <=2, =Number of Measurements per Position');
  writeln(' Parameter >p< : STWI_Amplifier ');}
  writeln(' Check and Change the main SW2 - Parameter in [SET UP] [SW2 SPECIAL]');
  writeln(' STWI-Apmlifier:  Factor of additional Preamplifiers (P. Stephan)');
  writeln(' HP-DVM (1), Keithley-DVM (2)');
  writeln(' KEITHLEY without Preamp 1801   : SW2 Special Filt = 0, Use front side input');
  writeln(' Keithley PreAmp Filter  Fast   : SW2 Special Filt = 1');
  writeln(' Keithley PreAmp Filter  Medium : SW2 Special Filt = 2');
  writeln(' Keithley PreAmp Filter  Slow   : SW2 Special Filt = 3');
  writeln(' DVM1Data.Range        : input in MilliVolts for Keithley-DVM');
  writeln(' DVM1Data.Range        : input in Volts for HP-DVM');
  writeln(' Sample Positions (1,2,3,4)  ');
  writeln(' Measurements per Position (1,2)');
  writeln(' Number of scans ');
end;

PROCEDURE MANUELLE_EINGABE;
var chp:char;
   code:integer;
begin
  START_HILFE;
  write('  press >q< to quit or >c< to continue ');
  chp:=#255;
  repeat chp:=readkey
  until (chp='q') or (chp='c');
  if chp='q' then
  begin
    ihhs:=0;
    exit;
  end;
  write(' DVM-Type (h,k):');
  readln(DVM_IDENT);
  writeln;
  write(' Choose Experimental Setup (1) (6)         :');
  readln(ihhs);
{  if (ihhs=6) then
  begin
    write('Number of Sample Positions (1,2,3,4)     :');
    readln(vier);
    write('Number of Measurements per Position (1,2):');
    readln(MesRep);
    write('STWI_Amplifier_factor                    :');
    readln(STWI_AMPLIFIER);
  end;}
END;

PROCEDURE PARAM_CHECK;
 var chp:char;
 code:integer;
BEGIN
  HHSBlocPos:=2;
  CheckDelta:=0.05;

  vier:=1;
  MesRep:=1;
  STWI_AMPLIFIER:=1;
  SMS_RAMP_TYP:='L';  {'L'=linear, 'S'=sinus}
  if SMS_RAMP_TYP='L' then SMS_RAMP_HIGHT:=0.22; {linear}
  if SMS_RAMP_TYP='S' then SMS_RAMP_HIGHT:=4.3; {sinus}
  dvm_ident:='k';
  SMS_Pause:=0;

  SMS1_Referenz:=84.400; {Position 4, UE56-Magnete, d=7.5mm}
(*  SMS1_Referenz:=76.000; {27.06.2000 Position 4, UE56-Magnete, d=7.5mm}*)
  SMS1_Referenz:=76.012; {06.07.2000 Raster:1.24mm}

  SMS2_Referenz:=72.500; {Position 4, UE56-Magnete, d=7.5mm, 550 Wdgn.}
  SMS2_Referenz:=77.500; {27.06.2000 Position 4, UE56-Magnete, d=7.5mm}
  SMS2_Referenz:=77.428;  {07.07.2000 Raster:1.24mm}
  SMSREF:=0;             {keine extra Referenzfahrt}

  if paramcount>0 then
  begin
    if paramstr(1)='h' then DVM_ident:='h';
    if paramstr(1)='k' then DVM_ident:='k';
    if paramstr(1)='?' then  START_HILFE;
  end;
  if paramcount>=2 then
  begin
    val(paramstr(2),vier,code);
    if vier>4 then vier:=4;
    if vier<1 then vier:=1;
  end;
  if paramcount>=3 then
  begin
    val(paramstr(3),MesRep,code);
    if MesRep>2 then MesRep:=2;
    if MesRep<1 then MesRep:=1;
  end;
  if paramcount>=4 then
  begin
    val(paramstr(4),STWI_Amplifier,code);
    if STWI_Amplifier<0 then STWI_Amplifier:=1;
  end;
  if DVM_ident<> 'k' then DVM_ident:='h';
 END;
(************* main program ***************************)
var
   dummy        : real;
   icheck       : integer;
   i{,number_of_scans}: integer;
   OldWin  : WinCtrlPtrType;
begin
  ErstellungsDate:=ErstellungsDatum;

  CHECKBREAK:=true;
  PARAM_CHECK;
  number_of_scans:=1;
  NEW(xdata0);
  NEW(yrawdata0);
  NEW(ydata0);
  NEW(yresdata0);
  NEW(xdata90);
  NEW(yrawdata90);
  NEW(ydata90);
  NEW(yresdata90);

{  NEW(gbspeed);}
{  NEW(LASER_VAL);}

  getdir(0, Path_to_Datafiles);
  writeln('home directory:', Path_to_Datafiles,' memavail:',memavail,' byte');
  writeln;
  write('  ******* MFM2_K2 ******');
  if (dvm_ident='h') then writeln(' HP 3458 DVM *********');
  if (dvm_ident='k') then writeln(' Keithley 2001 DVM  **');
  writeln('  *                                          *');
  writeln('  *      exit                    (0)         *');
  writeln('  *      Helmholtz Coil          (1)         *');
{  writeln('  *      5m-Granite Bench, field (2)         *');
  writeln('  *           "   , straightness (3)         *');
  writeln('  *      Stretched Wire System   (4)         *');
  writeln('  *       " ,Hall Pr.: dyn. eff. (5)         *');
}  writeln('  *      Stretched Wire System 2 (6)         *');
  writeln('  *      Start_help             (99)         *');
  writeln('  *                                          *');
  writeln('  *      winlib version: ',winlib.version,'                 *');
  writeln(' *********************************************');
{  writeln('  ** ',vier:2,' Sample positions ***',MesRep:2,' Measurements **');}

  writeln('  ');
  writeln('  ');
  write(' Choose Experimental Setup ');
  readln(ihhs);
  if ihhs=99 then Manuelle_Eingabe;
  if ihhs=0 then goto ENDE;
  if (ihhs<>1) and (ihhs<>6) then goto ENDE;
{  writeln(' number of scans');
 if ihhs <>6 then readln(number_of_scans);  }
  i_wake_up:=1;
{  if (ihhs = 22) then
   begin
     i_wake_up:=0;
     ihhs:=2;
   end;
}
  i_log       :=0;
  assign(outfile,'rs232.log');
  rewrite(outfile);
  close(outfile);

  ifirst      :=1;
  ifirstSTWI  :=1;
  ifirstdac   :=1;
  ifirstservo :=1;
  iterhos     :=0;   (* host mode *)
  ifirstlaser :=1;
  ifirstpar32 :=1;
  ifirstrs232 :=1;
  ifirstDVM   :=1;
  ifirstDMM   :=1;
  imeasure:=0;
  DEFINEData  := DEFINEDefData;
  GridData    := GridDefData;
  AnalysisData:= AnalysisDefData;
  DACData     := DACDefData;
  MBInc1Data  := MBInc1DefData;
(*  MBInc1Data  := MBInc1DefData;  *)
  SMS1Data    := SMS1DefData;
  SMS2DATA    := SMS2DefData;
  DVM1data     := DVM1DefData;
  ScaleData   := ScaleDefData;
  ServoData   := ServoDefData;
  ServoParData:= ServoParDefData;
  STWIData    := STWIDefData;
 {Aufbau der Graphik}
  WinInit(VGA);
  MouseInstall(MouseAllEvent, MousePoll);
  SetTextMode(true, true, true);

  WinErrRepOpen;

  CreateMainScreen;
  CreateKommandoMenue;
  CreateSteuerButtons;
  CreateAnzeigeFenster;
  CreateMessFenster;
  ChangeWin(MainScreen);

  event := 0;

  if ihhs = 1 then
  begin
    CFGFileName0:='hhs.cfg';
    CFGFileName:='hhs.cfg';
  end;


  if ihhs = 6 then
  begin
    NEW(SMSR1);
    NEW(SMSR2);
    NEW(xData_byz);
    NEW(xResData);
    NEW(yData_Byz);
    NEW(yResData_Byz);
    CFGFileName0:='stwi2a.cfg';
    CFGFileName:='stwi2a.cfg';

    if dvm_ident='h' then   {HP-Voltmeter}
    begin
      if SMS_RAMP_TYP='S' then
      begin
        CFGFileName0:='stwi2as.cfg';
        CFGFileName:='stwi2as.cfg';
      end;
      if SMS_RAMP_TYP='L' then
      begin
        CFGFileName0:='stwi2a.cfg';
        CFGFileName:='stwi2a.cfg';
      end;
    end;

    if dvm_ident='k' then    {Keithley}
    begin
      if SMS_RAMP_TYP='S' then
      begin
        CFGFileName0:='stwi2ksa.cfg';
        CFGFileName:='stwi2ksa.cfg';
      end;
      if SMS_RAMP_TYP='L' then
      begin
    {    CFGFileName0:='stwi2kl2.cfg';
        CFGFileName:='stwi2kl2.cfg';       }
         CFGFileName0:='stwi2kn.cfg';
        CFGFileName:='stwi2kn.cfg';
      end;
    end;

    STWICFGFileName0:='stwi_1.cfg';
    STWICFGFileName:='stwi_1.cfg';
  end;

  setupini:=1;

  SetUpLoadProc(setupini);
  adjustreadings;

  if ihhs = 1 then
  begin
    if ifirstdac = 1 then
    begin
      mbopen(slave1);
      DACini;
      ifirstdac:=0;
    end;
    DACDrive(aktdacvoltage,0.0,1);
    mbclose(slave1);
    ifirst:=1;
  end;



{  if (ihhs = 2)then
   begin
     ServoData.Act_Position.x:=0.0;
     ServoData.Act_Position.y:=0.0;
     ServoData.Act_Position.z:=0.0;
     ServoData.Act_Position.theta:=0.0;
   end;
}
(*******************************************************)

  repeat
  DateTimeTempActualieren;

(*******************************************************)

  if GetEvent(event)
     then
         begin
              case event of
               SetUpID  : SetUpProc;
               CalID    : begin
                          if ifirstSTWI = 1 then
                          begin
                          STWIini;
                          ifirstSTWI:=0;
                          end;
                          wr_text := '?M';
                          IEEE_write(SW1,wr_text);
                          delay(100);
                          ibrd(SW1,rd,255);
                          writeln(rd[1],rd[2],rd[3],rd[4],rd[5],rd[6],
                                  rd[7],rd[8],rd[9],rd[10],rd[11],rd[12]);
                          end; (* CalProc;   *)
{               AnalyseID: AnalyseProc;}
               ManualID : ManualProc;
               StartID  : begin
                            for i:=1 to number_of_scans do
                            begin
                              Start_measurementProc;
                            end;
                            changewin(mainscreen);
                          end;
               StopID   : StopProc;
{               StoreID  : StoreDataProc;
               LoadID   : LoadDataProc;}
               CommentID: CommentProc;
               BlocNameID: BlocNameProc;
               DataFileNameID: DataFileNameProc(1);
               ch1zeroID   : Displaychannel(1,1);
               ch2zeroID   : Displaychannel(1,2);
               ch1ninetyID : Displaychannel(2,1);
               ch2ninetyID : Displaychannel(2,2);
         end;
     end;

(*******************************************************)

    until (event=ExitID);
 ENDE:
    if ihhs=6 then
    begin
      DISPOSE(SMSR2);
      DISPOSE(SMSR1);
      DISPOSE(yResData_Byz);
      DISPOSE(yData_Byz);
      DISPOSE(xResData);
      DISPOSE(xData_Byz);
    end;
{    DISPOSE(LASER_VAL);}
{    DISPOSE(gbspeed);}
    DISPOSE(YRESDATA90);
    DISPOSE(YDATA90);
    DISPOSE(YRAWDATA90);
    DISPOSE(XDATA90);
    DISPOSE(YRESDATA0);
    DISPOSE(YDATA0);
    DISPOSE(YRAWDATA0);
    DISPOSE(XDATA0);

    exit_routine;
    MBClose(Slave1);
end.












