{ -------------------------------------------------------------------------- }
{                       MATHLIB Bibliothek                                   }
{                       ------------------                                   }
{  Written November 23, 1987 by Hugo Hemmerich, Refined Technologies.	     }
{  All code granted to the public domain. (TRIG.PAS)			     }
{  Questions and comments to CompuServe account number 72376,3505	     }
{									     }
{  Autor         : Hugo Hemmerich,Michael Pietsch                            }
{  Date          : 1992                                                      }
{  Update	 : May '92 by Chris Hellwig                                  }
{	           REAL Variables ==> DOUBLE                                 }
{        	   IF VAR - TYPE DOUBLE changes to an other real - type      }
{		   changing of the const Infinity is required !!!            }
{  Update        : 02.04.94 (RoundReal,TrucReal und NormReal neu  (mp))	     }
{  Update        : 28.06.94 (R_Round neu ersetzt Round von TP (mp))    	     }
{  Update        : 21.10.94 (Testweise neue Routinen nicht f�r Windows !)    }
{  Update        : 06.05.95 (Power und R_Round �berarbeitet (-2.5 -> -2) (mp)}
{  Version       : 2.3                                                       }
{  Unitname      : MATHLIB                                                   }
{  benutzte Units: --                   				     }
{                                                                   	     }
{ -------------------------------------------------------------------------- }


{$DEFINE TTEST} {Noch nicht allgemein freigegeben !!}

unit MATHLIB;

{$DEFINE CO_80387}   {CO-Processor ein   }

{$IFDEF CO_80387}
  {$N+}
{$ELSE}
  {$E+}
{$ENDIF}

INTERFACE

TYPE
  DegreeType =  RECORD
                   Degrees, Minutes, Seconds : DOUBLE;
                END;
CONST
  Version  = $23;
  Infinity = 1.7E+308;   {infinity double;
                          change this value if using var - type EXTENDED }


{  Radians  }
{ sin, cos, and arctan are predefined in TP}

{$IFDEF TEST}
FUNCTION Tan   ( Radians : EXTENDED): EXTENDED;
{$ELSE}
FUNCTION Tan   ( Radians : DOUBLE ) : DOUBLE;
{$ENDIF}

FUNCTION ArcSin( InValue : DOUBLE ) : DOUBLE;
FUNCTION ArcCos( InValue : DOUBLE ) : DOUBLE;

{  Degrees, expressed as a DOUBLE number  }

FUNCTION DegreesToRadians( Degrees : DOUBLE ) : DOUBLE;
FUNCTION RadiansToDegrees( Radians : DOUBLE ) : DOUBLE;
FUNCTION Sin_Degree      ( Degrees : DOUBLE ) : DOUBLE;
FUNCTION Cos_Degree      ( Degrees : DOUBLE ) : DOUBLE;
FUNCTION Tan_Degree      ( Degrees : DOUBLE ) : DOUBLE;



{  Degrees, in Degrees, Minutes, and Seconds, as DOUBLE numbers  }

FUNCTION DegreePartsToDegrees ( Degrees, Minutes, Seconds : DOUBLE ) : DOUBLE;
FUNCTION DegreePartsToRadians ( Degrees, Minutes, Seconds : DOUBLE ) : DOUBLE;
PROCEDURE DegreesToDegreeParts( DegreesIn : DOUBLE;
                                VAR Degrees, Minutes, Seconds : DOUBLE );
PROCEDURE RadiansToDegreeParts( Radians : DOUBLE;
                                VAR Degrees, Minutes, Seconds : DOUBLE );
FUNCTION Sin_DegreeParts      ( Degrees, Minutes, Seconds : DOUBLE ) : DOUBLE;
FUNCTION Cos_DegreeParts      ( Degrees, Minutes, Seconds : DOUBLE ) : DOUBLE;
FUNCTION Tan_DegreeParts      ( Degrees, Minutes, Seconds : DOUBLE ) : DOUBLE;



{  Degrees, expressed as DegreeType ( DOUBLEs in record ) }

FUNCTION  DegreeTypeToDegrees    ( DegreeVar : DegreeType ) : DOUBLE;
FUNCTION  DegreeTypeToRadians    ( DegreeVar : DegreeType ) : DOUBLE;
PROCEDURE DegreeTypeToDegreeParts( DegreeVar : DegreeType;
                                   VAR Degrees, Minutes, Seconds : DOUBLE );
PROCEDURE DegreesToDegreeType    ( Degrees : DOUBLE;
                                   VAR DegreeVar : DegreeType );
PROCEDURE RadiansToDegreeType    ( Radians : DOUBLE;
                                   VAR DegreeVar : DegreeType );
PROCEDURE DegreePartsToDegreeType( Degrees, Minutes, Seconds : DOUBLE;
                                   VAR DegreeVar : DegreeType );
FUNCTION Sin_DegreeType          ( DegreeVar : DegreeType ) : DOUBLE;
FUNCTION Cos_DegreeType          ( DegreeVar : DegreeType ) : DOUBLE;
FUNCTION Tan_DegreeType          ( DegreeVar : DegreeType ) : DOUBLE;


{  Hyperbolic functions  }

FUNCTION Sinh   ( Invalue : DOUBLE ) : DOUBLE;
FUNCTION Cosh   ( Invalue : DOUBLE ) : DOUBLE;
FUNCTION Tanh   ( Invalue : DOUBLE ) : DOUBLE;
FUNCTION Coth   ( Invalue : DOUBLE ) : DOUBLE;
FUNCTION Sech   ( Invalue : DOUBLE ) : DOUBLE;
FUNCTION Csch   ( Invalue : DOUBLE ) : DOUBLE;
FUNCTION ArcSinh( Invalue : DOUBLE ) : DOUBLE;
FUNCTION ArcCosh( Invalue : DOUBLE ) : DOUBLE;
FUNCTION ArcTanh( Invalue : DOUBLE ) : DOUBLE;
FUNCTION ArcCoth( Invalue : DOUBLE ) : DOUBLE;
FUNCTION ArcSech( Invalue : DOUBLE ) : DOUBLE;
FUNCTION ArcCsch( Invalue : DOUBLE ) : DOUBLE;

{  Logarithms, Powers, and Roots  }
{$IFDEF TEST}
FUNCTION Log2  (X: EXTENDED): EXTENDED;
{$ENDIF}

{ e to the x  is  exp() }
{ natural log is  ln()  }

FUNCTION  Log10 ( inVar : DOUBLE )              : DOUBLE;
{$IFDEF TEST}
FUNCTION Log (X: EXTENDED): EXTENDED;
{$ELSE}
FUNCTION  Log   ( Base, InNumber : DOUBLE )     : DOUBLE;  { log of any base }
{$ENDIF}


FUNCTION  Power ( InNumber, Exponent : DOUBLE ) : DOUBLE;
FUNCTION  Root  ( InNumber, TheRoot : DOUBLE )  : DOUBLE;

{$IFDEF TEST}
FUNCTION PowerOfTen (X: EXTENDED): EXTENDED;
FUNCTION PowerOfTwo (X: EXTENDED): EXTENDED;
{$ENDIF}


FUNCTION  R_Round (inpVal : DOUBLE) : LONGINT;

FUNCTION  RoundReal (    inpVal   : DOUBLE;
			 posi     : INTEGER) : DOUBLE;

FUNCTION  TruncReal (    inpVal   : DOUBLE;
			 posi     : DOUBLE) : DOUBLE;

PROCEDURE NormReal  (    inpVal   : DOUBLE;
		     VAR mantisse : DOUBLE;
	  	     VAR exponent : DOUBLE);


{----------------------------------------------------------------------}
IMPLEMENTATION

CONST
  RadiansPerDegree =  PI/180;
  DegreesPerRadian =  180/PI;
  MinutesPerDegree =    60.0;
  SecondsPerDegree =  3600.0;
  SecondsPerMinute =    60.0;
  LnOf10           =     2.30258509299404568;   {2.3025850930 zu ungenau (mp)}

{-----------}
{  Radians  }
{-----------}

{ sin, cos, and arctan are predefined in TP}
{$IFDEF TEST}
  {$IFDEF WINDOWS}

  {$L WF87.OBJ}

  { File WF87.OBJ is one of the files delivered by Borland with BP 7.0 RTL code }

  PROCEDURE __F87_TANGENT; NEAR; EXTERNAL;
  PROCEDURE __F87_EXP10;   NEAR; EXTERNAL;
  PROCEDURE __F87_EXP2;    NEAR; EXTERNAL;
  PROCEDURE __F87_LOG10;   NEAR; EXTERNAL;
  PROCEDURE __F87_LOG2;    NEAR; EXTERNAL;

  FUNCTION Tan ( Radians : EXTENDED): EXTENDED; ASSEMBLER;
  ASM
     FLD   TBYTE PTR [RADIAS]   { get argument }
     CALL  __F87_TANGENT
  END;

  FUNCTION Log10 (X: EXTENDED): EXTENDED; ASSEMBLER;
  ASM
     FLD   TBYTE PTR [X]    { get argument }
     CALL  __F87_LOG10;
  END;

  FUNCTION Log2  (X: EXTENDED): EXTENDED; ASSEMBLER;
  ASM
     FLD   TBYTE PTR [X]    { get argument }
     CALL  __F87_LOG2;
  END;

  FUNCTION PowerOfTen (X: EXTENDED): EXTENDED; ASSEMBLER;
  ASM
     FLD   TBYTE PTR [X]    { get argument }
     CALL  __F87_EXP10;
  END;

  FUNCTION PowerOfTwo (X: EXTENDED): EXTENDED; ASSEMBLER;
  ASM
     FLD   TBYTE PTR [X]    { get argument }
     CALL  __F87_EXP2;
  END;

  {ENDE WINDOWS-CODE}
{$ELSE} {TEST und nicht Windows}

  FUNCTION Tan ( Radians : EXTENDED): EXTENDED; ASSEMBLER;
  ASM
     FLD   TBYTE PTR [Radians]    { get argument }
     INT   3Eh                    { call shortcut interrupt }
     DW    90F0h                  { signal Tan wanted to shortcut handler }
  END;

  FUNCTION Log10 (X: EXTENDED): EXTENDED; ASSEMBLER;
  ASM
     FLD   TBYTE PTR [X]    { get argument }
     INT   3Eh              { call shortcut interrupt }
     DW    90F8h            { signal Log10 wanted to shortcut handler }
  END;

  FUNCTION Log2  (X: EXTENDED): EXTENDED; ASSEMBLER;
  ASM
     FLD   TBYTE PTR [X]    { get argument }
     INT   3Eh              { call shortcut interrupt }
     DW    90F6h            { signal Log2 wanted to shortcut handler }
  END;

  FUNCTION PowerOfTen (X: EXTENDED): EXTENDED; ASSEMBLER;
  ASM
     FLD   TBYTE PTR [X]    { get argument }
     INT   3Eh              { call shortcut interrupt }
     DW    90FEh            { signal Power of 10 wanted to shortcut handler}
  END;

  FUNCTION PowerOfTwo (X: EXTENDED): EXTENDED; ASSEMBLER;
  ASM
     FLD   TBYTE PTR [X]    { get argument }
     INT   3Eh              { call shortcut interrupt }
     DW    90FCh            { signal Power of 2 wanted to shortcut handler }
  END;

 {$ENDIF} {ENDE TEST und Windows}
{$ELSE}

  FUNCTION Log{ ( Base, InNumber : DOUBLE ) : DOUBLE };  { log of any base }
    BEGIN
      Log := ln( InNumber ) / ln( Base );
    END;

  FUNCTION Tan { ( Radians : DOUBLE ) : DOUBLE };
  { note: returns Infinity where appropriate }
  VAR
    CosineVal  : DOUBLE;
    TangentVal : DOUBLE;
    BEGIN
    CosineVal    := cos( Radians );
    IF CosineVal = 0.0 then
      Tan := Infinity
    ELSE
      BEGIN
      TangentVal := sin( Radians ) / CosineVal;
      IF ( TangentVal < -Infinity ) or ( TangentVal > Infinity ) then
        Tan := Infinity
      ELSE
        Tan := TangentVal;
      END;
    END;
{$ENDIF}


FUNCTION ArcSin{ ( InValue : DOUBLE ) : DOUBLE };
  { notes: 1) exceeding input range of -1 through +1 will cause runtime error }
  {        2) only returns principal values                                   }
  {           ( -pi/2 through pi/2 radians ) ( -90 through +90 degrees )    }
  BEGIN
  IF abs( InValue ) = 1.0 then
    ArcSin := pi / 2.0
  ELSE
    ArcSin := arctan( InValue / sqrt( 1 - InValue * InValue ) );
  END;

FUNCTION ArcCos{ ( InValue : DOUBLE ) : DOUBLE };
  { notes: 1) exceeding input range of -1 through +1 will cause runtime error }
  {        2) only returns principal values                                   }
  {           ( 0 through pi radians ) ( 0 through +180 degrees )           }
  VAR
    Result : DOUBLE;
  BEGIN
  IF InValue = 0.0 then
    ArcCos := pi / 2.0
  ELSE
    BEGIN
    Result := arctan( sqrt( 1 - InValue * InValue ) / InValue );
    IF InValue < 0.0 then
      ArcCos := Result + pi
    ELSE
      ArcCos := Result;
    END;
  END;

{---------------------------------------}
{  Degrees, expressed as a DOUBLE number  }
{---------------------------------------}

FUNCTION DegreesToRadians{ ( Degrees : DOUBLE ) : DOUBLE };
  BEGIN
  DegreesToRadians := Degrees * RadiansPerDegree;
  END;

FUNCTION RadiansToDegrees{ ( Radians : DOUBLE ) : DOUBLE };
  BEGIN
  RadiansToDegrees := Radians * DegreesPerRadian;
  END;

FUNCTION Sin_Degree{ ( Degrees : DOUBLE ) : DOUBLE };
  BEGIN
  Sin_Degree := sin( DegreesToRadians( Degrees ) );
  END;

FUNCTION Cos_Degree{ ( Degrees : DOUBLE ) : DOUBLE };
  BEGIN
  Cos_Degree := cos( DegreesToRadians( Degrees ) );
  END;

FUNCTION Tan_Degree{ ( Degrees : DOUBLE ) : DOUBLE };
  BEGIN
  Tan_Degree := Tan( DegreesToRadians( Degrees ) );
  END;


{--------------------------------------------------------------}
{  Degrees, in Degrees, Minutes, and Seconds, as DOUBLE numbers  }
{--------------------------------------------------------------}

FUNCTION DegreePartsToDegrees{ ( Degrees, Minutes, Seconds : DOUBLE ) : DOUBLE };
  BEGIN
  DegreePartsToDegrees := Degrees + ( Minutes / MinutesPerDegree ) +
                                    ( Seconds / SecondsPerDegree );
  END;

FUNCTION DegreePartsToRadians{ ( Degrees, Minutes, Seconds : DOUBLE ) : DOUBLE };
  BEGIN
  DegreePartsToRadians := DegreesToRadians( DegreePartsToDegrees( Degrees,
  					    Minutes, Seconds ) );
  END;

PROCEDURE DegreesToDegreeParts{ ( DegreesIn : DOUBLE;
                                  var Degrees, Minutes, Seconds : DOUBLE ) };
  BEGIN
  Degrees := int( DegreesIn );
  Minutes := ( DegreesIn - Degrees ) * MinutesPerDegree;
  Seconds := frac( Minutes );
  Minutes := int( Minutes );
  Seconds := Seconds * SecondsPerMinute;
  END;

PROCEDURE RadiansToDegreeParts{ ( Radians : DOUBLE;
                                  var Degrees, Minutes, Seconds : DOUBLE ) };
  BEGIN
  DegreesToDegreeParts( RadiansToDegrees( Radians ),
                        Degrees, Minutes, Seconds );
  END;

FUNCTION Sin_DegreeParts{ ( Degrees, Minutes, Seconds : DOUBLE ) : DOUBLE };
  BEGIN
  Sin_DegreeParts := sin( DegreePartsToRadians( Degrees, Minutes, Seconds ) );
  END;

FUNCTION Cos_DegreeParts{ ( Degrees, Minutes, Seconds : DOUBLE ) : DOUBLE };
  BEGIN
  Cos_DegreeParts := cos( DegreePartsToRadians( Degrees, Minutes, Seconds ) );
  END;

FUNCTION Tan_DegreeParts{ ( Degrees, Minutes, Seconds : DOUBLE ) : DOUBLE };
  BEGIN
  Tan_DegreeParts := Tan( DegreePartsToRadians( Degrees, Minutes, Seconds ) );
  END;


{-------------------------------------------------------}
{  Degrees, expressed as DegreeType ( DOUBLEs in record ) }
{-------------------------------------------------------}

FUNCTION DegreeTypeToDegrees{ ( DegreeVar : DegreeType ) : DOUBLE };
  BEGIN
  DegreeTypeToDegrees := DegreePartsToDegrees( DegreeVar.Degrees,
                                       DegreeVar.Minutes, DegreeVar.Seconds );
  END;

FUNCTION DegreeTypeToRadians{ ( DegreeVar : DegreeType ) : DOUBLE };
  BEGIN
  DegreeTypeToRadians := DegreesToRadians( DegreeTypeToDegrees( DegreeVar ) );
  END;

PROCEDURE DegreeTypeToDegreeParts{ ( DegreeVar : DegreeType;
                                     var Degrees, Minutes, Seconds : DOUBLE ) };
  BEGIN
  Degrees := DegreeVar.Degrees;
  Minutes := DegreeVar.Minutes;
  Seconds := DegreeVar.Seconds;
  END;

PROCEDURE DegreesToDegreeType{ ( Degrees : DOUBLE; var DegreeVar : DegreeType )};
  BEGIN
  DegreesToDegreeParts( Degrees, DegreeVar.Degrees,
                        DegreeVar.Minutes, DegreeVar.Seconds );
  END;

PROCEDURE RadiansToDegreeType{ ( Radians : DOUBLE; var DegreeVar : DegreeType )};
  BEGIN
  DegreesToDegreeParts( RadiansToDegrees( Radians ), DegreeVar.Degrees,
                        DegreeVar.Minutes, DegreeVar.Seconds );
  END;

PROCEDURE DegreePartsToDegreeType{ ( Degrees, Minutes, Seconds : DOUBLE;
                                     var DegreeVar : DegreeType ) };
  BEGIN
  DegreeVar.Degrees := Degrees;
  DegreeVar.Minutes := Minutes;
  DegreeVar.Seconds := Seconds;
  END;

FUNCTION Sin_DegreeType{ ( DegreeVar : DegreeType ) : DOUBLE };
  BEGIN
  Sin_DegreeType := sin( DegreeTypeToRadians( DegreeVar ) );
  END;

FUNCTION Cos_DegreeType{ ( DegreeVar : DegreeType ) : DOUBLE };
  BEGIN
  Cos_DegreeType := cos( DegreeTypeToRadians( DegreeVar ) );
  END;

FUNCTION Tan_DegreeType{ ( DegreeVar : DegreeType ) : DOUBLE };
  BEGIN
  Tan_DegreeType := Tan( DegreeTypeToRadians( DegreeVar ) );
  END;


{------------------------}
{  Hyperbolic functions  }
{------------------------}

FUNCTION Sinh{ ( Invalue : DOUBLE ) : DOUBLE };
  CONST
    MaxValue = 710.41998407;  { exceeds standard turbo precision
                                by using var type DOUBLE
                                check this value by using ArcSinh
                                with highest value
                                if using var type EXTENDED and
                                change MaxValue }
  VAR
    Sign : DOUBLE;
  BEGIN
  Sign := 1.0;
  IF Invalue < 0 then
    BEGIN
    Sign := -1.0;
    Invalue := -Invalue;
    END;
  IF Invalue > MaxValue then
    Sinh := Infinity
  ELSE
    Sinh := ( exp( Invalue ) - exp( -Invalue ) ) / 2.0 * Sign;
  END;

FUNCTION Cosh{ ( Invalue : DOUBLE ) : DOUBLE };
  CONST
    MaxValue = 710.41998407;  { exceeds standard turbo precision
                                by using var type DOUBLE
                                check this value by using ArcSinh
                                with highest value
                                if using var type EXTENDED and
                                change MaxValue }
  BEGIN
  Invalue := abs( Invalue );
  IF Invalue > MaxValue then
    Cosh := Infinity
  ELSE
    Cosh := ( exp( Invalue ) + exp( -Invalue ) ) / 2.0;
  END;

FUNCTION Tanh{ ( Invalue : DOUBLE ) : DOUBLE };
  BEGIN
  Tanh := Sinh( Invalue ) / Cosh( Invalue );
  END;

FUNCTION Coth{ ( Invalue : DOUBLE ) : DOUBLE };
  BEGIN
  Coth := Cosh( Invalue ) / Sinh( Invalue );
  END;

FUNCTION Sech{ ( Invalue : DOUBLE ) : DOUBLE };
  BEGIN
  Sech := 1.0 / Cosh( Invalue );
  END;

FUNCTION Csch{ ( Invalue : DOUBLE ) : DOUBLE };
  BEGIN
  Csch := 1.0 / Sinh( Invalue );
  END;

FUNCTION ArcSinh{ ( Invalue : DOUBLE ) : DOUBLE };
  var
    Sign : DOUBLE;
  BEGIN
  Sign := 1.0;
  IF Invalue < 0 then
    BEGIN
    Sign := -1.0;
    Invalue := -Invalue;
    END;
  ArcSinh := ln( Invalue + sqrt( Invalue*Invalue + 1 ) ) * Sign;
  END;

FUNCTION ArcCosh{ ( Invalue : DOUBLE ) : DOUBLE };
  VAR
    Sign : DOUBLE;
  BEGIN
  Sign := 1.0;
  IF Invalue < 0 then
    BEGIN
    Sign := -1.0;
    Invalue := -Invalue;
    END;
  ArcCosh := ln( Invalue + sqrt( Invalue*Invalue - 1 ) ) * Sign;
  END;

FUNCTION ArcTanh{ ( Invalue : DOUBLE ) : DOUBLE };
  VAR
    Sign : DOUBLE;
  BEGIN
  Sign := 1.0;
  IF Invalue < 0 then
    BEGIN
    Sign := -1.0;
    Invalue := -Invalue;
    END;
  ArcTanh := ln( ( 1 + Invalue ) / ( 1 - Invalue ) ) / 2.0 * Sign;
  END;

FUNCTION ArcCoth{ ( Invalue : DOUBLE ) : DOUBLE };
  BEGIN
  ArcCoth := ArcTanh( 1.0 / Invalue );
  END;

FUNCTION ArcSech{ ( Invalue : DOUBLE ) : DOUBLE };
  BEGIN
  ArcSech := ArcCosh( 1.0 / Invalue );
  END;

FUNCTION ArcCsch{ ( Invalue : DOUBLE ) : DOUBLE };
  BEGIN
  ArcCsch := ArcSinh( 1.0 / Invalue );
  END;

{---------------------------------}
{  Logarithms, Powers, and Roots  }
{---------------------------------}

{ e to the x  is  exp() }
{ natural log is  ln()  }

FUNCTION Log10 (inVar : DOUBLE) : DOUBLE;

BEGIN
  Log10 := LN(inVar) / LnOf10;
END;

(*
FUNCTION Power{ ( InNumber, Exponent : DOUBLE ) : DOUBLE };
BEGIN
  IF InNumber <> 0.0
  THEN Power := exp( Exponent * ln( InNumber ) )
  ELSE IF InNumber = 0.0
       THEN Power := 0.0;
       ELSE Power := InNumber / 0.0; { force runtime error }
END;
*)

FUNCTION Power{ ( InNumber, Exponent : DOUBLE ) : DOUBLE };
BEGIN
  IF InNumber <> 0.0
  THEN Power := EXP(Exponent * LN(InNumber))
  ELSE Power := 0.0;
END;


FUNCTION Root{ ( InNumber, TheRoot : DOUBLE ) : DOUBLE };
BEGIN
  Root := Power( InNumber, ( 1.0 / TheRoot ) );
END;

(*
FUNCTION R_Round (inpVal : DOUBLE) : LONGINT;

VAR
  rest   : DOUBLE;
  temp   : DOUBLE;

BEGIN
  temp := ABS(inpVal);
  rest := FRAC(temp);
  IF (rest > 0.49999999999)
  THEN BEGIN
          IF (inpVal < 0)
          THEN R_Round := (TRUNC(temp) +1) * -1
          ELSE R_Round := TRUNC (temp) +1;
       END
  ELSE R_Round := ROUND(inpVal);
END;
*)

FUNCTION R_Round (inpVal : DOUBLE) : LONGINT;

VAR
  rest   : DOUBLE;

BEGIN
  rest := ABS(FRAC(inpVal));
  IF inpVal < 0
  THEN BEGIN
         IF (rest > 0.5)                        { -2,5 -> -2}
         THEN R_Round := TRUNC(inpVal) - 1
         ELSE R_Round := TRUNC(inpVal);
       END
  ELSE BEGIN
         IF (rest > 0.49999999999)               { +2,5 -> +3}
         THEN R_Round := TRUNC (inpVal) +1
         ELSE R_Round := TRUNC (inpVal);
       END;
END;


FUNCTION RoundReal (inpVal : DOUBLE;posi : INTEGER) : DOUBLE;

{ *** Die Routine RoundReal rundet eine DOUBLE - Wert an der mit posi *** }
{ *** bezeichneten Stelle.                                            *** }

VAR
  temp  : DOUBLE;
  exp	: DOUBLE;

BEGIN
  exp := INT(Power(10,ABS(posi)) + 1E-9);        {Position ab der gerundet}
                                                 {werden soll}
  IF (posi <= 0)                                 {0 und < 0 = Nachkommastelle}
  THEN BEGIN                                     {runden}
  	 temp := R_Round(Frac(inpVAL) * exp);
  	 temp := INT(inpVal) + temp / exp;
       END
  ELSE BEGIN                                     {Vorkommastelle runden}
         temp := R_Round(inpVal / exp);
         temp := temp * exp;
       END;
  RoundReal := temp;
END;


FUNCTION TruncReal (inpVal : DOUBLE; posi : DOUBLE) : DOUBLE;

{ *** Diese Funktion setzt ab der mit pos angegebenen Stelle die Ziffern *** }
{ *** auf den Wert Null.                                                 *** }
{ *** Beispiel TruncPos(123.0,1) = 120 oder TruncPos(123.67,-1) = 123.60 *** }

VAR
  exp  : DOUBLE;
  temp : DOUBLE;

BEGIN
  exp := R_Round(Power(10,ABS(posi)));		 {Position ab der gerundet}
                                                 {werden soll}
  IF (posi <= 0.0)
  THEN BEGIN
  	 temp := INT(Frac(inpVAL) * exp);
  	 temp := INT(inpVal) + temp / exp;
       END
  ELSE BEGIN
          temp := INT(inpVal / exp);
          temp := temp * exp;
       END;
  TruncReal := temp;
END;

PROCEDURE NormReal (inpVal : DOUBLE;VAR mantisse  : DOUBLE;
	  	    VAR exponent : DOUBLE);

{ ** Die Routine NormReal zerlegt eine Realezahl in Mantisse und Exponent ** }
{ ** Die Mantisse hat den Wertebereich >= 1.0 bis < 10.0.		  ** }

BEGIN
   IF (inpVal = 0.0)
   THEN BEGIN
          mantisse := 0;
          exponent := 0;
        END
   ELSE BEGIN
   	  exponent := TRUNC(Log10(ABS(inpVal)));      {Exponenten zur Basis 10}
   	  mantisse := inpVal / Power(10,exponent);    {Mantisse normalisieren}
   	  IF (ABS(mantisse) < 1.0)
   	  THEN BEGIN
          	 mantisse := mantisse * 10;
                 exponent := R_Round(exponent - 1);
               END;
   END;
END;

END. { END Math_Lib }

