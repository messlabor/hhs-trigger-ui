
import sys
import os

sys.path.append("/opt/epics/ioc/hp3458a-gpib")
sys.path.append("/opt/epics/ioc/ioc-pmac-trigger")
sys.path.append(".")
sys.path.append("..")

from class_pmactrigger import *
from class_hp3458a import *
from hhs_tools import writeH5File, read_data
from time import sleep
from datetime import datetime
from tqdm import tqdm
from scipy.fft import fft, fftfreq

import matplotlib.pyplot as plt
import numpy as np

# Create the DVM Object
dmm = Hp3458a("DVM:", name = "dmm")
dmm.wait_for_connection()

# Create the PMAC object
trigger = PmacTrigger("PMAC:TRIGGER:", name = "pmac")
trigger.wait_for_connection(all_signals=True)

# Create the motor object
motor = PmacMotor("PMAC:M1", name = "motor")
motor.wait_for_connection(all_signals=True)

# Reset DMM
dmm.reset.put(1)
sleep(1)

if(trigger.PlcActive.get()==0 or trigger.PlcRunning.get()==0):
    trigger.PlcEnable.set(1).wait()

#### Settings ###
motor.jvel.set(720).wait() # in degrees per second
motor.jar.set(360).wait() # in degrees per second squared
trigger.n_total.set(1000).wait() # number of measurements
trigger.step_size.set(4096).wait() # count units of motor (360° = 65536 ct)
trigger.multiturn.set(65535).wait() 

# Arm Multimeter
run_measurement = "PRESET FAST;NPLC 1;MFORMAT ASCII;MEM FIFO;RANGE 10.0;NRDGS "+str(int(trigger.n_total.get()))+",EXT;TARM SGL,1;OFORMAT ASCII;RMEM 1,"+str(int(trigger.n_total.get()))+",1"
dmm.send.put(run_measurement) # configure measurement
sleep(1)

# Jog Motor
motor.jogf.put(1)
accel_time = motor.jvel.get()/motor.jar.get()
sleep(accel_time + 1) # wait for it to reach the speed

# check if the plc reached the next state
if(trigger.state.get()==1):
    print("STATE IDLE") 
    trigger.cmd_run.put(1) # start measurement

# wait for the plc to enter the next state
while(trigger.state.get()!=4):
    sleep(0.3)

print("WAITING MEASUREMENT")
pbar = tqdm(total=100)
estimated_measurement_time =    trigger.n_total.get() * trigger.step_size.get() \
                                / (motor.jvel.get() * 65535 /360)
while(trigger.state.get()==4):
    sleep(1)
    pbar.update(100 / estimated_measurement_time)
    if(motor.moving==False):
        raise Exception("Motor stopped")
pbar.close()

# stop the motor when DONE
if(trigger.state.get()==5):
    print("DONE")
    motor.hold.put(1)
    sleep(5)
    motor.kill.put(1) 
    trigger.done.put(1)
    while(trigger.state.get()!=1):
        sleep(0.3)

# make sure motor stops
if(motor.moving==True):
    motor.kill.put(1)

# Read the voltage values
print("Reading Multimeter Memory")
arr_dmm = dmm.read_memory()

# Read the positions
print("Reading PMAC Positions")
arr_pos = trigger.get_positions()

# Read measurement times
print("Reading Measurement Times")
arr_times = trigger.get_times()

# Stacking the two results
stacked_arr = np.stack((arr_pos, arr_dmm, arr_times), axis=1)

# Add column names
stacked_arr_with_names = np.vstack((['positions', 'voltage', 'times'], stacked_arr))

# Get timestamp
today = datetime.today().strftime("%Y%m%d_%H%M%m")

# Save backup data in case there are problems with the chosen path.
writeH5File(f"/home/hhs/Desktop/HHS_Results/backup/{today}_backup.hdf5", stacked_arr, trigger.step_size.get(), motor.jvel.get(), motor.jar.get(), trigger.n_total.get(), trigger.multiturn.get(), trigger.home_pos.get())

# Save data to chosen path.
filepath = os.getenv("HHS_OUTPUT_PATH")
filepath = filepath.replace("<timestamp>", today)
writeH5File(filepath, stacked_arr, trigger.step_size.get(), motor.jvel.get(), motor.jar.get(), trigger.n_total.get(), trigger.multiturn.get(), trigger.home_pos.get())

print(f"Data saved to {filepath}")
print("")
print("")