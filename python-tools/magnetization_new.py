import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import hhs_tools as hhs
import math

from scipy.fft import fft, fftfreq

path0 = "../data/20240905_152009_N444_NPLC1__0deg_results.hdf5"
path90 = "../data/20240905_152309_N444_NPLC1__90deg_results.hdf5"

def process_data(path, angle=0):
    # DVM_Aperture = 0.06841561833961954 # calculated from old data
    DVM_Aperture = 0.02
    radgra = 180 / np.pi

    # Read data
    data, df = hhs.read_data(path)

    # Exclude the first and last turn
    turn_numbers = df['Turn'].unique()
    filtered_turn_numbers = turn_numbers[1:-1]  # Remove the first and last turn
    df_new = df[df['Turn'].isin(filtered_turn_numbers)]
    df_new = df_new.reset_index(drop=True)
    print(df_new.head())

    # Calculate the average speed and voltage
    freq = df_new.loc[:, 'Speed_Hz'].mean()
    speed_deg = df_new.loc[:, 'Speed_deg'].mean()
    ydata = df_new["Voltage"].values

    # Fourier Analysis
    N = len(ydata)
    resolution = 65535
    T = (df_new["Position"][1] - df_new["Position"][0]) / resolution / freq
    # Fourier Analysis Mode 2
    # ampc = 0.0
    # amps = 0.0
    # print max number of turns
    # print(f"Max number of turns: {np.max(df_new['Turn'])}")
    # xiperiod = N / np.max(df_new["Turn"]) # number of pts per revolution
    # xk = 2.0 * math.pi / xiperiod
    # for i in range(0,N):
    #     ampc = ampc + math.cos(xk * i) * ydata[i]
    #     amps = amps + math.sin(xk * i) * ydata[i]
    # ampc = 2.0 * ampc / N
    # amps = 2.0 * amps / N
    # phi_orig = math.atan(-amps/ampc)
    # amp = ampc / math.cos(phi_orig)

    # Fourier Analysis Mode 1
    fy = fft(ydata)
    xf = fftfreq(N, T)[:N // 2]

    idx = np.argmax(np.abs(fy[0:N // 2]))
    ampc = 2 * np.real(fy[idx]) / N
    amps = 2 * np.imag(fy[idx]) / N
    phi_orig = np.arctan2(-amps, ampc)
    amp = (ampc**2 + amps**2)**0.5

    print(f'ampc: {ampc}')
    print(f'amps: {amps}')
    print(f'phi_orig: {phi_orig}')
    print(f'amp: {amp}')

    # # Phase corrections for 0 degrees
    Delta_phi = math.pi / 4 - 0.5 * DVM_Aperture * 2 * math.pi * freq
    print(f'Delta_phi: {Delta_phi}')
    phi = phi_orig - Delta_phi


    print(f'phi: {phi}')
    # Calculate magnetization components for 0 degrees
    if angle == 0:
        mv = amp * np.cos(phi)
        mh = -amp * np.sin(phi)
    elif angle == 90:   
        mv = -amp * np.cos(phi)
        mh = amp * np.sin(phi)

    
    print('-------------------------')
    print(f' Mv         = {1000 * mv / abs(freq): .14E}')
    print(f' Mh     = {1000 * mh / abs(freq): .14E}')
    print('')
    print('--------------')
    print(f' averaged speed (Hz)    : {freq: .14E}')
    print(f' Amplitude (mV/Hz)      : {1000 * amp / freq: .14E}')
    print(f' Phase (deg)            : {phi * radgra: .14E}')
    print('')

    return freq, mv, mh, amp, phi

freq0, mz, my_0, amp0, phi0 = process_data(path0, 0)
# freq90, mx, my_90, amp90, phi90 = process_data(path90, 90)