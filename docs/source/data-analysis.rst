Data Analysis
================

Some comments about the codes and tests for analysing the data from the new Helmholtz in comparison to the old one.

- The new system is collecting a position and a voltage at a certain frequency from devices and the start point of this collection is not controlled. Which means that measurement can start whenever the code is ready and the magnet can be at any certain angle. However, because we collect ten times more points from devices and python is available nowadays we should be able to reach results which are even more reliables than the old system.

- The old system starts the measurement, if I am not wrong, always at the same position and does 5 turns with a continuous measurement of the multimeter. The analysis of the data is done inside the pascal code called MFM2_ANK.PAS and it makes use of the first order fourier transform to find the magnetizations. This is because the first order fourier transform approximates the signal as a sum of cosines and sines and the amplitudes of its contributions give us the magnetizations.

The problem faced during the moments I tried to validate the magnetization were mostly related to the phase amplitude calculated. The pascal code has a phase shift added to the value of the phase calculated between the amplitudes of the sine and cosine contribution and I believe, humbly, that we just need to find how that was done and how do we transfer to the new system. 

Additionally, one will see inside the /python-tools codes, different data analysis files and different functions which were tested and I tried to use it. Unfortunately, maybe, I did not spend enough time on this task to make it work. But hopefully the codes will give a startpoint on what not to do.